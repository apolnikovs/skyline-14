DROP TRIGGER IF EXISTS job_update;

DELIMITER ;;

CREATE TRIGGER job_update BEFORE UPDATE ON job FOR EACH ROW 
BEGIN

	/*Insert closed date if Job Status is being changed to cancelled*/
	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ClosedDate IS NULL
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;
	
	/*Insert Service Provider Despatch Date if Job Status is being changed to cancelled*/
	IF (NEW.StatusID = 33 OR NEW.StatusID = 41) AND NEW.ServiceProviderDespatchDate IS NULL
	THEN SET NEW.ServiceProviderDespatchDate = CURDATE();
	END IF;
	
	/*Insert RepairCompleteDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.RepairCompleteDate IS NULL)
	THEN SET NEW.RepairCompleteDate = CURDATE();
	END IF;
	
	/*Insert ServiceProviderDespatchDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.ServiceProviderDespatchDate IS NULL)
	THEN SET NEW.ServiceProviderDespatchDate = CURDATE();
	END IF;
	
	/*Insert DateReturnedToCustomer if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF ((NEW.OpenJobStatus = 'awaiting_collection' OR NEW.OpenJobStatus = 'customer_notified' OR NEW.OpenJobStatus = 'closed') AND OLD.DateReturnedToCustomer IS NULL)
	THEN SET NEW.DateReturnedToCustomer = CURDATE();
	END IF;
	
	/*Insert DateReturnedToCustomer if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF (NEW.OpenJobStatus = 'closed' AND OLD.DateReturnedToCustomer IS NULL)
	THEN SET NEW.DateReturnedToCustomer = CURDATE();
	END IF;
	
	/*Insert ClosedDate if doesn't exist when OpenJobStatus is changed to awaiting_collection, customer_notified or closed*/
	IF (NEW.OpenJobStatus = 'closed' AND OLD.ClosedDate IS NULL)
	THEN SET NEW.ClosedDate = CURDATE();
	END IF;
	
	/*Insert ServiceProviderDespatchDate if it doesn't exist when ClosedDate is set*/
	IF (OLD.ClosedDate IS NULL AND NEW.ClosedDate IS NOT NULL AND NEW.ServiceProviderDespatchDate IS NULL)
	THEN SET NEW.ServiceProviderDespatchDate = CURDATE();
	END IF;

	/*NULL ServiceBaseModel if ModelID is being set or vice versa*/
	IF (NEW.ModelID IS NOT NULL)
	THEN SET NEW.ServiceBaseModel = NULL;
	END IF;

	/*When ServiceBaseModel is not null then set ModelID to null*/
	IF (NEW.ServiceBaseModel IS NOT NULL)
	THEN SET NEW.ModelID = NULL;
	END IF;

	/*When ServiceProviderDespatchDate is changed and ProductLocation is not 'Branch then set ClosedDate'*/
	/*
	IF (NEW.ProductLocation != 'Branch' OR NEW.ProductLocation IS NULL)
	THEN SET NEW.ClosedDate = NEW.ServiceProviderDespatchDate;
	END IF;
	*/
	
	IF (NEW.ProductLocation != 'Branch' OR NEW.ProductLocation IS NULL)
	THEN SET NEW.ClosedDate = NEW.ServiceProviderDespatchDate;
	ELSEIF ((SELECT branch.OpenJobsManagement FROM branch WHERE branch.BranchID = NEW.BranchID) = 'No')
	THEN SET NEW.ClosedDate = NEW.ServiceProviderDespatchDate;
	END IF;	

	/*Update modified date when any field is changed*/
	SET NEW.ModifiedDate = NOW();
	
	
	/*Audit logging begins here*/

	/*Parameter definitions*/
	/*CALL trigger_update([old data], [new data], [table name], [field name], [primary ID], [modified user ID]);*/

	/*StatusID*/
	CALL trigger_update(OLD.StatusID, NEW.StatusID, 'job', 'StatusID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ProductID*/
	CALL trigger_update(OLD.ProductID, NEW.ProductID, 'job', 'ProductID', OLD.JobID, NEW.ModifiedUserID);
		
	/*ServiceBaseModel*/
	CALL trigger_update(OLD.ServiceBaseModel, NEW.ServiceBaseModel, 'job', 'ServiceBaseModel', OLD.JobID, NEW.ModifiedUserID);

	/*ManufacturerID*/
	CALL trigger_update(OLD.ManufacturerID, NEW.ManufacturerID, 'job', 'ManufacturerID', OLD.JobID, NEW.ModifiedUserID);
							
	/*ServiceBaseUnitType*/
	CALL trigger_update(OLD.ServiceBaseUnitType, NEW.ServiceBaseUnitType, 'job', 'ServiceBaseUnitType', OLD.JobID, NEW.ModifiedUserID);
	
	/*RepairType*/
	CALL trigger_update(OLD.RepairType, NEW.RepairType, 'job', 'RepairType', OLD.JobID, NEW.ModifiedUserID);

	/*ServiceTypeID*/
	CALL trigger_update(OLD.ServiceTypeID, NEW.ServiceTypeID, 'job', 'ServiceTypeID', OLD.JobID, NEW.ModifiedUserID);

	/*SerialNo*/
	CALL trigger_update(OLD.SerialNo, NEW.SerialNo, 'job', 'SerialNo', OLD.JobID, NEW.ModifiedUserID);
	
	/*ProductLocation*/
	CALL trigger_update(OLD.ProductLocation, NEW.ProductLocation, 'job', 'ProductLocation', OLD.JobID, NEW.ModifiedUserID);

	/*DateOfPurchase*/
	CALL trigger_update(OLD.DateOfPurchase, NEW.DateOfPurchase, 'job', 'DateOfPurchase', OLD.JobID, NEW.ModifiedUserID);
	
	/*OriginalRetailer*/
	CALL trigger_update(OLD.OriginalRetailer, NEW.OriginalRetailer, 'job', 'OriginalRetailer', OLD.JobID, NEW.ModifiedUserID);
	
	/*ReceiptNo*/
	CALL trigger_update(OLD.ReceiptNo, NEW.ReceiptNo, 'job', 'ReceiptNo', OLD.JobID, NEW.ModifiedUserID);

	/*Insurer*/
	CALL trigger_update(OLD.Insurer, NEW.Insurer, 'job', 'Insurer', OLD.JobID, NEW.ModifiedUserID);

	/*PolicyNo*/
	CALL trigger_update(OLD.PolicyNo, NEW.PolicyNo, 'job', 'PolicyNo', OLD.JobID, NEW.ModifiedUserID);

	/*AuthorisationNo*/
	CALL trigger_update(OLD.AuthorisationNo, NEW.AuthorisationNo, 'job', 'AuthorisationNo', OLD.JobID, NEW.ModifiedUserID);

	/*AgentRefNo*/
	CALL trigger_update(OLD.AgentRefNo, NEW.AgentRefNo, 'job', 'AgentRefNo', OLD.JobID, NEW.ModifiedUserID);
	    				
	/*Notes*/
	CALL trigger_update(OLD.Notes, NEW.Notes, 'job', 'Notes', OLD.JobID, NEW.ModifiedUserID);
	
	/*ReportedFault*/
	CALL trigger_update(OLD.ReportedFault, NEW.ReportedFault, 'job', 'ReportedFault', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableLabourCost*/
	CALL trigger_update(OLD.ChargeableLabourCost, NEW.ChargeableLabourCost, 'job', 'ChargeableLabourCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableLabourVATCost*/
	CALL trigger_update(OLD.ChargeableLabourVATCost, NEW.ChargeableLabourVATCost, 'job', 'ChargeableLabourVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeablePartsCost*/
	CALL trigger_update(OLD.ChargeablePartsCost, NEW.ChargeablePartsCost, 'job', 'ChargeablePartsCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableDeliveryCost*/
	CALL trigger_update(OLD.ChargeableDeliveryCost, NEW.ChargeableDeliveryCost, 'job', 'ChargeableDeliveryCost', OLD.JobID, NEW.ModifiedUserID);

	/*ChargeableDeliveryVATCost*/
	CALL trigger_update(OLD.ChargeableDeliveryVATCost, NEW.ChargeableDeliveryVATCost, 'job', 'ChargeableDeliveryVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableVATCost*/
	CALL trigger_update(OLD.ChargeableVATCost, NEW.ChargeableVATCost, 'job', 'ChargeableVATCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableSubTotal*/
	CALL trigger_update(OLD.ChargeableSubTotal, NEW.ChargeableSubTotal, 'job', 'ChargeableSubTotal', OLD.JobID, NEW.ModifiedUserID);
	
	/*ChargeableTotalCost*/
	CALL trigger_update(OLD.ChargeableTotalCost, NEW.ChargeableTotalCost, 'job', 'ChargeableTotalCost', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCompanyName*/
	CALL trigger_update(OLD.ColAddCompanyName, NEW.ColAddCompanyName, 'job', 'ColAddCompanyName', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddBuildingNameNumber*/
	CALL trigger_update(OLD.ColAddBuildingNameNumber, NEW.ColAddBuildingNameNumber, 'job', 'ColAddBuildingNameNumber', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddStreet*/
	CALL trigger_update(OLD.ColAddStreet, NEW.ColAddStreet, 'job', 'ColAddStreet', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddLocalArea*/
	CALL trigger_update(OLD.ColAddLocalArea, NEW.ColAddLocalArea, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddTownCity*/
	CALL trigger_update(OLD.ColAddTownCity, NEW.ColAddTownCity, 'job', 'ColAddLocalArea', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCountyID*/
	CALL trigger_update(OLD.ColAddCountyID, NEW.ColAddCountyID, 'job', 'ColAddCountyID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddCountryID*/
	CALL trigger_update(OLD.ColAddCountryID, NEW.ColAddCountryID, 'job', 'ColAddCountryID', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPostcode*/
	CALL trigger_update(OLD.ColAddPostcode, NEW.ColAddPostcode, 'job', 'ColAddPostcode', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddEmail*/
	CALL trigger_update(OLD.ColAddEmail, NEW.ColAddEmail, 'job', 'ColAddEmail', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPhone*/
	CALL trigger_update(OLD.ColAddPhone, NEW.ColAddPhone, 'job', 'ColAddPhone', OLD.JobID, NEW.ModifiedUserID);
	
	/*ColAddPhoneExt*/
	CALL trigger_update(OLD.ColAddPhoneExt, NEW.ColAddPhoneExt, 'job', 'ColAddPhoneExt', OLD.JobID, NEW.ModifiedUserID);

	/*Accessories*/
	CALL trigger_update(OLD.Accessories, NEW.Accessories, 'job', 'Accessories', OLD.JobID, NEW.ModifiedUserID);
	
	/*UnitCondition*/
	CALL trigger_update(OLD.UnitCondition, NEW.UnitCondition, 'job', 'UnitCondition', OLD.JobID, NEW.ModifiedUserID);
	
	/*ServiceProviderID*/
	CALL trigger_update(OLD.ServiceProviderID, NEW.ServiceProviderID, 'job', 'ServiceProviderID', OLD.JobID, NEW.ModifiedUserID);
	
	/*DateBooked*/
	CALL trigger_update(OLD.DateBooked, NEW.DateBooked, 'job', 'DateBooked', OLD.JobID, NEW.ModifiedUserID);
	
	/*OpenJobStatus*/
	CALL trigger_update(OLD.OpenJobStatus, NEW.OpenJobStatus, 'job', 'OpenJobStatus', OLD.JobID, NEW.ModifiedUserID);
	
	/*ModelID*/
	CALL trigger_update(OLD.ModelID, NEW.ModelID, 'job', 'ModelID', OLD.JobID, NEW.ModifiedUserID);

	/*ServiceProviderDespatchDate*/
	CALL trigger_update(OLD.ServiceProviderDespatchDate, NEW.ServiceProviderDespatchDate, 'job', 'ServiceProviderDespatchDate', OLD.JobID, NEW.ModifiedUserID);

	/*ClosedDate*/
	CALL trigger_update(OLD.ClosedDate, NEW.ClosedDate, 'job', 'ClosedDate', OLD.JobID, NEW.ModifiedUserID);
	
	/*ProductLocation*/
	CALL trigger_update(OLD.ProductLocation, NEW.ProductLocation, 'job', 'ProductLocation', OLD.JobID, NEW.ModifiedUserID);
	
END
;;

DELIMITER ;