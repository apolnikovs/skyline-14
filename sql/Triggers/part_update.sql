DROP TRIGGER IF EXISTS part_update;

DELIMITER ;;

CREATE TRIGGER part_update BEFORE UPDATE ON part FOR EACH ROW 
BEGIN

	/*Parameter definitions*/
	/*CALL trigger_update([old data], [new data], [table name], [field name], [primary ID], [modified user ID]);*/

	/*PartID*/
	CALL trigger_update(OLD.PartID, NEW.PartID, 'part', 'PartID', OLD.PartID, NEW.ModifiedUserID);

	/*JobID*/
	CALL trigger_update(OLD.JobID, NEW.JobID, 'part', 'JobID', OLD.PartID, NEW.ModifiedUserID);
	
	/*PartNo*/
	CALL trigger_update(OLD.PartNo, NEW.PartNo, 'part', 'PartNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*PartDescription*/
	CALL trigger_update(OLD.PartDescription, NEW.PartDescription, 'part', 'PartDescription', OLD.PartID, NEW.ModifiedUserID);
	
	/*Quantity*/
	CALL trigger_update(OLD.Quantity, NEW.Quantity, 'part', 'Quantity', OLD.PartID, NEW.ModifiedUserID);
	
	/*Supplier*/
	CALL trigger_update(OLD.Supplier, NEW.Supplier, 'part', 'Supplier', OLD.PartID, NEW.ModifiedUserID);
	
	/*SectionCode*/
	CALL trigger_update(OLD.SectionCode, NEW.SectionCode, 'part', 'SectionCode', OLD.PartID, NEW.ModifiedUserID);
	
	/*DefectCode*/
	CALL trigger_update(OLD.DefectCode, NEW.DefectCode, 'part', 'DefectCode', OLD.PartID, NEW.ModifiedUserID);
	
	/*RepairCode*/
	CALL trigger_update(OLD.RepairCode, NEW.RepairCode, 'part', 'RepairCode', OLD.PartID, NEW.ModifiedUserID);
	
	/*IsMajorFault*/
	CALL trigger_update(OLD.IsMajorFault, NEW.IsMajorFault, 'part', 'IsMajorFault', OLD.PartID, NEW.ModifiedUserID);
	
	/*CircuitReference*/
	CALL trigger_update(OLD.CircuitReference, NEW.CircuitReference, 'part', 'CircuitReference', OLD.PartID, NEW.ModifiedUserID);
	
	/*PartSerialNo*/
	CALL trigger_update(OLD.PartSerialNo, NEW.PartSerialNo, 'part', 'PartSerialNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*OrderNo*/
	CALL trigger_update(OLD.OrderNo, NEW.OrderNo, 'part', 'OrderNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*OrderStatus*/
	CALL trigger_update(OLD.OrderStatus, NEW.OrderStatus, 'part', 'OrderStatus', OLD.PartID, NEW.ModifiedUserID);
	
	/*OrderDate*/
	CALL trigger_update(OLD.OrderDate, NEW.OrderDate, 'part', 'OrderDate', OLD.PartID, NEW.ModifiedUserID);
	
	/*ReceivedDate*/
	CALL trigger_update(OLD.ReceivedDate, NEW.ReceivedDate, 'part', 'ReceivedDate', OLD.PartID, NEW.ModifiedUserID);
	
	/*InvoiceNo*/
	CALL trigger_update(OLD.InvoiceNo, NEW.InvoiceNo, 'part', 'InvoiceNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*UnitCost*/
	CALL trigger_update(OLD.UnitCost, NEW.UnitCost, 'part', 'UnitCost', OLD.PartID, NEW.ModifiedUserID);
	
	/*SaleCost*/
	CALL trigger_update(OLD.SaleCost, NEW.SaleCost, 'part', 'SaleCost', OLD.PartID, NEW.ModifiedUserID);
	
	/*VATRate*/
	CALL trigger_update(OLD.VATRate, NEW.VATRate, 'part', 'VATRate', OLD.PartID, NEW.ModifiedUserID);
	
	/*SBPartID*/
	CALL trigger_update(OLD.SBPartID, NEW.SBPartID, 'part', 'SBPartID', OLD.PartID, NEW.ModifiedUserID);
	
	/*PartStatus*/
	CALL trigger_update(OLD.PartStatus, NEW.PartStatus, 'part', 'PartStatus', OLD.PartID, NEW.ModifiedUserID);
	
	/*OldPartSerialNo*/
	CALL trigger_update(OLD.OldPartSerialNo, NEW.OldPartSerialNo, 'part', 'OldPartSerialNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*IsOtherCost*/
	CALL trigger_update(OLD.IsOtherCost, NEW.IsOtherCost, 'part', 'IsOtherCost', OLD.PartID, NEW.ModifiedUserID);
	
	/*IsAdjustment*/
	CALL trigger_update(OLD.IsAdjustment, NEW.IsAdjustment, 'part', 'IsAdjustment', OLD.PartID, NEW.ModifiedUserID);
	
	/*DueDate*/
	CALL trigger_update(OLD.DueDate, NEW.DueDate, 'part', 'DueDate', OLD.PartID, NEW.ModifiedUserID);
	
	/*SupplierOrderNo*/
	CALL trigger_update(OLD.SupplierOrderNo, NEW.SupplierOrderNo, 'part', 'SupplierOrderNo', OLD.PartID, NEW.ModifiedUserID);
	
	/*ModifiedUserID*/
	CALL trigger_update(OLD.ModifiedUserID, NEW.ModifiedUserID, 'part', 'ModifiedUserID', OLD.PartID, NEW.ModifiedUserID);
	
END
;;

DELIMITER ;