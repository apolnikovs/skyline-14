# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.297');

# ---------------------------------------------------------------------- #
# Vyk Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_history ADD COLUMN Image VARCHAR(250) NULL DEFAULT NULL AFTER Notes;


# ---------------------------------------------------------------------- #
# Raju Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE job ADD FromJobId INT NULL DEFAULT NULL AFTER StatusID , ADD ToJobId INT NULL DEFAULT NULL AFTER FromJobId, ADD ReplicateJob VARCHAR(255) NULL AFTER ToJobId;

INSERT INTO permission (PermissionID, Name, Description, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, URLSegment) VALUES ('11010', 'Job Update Page - Replicate Job', 'Allow the user to replicate the details on a job', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, CURRENT_TIMESTAMP, 'replicateJob');

INSERT INTO audit_trail_action (AuditTrailActionID, BrandID, Action, ActionCode, Type, Status) VALUES (NULL, '1000', 'Replicate Job', '13', 'Process', 'Active');


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.298');
