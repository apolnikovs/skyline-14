$(document).ready(function() {
    var domain=location.hostname;
    console.log(domain);
    $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="../images/ajax-loading_medium.gif">',
                ajax: {
                    url: '../Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random()
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
                solo: true // Only show one tooltip at a time
            },
            Hide: {
                
            
            },
            
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault();
    });
        
});