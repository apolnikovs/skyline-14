<script>
    $( "#brandID" ).combobox({
        change: function(){
            if($(this).val() == "") {
                $("#primaryFieldP, #alternativeFieldP, #statusP").hide();
                $("#insert_save_btn, #cancel_btn").hide();
                $.colorbox.resize();
            } else {
                $.post("{$_subdomain}/SystemAdmin/getAltNames", { brandID : $("#brandID").val() }, function(data) {
                    var fields = $.parseJSON(data);
                    var html = "";
                    html += '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                    for(var i = 0; i < fields.length; i++) {
                        html += '<option value="' + fields[i]["primaryFieldID"] + '">' + fields[i]["primaryFieldName"] + '</option>';
                    }
                    $("#primaryFieldID").html(html);	    
                    $("#primaryFieldP, #alternativeFieldP, #statusP").css("display","inline-block");
                    $("#insert_save_btn, #cancel_btn").css("display","inline-block");
                    $.colorbox.resize();
                });
            }
        }
    });
    $("#primaryFieldID").combobox();
    $(document).ready(function() {

	if($("#alternativeFieldName").val()=="" || !$("#alternativeFieldName").val()) {
	    $("#primaryFieldP, #alternativeFieldP, #statusP").css("display","none");
	    $("#insert_save_btn, #cancel_btn").css("display","none");
	} else {
	    $("#alternativeFieldP").attr("disabled","disabled");
	}

    });

</script>


{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
	    <fieldset>
		<legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
		<p><label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br/><br/></p>
		<p>
		    <span class= "bottomButtons" >
			<input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}"/>
		    </span>
		</p>
	    </fieldset>   
        </form>            

    </div>    
    
{else}
    
    <div id="countryFormPanel" class="SystemAdminFormPanel" >
    
	<form id="CForm" name="CForm" method="post"  action="" class="inline">

	    <fieldset>

		<legend title="">{$form_legend|escape:'html'}</legend>

		<p><label id="suggestText"></label></p>
		
		<p>
		    <label></label>
		    <span class="topText">
			{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}
		    </span>
		</p>

		<p>
		    <label class="fieldLabel" style="width:140px;" for="brandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp;
		    <select name="brandID" id="brandID" {if !isset($brands)}disabled="disabled"{/if}>
			{if isset($brands)}
			    <option value="" {if $datarow.brandID==''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
			    {foreach $brands as $brand}
				<option value="{$brand.BrandID|escape:'html'}" {if $datarow.brandID==$brand.BrandID} selected="selected" {/if}>{$brand.BrandName|escape:'html'}</option>
			    {/foreach}
			{else}
			    <option value="{$datarow.brandID}" selected="selected">{$datarow.BrandName|escape:'html'}</option>
			{/if}
		    </select>
		</p>
		
		<p id="primaryFieldP">
		    <label class="fieldLabel" style="width:140px;" for="primaryFieldName">
			{$page['Labels']['primary_field_name']|escape:'html'}:<sup>*</sup>
		    </label>
		    &nbsp;&nbsp;
		    <select name="primaryFieldID" id="primaryFieldID" {if !isset($brands)}disabled="disabled"{/if}>
			{if isset($fields)}
			    <option value="" {if $datarow.primaryFieldID==''}selected="selected"{/if}>
				{$page['Text']['select_default_option']|escape:'html'}
			    </option>
			    {foreach $fields as $field}
				<option value="{$field.primaryFieldID|escape:'html'}" class="" 
				{if $datarow.primaryFieldID==$field.primaryFieldID} selected="selected" {/if}>
				    {$field.primaryFieldName|escape:'html'}
				</option>
			    {/foreach}
			{else}
			    <option value="{$datarow.primaryFieldID}" selected="selected">
				{$datarow.primaryFieldName|escape:'html'}
			    </option>
			{/if}
		    </select>
		</p>
		
		<p id="alternativeFieldP">
		    <label class="fieldLabel" style="width:140px;" for="alternativeFieldName">
			{$page['Labels']['alternative_field_name']|escape:'html'}:<sup>*</sup>
		    </label> &nbsp;&nbsp;
		    <input  type="text" class="text"  name="alternativeFieldName" value="{$datarow.alternativeFieldName|escape:'html'}" id="alternativeFieldName" />
		</p>

		{if isset($datarow.alternativeFieldID)}
		    <input type="hidden" name="alternativeFieldID" value="{$datarow.alternativeFieldID}" />
		{/if}
		
		<p id="statusP">
		    <label class="fieldLabel" style="width:140px;" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
		    &nbsp;&nbsp; 
		    {foreach $statuses as $status}
			<input  type="radio" name="status"  value="{$status.Code}" {if $datarow.status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 
		    {/foreach}    
		</p>
		
		<p>
		    <span class="bottomButtons" >
			<input type="hidden" name="CountyID" value="{*$field.alternativeFieldID|escape:'html'*}" >
			<input type="hidden" name="CountyCode" value="{*$field.alternativeFieldID|escape:'html'*}" >
			{if $datarow.alternativeFieldName neq '' && $datarow.alternativeFieldName neq '0'}
			    <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{else}
			    <input type="submit" name="insert_save_btn" class="btnConfirm" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			{/if}
			<input type="submit" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" />
		    </span>
		</p>
		
	    </fieldset>    

	</form>        
       
    </div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 
                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;">   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>
			
    </form>            
    
</div>    


{* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>            
    
</div>                            
                        
