{extends "DemoLayout.tpl"}

{block name=config}

    {$def=0}

    {if $SuperAdmin} {$def=1} {/if}
 
    {if !isset($allocOnly)||$allocOnly!='AllocationOnly'}
        
     {$def=1}
     
    {/if}
     
     
    {if $allocOnly=='NoViamente'}
     
        {$def=2}
        
     {/if}
     
{$PageId = $AppointmentDiaryPage}
{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.timepicker.js?v=0.3.1"></script>
   <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
   <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>

   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.ui.timepicker.css?v=0.3.1" type="text/css" />
   <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
   
<style type="text/css" >
      
    .ui-combobox-input {
         width:270px;
     }  
     
     
     
     #LunchBreakDurationElement input {
         width:235px;
     }
         
</style>

<script type="text/javascript">
    $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
</script>
{/block}

{block name=scripts}


{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
        



<script type="text/javascript">
    
    function inactiveRow(nRow, aData)
    {
        //$('td:eq(0)', nRow).attr("id", aData[0]);
        
        $('td:eq(0)', nRow).parent().attr("id", "EngID_"+aData[0]);
        
        
        if(aData[0]=="{$datarow.ServiceProviderEngineerID}")
        {
            $checked=" checked='checked' ";
        }
        else
        {
            $checked="";
        }
        
        
        $('td:eq(0)', nRow).html( '<input type="radio" name="RadioButtonEngineerID" value="'+aData[0]+'" '+$checked+'  >'+aData[1]);

        if (aData[2]=="In-active")
        {  
             $('td:eq(1)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" align="center"  width="20" height="20" >' );
        }
        else
        {   
              $('td:eq(1)', nRow).html( '<img src="{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png" align="center" width="20" height="20" >' );
        }
    }
    
    
    
    
           
       
    
    
    $(document).ready(function() {
    
     $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }                  
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation                
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
    
    
    
    
     function adjustPanelHeights ()
     {
        $('#innerFirstDiv').css("height", '');
        $('#firstDiv').css("height", '');
        $('#secondDiv').css("height", ''); 
        
         
        $height1 = $('#firstDiv').height();
        $height2 = $('#secondDiv').height();

       
        $maxHeight = 400;
        
        if($height1>$maxHeight)
        {
           $maxHeight = $height1;
        } 
        
        if($height2>$maxHeight)
        {
           $maxHeight = $height2;
        } 

        $maxHeight = $maxHeight+50;
        
        $('#firstDiv').css("height", $maxHeight+"px");
        $('#secondDiv').css("height", $maxHeight+"px");
        $('#innerFirstDiv').css("height", ($maxHeight-30)+"px");
     }
     
     
    
     
     function showHomePage()
     {
        clearDataForm(); 
        $("#addEngineer").show();
        
        $("#navigationButtons").show();
        
        $("#ShowAllText").show();
        
        $("#AllocatePostCodes").show();
        
        
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
        $("#formDiv").hide();
        $('#innerFirstDiv').css("height", '');
        $('#firstDiv').css("height", '');
        $('#secondDiv').css("height", '');
        $("#defaultDiv").show();
        adjustPanelHeights();
     }
    
    
       adjustPanelHeights();
     
     
     
     
        //Click handler for Viamente checkbox.
//        $(document).on('click', '#adViamente', 
//                      function() {
//
//
//                             checkboxViamente();
//
//                      });


       
    
    
        //Click handler for Show All checkbox.
        $(document).on('click', '#ShowAll', 
                      function() {

                        
                            if ($('#ShowAll').is(':checked')) {
                                
                                document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val()+"/ShowAll=1";
                                
                            } 
                            else 
                            {
                            
                                document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val()+"/ShowAll=0";    
                            } 
                        
                        
                          
                      });





          $(document).on('click', '#saveRecord5', 
                      function() {
                      
                        $("#ReplicateType").val($("input[name='MWReplicateType']:checked").val());
                        $("#ReplicateStatusFlag").val($("input[name='MWReplicateStatusFlag']:checked").val());
                        $("#ReplicatePostcodeAllocation").val($("input[name='MWReplicatePostcodeAllocation']:checked").val());
                        
                        
                        
                        $("#ReplicateTypeFlag").val(1);
                        
                        
                       // $.colorbox.close();
                        $("#saveRecord").trigger("click");
                        
                        return false;
                          
                      });
                      
          $(document).on('click', '#cancelChanges5', 
                      function() {
                      
                        $.colorbox.close();
                       
                        return false;
                          
                      });               






        //Click handler for delete engineer.
        $(document).on('click', '#deleteEngineer', 
                      function() {
                      
                      
                      $.colorbox({ 
                      
                                inline:true,
                                href:"#deleteEngineerConfirmation",
                                title: "{$page['Buttons']['delete_engineer']|escape:'html'}",
                                opacity: 0.75,
                                height:270,
                                width:720,
                                overlayClose: false,
                                escKey: false

                        });
                      
                        
                        return false;
                          
                      });
                      
          $(document).on('click', '#cancelChanges2', 
                      function() {
                      
                        $.colorbox.close();
                       
                        return false;
                          
                      });            
                      




       
       //Click handler for add engineer.
        $(document).on('click', '#addEngineer', 
                      function() {
                      
                        showDataForm();
                        clearDataForm();
                        
                        return false;
                          
                      });
                      
        //Click handler for cancelChanges.
        $(document).on('click', '#cancelChanges', 
                      function() {
                      
                        document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val();
                        return false;
                          
                      }); 
                      
                      
         //Click handler for AllocatePostCodes.
        $(document).on('click', '#AllocatePostCodes', 
                      function() {
                      
                        document.location.href = "{$_subdomain}/AppointmentDiary/skillsAreaMap/spID="+$("#ServiceProviderID").val();
                        return false;
                          
                      });               
                      
              
              
              
                      
                      
                      
                      
       //Click handler for row selected.
//        $(document).on('click', '#SPEResults tr', 
//                      function() {
//                      
//                         
//                         $EngID = $(this).attr("id");
//                         
//                         if($EngID)
//                         {
//                           $EngID =  $EngID.replace("EngID_","");
//                         }
//                         document.location.href="{$_subdomain}/AppointmentDiary/index/update/"+$EngID;
//                         
//                        return false;
//                          
//                      });                
                      
                      
        $(document).on('click', 'input[name=RadioButtonEngineerID]:radio', 
                      function() {
                        
                        $('input[name=RadioButtonEngineerID]:radio').css( 'cursor', 'wait' );
                        $('body').css( 'cursor', 'wait' );
                        
                        {if $allocOnly eq 'AllocationOnly' && $def eq 0}
                            
                            document.location.href="{$_subdomain}/AppointmentDiary/skillsAreaMap/spID="+$("#ServiceProviderID").val()+"/eID="+$(this).val()+"/d="+$("#start_date").val();
                            
                        {else}
                            
                            document.location.href="{$_subdomain}/AppointmentDiary/index/update/"+$(this).val();
                            
                        {/if}
                        
                        
                      });  
       
       
        
        
      {if $tInfo} 
          
           $("#centerInfoText").html("{$page['Text']['data_saveed_msg']|escape:'html'}").fadeIn('slow').delay("5000").fadeOut('slow');  
      
      {/if}
          
           
          
          
     function clearDataForm()
     {
         {if $def==1}
        $("#EngineerFirstName").val('');
        $("#EngineerLastName").val('');
        $("#ServiceBaseUserCode").val('');
        $("#EmailAddress").val('');
        $("#MobileNumber").val('');
        $("#RouteColour").val('');
        $("#StartShift").val('');
        $("#EndShift").val('');
        $("#LunchBreakDuration").val('');
        $("#LunchPeriodFrom").val('');
        $("#LunchPeriodTo").val('');
        $("#StartPostcode").val('');
        $("#EndPostcode").val('');
        $("#ServiceProviderEngineerID").val('');
        {/if}
        
     }   
                      
     function showDataForm()
     {
        $("#defaultDiv").hide();
        //$("#weekButtons1").trigger("click");
        $("#formDiv").show();
        $(".ui-multiselect").css("width", "110px");
        $(".DateSaveButton").css("text-decoration", "underline");
        
        //$(".ui-multiselect-menut").css("width", "270px");
        
        
        
        
        
       // $('#secondDiv').css("height", '');
      //  $('#formDiv').css("height", '');
     //   alert($("#formDiv").height());
        
        adjustPanelHeights();
        
        
        

        $("#saveRecord").show();
        $("#cancelChanges").show();


        $("#addEngineer").hide();
        $("#navigationButtons").hide();
        $("#ShowAllText").hide();
        $("#AllocatePostCodes").hide();
     }
     
     function checkboxViamente()
     {
        if ($('#adViamente').is(':checked')) 
        {
            $("#skillsPostcodeBtn").hide();
            $("#addEngineer").hide();
            $("#navigationButtons").hide();
            $("#ShowAllText").hide();
            $("#AllocatePostCodes").hide();
            $("#saveRecord").hide();
            $("#cancelChanges").hide();
            $("#formDiv").hide();
            $('#innerFirstDiv').css("height", '');
            $('#firstDiv').css("height", '');
            $('#secondDiv').css("height", '');
            $("#defaultDiv").show();
            
            adjustPanelHeights();

           
        } 
        else 
        {
             $("#skillsPostcodeBtn").show();
             $("#addEngineer").show();
             $("#navigationButtons").show();
             $("#ShowAllText").show();
             $("#AllocatePostCodes").show();
            
        } 
     }
  
    // checkboxViamente();
    
     $('#RouteColour').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '150px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
    
    $(".Color").css({ width: "25px", height: "24px", padding: "0px" });
  
    
    
    
    
    
    
    
    {for $wB=1 to 4}
    
        $('#MondayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#MondayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });



        $('#TuesdayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#TuesdayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });



        $('#WednesdayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#WednesdayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });



        $('#ThursdayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#ThursdayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });


        $('#FridayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#FridayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#SaturdayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#SaturdayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });


         $('#SundayStartShift{$wB}').timepicker({
            defaultTime: '00:02'
        });

        $('#SundayEndShift{$wB}').timepicker({
            defaultTime: '00:02'
        });
        
     $("#MondaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500]  } );
     $("#TuesdaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
     $("#WednesdaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
     $("#ThursdaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
     $("#FridaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
     $("#SaturdaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
     $("#SundaySkillsSet{$wB}").multiselect( {  noneSelectedText: "{$page['Text']['select_skills']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500] } );
    
    {/for}
    
    
    
    
    
    $('#LunchPeriodFrom').timepicker({
        defaultTime: '00:00',
        onHourShow: tpStartOnHourShowCallbackLunch,
        onMinuteShow: tpStartOnMinuteShowCallbackLunch
    });
    
    $('#LunchPeriodTo').timepicker({
        defaultTime: '00:00',
        onHourShow: tpEndOnHourShowCallbackLunch,
        onMinuteShow: tpEndOnMinuteShowCallbackLunch
    });
    
    
    
    
    
    
    
    $( "#LunchBreakDuration" ).combobox();
    
     {if $SuperAdmin}
     
        $( "#ServiceProviderID" ).combobox( { change: function() { document.location.href="{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val(); } } );
    
     {/if}
    
    
     
    
    
    $('.ui-multiselect-checkboxes label').css("text-align", "left");
    //$(".ui-multiselect").css("width", "314px");
    
    $('button[aria-haspopup=true]').css("width", "310px");
    $('button[aria-haspopup=true]').css("border-radius", "7px");
    
    
    
    
    $(document).on('click', '.DateSaveButton', 
                            function() {
                            
                               $("#FocusDate").val($(this).attr("id"));
                               $("#saveRecord").trigger("click");
                               
                               return false;
                                
                            });
                            
     $(document).on('click', '#PostcodeAllocationSubMenuButton', 
                            function() {
                            
                               $("#FocusDate").val($("#start_date").val());
                               $("#saveRecord").trigger("click");
                               
                               return false;
                                
                            });
                            
               $(document).on('click', '#GeoCellAllocationSubMenuButton', 
                            function() {
                            
                               $("#FocusDate").val($("#start_date").val());
                               $("#saveRecord").trigger("click");
                               
                               return false;
                                
                            });             
                            
                            
                            
     function engineerFormValidate($FocusDate)
     {
         
           //If the Engineer is in-actived then disabling all the days
           
           
            if($("input[name='Status']:checked:enabled").val()=="In-active")
            {    
                $('.EngCheckBox').removeAttr('checked');
            }
         
            if($("label.fieldError:visible").length>0)
            {

            }
            else
            {
                $("#saveRecord").hide();
                $("#cancelChanges").hide();
                $("#processDisplayText").show();
                
                $.colorbox({ 
                                                
                        html:$('#waitDiv').html(), 
                        title:"",
                        escKey: false,
                        overlayClose: false,
                        onLoad: function() {
                            $('#cboxClose').hide();
                        }

                });
                
            }
            
            if($FocusDate!='')
            {
                $("#SentToViamente").val('No');
            }
            else
            {
                $("#SentToViamente").val('Yes');     
            }
            
           
            
             $('#appointmentDiaryForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                               
                                                    EngineerLastName:
                                                       {
                                                           required: true
                                                       },
                                                    ServiceBaseUserCode:
                                                       {
                                                           required: true
                                                       },
                                                    EmailAddress:
                                                       {
                                                           email: true
                                                       },
                                                    MobileNumber:
                                                       {
                                                           digits: true
                                                       },
                                                    SpeedFactor:
                                                       {
                                                           required: true,
                                                           digits: true,
                                                           max: 400,
                                                           min:20
                                                       },   
                                                     RouteColour:
                                                       {
                                                           required: {if $def!=1}false{else}true{/if}
                                                       }, 
                                                     EngineerImportance:
                                                       {
                                                           digits: true
                                                       }    
                                                       
                                                   },
                                            messages: {
                                            
                                                      EngineerLastName:
                                                      {
                                                          required: "{$page['Errors']['surname']|escape:'html'}"
                                                      },
                                                      ServiceBaseUserCode:
                                                      {
                                                          required: "{$page['Errors']['user_code']|escape:'html'}"
                                                      },
                                                      EmailAddress:
                                                      {
                                                          email: "{$page['Errors']['invalid_email']|escape:'html'}"
                                                      },
                                                      MobileNumber:
                                                      {
                                                          digits: "{$page['Errors']['invalid_phone']|escape:'html'}"
                                                      },
                                                      SpeedFactor:
                                                      {
                                                          required: "{$page['Errors']['speed_factor']|escape:'html'}"
                                                      },
                                                      RouteColour:
                                                      {
                                                          required: "{$page['Errors']['route_colour']|escape:'html'}"
                                                      }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
                                                if(element.attr("name")=="RouteColour")
                                                {
                                                     error.insertAfter( $("#RouteColourError") );
                                                }
                                                else if(element.attr("name")=="EngineerImportance")
                                                {
                                                     error.insertAfter( $("#EngineerImportanceHelp") );
                                                }
                                                else
                                                {
                                                     error.insertAfter( element );
                                                }
                                                
                                               // alert($("#FocusDate").val());
                                                
                                                $("#processDisplayText").hide();
                                                $("#saveRecord").show();
                                                $("#cancelChanges").show();
                                                adjustPanelHeights();
                                                $.colorbox.close();
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                               
                                               
                                               if($("#ServiceProviderEngineerID").val()!='')
                                               {  
                                                    $.post("{$_subdomain}/Data/getEngineerAppointments/",        

                                                             $("#appointmentDiaryForm").serialize(),      
                                                             function(data){
                                                                 // DATA NEXT SENT TO COLORBOX
                                                                 var p = eval("(" + data + ")");

                                                                         if(p['Status']=="ERROR")
                                                                         {


                                                                             $("#processDisplayText").hide();
                                                                             $("#saveRecord").show();
                                                                             $("#cancelChanges").show();
                                                                             $.colorbox.close();
                                                                                    
                                                                             alert(p['Message']);       

                                                                         }
                                                                         else if(p['Status']=="OK")
                                                                         {
                                                                            
                                                                             if(p['Count'])  
                                                                             {
                                                                                $("#processDisplayText").hide();
                                                                                $("#saveRecord").show();
                                                                                $("#cancelChanges").show();
                                                                                 
                                                                                 
                                                                                  $html_code_text = '';
                                                                                 
                                                                                  
                                                                                  
                                                                                  
                                                                                  $hidden_appointments_list = '<input type="hidden" id="AppointmentHandlingType" name="AppointmentHandlingType" value="" ><input type="hidden" id="hidFocusDate" name="hidFocusDate" value="'+$FocusDate+'" >';
                                                                                 
                                                                                  var $apCnt = 0;
                                                                                 
                                                                                  for($i=0;$i<p['Count'];$i++)
                                                                                  {
                                                                                      
                                                                                      $spJobNo = p['Message'][$i]['ServiceCentreJobNo'];
                                                                                      $custPostcode = p['Message'][$i]['CustPostCode'];
                                                                                      
                                                                                      if(!$spJobNo)
                                                                                      {
                                                                                          $spJobNo = p['Message'][$i]['ServiceProviderJobNo'];
                                                                                          $custPostcode = p['Message'][$i]['NSCustPostCode'];
                                                                                      }
                                                                                      
                                                                                      
                                                                                      if(p['Message'][$i]['ForceEngineerToViamente']!='0')
                                                                                      {
                                                                                         $apCnt++; 
                                                                                         $html_code_text += '(<b>'+$apCnt+'</b>) Service Provider Job No. <b>'+$spJobNo+'</b> - <b>'+p['Message'][$i]['AppointmentType']+'</b> - <b>'+$custPostcode+'</b> - <b>'+p['Message'][$i]['AppointmentDate']+'</b> - <b>'+p['Message'][$i]['AppointmentTime']+'</b><br>';
                                                                                      }
                                                                                      
                                                                                      
                                                                                      $hidden_appointments_list +=  '<input type="hidden" name="AppointmentIDs[]" value="'+p['Message'][$i]['AppointmentID']+'" >';
                                                                                      
                                                                                      
                                                                                      if(p['Message'][$i]['EngCount'])
                                                                                      {
                                                                                              $newEngineersList = '<table style="margin-left:20px;" ><tr><td></td></tr>';
                                                                                              
                                                                                              for($j=0;$j<p['Message'][$i]['EngCount'];$j++)
                                                                                              {
                                                                                                   if($j)
                                                                                                   {
                                                                                                       $newEngineersList += '<tr><td style="background-color:#FFFFFF;padding-left:145px;" ><input type="radio" name="NewServiceProviderEngineerID_'+p['Message'][$i]['AppointmentID']+'" value="'+p['Message'][$i]['Engineers'][$j]['ServiceProviderEngineerID']+'" >'+p['Message'][$i]['Engineers'][$j]['Name']+'</td></tr>';
                                                                                                   } 
                                                                                                   else
                                                                                                   {
                                                                                                       $newEngineersList += '<tr><td style="background-color:#FFFFFF;padding-left:10px;" ><label style="width:115px;font-weight:bold;" >'+"{$page['Text']['specify_engineer']|escape:'html'}"+':</label><input type="radio" name="NewServiceProviderEngineerID_'+p['Message'][$i]['AppointmentID']+'" value="'+p['Message'][$i]['Engineers'][$j]['ServiceProviderEngineerID']+'" checked="checked" >'+p['Message'][$i]['Engineers'][$j]['Name']+'</td></tr>';    
                                                                                                   }
                                                                                                   
                                                                                                   
                                                                                              }
                                                                                              
                                                                                              $newEngineersList += '</table>';
                                                                                              
                                                                                              $html_code_text = $html_code_text+$newEngineersList;
                                                                                             

                                                                                      }
                                                                                      
                                                                                  }
                                                                                  
                                                                                  $("#appointmentsList").html($html_code_text);
                                                                                  $("#hidden_appointment_ids").html($hidden_appointments_list);
                                                                                        
                                                                                 
                                                                                  if($apCnt!=0)
                                                                                  {
                                                                                        $.colorbox({ 

                                                                                           inline:true,
                                                                                           href:"#appointmentsExist",
                                                                                           title: "{$page['Text']['appointment_exists']|escape:'html'}",
                                                                                           opacity: 0.75,
                                                                                           width:720,
                                                                                           overlayClose: false,
                                                                                           escKey: false,
                                                                                           onLoad: function() {
                                                                                               $('#cboxClose').show();
                                                                                           }

                                                                                       });
                                                                                   
                                                                                   }
                                                                                   else
                                                                                   {
                                                                                         $("#modifyAppointment").trigger("click");
                                                                                   }
                                                                            }
                                                                            else
                                                                            {
                                                                    
                                                                               PostEngineerDetails($FocusDate)
                                                                            }
                                                                           
                                                                            
                                                                             
                                                                         }
                                                             }); //Post ends here... 

                                                }
                                                else
                                                {
                                                    PostEngineerDetails($FocusDate)
                                                }
                                                
                                                
                                            }
                                                    
                            
                            
                            });
         
     }
     
     
     
     
     
     function PostEngineerDetails($FocusDate)
     {

            $.post("{$_subdomain}/AppointmentDiary/ProcessData/Engineers/",        

                                                        $("#appointmentDiaryForm").serialize()+"&"+$("#appointmentsExistForm").serialize(),      
                                                        function(data){
                                                            // DATA NEXT SENT TO COLORBOX
                                                            var p = eval("(" + data + ")");
                                                                 
                                                                    if(p['status']=="ERROR")
                                                                    {
                                                                        
                                                                        $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow');  
                                                                        
                                                                        $("#processDisplayText").hide();
                                                                        $("#saveRecord").show();
                                                                        $("#cancelChanges").show();
                                                                        
                                                                        
                                                                        $("#processDisplayText3").hide();
                                                                        $("#modifyAppointment").show();
                                                                        $("#cancel_changes").show();
                                                                        
                                                                        
                                                                        $(".DateSaveButton").css("color", "");
                                                                        
                                                                        if(p['days'])
                                                                        {    
                                                                            for($d=0;$d<p['days'].length;$d++)
                                                                            {
                                                                                $("#"+p['days'][$d]).css("color", "red");
                                                                            }  
                                                                        }
                                                                        
                                                                        
                                                                        $.colorbox.close();
                                                                        
                                                                        document.location.href = "#errorCenterInfoText";

                                                                       
                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {

                                                                     
                                                                       if($FocusDate!='')
                                                                       {
                                                                           document.location.href = "{$_subdomain}/AppointmentDiary/skillsAreaMap/spID="+$("#ServiceProviderID").val()+"/eID="+p['eID']+"/d="+$FocusDate;
                                                                       }
                                                                       else
                                                                       {
                                                                           document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val()+"/tInfo=1";    
                                                                       }
                                                                     
                                                                    }
                                                        }); //Post ends here...
                                                
     }
     
     
     
      
      
      $(document).on({
                mousedown: function(e) {
                   $("#errorCenterInfoText").fadeOut('slow');
                },
                keypress: function(e) {
                   $("#errorCenterInfoText").fadeOut('slow');
                }
            });
     
     
     
     
     
     $(document).on('click', '#modifyAppointment', 
                            function() {
                     
            $("#AppointmentHandlingType").val('modifyAppointment');          
                     
            $("#processDisplayText3").show();
            $("#modifyAppointment").hide();
            $("#cancel_changes").hide();
            
           
                                                                        
            PostEngineerDetails($("#hidFocusDate").val())
               
               
           return false;    
      });
      
      
      $(document).on('click', '#cancel_changes', 
                            function() {
      
           $("#ReplicateTypeFlag").val('');
           
           $.colorbox.close(); 
           
           return false;
           
      });
     
     $(document).on('click', '#saveRecord2', 
                            function() {
                            
                $("#processDisplayText2").show();
                $("#saveRecord2").hide();
                $("#cancelChanges2").hide();              
                
                $("#Deleted").val("Yes");
                      
                $.post("{$_subdomain}/AppointmentDiary/ProcessData/Engineers/",        

                $("#appointmentDiaryForm").serialize(), 
                
                function(data){
                    // DATA NEXT SENT TO COLORBOX
                    var p = eval("(" + data + ")");

                            if(p['status']=="ERROR")
                            {

                                $("#errorCenterInfoText2").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  

                                $("#processDisplayText2").hide();
                                $("#saveRecord2").show();
                                $("#cancelChanges2").show();
                                
                                
                            }
                            else if(p['status']=="OK")
                            {

                                   document.location.href = "{$_subdomain}/AppointmentDiary/index/spID="+$("#ServiceProviderID").val()+"/tInfo=1";    
                            }
                }); //Post ends here...
                                                    
              return false;   
     });
     
    
    
     $(document).on('click', '#saveRecord', 
                            function() {
                            
                        
                            
                        if($("input[name='SetupType']:checked").val()=="Replicate"  && !$("#FocusDate").val() && $("#ReplicateTypeFlag").val()=='')
                        {
                             $("#FocusDate").val('');
                             $.colorbox({ 
                      
                                    inline:true,
                                    href:"#ReplicateWeekDataDiv",
                                    title: "{$page['Text']['replicate_engineer']|escape:'html'}",
                                    opacity: 0.75,
                                    height:500,
                                    width:750,
                                    overlayClose: false,
                                    escKey: false

                            });
                            
                            return false;
                        }
                        else
                        {
                            engineerFormValidate($("#FocusDate").val()); 
                            $("#FocusDate").val('');
                        }
                            
    
    });
    
    
                    {if $showUpdatePage} 

                      var  $displayEngNo  = 19;
                        
                    {else}
                        var  $displayEngNo  = 10;
                    {/if}    
    
    
                    //Engineers data table starts here...   
                    $('#SPEResults').PCCSDataTable( {


                            "aoColumns": [ 
                                                        /* ServiceProviderEngineerID */  {  "bVisible":    false },    
                                                        /* Engineer Name. */   {  "bSortable":    false },
                                                        /* Status */ {  "bSortable":    false }
                                                ],
                            
                            
                            
                            
                            
                               
                            displayButtons:  '',    
                            htmlTablePageId: 'innerFirstDiv',
                            htmlTableId:     'SPEResults',
                            fetchDataUrl:    '{$_subdomain}/AppointmentDiary/ProcessData/Engineers/fetch/{$ServiceProviderID}/{$ShowAll}',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  $displayEngNo,
                            sDom: 'trp',
                            sPaginationType: 'two_button',
                            hidePaginationNorows:  true,
                            "aaSorting": [[ 1, "asc" ]],
                            bServerSide: false
                           


                        });
                     //Engineers data table ends here... 
    
    
    
                //Click handler for Week buttons.
                $(document).on('click', '#WeekButtons label', 
                      function() {
                      
                        $('#WeekButtons label').each(function() {
                
                           $(this).removeClass("weekButtonsActive"); 
                           $(this).addClass("weekButtons");

                        });
                        
                        {for $wB=1 to 4}
                            
                            $(".week{$wB}Element").hide();
                            
                        {/for}
                        
                        $showID = $(this).attr("id").replace("weekButtons", "");
                        
                        $(".week"+$showID+"Element").show();
                        
                        $(this).addClass("weekButtonsActive");
                      
                        $(".ui-multiselect").css("width", "110px");
                        $(".DateSaveButton").css("text-decoration", "underline");
                          
                      });       
    
    
                
    
    
             
         
                {for $wB=1 to 4}
                   
                   {if $datarow.$wB.WeekClassName neq "weekButtonsActive" }
                    
                     $(".week{$wB}Element").hide();
                   
                   {/if}

                {/for}
                    
             {if $showUpdatePage} 
                 
                $(".ui-multiselect").css("width", "110px");
                $(".DateSaveButton").css("text-decoration", "underline");

                

                showDataForm();

               // alert($(".ui-multiselect").width());

              //  $("#weekButtons1").trigger("click");

             {/if}         
    
             $("#{$datarow.FocusField}").focus();
             
             
             
            
         {if $DiaryWizardSetup}
         
            $('#menu').after('<div class="customOverlay" style="display: block; opacity: 0.25; top:0; cursor: default; width:955px;height:200px;"></div>');
        
         {/if}
             
             
             
         
        $(document).on('click', '#nextButton', 
                                function() {

                    if($('[name="RadioButtonEngineerID"]').length) 
                    {    
                     document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=7";    
                    }
                    else
                    {
                        alert("{$page['Errors']['engineer']|escape:'html'}");
                    }
                    return false;

            });

        $(document).on('click', '#previousButton', 
                                function() {

                    document.location.href = "{$_subdomain}/AppointmentDiary/skillsSetSP/spID="+$("#ServiceProviderID").val();    

                    
                    return false;
                });    
     
             
    
    });
function showPointMap(n){
    $.colorbox({
        href:"{$_subdomain}/Diary/showPointMap/"+n,
        title: "Skyline GeoTag Map",
        opacity: 0.75,
        height:740,
        width:1000,
        overlayClose: false,
        escKey: false
    });
}
function showHelpIcons(){
    $('#help_video').toggle();
    $('#help_pdf').toggle();
    $('#help_pdf_new').toggle();
    $('#help_faq').toggle();
}
// validateShifts function added by Raju on 3rd June 2013 to validate the shift times code starts here
function validateShifts()
{
    var result = 0;
    $('.hasTimepicker').each(function() {
        if($(this).is(':visible'))
        {
            if($(this).attr('id').contains("StartShift")){
                var nxt_element = $(this).attr('id').replace('StartShift','EndShift');
                result = checkShiftDifference($(this).val(),$('#'+nxt_element).val())
                if(result > 0)
                    return false;
            }
        }
    });
    if(result < 10 && result == 1)
    {
        $("#startEndShiftsErrorInfoText").html("{$page['Errors']['shift_error']|escape:'html'}").css('color','red').fadeIn('slow');
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
    }
    else if(result > 10 && result == 11)
    {
        $("#startEndShiftsErrorInfoText").html("{$page['Errors']['start_shift']|escape:'html'}").css('color','red').fadeIn('slow');
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
    }
    else if(result > 10 && result == 12)
    {
        $("#startEndShiftsErrorInfoText").html("{$page['Errors']['start_shift_error']|escape:'html'}").css('color','red').fadeIn('slow');
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
    }
    else if(result > 20 && result == 21)
    {
        $("#startEndShiftsErrorInfoText").html("{$page['Errors']['end_shift']|escape:'html'}").css('color','red').fadeIn('slow');
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
    }
    else if(result > 20 && result == 22)
    {
        $("#startEndShiftsErrorInfoText").html("{$page['Errors']['end_shift_error']|escape:'html'}").css('color','red').fadeIn('slow');
        $("#saveRecord").hide();
        $("#cancelChanges").hide();
    }
    else if(result == 0)
    {
        $("#startEndShiftsErrorInfoText").html("").hide();
        $("#saveRecord").show();
        $("#cancelChanges").show();
    }
}
function checkShiftDifference(stShift,enShift)
{
    var intRegex = /\d\d\:\d\d$/;
    var stShiftArray = stShift.split(":");
    var enShiftArray = enShift.split(":");
    if(stShift != "" && !intRegex.test(stShift))
        return 11;
    else if(stShift == "00:00" && enShift != "00:00")
        return 12;
    else if(stShiftArray[1] > 60)
        return 12;
    else if(stShiftArray[0] > 23)
        return 12;
    else if(enShift != "" && !intRegex.test(enShift))
        return 21;
    else if(enShift != "00:00" && enShiftArray[1] > 60)
        return 22;
    else if(enShift != "00:00" && enShiftArray[0] > 23)
        return 22;
    else if(stShift != "00:00" && enShift != "00:00")
    {
        if(enShiftArray[1] > stShiftArray[1] && enShiftArray[0] > stShiftArray[0])
            return 0;
        else if(enShiftArray[1] > stShiftArray[1] && enShiftArray[0] == stShiftArray[0])
            return 0;
        else if(enShiftArray[1] < stShiftArray[1] && enShiftArray[0] > stShiftArray[0])
            return 0;
        else if(enShiftArray[1] == stShiftArray[1] && enShiftArray[0] > stShiftArray[0])
            return 0;
        else
            return 1;
    }
    else
        return 0;
}
// validateShifts function added by Raju on 3rd June 2013 to validate the shift times code ends here
</script>


    
{/block}

{block name=body}

   {if $def==1} 
<div class="breadcrumb">
    
    
    
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map']|escape:'html'}</a>  / 
        <a href="{$_subdomain}/AppointmentDiary/" >{$page['Text']['page_title']|escape:'html'}</a> / 
        {$page['Text']['defaults']|escape:'html'}
    </div>
</div>
 {/if}
{include file='include/site_map_menu.tpl'}

        
         {if $allocOnly eq 'AllocationOnly'}
             <div style="position:relative;float:right">
                 <div id="help_menu" style="position:absolute;border:none;float:right;width:300px;right:10px;top:-40px"> 
                         
              <img title="Click For Help" id onclick="showHelpIcons()" style="position:relative;float:right;border:none;cursor :pointer" src="{$_subdomain}/images/diary/user_guide3.png">
             <!--<img title="Video Tutorial" id="help_video" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/dairy_help_play.png">-->
              <a href="{$_subdomain}/files/Skyline_One_Touch_Use_Guide.pdf" target="_blank"><img onclick="" title="Online User Guide" id="help_pdf" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/manual_icon_button3.png"></a>
              <!--<a href="{$_subdomain}/files/Skyline_Online_Diary_(v3).pdf" target="_blank"><img onclick="" title="Latest Update" id="help_pdf_new" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/update_icon_button3.png"></a>-->
              <!--<a href="{$_subdomain}/Diary/faq" target="_blank"><img onclick="" title="F.A.Q" id="help_faq" style="position:relative;float:right;border:none;display:none;cursor :pointer;margin-right:5px;" src="{$_subdomain}/images/diary/faq_icon_button3.png"></a>-->
            
        
        </div>
               </div>  
                 {/if}
<div class="main" id="appointmentDiary" >   
   
   <div class="siteMapPanel" >
      
         <form id="appointmentDiaryForm" name="appointmentDiaryForm" method="post"  action="#" class="inline">
       {if $def!=1}
                        <input  type="hidden" name="defaultsOnly" id="def"  value="1" />   
                           {/if}
             
            <fieldset>
           
                        <div id="firstDiv" class="firstDiv borderDiv">
                           
                           <div id="innerFirstDiv" >
                            
                            {if $SuperAdmin}
                            
                             <select  name="ServiceProviderID" id="ServiceProviderID"  class="text auto-hint" >
                                {foreach from=$serviceProvidersList item=sp}
                                 <option value="{$sp.ServiceProviderID}" {if $ServiceProviderID eq $sp.ServiceProviderID}selected="selected"{/if} >{$sp.CompanyName|escape:'html'}</option>
                                 {/foreach}

                             </select>  
                                 
                            {else}
                            
                                <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID|escape:'html'}" >
                                
                             {/if}
                                 
                            <table id="SPEResults" border="0" cellpadding="0" cellspacing="0" class="browse" style="padding-top:4px;" >
                                <thead>
                                        <tr>
                                                <th></th> 
                                                {if $allocOnly eq 'AllocationOnly'}
                                                    <th width="90%" title="{$page['Text']['appointment_type']|escape:'html'}"  >{$page['Text']['appointment_type']|escape:'html'}</th>
                                                {else}
                                                    <th width="90%" title="{$page['Text']['engineers']|escape:'html'}"  >{$page['Text']['engineers']|escape:'html'}</th>
                                                {/if}    
                                                <th></th>
                                        </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>      
                           </div>
                           
                          {* <p style="padding-top:5px;" >
                           <input type="checkbox" name="adViamente" id="adViamente"  >{$page['Text']['viamente_enabled']|escape:'html'}
                            
                           </p> *}
                           
                        </div>
                        
                        <div id="secondDiv" class="secondDiv borderDiv" > 
                            <div style="width:100%;" id="defaultDiv" > 
                                   
                                {if $def==1}
                                    <p style="text-align:center;" >
                                        
                                         <img src="{$_subdomain}/css/Skins/{$_theme}/images/viamente-skyline.png" width="560" height="278" style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p style="margin-left:0px;text-align:center;padding:0px 100px 0px 100px;font-size:14px; line-height:21px; font-weight:normal; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; color:#737373;" >
                                        <span style="color:#336699;padding-left:20px;" >{$page['Text']['default_text1']|escape:'html'} </span>{$page['Text']['default_text2']|escape:'html'}

                                    </p>
        {/if}
         
        {if $def==2}
                                    <p style="text-align:center;" >
                                       
                                         <img src="{$_subdomain}/images/diary/skyline_diary.png" width="570"  style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p >
                                        <img src="{$_subdomain}/images/diary/skyline_diary_circles.png" width="570"   style="margin:0px; padding:0px;" >
                                         
                                    </p>
        {/if}
        {if $def==0}
                                    <p style="text-align:center;" >
                                       
                                         <img src="{$_subdomain}/images/diary/samsung_logo.png"   style="margin:0px; padding:0px;float:left" >
                                    </p>
                                    
                                    <p >
                                        <img src="{$_subdomain}/images/diary/one_touch_booking.png" width="565"   style="margin-top:90px; padding:0px; margin-bottom:90px;" >
                                         
                                    </p>
                                    <p >
                                        <img src="{$_subdomain}/images/diary/samsung_logo_standard.png"   style="margin:0px; padding:0px;float:right" >
                                         
                                    </p>
        {/if}
        
                                    <div id="centerInfoText" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                            
                            </div>
                                
                                
                                 
                                
                                
                            <div style="width:100%;display:none;" id="formDiv" > 
                                
                                <p style="text-align:center;" >
                                
                                {if $allocOnly eq 'AllocationOnly'}
                                
                                    <label class="subMenuButtonsActive" style="width:260px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="EngineerDefaultsSubMenuButton" >{$page['Text']['appointment_type']|escape:'html'}</label>
                                
                                {else}
                                    
                                    <label class="subMenuButtonsActive" style="width:260px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="EngineerDefaultsSubMenuButton" >{$page['Labels']['engineer_defaults']|escape:'html'}</label>
                                
                                {/if}
                                
                                
                                {if $dat=="GridMapping"}
                                    
                                <label  class="subMenuButtons" style="width:260px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="GeoCellAllocationSubMenuButton" >Map Area Allocation</label>
                               {else}
                                    <label class="subMenuButtons" style="width:260px;padding:10px 0px 10px 0px;margin:0px;font-size: 14px;" id="PostcodeAllocationSubMenuButton" >{$page['Labels']['postcode_allocation']|escape:'html'}</label>
                                   {/if}
                                </p>
                               
                                {if $datarow.SentToViamente=="No" && $def==1}
                                    
                                    <p  style="text-align:center;padding-top:5px;" class="formError" >
                                        <blink>{$page['Text']['details_not_sent']|escape:'html'}</blink>
                                    </p>
                                
                                {/if}
                                 
                                {if $datarow.ServiceProviderEngineerID !=''}
                                    
                                    <h3 id="SiteMapPageHeader"  >{$page['Text']['engineer_update_page']|escape:'html'}</h3> 
                                 
                                {else}
                                         
                                    <h3 id="SiteMapPageHeader"  >{$page['Text']['engineer_insert_page']|escape:'html'}</h3>
                                    
                                {/if}   
                                   
                                 <div id="errorCenterInfoText" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                            
                                 
                                
                                  <p>
                                    <label class="fieldLabel" for="EngineerFirstName" >{$page['Labels']['firstname']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText"  name="EngineerFirstName" value="{$datarow.EngineerFirstName|escape:'html'}" id="EngineerFirstName"  {if $datarow.ServiceProviderEngineerID !=''} readonly="readonly"  {/if} >

                                  </p>
                                  
                                  <p>
                                    <label class="fieldLabel" for="EngineerLastName" >{$page['Labels']['surname']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText"  name="EngineerLastName" value="{$datarow.EngineerLastName|escape:'html'}" id="EngineerLastName"  {if $datarow.ServiceProviderEngineerID !=''} readonly="readonly"  {/if} >

                                  </p>
                                  
                                  <p {if $def!=1&&$def!=2}style="display:none"{/if}>
                                    <label class="fieldLabel" for="ServiceBaseUserCode" >{$page['Labels']['service_base_user_code']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp; <input maxlength="10" type="text" class="text uppercaseText"  name="ServiceBaseUserCode" value="{if $def!=1&&$def!=2}ff{else}{$datarow.ServiceBaseUserCode|escape:'html'}{/if}" id="ServiceBaseUserCode" >
                                    
                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ServiceBaseUserCodeHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                  </p>
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="EmailAddress" >{$page['Labels']['email']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="EmailAddress" value="{$datarow.EmailAddress|escape:'html'}" id="EmailAddress" >

                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngEmailAddressHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                  </p>
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="MobileNumber" >{$page['Labels']['mobile_number']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="MobileNumber" value="{$datarow.MobileNumber|escape:'html'}" id="MobileNumber" >

                                  </p>
                                  
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="SpeedFactor" >{$page['Labels']['driver_travel_speed']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="SpeedFactor" value="{if $def!=1}100{else}{$datarow.SpeedFactor|escape:'html'}{/if}" id="SpeedFactor" >

                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="SpeedFactorHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                  </p>
                                  
                                  
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="RouteColour" >{$page['Labels']['route_colour']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="RouteColour" value="{if $def!=1}ffffff{else}{$datarow.RouteColour|escape:'html'}{/if}" id="RouteColour" style="width:270px;" >
                                    
                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="RouteColourHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                    
                                  </p>
                                  <p id="RouteColourError" style="padding:0px;margin:0px;" ></p>
                                  
                                  
                                  <p style="text-align:center;" id="WeekButtons" >
                                  <br>
                                  
                                    {for $wB=1 to 4}
                                    
                                    <label class="{$datarow.$wB.WeekClassName|escape:'html'}" id="weekButtons{$wB}" >{$datarow.$wB.WeekName|escape:'html'}</label>
                                   
                                    {/for}
                                     <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="WeekButtonsHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                    <br>
                                    <span style="float:right;" >
                                     <input  type="radio"  name="SetupType"  value="Unique"  {if $datarow.SetupType neq 'Replicate'} checked="checked" {/if} > {$page['Labels']['unique_setup']|escape:'html'}  &nbsp;&nbsp; 
                                     <input  type="radio"  name="SetupType"  value="Replicate" {if $datarow.SetupType eq 'Replicate'} checked="checked" {/if} > {$page['Labels']['recycle_weeks']|escape:'html'}
                                    </span>
                                  <br><br>
                                  </p>
                                  
                                  <!-- This Div Added By Raju on 3rd June 2013 to display Shift Time Errors-->
                                  <div id="startEndShiftsErrorInfoText" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                                  <p>
                                    
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:center;" > <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngWeekDayHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                      </label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  >{$page['Labels']['active']|escape:'html'}</label>
                                    
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  >{$page['Labels']['start']|escape:'html'}<br>{$page['Labels']['shift']|escape:'html'}</label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  >{$page['Labels']['end']|escape:'html'}<br>{$page['Labels']['shift']|escape:'html'}</label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  >{$page['Labels']['start']|escape:'html'}<br>Location</label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  >{$page['Labels']['end']|escape:'html'}<br>Location</label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;"  >{$page['Labels']['skills']|escape:'html'}</label>
                                    
                                    
                                  <p/>
                                
                                  
                                 {for $wB=1 to 4}
                                 
                                  <p id="MondayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.MondayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.MondayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="MondayActive{$wB}"  name="MondayActive{$wB}"  {if $datarow.$wB.MondayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"   name="MondayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.MondayStartShift|escape:'html'}{/if}" id="MondayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="MondayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.MondayEndShift|escape:'html'}{/if}" id="MondayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="MondayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.MondayStartPostcode|escape:'html'}{/if}" id="MondayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="MondayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.MondayEndPostcode|escape:'html'}{/if}" id="MondayEndPostcode{$wB}" ></label>
                                    
                                    
                                     <select  name="MondaySkillsSet{$wB}[]" id="MondaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.MondaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                    
                                  <p/>
                                  
                                  
                                  
                                  
                                  <p id="TuesdayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.TuesdayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.TuesdayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="TuesdayActive{$wB}" name="TuesdayActive{$wB}"  {if $datarow.$wB.TuesdayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="TuesdayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.TuesdayStartShift|escape:'html'}{/if}" id="TuesdayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="TuesdayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.TuesdayEndShift|escape:'html'}{/if}" id="TuesdayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="TuesdayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.TuesdayStartPostcode|escape:'html'}{/if}" id="TuesdayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="TuesdayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.TuesdayEndPostcode|escape:'html'}{/if}" id="TuesdayEndPostcode{$wB}" ></label>
                                    
                                    
                                    <select  name="TuesdaySkillsSet{$wB}[]" id="TuesdaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.TuesdaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                    
                                  <p/>
                                  
                                  
                                  
                                  <p id="WednesdayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.WednesdayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.WednesdayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="WednesdayActive{$wB}" name="WednesdayActive{$wB}"  {if $datarow.$wB.WednesdayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="WednesdayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.WednesdayStartShift|escape:'html'}{/if}" id="WednesdayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="WednesdayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.WednesdayEndShift|escape:'html'}{/if}" id="WednesdayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="WednesdayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.WednesdayStartPostcode|escape:'html'}{/if}" id="WednesdayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="WednesdayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.WednesdayEndPostcode|escape:'html'}{/if}" id="WednesdayEndPostcode{$wB}" ></label>
                                    
                                    
                                    <select  name="WednesdaySkillsSet{$wB}[]" id="WednesdaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.WednesdaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                    </select>
                                    
                                  <p/>
                                  
                                  
                                  <p id="ThursdayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.ThursdayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.ThursdayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="ThursdayActive{$wB}"  name="ThursdayActive{$wB}"  {if $datarow.$wB.ThursdayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="ThursdayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.ThursdayStartShift|escape:'html'}{/if}" id="ThursdayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="ThursdayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.ThursdayEndShift|escape:'html'}{/if}" id="ThursdayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="ThursdayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.ThursdayStartPostcode|escape:'html'}{/if}" id="ThursdayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="ThursdayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.ThursdayEndPostcode|escape:'html'}{/if}" id="ThursdayEndPostcode{$wB}" ></label>
                                    
                                    
                                    <select  name="ThursdaySkillsSet{$wB}[]" id="ThursdaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.ThursdaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                    
                                  <p/>
                                  
                                  
                                  <p id="FridayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.FridayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.FridayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="FridayActive{$wB}"  name="FridayActive{$wB}"  {if $datarow.$wB.FridayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="FridayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.FridayStartShift|escape:'html'}{/if}" id="FridayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="FridayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.FridayEndShift|escape:'html'}{/if}" id="FridayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="FridayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.FridayStartPostcode|escape:'html'}{/if}" id="FridayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="FridayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.FridayEndPostcode|escape:'html'}{/if}" id="FridayEndPostcode{$wB}" ></label>
                                    
                                    
                                    <select  name="FridaySkillsSet{$wB}[]" id="FridaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.FridaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                    
                                  <p/>
                                  
                                  
                                   <p id="SaturdayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.SaturdayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.SaturdayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="SaturdayActive{$wB}"  name="SaturdayActive{$wB}"  {if $datarow.$wB.SaturdayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="SaturdayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.SaturdayStartShift|escape:'html'}{/if}" id="SaturdayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="SaturdayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.SaturdayEndShift|escape:'html'}{/if}" id="SaturdayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="SaturdayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.SaturdayStartPostcode|escape:'html'}{/if}" id="SaturdayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="SaturdayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.SaturdayEndPostcode|escape:'html'}{/if}" id="SaturdayEndPostcode{$wB}" ></label>
                                    
                                    <select  name="SaturdaySkillsSet{$wB}[]" id="SaturdaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.SaturdaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                    
                                  <p/>
                                  
                                  
                                  <p id="SundayElement{$wB}" class="week{$wB}Element" >
                                    <label class="topOneThirdTextLabel" style="width:75px;text-align:right;" ><a href="#" id="{$datarow.$wB.SundayDate|escape:'html'}" class="DateSaveButton" >{$datarow.$wB.SundayLabel|escape:'html'}</a></label>
                                    <label class="topOneThirdTextLabel" style="width:25px;text-align:center;"  ><input  type="checkbox" class="EngCheckBox" id="SundayActive{$wB}"  name="SundayActive{$wB}"  {if $datarow.$wB.SundayActive eq 'Active'} checked="checked"  {/if} value="Active" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="SundayStartShift{$wB}" value="{if $def!=1}00:02{else}{$datarow.$wB.SundayStartShift|escape:'html'}{/if}" id="SundayStartShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:55px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text" style="width:40px;"  name="SundayEndShift{$wB}" value="{if $def!=1}10:00{else}{$datarow.$wB.SundayEndShift|escape:'html'}{/if}" id="SundayEndShift{$wB}" onchange="validateShifts();" ></label>
                                    <label class="topOneThirdTextLabel" style="width:82px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="SundayStartPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.SundayStartPostcode|escape:'html'}{/if}" id="SundayStartPostcode{$wB}" ></label>
                                    <label class="topOneThirdTextLabel" style="width:72px;text-align:center;{if $def!=1}display:none{/if}"  ><input  type="text" class="text uppercaseText"  style="width:65px;"  name="SundayEndPostcode{$wB}" value="{if $def!=1}NN1 5EX{else}{$datarow.$wB.SundayEndPostcode|escape:'html'}{/if}" id="SundayEndPostcode{$wB}" ></label>
                                    
                                    <select  name="SundaySkillsSet{$wB}[]" id="SundaySkillsSet{$wB}"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                   
                                       
                                        {foreach from=$skillSet item=sset}
                                         <option value="{$sset.ServiceProviderSkillsetID}" {if in_array($sset.ServiceProviderSkillsetID, $datarow.$wB.SundaySkillsSet)}selected="selected"{/if} >{$sset.SkillSetName|escape:'html'}</option>
                                         {/foreach}

                                    </select>
                                    <br><br>
                                  <p/>
                                  
                                  
                                  {/for}
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="StartHomePostcode" >Home Location:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text" style="width:200px"  name="StartHomePostcode" value="{$datarow.StartHomePostcode|escape:'html'}" id="StartHomePostcode" >
                                   
                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngHomePostcodeHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                     <img onclick="showPointMap('StartHomePostcode')" src="{$_subdomain}/images/diary/Google_Maps_Marker.png" style="float:right;margin-right:50px;">
                                  <p/>
                                  
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="EndHomePostcode" >Workshop Location:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text" style="width:200px"  name="EndHomePostcode" value="{if isset($datarow.EndHomePostcode)}{$datarow.EndHomePostcode|escape:'html'}{/if}" id="EndHomePostcode" >

                                    
                                      <img onclick="showPointMap('EndHomePostcode')" src="{$_subdomain}/images/diary/Google_Maps_Marker.png" style="float:right;margin-right:90px;">
                                  <p/>
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  <div {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="EngineerImportance" style="margin-top:5px;margin-bottom:5px">Field/Workshop Preference:</label>
                                
                                    <label style="margin-left:13px;">Field</label>
                                   
                                    <input type="radio" {if $datarow.EngineerImportance<25&&$datarow.EngineerImportance!="EngineerImportance"&&$datarow.EngineerImportance!=""} checked=checked{/if} onclick="$('#EngineerImportance').val(this.value)" name="cost" value="1" >
                                    <input type="radio" {if $datarow.EngineerImportance<50&&$datarow.EngineerImportance>=25} checked=checked{/if} onclick="$('#EngineerImportance').val(this.value)" name="cost" value="25" >
                                    <input type="radio" {if $datarow.EngineerImportance<75&&$datarow.EngineerImportance>=50||$datarow.EngineerImportance=="EngineerImportance"||$datarow.EngineerImportance==""} checked=checked{/if} onclick="$('#EngineerImportance').val(this.value)" name="cost" value="50" >
                                    <input type="radio" {if $datarow.EngineerImportance<100&&$datarow.EngineerImportance>=75} checked=checked{/if} onclick="$('#EngineerImportance').val(this.value)" name="cost" value="75" >
                                    <input type="radio" {if $datarow.EngineerImportance>=100} checked=checked{/if} onclick="$('#EngineerImportance').val(this.value)" name="cost" value="100" >
                                  
                                      <label>Workshop</label>
                                      <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngHomePostcodeHelp2" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                  <input  type="hidden" class="number"  name="EngineerImportance" value="{if $datarow.EngineerImportance=="EngineerImportance"}50{else}{$datarow.EngineerImportance|escape:'html'}{/if}" id="EngineerImportance" > 

<!--                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngineerImportanceHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >-->
                                    
                                  </div>
                                  
                                   <p {if $def!=1}style="display:none"{/if}>
                                        <label class="fieldLabel" for="Status" >Start Time Type:</label>

                                        &nbsp;&nbsp; 

                                        <input  type="radio" name="ViamenteStartType"  value="Fixed" {if $datarow.ViamenteStartType eq 'Fixed'} checked="checked" {/if}  /> <span class="text" >Fixed</span> 
                                        <input  type="radio" name="ViamenteStartType"  value="Flexible" {if $datarow.ViamenteStartType eq 'Flexible'} checked="checked" {/if}  /> <span class="text" >Flexible</span> 
                                        
                                        
                                         <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="startTimeTypeHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >

                                      </p>
                                  
                                   <p id="LunchBreakDurationElement" {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="LunchBreakDuration" >{$page['Labels']['lunch_break_rest_period']|escape:'html'}:</label>

                                    &nbsp;&nbsp;
                                    <select  name="LunchBreakDuration" id="LunchBreakDuration"  class="text auto-hint" >
                   
                                        <option value="" {if $datarow.LunchBreakDuration eq ''}selected="selected"{/if}   ></option>
                                        {foreach from=$lunchRestPeriod item=lrp}
                                         <option value="{$lrp.Value}" {if $datarow.LunchBreakDuration eq $lrp.Value}selected="selected"{/if} >{$lrp.Text|escape:'html'}</option>
                                         {/foreach}

                                     </select>
                                         
                                         (Mins. only)

                                  </p>
                                  
                                 <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="LunchPeriodFrom" >{$page['Labels']['lunch_period_from']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="LunchPeriodFrom" value="{$datarow.LunchPeriodFrom|escape:'html'}" id="LunchPeriodFrom" >
                                        
                                    
                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngLunchPeriodFromHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    
                                    
                                  </p>
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="LunchPeriodTo" >{$page['Labels']['lunch_period_to']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="LunchPeriodTo" value="{$datarow.LunchPeriodTo|escape:'html'}" id="LunchPeriodTo" >

                                  </p>
                                  
                                  
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                    <label class="fieldLabel" for="AvarageAppPerHour" >{$page['Labels']['AvarageAppPerHour']|escape:'html'}:</label>

                                    &nbsp;&nbsp; <input  type="text" class="text"  name="AvarageAppPerHour" value="{$datarow.AvarageAppPerHour|default:'10'}" id="AvarageAppPerHour" >
                                      
                                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AvarageAppPerHourHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                   
                                  </p>
                                  
                                  
                                  
                                  <p {if $def!=1||$ServiceProviderID!=64}style="display:none"{/if}>
                                    <label class="fieldLabel" for="AppsBeforeOptimise" >App. Before Optimisation:</label>

                                    &nbsp;&nbsp; <input   type="text" class="text"  name="AppsBeforeOptimise" value="{$datarow.AppsBeforeOptimise|escape:'html'}" id="AppsBeforeOptimise" >
                                         <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AppsBeforeOptimiseHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                  </p>
                                  
                                  
                                  <script>
                                  function checkFullKey(){
                                  if($('#PrimarySkill').val()=="Brown Goods"){
                                  {if $datarow.BrownEngLeft neq ""}
                                  if({$datarow.BrownEngLeft}<=0){
                                    alert("Licensed Brown Goods engineers exceeds the quota for the selected skill type, if you wish to increase the number please contact PCCS");
                                    $('#PrimarySkill').val("{$datarow.PrimarySkill}");
                                  }
                                  {/if}
                                  }
                                  else{ if($('#PrimarySkill').val()=="White Goods"){
                                 
                                  if({$datarow.WhiteEngLeft}<=0){
                                  alert("Licensed White Goods engineers exceeds the quota for the selected skill type, if you wish to increase the number please contact PCCS");
                                  $('#PrimarySkill').val("{$datarow.PrimarySkill}");
                                  
                                  }
                                  }
                                  }
                                  }
                                   </script>
                                  
                                   <p {if $def!=1||$ServiceProviderID!=64}style="display:none"{/if}>
                                    <label class="fieldLabel" for="PrimarySkill" >Primary Skill:</label>
                                    <select name="PrimarySkill" id="PrimarySkill" style="width:260px;margin-left:12px;" onchange="checkFullKey()" >
                                        <option></option>
                                        <option {if $datarow.PrimarySkill eq 'Brown Goods'} selected="selected" {/if} value="Brown Goods">Brown Goods</option>
                                        <option {if $datarow.PrimarySkill eq 'White Goods'} selected="selected" {/if} value="White Goods">White Goods</option>
                                    </select>
                                 
                                         <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AppsBeforeOptimiseHelp" class="helpTextIconQtip" style="float:right;margin-right:20px;" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                  </p>
                                  
                                  <p {if $def!=1}style="display:none"{/if}>
                                        <label class="fieldLabel" for="Status" >Exclude From Optimisation:</label>

                                        &nbsp;&nbsp; 

                                        <input  type="radio" name="ViamenteExclude"  value="Yes" {if $datarow.ViamenteExclude eq 'Yes'} checked="checked" {/if}  /> <span class="text" >Yes</span> 
                                        <input  type="radio" name="ViamenteExclude"  value="No" {if $datarow.ViamenteExclude eq 'No'} checked="checked" {/if}  /> <span class="text" >No</span> 


                                      </p>
                                      
                                  <p >
                                        <label class="fieldLabel" for="Status" >Hide From Table:</label>

                                        &nbsp;&nbsp; 

                                        <input  type="radio" name="ExcludeFromAppTable"  value="Yes" {if $datarow.ExcludeFromAppTable eq 'Yes'} checked="checked" {/if}  /> <span class="text" >Yes</span> 
                                        <input  type="radio" name="ExcludeFromAppTable"  value="No" {if $datarow.ExcludeFromAppTable eq 'No'} checked="checked" {/if}  /> <span class="text" >No</span> 


                                      </p>
                                  
                                  {if $datarow.ServiceProviderEngineerID !=''}
                                    
                                    <p S>
                                        <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>

                                        &nbsp;&nbsp; 

                                        <input  type="radio" name="Status"  value="Active" {if $datarow.Status eq 'Active'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['active']|escape:'html'}</span> 
                                        <input  type="radio" name="Status"  value="In-active" {if $datarow.Status eq 'In-active'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['inactive']|escape:'html'}</span> 


                                      </p>
                                     
                                  
                                  {else}
                                      <input  type="hidden" name="Status"  value="Active" />
                                  {/if}   
                                   
                                  
                                  {if $datarow.Status eq 'In-active'}
                                  
                                  <p style="text-align:right;" >

                                     <input type="submit" name="deleteEngineer" id="deleteEngineer"  class="btnCancel"   value="{$page['Buttons']['delete_engineer']|escape:'html'}" >
                                     
                                  </p>
                                  
                                  
                                      
                                  {/if}
                            </div>    
                        </div>
                       
                       <div id="thirdDiv" class="firstDiv assistDiv" >
                           
                        <input  type="hidden" name="Deleted" id="Deleted"  value="" />
                        
                        <input  type="hidden" name="ReplicateTypeFlag" id="ReplicateTypeFlag"  value="" />
                        <input  type="hidden" name="ReplicateType" id="ReplicateType"  value="{$datarow.ReplicateType|escape:'html'}" />
                        <input  type="hidden" name="ReplicateStatusFlag" id="ReplicateStatusFlag"  value="{$datarow.ReplicateStatusFlag|escape:'html'}" />
                        <input  type="hidden" name="ReplicatePostcodeAllocation" id="ReplicatePostcodeAllocation"  value="{$datarow.ReplicatePostcodeAllocation|escape:'html'}" />
                        
                        
                        
                        <input  type="hidden" name="FocusDate" id="FocusDate"  value="" />  
                        
                        <input  type="hidden" name="SentToViamente" id="SentToViamente"  value="{$datarow.SentToViamente|escape:'html'}" /> 
                        
                       
                        <input  type="hidden" name="start_date" id="start_date"  value="{$datarow.start_date|escape:'html'}" />   
                        
                        <input type="submit" name="addEngineer" id="addEngineer"  class="btnStandard"   value="{$page['Buttons']['add_engineer']|escape:'html'}" >
                        
                        {* <input type="submit" name="AllocatePostCodes" id="AllocatePostCodes"  class="btnStandard"   value="{$page['Buttons']['allocate_postcodes']|escape:'html'}" > *}
                        
                       </div>
                       
                       
                       <div id="fourthDiv" class="secondDiv assistDiv"  >  
                        
                       <span style="font-size:13px;float:right" id="ShowAllText" >{$page['Text']['display_inactive_engineers']|escape:'html'} <input type="checkbox" name="ShowAll" value="1"  {if $ShowAll eq 1} checked="checked"  {/if}  id="ShowAll" ></span>   
                           
                           
                           <input type="hidden" name="ServiceProviderEngineerID" id="ServiceProviderEngineerID"  value="{$datarow.ServiceProviderEngineerID|escape:'html'}" > 
                       
                           <input type="submit" name="saveRecord" id="saveRecord" style="display:none;"   class="btnStandard"   value="{$page['Buttons']['save_record']|escape:'html'}" > 
                           &nbsp;&nbsp;
                           <input type="submit" name="cancelChanges" id="cancelChanges" style="display:none;"  class="btnCancel"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                       
                           <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                           <span id="hidden_appointment_ids" ></span>
                           
                           
                           
                           
                           {if $DiaryWizardSetup}
                                      <p style="text-align:center;position:relative;" id="navigationButtons" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="btnStandard"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="btnStandard"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                            {/if}
                           
                           
                           
                       </div>
                       
                   </fieldset> 

        
        </form>
    </div>  
                                    
</div>
                           
<div id="waitDiv" style="display:none">
    <img src="{$_subdomain}/images/processing.gif"><br>
</div>
    
    
{if $datarow.Status eq 'In-active'}
    
	<div style="display:none;" >
                
            <div id="deleteEngineerConfirmation" class="SystemAdminFormPanel" >
                <form name="deleteForm" action="#" id="deleteForm" method="post" class="inline" >
                    <fieldset> 
                        <div id="errorCenterInfoText2" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                        <p>&nbsp;</p>
                        <p style="text-align:center" >
                        {$page['Text']['engineer_delete_confirm']|escape:'html'}: <strong>{$datarow.EngineerFirstName|escape:'html'} {$datarow.EngineerLastName|escape:'html'}?</strong>

                        </p>
                        <p>&nbsp;</p>
                        <p style="text-align:center" >
                          <span class= "bottomButtons" >       
                            <input type="submit" name="saveRecord2" id="saveRecord2"  class="btnCancel"   value="{$page['Buttons']['delete_engineer']|escape:'html'}" > 
                               &nbsp;&nbsp;
                            <input type="submit" name="cancelChanges2" id="cancelChanges2"  class="textSubmitButton"   value="{$page['Buttons']['cancel_deletion']|escape:'html'}" > 
                          </span>
                          <span id="processDisplayText2" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                        </p>
                        <p>&nbsp;</p>
                     </fieldset>          
                 </form> 
            </div>
                      
       </div>

{/if}  



<div style="display:none;" >
                
    <div id="appointmentsExist" class="SystemAdminFormPanel" >
        <form name="appointmentsExistForm" action="#" id="appointmentsExistForm" method="post" class="inline" >
            <fieldset> 
                <div id="errorCenterInfoText3" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                <p>&nbsp;</p>
                <p style="text-align:left" >
                
                    <strong>{$datarow.EngineerFirstName|escape:'html'} {$datarow.EngineerLastName|escape:'html'}</strong> {$page['Text']['appointment_exists_info']|escape:'html'}: 

                </p>
                <p id="appointmentsList" ></p>
                <p>&nbsp;</p>
                <p style="text-align:center" >
                  <span class= "bottomButtons" >       
                    <input type="submit" name="modifyAppointment" id="modifyAppointment"  class="textSubmitButton"   value="{$page['Buttons']['confirm_changes']|escape:'html'}" > 
                    <input type="submit" name="cancel_changes" id="cancel_changes"  class="textSubmitButton"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" >
                  </span>
                  <span id="processDisplayText3" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                </p>
                <p>&nbsp;</p>
             </fieldset>          
         </form> 
    </div>
                
                
                
    <div id="newEngineersList" class="SystemAdminFormPanel" >
        <form name="newEngineersListForm" action="#" id="newEngineersListForm" method="post" class="inline" >
            <fieldset> 
                <div id="errorCenterInfoText4" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                <p>&nbsp;</p>
                
                <p style="text-align:left" id="newEngineers" >
                
                    
                </p>
                
                <p>&nbsp;</p>
                <p style="text-align:center" >
                  <span class= "bottomButtons" >       
                    <input type="submit" name="ConfirmNewEngineer" id="ConfirmNewEngineer"  class="textSubmitButton"   value="{$page['Buttons']['confirm_new_engineer']|escape:'html'}" > 
                  </span>
                  <span id="processDisplayText4" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                </p>
                <p>&nbsp;</p>
             </fieldset>          
         </form> 
    </div>  
                
                
                
    {* Replicate week form starts here.. *} 
    
    <div id="ReplicateWeekDataDiv" class="SystemAdminFormPanel" >
        <form name="ReplicateWeekForm" action="#" id="ReplicateWeekForm" method="post" class="inline" >
            <fieldset> 
                <legend>{$page['Text']['replicate_engineer']|escape:'html'}</legend>
                <div id="errorCenterInfoText5" style="display:none;padding-top:20px;" class="centerInfoText" ></div>
                <p>&nbsp;</p>
                <p>
                {$page['Text']['replicate_page_text']|escape:'html'}
                </p>
                <p>
                    <br>
                    {$page['Text']['replicate_page_text2']|escape:'html'}:<br><br>
                    <input type="radio" name="MWReplicateType" value="FourWeeksOnly" {if $datarow.ReplicateType eq 'FourWeeksOnly'} checked="checked" {/if} > {$page['Text']['current_4_weeks_only']|escape:'html'} &nbsp;&nbsp;<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Current4WeeksOnlyHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ><br>
                    <input type="radio" name="MWReplicateType" value="WeeklyCycle" {if $datarow.ReplicateType eq 'WeeklyCycle'} checked="checked" {/if} > {$page['Text']['replicate_to_future_weeks_weekly']|escape:'html'} &nbsp;&nbsp;<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ReplicateWeeklyCycleHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ><br>
                    <input type="radio" name="MWReplicateType" value="MonthlyCycle" {if $datarow.ReplicateType eq 'MonthlyCycle'} checked="checked" {/if} > {$page['Text']['replicate_to_future_weeks_monthly']|escape:'html'} &nbsp;&nbsp;<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ReplicateMonthlyCycleHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ><br>
                
                </p>
                
                <p>
                <br>
                <input type="checkbox" name="MWReplicateStatusFlag" value="Yes" {if $datarow.ReplicateStatusFlag eq 'Yes'} checked="checked" {/if} > {$page['Text']['engineers_work']|escape:'html'} &nbsp;&nbsp;<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ReplicateStatusFlagHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ><br>
                <input type="checkbox" name="MWReplicatePostcodeAllocation" value="Yes" {if $datarow.ReplicatePostcodeAllocation eq 'Yes'} checked="checked" {/if} > {$page['Text']['replicate_postcode_allocation']|escape:'html'} &nbsp;&nbsp;<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ReplicatePostcodeAllocationHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" ><br>
                
                </p>
                
                
                
                <p>&nbsp;</p>
                <p>
                  <span class= "bottomButtons" style="text-align:right" >       
                    <input type="submit" name="saveRecord5" id="saveRecord5"  class="textSubmitButton"   value="{$page['Buttons']['submit']|escape:'html'}" > 
                       &nbsp;&nbsp;
                    <input type="submit" name="cancelChanges5" id="cancelChanges5"  class="textSubmitButton"   value="{$page['Buttons']['cancel']|escape:'html'}" > 
                  </span>
                  <span id="processDisplayText5" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    
                </p>
                <p>&nbsp;</p>
             </fieldset>          
         </form> 
    </div>   
                
   {* Replicate week form ends here.. *}             
                

</div>
    
    
{/block}
