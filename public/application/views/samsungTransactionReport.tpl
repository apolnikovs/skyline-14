<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>


<script>

//adding custom method to validate a date field
$.validator.addMethod('regex', function(value, element, param) {
    return this.optional(element) || value.match(typeof param == 'string' ? new RegExp(param) : param);
    },
    'Please enter a value in the correct format.'
);
{literal}
    var pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
{/literal}


var hash;
var status = 0;
var requestActive = false;
var fileName;

//recursive progress monitoring function which also outputs the file when generating is done
function progress() {
    if(status == 1) {
	if(!requestActive) {
	    requestActive = true;
	    $.post("{$_subdomain}/Report/reportProgress", { hash: hash }, function(response) {
		update(response);
		requestActive = false;
	    });
	}
	setTimeout(function() {	progress(); }, 1000);
    }
}


function update(json) {

    var data = $.parseJSON(json);
    //if finished, download the file, if not - continue displaying progress
    //console.log(data);
    
    if(data.status == "finished") {
    
	//call the method to delete generated file
	//$.post("{$_subdomain}/Report/downloadReportFile/", { hash: hash, filename: fileName }, function(response) {
	    //nothing to do
	//});
	
	//stop the recursive loop
	$.colorbox.close();
	status = 0;
	
	location.href = "{$_subdomain}/Report/downloadReportFile/?hash=" + hash + "&filename=" + fileName;
	
	/*
	setTimeout(function() { 
	    location.href = "{$_subdomain}/reports/" + fileName + ".xlsx";
	}, 2000)
	*/
	
	//location.href = "{$_subdomain}/reports/" + fileName + ".xlsx";
	
    } else {
    
	//do the visual progress here
	var val = Math.round((data.current / data.total) * 100);
	if(isNaN(val)) {
	    val = 0;
	}
	$("#progressbar").progressbar({ value: val });
	$("#percentage").text(val + "%");
	
    }
    
}


$(document).ready(function() {

    $("input[name=dateFrom], input[name=dateTo]").datepicker({ dateFormat: 'dd/mm/yy' });

    $(document).on("click", "#generate", function() {
	
	//console.log($("#branch").multiselect("getChecked").map(function() { return this.value; }).get());
	//return false;
	
	var date = new Date();
	fileName =  "Transaction Report " + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay() + " " + 
		    date.getHours() + "." + date.getMinutes() + "." + date.getSeconds() + "." + date.getMilliseconds();
	
	$("#transactionReportForm").validate = null;

	$("#transactionReportForm").validate({
	    rules: {
		dateFrom:   { regex: pattern },
		dateTo:	    { regex: pattern },
	    },
	    messages: {
		dateFrom:   { regex: "Must contain a valid date." },
		dateTo:	    { regex: "Must contain a valid date." },
	    },
	    errorPlacement: function(error, element) {
		error.insertAfter(element);
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "p",
	    submitHandler: function() {
		
		$("#client").next(".ui-combobox").remove();
		$("#branch").next(".ui-multiselect").remove();
		$("#network").next(".ui-combobox").remove();
		//$("#branch, #client, #network").remove();
		$("label[for=branch], label[for=client], label[for=network]").remove();
		
		status = 1;
		hash = randomString();
		
		$.post("{$_subdomain}/Report/samsungTransactionReport/", 
			$("#transactionReportForm").serialize() + "&hash=" + hash + "&filename=" + fileName + "&branches=" + JSON.stringify($("#branch").multiselect("getChecked").map(function() { return this.value; }).get()),
			function(response) {
			    //nothing to do, generating script is initiated, so progress tracking script takes over
			}
		);
		
		$("label[for=dateFrom], input[name=dateFrom], label[for=dateTo], input[name=dateTo], #btnWrapper").hide();
		var html = "\
			    <div id='progressBarWrapper' class='rowWrapper'>\
				<div id='percentage'>0%</div>\
				<div id='progressbar'></div>\
			    </div>\
			   ";
		$(html).appendTo($("#transactionReportForm"));
		$("#progressbar").progressbar({ value: 0 });
		$.colorbox.resize();
    
		progress();
		//var url = "{$_subdomain}/Report/samsungTransactionReport/?" + $("#transactionReportForm").serialize() + "&" + hash;
		//location.href = url;
		return false;
	    }
	});

	$("#transactionReportForm").submit();
	
	$("#generate").die();
	$("#generate").off();
	
	return false;
	
    });
    
    
    $(document).on("click", "#cancel", function() {
	$.colorbox.close();
	return false;
    });


});


</script>


<div id="samsungTransactionReportContainer">
    
    <fieldset>
	
	<legend align="center">Service Transaction Report</legend>
	
	<form id="transactionReportForm" action="" method="post">
	    
	    <p style="margin:10px 0px;">This report will list all Service Transactions associated to the current User</p>
	    
	    {if $userLevel == "Admin"}
		<label for="network" style="width: 90px; margin-right:0;">Network: </label>
		<select id="network" name="network">
		    <option value="">Select Network</option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}">{$network.CompanyName}</option>
		    {/foreach}
		</select>
	    {/if}
	    
	    {if $userLevel == "Network"}
		<label for="client" style="width: 90px; margin-right:0;">Client: </label>
		<select id="client" name="client">
		    <option value="">Select Client</option>
		    {foreach $clients as $client}
			<option value="{$client.ClientID}">{$client.ClientName}</option>
		    {/foreach}
		</select>
	    {/if}

	    {if $userLevel == "Client"}
		<label for="branch" style="width: 90px; margin-right:0;">Branch: </label>
		<select id="branch" name="branch">
		    {foreach $branches as $branch}
			<option value="{$branch.BranchID}">{$branch.BranchName}</option>
		    {/foreach}
		</select>
	    {/if}

	    <br/>
	    
	    <label for="dateFrom" style="margin-right:0; width: 90px;">Booked From:</label>
	    <input type="text" style="width:80px;" name="dateFrom" />

	    <label for="dateTo" style="margin-right:5px; margin-left:5px; width: 67px;">Booked To:</label>
	    <input type="text" style="width:80px;" name="dateTo" />
	    
	</form>
	
	<div id="btnWrapper" style="text-align:center; position:relative; display:block; float:left; width:100%; clear:both; margin:10px 0px 10px 0px;">
	    <a href="#" id="generate" class="btnConfirm" style="margin-right:10px; display:inline-block; width:60px;">Generate</a>
	    <a href="#" id="cancel" class="btnCancel" style="display:inline-block; float:none; width:60px;">Cancel</a>
	</div>
	
    </fieldset>
    
</div>
