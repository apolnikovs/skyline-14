 <script type="text/javascript">   
     
 $(document).ready(function() {
 
    $('#ServiceProviderColourID').combobox();
   
    $('#DefaultServiceProviderSupplierID').combobox();
      $(function() {
    $( "#tabs" ).tabs();
  });
  $('#Model').combobox();
              
   }); 
   
   
  </script>  
    <style>
    .ui-combobox input{
        width:300px!important;
        }
</style>  
    <div id="PartOrderStatusFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartOrderStatusForm" name="PartOrderStatusForm" method="post"  action="{$_subdomain}/Stock/savePartStockTemplate" class="inline" >
                    
                    <input type="hidden" name="SpPartStockTemplateID" value="{if isset($datarow.SpPartStockTemplateID)}{$datarow.SpPartStockTemplateID}{/if}"> 
                    <input type="hidden" name="ServiceProviderID" value="{if isset($ServiceProviderID)}{$ServiceProviderID}{/if}"> 
                    
                <fieldset>
                    <legend title="" >Part Template</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
                       <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Stock Template Details</a></li>
    {if $datarow.SpPartStockTemplateID|default:''!=""}
    <li><a   onclick="loadAccessoryList('Model')" href="#tabs-2">Attached Models</a></li>
    
    {/if}
   </ul>
                                  
                                   <div id="tabs-1" class="SystemAdminFormPanel inline">
                       
                         
                         
                         
                       <p>
                           <label  class="cardLabel" for="PartNumber" >Part No:</label>
                           
                             &nbsp;&nbsp; <input class="text"   type="text"   name="PartNumber" value="{$datarow.PartNumber}" id="PartNumber" >
                        </p>
                         <p>
                           <label  class="cardLabel" for="Description" >Description:</label>
                           
                             &nbsp;&nbsp; <input class="text"   type="text"   name="Description" value="{$datarow.Description}" id="Description" >
                        </p>
                        
                        <p>
                            <label class="cardLabel" for="ServiceProviderColourID" >Colour:</label>
                            &nbsp;&nbsp; 
                                <select name="ServiceProviderColourID" id="ServiceProviderColourID" class="text" >
                                <option value="" {if isset($datarow)&& $datarow.ServiceProviderColourID eq ''}selected="selected"{/if}>Select Colour</option>

                                
                                {foreach $colours as $c}

                                    <option value="{$c.ServiceProviderColourID}" {if isset($datarow)&& $datarow.ServiceProviderColourID eq $c.ServiceProviderColourID}selected="selected"{/if}>{$c.ColourName|escape:'html'}</option>
                                    
                                {/foreach}
                                
                            </select>
                                
                        </p>
                        <p>
                            <label class="cardLabel" for="DefaultServiceProviderSupplierID" >Default Supplier:</label>
                            &nbsp;&nbsp; 
                                <select name="DefaultServiceProviderSupplierID" id="DefaultServiceProviderSupplierID" class="text" >
                                <option value="" {if isset($datarow.DefaultServiceProviderSupplierID)&& $datarow.DefaultServiceProviderSupplierID eq ''}selected="selected"{/if}>Select Default Supplier</option>

                                
                                {foreach $suppliers as $c}

                                    <option value="{$c.ServiceProviderSupplierID}" {if isset($datarow)&& $datarow.DefaultServiceProviderSupplierID eq $c.ServiceProviderSupplierID}selected="selected"{/if}>{$c.CompanyName|escape:'html'}</option>
                                    
                                {/foreach}
                                
                            </select>
                                
                        </p> 
                        <input type="hidden" name="PrimaryServiceProviderModelID" value=0>
                       <p>
                           <label  class="cardLabel" for="PurchaseCost" >Purchase Cost:</label>
                           
                             &nbsp;&nbsp; <input class="text"   type="text"   name="PurchaseCost" value="{$datarow.PurchaseCost|string_format:"%.2f"}" id="PurchaseCost" >
                        </p>
                        
                      {if isset($datarow.InStock)}
                        <p>
                           <label  class="cardLabel"  >In Stock:</label>
                           
                             &nbsp;&nbsp; <label    style="float:left;padding-top:5px;">{$datarow.InStock}</label>
                        </p>
                        <p>
                           <label  class="cardLabel"  >Available:</label>
                           
                           &nbsp;&nbsp;  <label    style="float:left;padding-top:5px;">{$datarow.InStockAvailable}</label>
                        </p>
                        {/if}
                        
                       <p>
                           <label  class="cardLabel" for="MinStock" >Min. Stock:</label>
                           
                             &nbsp;&nbsp; <input class="text"   type="text"   name="MinStock" value="{$datarow.MinStock}" id="MinStock" >
                        </p>
                       <p>
                       <label  class="cardLabel" for="MakeUpTo" >Make Up To:</label>
                           
                             &nbsp;&nbsp; <input class="text"   type="text"   name="MakeUpTo" value="{$datarow.MakeUpTo}" id="MakeUpTo" >
                        </p>
                        
                         
                       <p>
                           <label style="width:70%" class="cardLabel" for="Status" >In-active:</label>
                           
                             &nbsp;&nbsp; <input  style="float: right;margin-right: 135px;" type="checkbox"   name="Status" {if $datarow.Status=="In-active"}checked=checked{/if} id="Status" >
                        </p>
                         </p>
                         
                         
                         
                       
                     

                         <p>
                <hr>
                               
                                <div style="height:20px;margin-bottom: 10px">
                                <button type="submit" style="float: left" class="gplus-blue">Save</button>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Cancel</button>
                                </div>
            </p>    

                           
                          
                         
                  </div>       
 <div id="tabs-2" class="SystemAdminFormPanel inline">
      {if $datarow.SpPartStockTemplateID|default:''!=""}
         
                         <p>
                                 <label class="cardLabel"  for="Models" style="width:100px" >Models:</label>
                            &nbsp;&nbsp; 
                                <select name="Model" id="Model" class="text" style="margin-bottom:25px" >
                                <option value="" >Select Model</option>

                                
                                {foreach $models as $s}

                                    <option value="{$s.ServiceProviderModelID}" >{$s.ModelNumber|escape:'html'}</option>

                                {/foreach}
                                
                                </select>&nbsp;<img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="additem('Model')">
                                
                        </p>
                            <p id="ModelP"></p>
                             <div style="text-align:center" id="ajaxLoader" class="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
     
                             
                            
     <script>

                        function additem(item){
                        item=item;
                       $('#'+item+'P').html('');
                       $('.ajaxLoader').show();
                         $.post("{$_subdomain}/Stock/add"+item+"/",{ SpPartStockTemplateID:{$datarow.SpPartStockTemplateID} ,ACCID:$('#'+item).val()},
function(data) {

removeAccessoryOption($('#'+item).val(),item);
loadAccessoryList(item);
});
                        }
function removeAccessoryOption(val,item){
$("#"+item+" option[value="+val+"]").remove();
}

function loadAccessoryList(item){
$('#ajaxLoader').show();
$('#'+item+'P').html('');
 $.post("{$_subdomain}/Stock/load"+item+"/",{ SpPartStockTemplateID:{$datarow.SpPartStockTemplateID}},
function(data) {

en=jQuery.parseJSON(data);
$.each(en, function(key, value) { 
removeAccessoryOption(en[key]['id'],item)
$('#'+item+'P').append("<label class='cardLabel'></label><label title='Click to Remove' class='saFormSpan' style='cursor:pointer' onclick=removeAccessory("+en[key]['id']+",'"+item+"')>"+en[key]['name']+"</label><br>");
});
$('.ajaxLoader').hide();
});
}
function removeAccessory(id,item){

if (confirm('Are you sure you want to remove this?')) {
    $('.ajaxLoader').show();
   $.post("{$_subdomain}/Stock/del"+item+"/",{ SpPartStockTemplateID:{$datarow.SpPartStockTemplateID} ,ACCID:id},
function() {
loadAccessoryList(item);
});
} else {
    // Do nothing!
}
}
                    
                        </script>
   
                        
                        
{/if}
                             
                             
 </div>      
                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
