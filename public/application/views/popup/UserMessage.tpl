<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head>


{block name=config}
{$Title = ""}
{$PageId = 88}



{/block}

{block name=scripts}

        <link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" /> 
      
        
       <script type="text/javascript" src="{$_subdomain}/js/jquery-1.7.1.js"></script>

        <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
  
      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
    
    $(document).ready(function() {
     $.colorbox({ html:$('#h12s').html(), title:"{$msgTitle}",escKey: false,
    overlayClose: false,
                        
                        onClosed:function(){
                       if(modedd==1){
                        window.location="{$_subdomain}/Diary/default/table=1";
                        }
                       }
});
   
});

</script>
 {/block}
</head>
        
<body>
{if $type="error"}{$color="color:#FF9900"}{/if}
{if $type="confirm"}{$color="color:#8FD380"}{/if}
     <div style="text-align:center;color:#6b6b6b;height:200px;display:none;" id="h12s" >
    <div>
        <h1 style="{$color}">{$msgTitle}</h1>
        <div style="color:#6b6b6b">{$msgText}</div>
    </div>
    </div>

</body>