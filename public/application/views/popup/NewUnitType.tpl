<script type="text/javascript" >
 $(document).ready(function() {  
 
 
 
                    $(document).on('click', '.helpClose', 
                    
                        function() {
                        
                            $.colorbox.close();
                            return false;
                        
                        });
                       
                       
                    

                        $(document).on('click', '#unittype_send_btn', 



                            function() {


                                if($("label.fieldError:visible").length>0)
                                {

                                }
                                else
                                {
                                    $("#processDisplayHelpText").show();
                                    $("#unittype_send_btn").hide();
                                    $("#unittype_cancel_btn").hide();
                                }


                                $('#UnitTypeEntryForm').validate({

                                            ignore: '',

                                            rules:  {
                                                        UnitTypeText:
                                                        {
                                                                required: true

                                                        }

                                            },
                                            messages: {
                                                        UnitTypeText:
                                                        {
                                                                required: "{$page['Errors']['unit_type']|escape:'html'}"
                                                        }
                                            },

                                            errorPlacement: function(error, element) {

                                                    error.insertAfter( element );

                                                    $("#processDisplayHelpText").hide();

                                                    $("#unittype_send_btn").show();
                                                    $("#unittype_cancel_btn").show();

                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',
                                            submitHandler: function() {

                                                    $("#processDisplayHelpText").show();
                                                    $("#unittype_send_btn").hide();
                                                    $("#unittype_cancel_btn").hide();



                                                    $("#RequestNewUnitTypeText").val($("#UnitTypeText").val());
                                                    
                                                    $("#jbUnitTypeID").val("-1").removeAttr('disabled');//Assigning UNKNOWN unit type.
                                                    $("#jbUnitTypeID").blur();
                                                    setValue("#jbUnitTypeID");

                                                    $.colorbox.close();
                                                       

                                                      

                                                }

                                            });




                            });
                
                   
            });
     

</script>


    
    <div id="UnitTypeEntryDiv"  class="SystemAdminFormPanel" >
    
        
    <form action="{$_subdomain}/Job/unitTypeEntry/" id="UnitTypeEntryForm" name="UnitTypeEntryForm" method="post" >
        <fieldset>
            <legend title="">{$form_legend|escape:'html'}</legend>

                <p><label id="suggestText" ></label></p>
                            
                <p>
                
                {$page['Text']['description']|escape:'html'}
                
                </p>
                 
                <p>
                   <label class="fieldLabel" for="UnitTypeText" >{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>

                    &nbsp;&nbsp; <input  type="text" class="text"  name="UnitTypeText" value="" id="UnitTypeText" >

                </p>


              
                <p>

                    <span class= "bottomButtons" >
                       
                            <input type="submit" name="unittype_send_btn" class="textSubmitButton" id="unittype_send_btn"  value="{$page['Buttons']['ok']|escape:'html'}" >

                            &nbsp;&nbsp;
                            <input type="submit" name="unittype_cancel_btn" class="textSubmitButton helpClose" id="unittype_cancel_btn"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                            <span id="processDisplayHelpText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span> 
                     </span>
                                           
                </p>


        </fieldset>
    </form>
                     
     
     
    </div>  
    
                      
