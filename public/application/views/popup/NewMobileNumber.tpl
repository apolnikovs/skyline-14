<script type="text/javascript" >
 $(document).ready(function() {  
                    
                    $('.fieldLabel').css('width', '230px');
                    $('.fieldLabel').css('text-align', 'left');
                    $('.fieldLabel').css('padding', '0px');
                    
 
 
                    $(document).on('click', '.helpClose', 
                    
                        function() {
                        
                            $("#SMSPopupPage").val('0');
                            //$.colorbox.close();
                            
                            $("#confirm_booking_btn").trigger("click");
                            return false;
                        
                        });
                        
                        
                        $(document).on('keypress', '#jbContactMobile1', 
                    
                        function() {
                        
                            $("#SendServiceCompletionTextRadio2").attr("checked", "checked");
                        
                        });
                        
                        $(document).on('keypress', '#jbContactAltMobile1', 
                    
                        function() {
                        
                            $("#SendServiceCompletionTextRadio3").attr("checked", "checked");
                        
                        });
                        
                        
                        
                       
                       
                    

                        $(document).on('click', '#mobile_number_send_btn', 



                            function() {


                                if($("label.fieldError:visible").length>0)
                                {

                                }
                                else
                                {
                                    $("#processDisplayHelpText").show();
                                    $("#mobile_number_send_btn").hide();
                                    $("#mobile_number_cancel_btn").hide();
                                }


                                $('#MobileNumberEntryForm').validate({

                                            ignore: '',

                                            rules:  {
                                                        SendServiceCompletionTextRadio:
                                                        {
                                                                required: true

                                                        },
                                                        ContactMobile1:
                                                        {
                                                                required: function (element) {
                                                                if($('#SendServiceCompletionTextRadio2').is(':checked'))
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }

                                                        },
                                                        ContactAltMobile1:
                                                        {
                                                                required: function (element) {
                                                                if($('#SendServiceCompletionTextRadio3').is(':checked'))
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }

                                                        }        

                                            },
                                            messages: {
                                                        SendServiceCompletionTextRadio:
                                                        {
                                                                required: "{$page['Errors']['select_option']|escape:'html'}"
                                                        },
                                                        ContactMobile1:
                                                        {
                                                                required: "{$page['Errors']['mobile']|escape:'html'}"
                                                        },
                                                        ContactAltMobile1:
                                                        {
                                                                required: "{$page['Errors']['alt_mobile']|escape:'html'}"
                                                        }        
                                            },

                                            errorPlacement: function(error, element) {

                                                    error.insertAfter( element );

                                                    $("#processDisplayHelpText").hide();

                                                    $("#mobile_number_send_btn").show();
                                                    $("#mobile_number_cancel_btn").show();

                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',
                                            submitHandler: function() {

                                                    $("#processDisplayHelpText").show();
                                                    $("#mobile_number_send_btn").hide();
                                                    $("#mobile_number_cancel_btn").hide();



                                                    $("#SendServiceCompletionText").val($('input[name=SendServiceCompletionTextRadio]:radio:checked').val());
                                                    
                                                    
                                                    if($('input[name=SendServiceCompletionTextRadio]:radio:checked').val()=="MobileNo")
                                                        {
                                                                $("#jbContactMobile").val($('#jbContactMobile1').val());
                                                                $("#jbContactMobile").removeClass('auto-hint');
                                                        }
                                                        
                                                       if($('input[name=SendServiceCompletionTextRadio]:radio:checked').val()=="AlternativeMobileNo")
                                                        {
                                                                $("#ContactAltMobile").val($('#jbContactAltMobile1').val());
                                                                
                                                                $("#jbContactAltMobileElement").show();
                                                                $("#ContactAltMobile").removeClass('auto-hint');
                                                        }
                                                        else
                                                        {
                                                                $("#jbContactAltMobileElement").hide();
                                                        }
                                                    
                                                    $("#SMSPopupPage").val('0');
                                                    
                                                    
                                                   
                                                   // $.colorbox.close();
                                                    
                                                    $("#confirm_booking_btn").trigger("click");
                                                  

                                                }

                                            });




                            });
                
                   
            });
     

</script>


    
    <div id="MobileNumberEntryDiv"  class="SystemAdminFormPanel" >
    
        
    <form action="{$_subdomain}/Job/mobileNumberEntry/" id="MobileNumberEntryForm" name="MobileNumberEntryForm" method="post" >
        <fieldset>
            <legend title="">{$form_legend|escape:'html'}</legend>

                <p><label id="suggestText" ></label></p>
                            
                <p>
                
                {$page['Text']['description']|escape:'html'}
                
                </p>
                 
                <p >
                   
                    <label class="fieldLabel"  ><input style="margin:0px;" type="radio" name="SendServiceCompletionTextRadio" {if $ssct eq 'No'} checked="checked" {/if} value="No" id="SendServiceCompletionTextRadio1" > {$page['Text']['dont_text']|escape:'html'}</label> <br>
                </p>
                
                <p>
                    
                    <label class="fieldLabel" for="jbContactMobile1" ><input style="margin:0px;"  type="radio" name="SendServiceCompletionTextRadio" {if $ssct eq 'MobileNo'} checked="checked" {/if} value="MobileNo" id="SendServiceCompletionTextRadio2" > {$page['Text']['use_mobile_no']|escape:'html'}: </label> 
                    
                    {if $mobileNumber}
                       
                       <span style="padding:0px;height:28px;" >{$mobileNumber|escape:'html'}</span>
                       
                       <input  type="hidden" class="text" id="jbContactMobile1" name="ContactMobile1" value="{$mobileNumber|escape:'html'}"  >
                        
                    {else}
                        
                        
                        <input  type="text" class="text" id="jbContactMobile1" name="ContactMobile1" value=""  >
                        
                    {/if}    
                    
                 
                </p>
                
                <p>
                    
                    <label class="fieldLabel" for="jbContactAltMobile1" ><input style="margin:0px;"  type="radio" name="SendServiceCompletionTextRadio"  {if $ssct eq 'AlternativeMobileNo'} checked="checked" {/if} value="AlternativeMobileNo" id="SendServiceCompletionTextRadio3" > {$page['Text']['use_alternative_mobile_no']|escape:'html'}:</label>
                    
                    <input  type="text" class="text" id="jbContactAltMobile1" name="ContactAltMobile1" value="{$ContactAltMobile1|escape:'html'}"  >
                    
                    
                </p>


              
                <p>
                    <br>
                    <span class= "bottomButtons" >
                       
                            <input type="submit" name="mobile_number_send_btn" class="textSubmitButton" id="mobile_number_send_btn"  value="{$page['Buttons']['ok']|escape:'html'}" >

                            &nbsp;&nbsp;
                            <input type="submit" name="mobile_number_cancel_btn" class="textSubmitButton helpClose" id="mobile_number_cancel_btn"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                            <span id="processDisplayHelpText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span> 
                     </span>
                                           
                </p>


        </fieldset>
    </form>
                     
     
     
    </div>  
    
                      
