 <script type="text/javascript">
 $(document).ready(function() {
    $("#ServiceProviderID").combobox();
    $("#ManufacturerID").combobox();
    $("#JobWeighting").combobox();
    $("#RepairIndex").combobox();
    $(function() {
        $( "#tabs" ).tabs({
            show: function(event, ui) {
                $.colorbox.resize();
            }
        });
    });
    $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
                hide: function(){
                    $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
                solo: true // Only show one tooltip at a time
            },

            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });
});
</script>
{if $deleteFlag eq 'Yes'}
<div id="deleteDiv">
    <form id="CForm" name="CForm" method="post"  action="" class="inline">
        <input type="hidden" name="RepairTypeID" value="{$datarow.RepairTypeID|default:""}">
        <fieldset>
        <legend>Delete Record</legend>
        <p id="deleteP">Are you sure you want to delete this <strong>'{$datarow.RepairType}'</strong> Repair Type?</p>
            <div style="height:20px; margin-bottom: 10px;text-align: center;">
                <input type="submit" name="delete_save_btn" class="textSubmitButton centerBtn" id="delete_save_btn" value="{$page['Buttons']['yes_btn']|escape:'html'}" />
                <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" style="float:right; margin-left: 20px;" value="{$page['Buttons']['no_btn']|escape:'html'}" />
            </div>
        </fieldset>
    </form>
</div>
{else}
    <div id="RepairTypeFormPanel" class="SystemAdminFormPanel" >
        <form id="CForm" name="CForm" method="post"  action="" class="inline">
            <input type="hidden" name="RepairTypeID" value="{$datarow.RepairTypeID|default:""}">
            <fieldset>
                <legend title="" >Repair Types</legend>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">{$page['Labels']['tepair_type_details_tab_label']|escape:'html'}</a></li>
                        <li><a href="#tabs-2">{$page['Labels']['category_tab_label']|escape:'html'}</a></li>
                    </ul>
                    <p>
                        <label ></label>
                        <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                    </p>
                    <div id="tabs-1" class="SystemAdminFormPanel inline">
                        <p>
                            <label class="cardLabel" for="RepairType">{$page['Text']['repair_type_text']|escape:'html'}<sup>*</sup></label>
                            &nbsp;
                            <input  type="text" class="text" name="RepairType" value="{$datarow.RepairType|default:''}" id="RepairType" style="width: 280px;">
                        </p>
                        <p>
                            <label class="cardLabel" for="Status">{$page['Text']['status_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="Status" value="In-active" {if $datarow.Status eq 'In-active'}checked="checked"{/if} />&nbsp;<label>Inactive</label></span>
                        </p>
                        {if $SupderAdmin eq true}
                        <p>
                            <label class="cardLabel" for="ServiceProviderID">{$page['Text']['serviceProvider_text']|escape:'html'}<sup>*</sup></label>
                            &nbsp;
                            <select id="ServiceProviderID" name="ServiceProviderID">
                            <option value="">Select Service Provider</option>
                                {foreach $splist as $s}
                                    <option value="{$s.ServiceProviderID}" {if $datarow.ServiceProviderID eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym|default:''}</option>
                                {/foreach}
                            </select>
                        </p>
                        {else}
                            <input type="hidden" name="ServiceProviderID" value="{$sId}" />
                        {/if}
                        <p>
                            <label class="cardLabel" for="ManufacturerID">{$page['Text']['manufacturer_id_text']|escape:'html'}<sup>*</sup></label>
                            &nbsp;
                            <select id="ManufacturerID" name="ManufacturerID">
                            <option value="">Select Manufacturer</option>
                                {foreach $manufacturerList as $s}
                                    <option value="{$s.ManufacturerID}" {if $datarow.ManufacturerID eq $s.ManufacturerID}selected="selected"{/if}>{$s.ManufacturerName|default:''}</option>
                                {/foreach}
                            </select>
                        </p>
                        <p>
                            <label class="cardLabel" for="JobWeighting">{$page['Text']['job_weighting_text']|escape:'html'}</label>
                            &nbsp;
                            <select id="JobWeighting" name="JobWeighting">
                            <option value="">Select</option>
                                {for $var=0 to 99}
                                    <option value="{$var}" {if $datarow.JobWeighting eq $var}selected="selected"{/if}>{$var|default:''}</option>
                                {/for}
                            </select>
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Job_Weighting_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" >
                        </p>
                        <p>
                            <label class="cardLabel" for="RepairIndex" >{$page['Text']['repair_index_text']|escape:'html'}</label>
                            &nbsp;
                            <select id="RepairIndex" name="RepairIndex">
                            <option value="">Select</option>
                                {for $var=1 to 10}
                                    <option value="{$var}" {if $datarow.RepairIndex eq $var}selected="selected"{/if}>{$var|default:''}</option>
                                {/for}
                            </select>
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Repair_Index_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" >
                        </p>
                        <p>
                            <label class="cardLabel" for="Chargeable">{$page['Text']['chargeable_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="Chargeable" value="Yes" {if $datarow.Chargeable eq 'Yes'}checked="checked"{/if} />&nbsp;<label>Yes</label>
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Chargeable_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="Warranty">{$page['Text']['warranty_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="Warranty" value="Yes" {if $datarow.Warranty eq 'Yes'}checked="checked"{/if} />&nbsp;<label>Yes</label>
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Warranty_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="WarrantyCode">{$page['Text']['warranty_code_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="text" class="text" name="WarrantyCode" value="{$datarow.WarrantyCode|default:''}" id="WarrantyCode" style="width: 280px;">
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Warranty_Code_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="CompulsoryFaultCoding">{$page['Text']['compulsory_fault_coding_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="CompulsoryFaultCoding" value="Yes" {if $datarow.CompulsoryFaultCoding eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Compulsory_Fault_Coding_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="ExcludeFromEDI">{$page['Text']['exclude_from_EDI_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="ExcludeFromEDI" value="Yes" {if $datarow.ExcludeFromEDI eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Exclude_From_EDI_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="ForceAdjustmentIfNoPartsUsed">{$page['Text']['force_adjustment_ifno_partsused_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="ForceAdjustmentIfNoPartsUsed" value="Yes" {if $datarow.ForceAdjustmentIfNoPartsUsed eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Force_Adjustment_IfNo_Parts_Used_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="ExcludeFromInvoicing">{$page['Text']['exclude_from_invoicing_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="ExcludeFromInvoicing" value="Yes" {if $datarow.ExcludeFromInvoicing eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Exclude_From_Invoicing_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="Chargeable">{$page['Text']['prompt_for_exchange_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="PromptForExchange" value="Yes" {if $datarow.PromptForExchange eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Prompt_For_Exchange_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="NoParts">{$page['Text']['no_parts_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="NoParts" value="Yes" {if $datarow.NoParts eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_NoParts_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="ScrapExchange">{$page['Text']['scrap_exchange_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="ScrapExchange" value="Yes" {if $datarow.ScrapExchange eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Scrap_Exchange_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="ExcludeRRCHandlingFee">{$page['Text']['exclude_rrchandling_fee_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="ExcludeRRCHandlingFee" value="Yes" {if $datarow.ExcludeRRCHandlingFee eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Exclude_RRCHandling_Fee_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                        <p>
                            <label class="cardLabel" for="Unavailable">{$page['Text']['unavailable_text']|escape:'html'}</label>
                            &nbsp;
                            <span class="formRadioCheckText"><input type="checkbox" name="Unavailable" value="Yes" {if $datarow.Unavailable eq 'Yes'}checked="checked"{/if} />
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Unavailable_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                        </p>
                    </div>
                    <div id="tabs-2" class="SystemAdminFormPanel inline">
                        <p>
                            <label class="cardLabel" for="Type">{$page['Text']['type_text']|escape:'html'} </label>
                            &nbsp;
                            
                                {for $var=0 to $categoryTypeCount}
                                    {if $var eq 0}
                                        <span class="formRadioCheckText"><input type="radio" name="Type" value="{$categoryType[$var]}" {if $datarow.Type|default:"Repair" eq $categoryType[$var]}checked="checked"{/if} />&nbsp;<label>{$categoryType[$var]}</label> <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="Repair_Type_Category_Type_Help" class="helpTextIconQtip" title="Help" alt="help" width="15" height="15" ></span>
                                    {else if $var eq $categoryTypeCount}
                                        <span class="formRadioCheckText" style="padding-left: 218px;"><input type="radio" name="Type" value="{$categoryType[$var]}" {if $datarow.Type|default:"Repair" eq $categoryType[$var]}checked="checked"{/if} />&nbsp;<label>{$categoryType[$var]}</label></span>
                                    {else}
                                        <span class="formRadioCheckText" style="padding-left: 218px;"><input type="radio" name="Type" value="{$categoryType[$var]}" {if $datarow.Type|default:"Repair" eq $categoryType[$var]}checked="checked"{/if} />&nbsp;<label>{$categoryType[$var]}</label><br /></span>
                                    {/if}

                                {/for}
                                
                        </p>
                    </div>
                    <hr>
                    <div style="height:20px; margin-bottom: 10px;text-align: center;">
                        <span style="flaot:center;">
                    {*<button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                    <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>*}
                    {if $datarow.RepairTypeID != '' && $datarow.RepairTypeID != '0'}
                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
                   {else}
                       <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
                   {/if}
                  </span><span style="float:right;">
                   <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" style="bottom:5px;right:10px;" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" />
                  </span>
                    </div>
                </div>
            </fieldset>
        </form> 
    </div>
{/if}

{* This block of code is for to display message after data updation *} 
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;">
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post" class="inline">  
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>		
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataDeletedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataDeletedMsgForm" name="dataDeletedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_deleted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>