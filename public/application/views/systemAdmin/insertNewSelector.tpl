 <script type="text/javascript">   
     
 $(document).ready(function() {
 var filtercounter=0;
 $.colorbox.resize({ 'width':600});

    var oTable2 = $('#Results').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		 "oLanguage": {
                                
                                "sSearch": "{$page['Labels']['tableName']}"
                            },
    "sAjaxSource": "{$_subdomain}/{$controller}/load{$page['Labels']['tableName']}List",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                                $('#Results').show();
                                $.colorbox.resize();
                                filtercounter+=1;
                                if( filtercounter==4)
                                {
                               $('#insertNewButtons').show();
                                $.colorbox.resize();
                                }
                               
			} );
                        },
                        
                            
                        

"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 20, 50, 100 , 500, 1000, -1], [20, 50, 100 , 500, 1000, "ALL!"]],
"iDisplayLength" : 20,

"aoColumns": [ 
			{ "bVisible":0 },
                               true,
                        { "bVisible":1,"bSortable":0 }       
                            
		],
                "bFilter": true
               
   
 
        
          
});//datatable end
 
 
    
  
   
 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
/* Add a click handler to the rows - this could be used as a callback */
	$("#Results tbody").click(function(event) {
                 $('#insertNewButtons').show();
                                $.colorbox.resize();
		$(oTable2.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable2 );
                var aData = oTable2.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
        
        /* Add a click handler for the useThisButton */
	$('button[id^=useThisButton]').click( function() {
		$selected= new Array;
                $('#Results tr td input:checkbox:checked').each(function (){
           
           $selected.push($(this).closest('tr').attr('id'))
           
           
    });
    console.log($selected.length);
                
    if ($selected.length>0)  // 
    {
        if($selected.length==1){
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/process{$page['Labels']['tableName']}/copy=yes/id="+$selected[0],
                        title: "Edit ",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
        }else{
        //more then 1 record selected
        
        $.post("{$_subdomain}/SystemAdmin/CopyFromMain/{$page['Labels']['tableName']}/",{ ids:$selected },
        function(data) {
        
       window.location="{$_subdomain}/{$controller}/{$action|default:"Manufacturers"}";
        
        }
        );
        
        
        }
    }else{
    //no records selected
    alert("Please select atleast one record");
    }
		
	} );    
        
        /* Add a click handler for the newItemButton */
	$('button[id^=newItemButton]').click( function() {
		
 
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/process{$page['Labels']['tableName']}/new=yes",
                        title: "New ",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    
		
	} );                            
      /* Add a click handler for the checkbox */
	$('#chToggleALL').click( function() {
		
                $t=$(this).attr('checked');
                if(!$t){ $t=false;}
            $('#Results tr td input:checkbox').each(function (){
           
            $(this).attr('checked',$t);
           
    });
    });
    
   }); //end doc ready
   
  </script>  
    
    <div id="NewSelectorFormPanel" class="SystemAdminFormPanel" >
     
                <form id="NewSelectorForm" name="NewSelectorForm" method="post"  action="{$_subdomain}/" class="inline" >
       
                <fieldset>
                    <legend title="" >{$page['Text']['insert_page_legend']}</legend>
                        <div id="textbar">{$page['Text']['description']}</div>
                    <p>
                  
                    
                     <table id="Results" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                <th>{$page['Labels']['tableName']}</th>
                                <th>{$page['Labels']['tableName']}</th>
                                <th  style="width:10px;text-align: center"><input id="chToggleALL" type="checkbox"></th>
			    </tr>
                        </thead>
                        <tbody>
                          
                          
                        </tbody>
                    </table>  
                    
                    </p>
                <hr>
                               
                                <div id="insertNewButtons" style="height:20px;margin-bottom: 10px;display:block">
                                <button id="useThisButton" type="button" style="float: left" class="gplus-blue">Select Tagged Records</button>
                                <button type="button" id="newItemButton" style="float:right" class="gplus-blue">Create new</button>
                                </div>
            </p>    

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
