<script type="text/javascript">
$(document).ready(function() {
    $("#ModelId").multiselect( { noneSelectedText: "{$page['Text']['select_models']|escape:'html'}", minWidth: 310, height: "auto", show:['slide', 500] } );
    $("#ServiceProviderID").combobox();
    $("#ManufacturerID").combobox();
    $("#RepairTypeIDForChargeable").combobox();
    $("#RepairTypeIDForWarranty").combobox();
    $(function() {
        $( "#tabs" ).tabs({
        show: function(event, ui) {
            $.colorbox.resize();
        }
    });
});
    if($('#ForcePartFaultCode').attr('checked')=="checked"){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide() ;$.colorbox.resize();}
    if($('#RestrictLookup').attr('checked')=="checked"){
    $('#addFieldsDop2').show();
    $('#reqradio').addClass("required");
    $.colorbox.resize();}
    
    else{
    $('#addFieldsDop2').hide();$.colorbox.resize(); 
    $('#reqradio').removeClass("required");
    }
    if($('#ReqOnlyForRepairType').attr('checked')=="checked"){ $('#addFieldsDop3').show();$.colorbox.resize();}else{ $('#addFieldsDop3').hide();$.colorbox.resize(); }
    
 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    }); 
    
    
    /////////validation
    
    $("#JobFaultCodesLookupForm").validate = null;
	
	$("#JobFaultCodesLookupForm").validate({
	    rules: {
                       
                        LookupName:{
                             required: function (element) {
                                    }    
                                  },
                        Description:{
                             required: function (element) {
                                   
                                   }
                                   }
                              
                        

                    },
	   
 
	    errorPlacement: function(error, element) {
		error.appendTo(element.parent("p"));
                
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "label"
	   
	});
    
    
    
   }); 
   
   function toggleRL(t){
   
   if(t.checked==true){
   $('#reqradio').addClass('required');
   $('#addFieldsDop2').show();
   $.colorbox.resize();
    }else{
    $('#reqradio').removeClass('required'); 
    $('#addFieldsDop2').hide();
    $.colorbox.resize(); }
   }
</script>
{if $deleteFlag eq 'Yes'}
<div id="deleteDiv">
    <form id="CForm" name="CForm" method="post"  action="" class="inline">
        <input type="hidden" name="JobFaultCodeLookupID" value="{$datarow.JobFaultCodeLookupID}">
        <fieldset>
        <legend>{$page['Text']['delete_page_legend']|escape:'html'}</legend>
        <p id="deleteP">Are you sure you want to delete this '<strong>{$datarow.LookupName}</strong>' Job Fault Code Lookup?</p>
            <div style="height:20px; margin-bottom: 10px;text-align: center;">
                <input type="submit" name="delete_save_btn" class="textSubmitButton centerBtn" id="delete_save_btn" value="{$page['Buttons']['yes_btn']|escape:'html'}" />
                <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" style="float:right; margin-left: 20px;" value="{$page['Buttons']['no_btn']|escape:'html'}" />
            </div>
        </fieldset>
    </form>
</div>
{else}
<div id="JobFaultCodesLookupFormPanel" class="SystemAdminFormPanel" >
    <form id="CForm" name="CForm" method="post"  action="" class="inline">
    <input type="hidden" name="JobFaultCodeLookupID" value="{$datarow.JobFaultCodeLookupID}">
    <fieldset>
    <legend title="">{$page['Text']['job_fault_codes_lookups']|escape:'html'}</legend>                   
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">{$page['Labels']['tab1_label']|escape:'html'}</a></li>
            <li><a href="#tabs-2">{$page['Labels']['tab2_label']|escape:'html'}</a></li>
            {*<li><a href="#tabs-3">{$page['Labels']['tab3_label']|escape:'html'}</a></li>*}
        </ul>
        <p>
            <label></label>
            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
        </p>
        <div id="tabs-1" class="SystemAdminFormPanel inline">          
            <p>
                <label class="cardLabel" for="LookupName" >{$page['Text']['jobfaultcode_lookupname_text']|escape:'html'}<sup>*</sup></label>
                &nbsp;
                <input type="text" class="text" name="LookupName" value="{$datarow.LookupName}" id="LookupName" >
            </p>
            <p>
                <label class="cardLabel" for="Description" >{$page['Text']['description_text']|escape:'html'}<sup>*</sup></label>
                &nbsp;
                <input   type="text" class="text"  name="Description" value="{$datarow.Description}" id="Description" >
            </p>
            {if $SupderAdmin eq true}
                <p>
                    <label class="cardLabel" for="ServiceProviderID">{$page['Text']['serviceProvider_text']|escape:'html'}<sup>*</sup></label>
                    &nbsp;
                    <select id="ServiceProviderID" name="ServiceProviderID">
                    <option value="" {if $datarow.ServiceProviderID eq ""}selected="selected"{/if}>Select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}" {if $datarow.ServiceProviderID eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym}</option>
                        {/foreach}
                    </select>
                </p>
		
	{* updated by Praveen Kumar N [START] *}
	{if $datarow.JobFaultCodeLookupID != '' && $datarow.JobFaultCodeLookupID != '0'}
	<p>
        <label class="cardLabel" for="ApproveStatus" >{$page['Text']['approve_status_text']|escape:'html'}</label>
        &nbsp;
	<span class="formRadioCheckText"  class="saFormSpan">
	<input  type="checkbox" name="ApproveStatus"  value="Pending" {if $datarow.ApproveStatus eq 'Pending'}checked="checked"{/if}  /><label>Unapproved</label>
	</span>
       
	</p> {/if}
	{* [END] *}
            {else}
                <input type="hidden" name="ServiceProviderID" value="{$sId}" />
		<input type="hidden" name="ApproveStatus" value="Pending" />
            {/if}
            <p>
                <label class="cardLabel" for="ManufacturerID" >{$page['Text']['manufacturer_text']|escape:'html'}<sup>*</sup></label>
                &nbsp;
                <select name="ManufacturerID" id="ManufacturerID" class="text">
                <option value="" {if $datarow.ManufacturerID|default:"" eq ''}selected="selected"{/if}>Select from drop down</option>
                {foreach $manufacturersList as $s}
                    <option value="{$s.ManufacturerID}" {if $datarow.ManufacturerID eq $s.ManufacturerID}selected="selected"{/if}>{$s.ManufacturerName}</option>
                {/foreach}
                </select>
            </p>
            <p>
                <label class="cardLabel" for="Status" >{$page['Text']['status_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <input  type="checkbox" name="Status"  value="Inactive" {if $datarow.Status eq 'Inactive'}checked="checked"{/if}  /><label>Inactive</label>&nbsp;
                </span>
            </p>
	    
        </div>
        <div id="tabs-2" class="SystemAdminFormPanel inline">
            <p>
                <label class="cardLabel" for="ExcludeFromBouncerTable" >{$page['Text']['excludefrombouncertable_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <input id="ExcludeFromBouncerTable" type="checkbox" name="ExcludeFromBouncerTable"  value="Yes" {if $datarow.ExcludeFromBouncerTable eq 'Yes'}checked="checked"{/if}  />
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_ExcludeFromBouncerTable_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <p>
                <label class="cardLabel" for="ForcePartFaultCode" >{$page['Text']['forcepartfaultcode_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <input  id="ForcePartFaultCode" onclick="if(this.checked==true){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide();$.colorbox.resize(); }"  type="checkbox" name="ForcePartFaultCode"  value="Yes" {if $datarow.ForcePartFaultCode eq 'Yes'}checked="checked"{/if}  />
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_ForcePartFaultCode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <div id="addFieldsDop" style="display:none">
                <p>
                    <label class="cardLabel" for="PartFaultCodeNo" >{$page['Text']['partfaultcodeno_text']|escape:'html'}<sup>*</sup></label>
                    &nbsp;
                    <input style="width:142px"  type="text" class="text"  name="PartFaultCodeNo" value="{$datarow.PartFaultCodeNo}" id="PartFaultCodeNo" >
                    </p>
            </div>
            <p>
                <label class="cardLabel" for="JobTypeAvailability" >{$page['Text']['jobtypeavailability_text']|escape:'html'}</label>
                &nbsp;<span class="formRadioCheckText"><input {if $datarow.JobTypeAvailability eq "Chargeable Jobs"}checked=checked{/if}  type="radio" name="JobTypeAvailability" id="JobTypeAvailability" value="Chargeable Jobs" id="FieldType" > Chargeable Jobs</span>
                <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.JobTypeAvailability eq "Warranty Jobs"}checked=checked{/if}  type="radio" name="JobTypeAvailability" id="JobTypeAvailability" value="Warranty Jobs" id="FieldType" > Warranty Jobs<br></span>
                <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.JobTypeAvailability|default:"Both"=="Both"}checked=checked{/if}  type="radio" name="JobTypeAvailability" id="JobTypeAvailability" value="Both" id="FieldType">  Both</span>
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_JobTypeAvailability_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
            </p>
            <p>
                <label class="cardLabel" for="PromptForExchange">{$page['Text']['promptforexchange_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <input id="PromptForExchange" type="checkbox" name="PromptForExchange"  value="Yes" {if $datarow.PromptForExchange eq 'Yes'}checked="checked"{/if}  />
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_PromptForExchange_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <p>
                <label class="cardLabel" for="RelatedPartFaultCode">{$page['Text']['relatedpartfaultcode_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <input id="RelatedPartFaultCode" type="checkbox" name="RelatedPartFaultCode"  value="Yes" {if $datarow.RelatedPartFaultCode eq 'Yes'}checked="checked"{/if}  />
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_RelatedPartFaultCode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <p>
                <label class="cardLabel" for="RepairTypeIDForChargeable">{$page['Text']['repairtypechargeable_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <select name="RepairTypeIDForChargeable" id="RepairTypeIDForChargeable" class="text" >
                    <option value="" {if $datarow.RepairTypeIDForChargeable eq ''}selected="selected"{/if}>Select from drop down</option>
                    {foreach $RepairTypeList as $s}
                        <option value="{$s.RepairTypeID}" {if $datarow.RepairTypeIDForChargeable eq $s.RepairTypeID}selected="selected"{/if}>{$s.RepairType}</option>
                    {/foreach}
                </select>
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_RepairTypeIDForChargeable_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <p>
                <label class="cardLabel" for="RepairTypeIDForWarranty">{$page['Text']['repairtypewarranty_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText"  class="saFormSpan">
                <select name="RepairTypeIDForWarranty" id="RepairTypeIDForWarranty" class="text" >
                    <option value="" {if $datarow.RepairTypeIDForWarranty eq ''}selected="selected"{/if}>Select from drop down</option>
                    {foreach $RepairTypeList as $s}
                        <option value="{$s.RepairTypeID}" {if $datarow.RepairTypeIDForWarranty eq $s.RepairTypeID}selected="selected"{/if}>{$s.RepairType}</option>
                    {/foreach}
                </select>
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_RepairTypeIDForWarranty_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                </span>
            </p>
            <p>
                <label class="cardLabel" for="RestrictLookup" >{$page['Text']['restrictlookup_text']|escape:'html'}</label>
                &nbsp;
                <span class="formRadioCheckText" class="saFormSpan">
                <input id="RestrictLookup" type="checkbox" name="RestrictLookup"  value="Yes" {if $datarow.RestrictLookup eq 'Yes'}checked="checked"{/if}  />
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_RestrictLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
                </span>
            </p>
            <p>
                <label class="cardLabel" for="RestrictLookupTo" >{$page['Text']['restrictlookupto_text']|escape:'html'}</label>
                &nbsp;<span class="formRadioCheckText"><input {if $datarow.RestrictLookupTo eq "Part Used"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Part Used" id="RestrictLookupTo" >    <label class="saFormSpan">Part Used</label></span>
                <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.RestrictLookupTo eq "Part Correction"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Part Correction" id="RestrictLookupTo" > <label class="saFormSpan">Part Correction</label><br></span>
                <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.RestrictLookupTo eq "Adjustment"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Adjustment" id="RestrictLookupTo" > <label class="saFormSpan">Adjustment</label></span>
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_RestrictLookupTo_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
            </p>
        </div>
        {*<div id="tabs-3" class="SystemAdminFormPanel inline">
            <p>
                <label class="cardLabel" for="ModelId" >{$page['Text']['excludedmodel_text']|escape:'html'}</label>
                &nbsp;<span class="formRadioCheckText">
                <select name="ModelId[]" id="ModelId"  multiple="multiple" class="text auto-hint" style="width: 280px;">
                    {foreach $modelsList as $s}
                        <option value="{$s.ServiceProviderModelID}" >{$s.ModelName} {$s.ModelNumber}</option>
                    {/foreach}
                </select>
                </span>
                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="JobFaultCodesLookupFormPanel_ModelId_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
            </p>
        </div>*}
        <hr>
        <div style="height:20px; margin-bottom: 10px;text-align: center;">
            {if $datarow.JobFaultCodeLookupID != '' && $datarow.JobFaultCodeLookupID != '0'}
                <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
            {else}
               <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
            {/if}
            <br/>
            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" style="float:right; margin-left: 20px;" value="{$page['Buttons']['cancel']|escape:'html'}" />
        </div>
    </div>
    </fieldset> 
    </form>
</div>
{/if}

{* This block of code is for to display message after data updation *} 
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;">
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post" class="inline">  
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>		
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataDeletedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataDeletedMsgForm" name="dataDeletedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_deleted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>
                 
 
                          
                        
