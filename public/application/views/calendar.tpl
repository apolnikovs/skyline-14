

{$numDays=7}
{$numWeeks=4}
{$todayDayNumber=$smarty.now|date_format:"%m"}
{$todayDayName=$smarty.now|date_format:"%a"}
{$dayCounter=1}
{$weekCounter=1}
{$none=false}
{$holiday=false}
{$full=false}
{if !isset($daySelected)}
    {$daySelected=null}
   {/if}

<div class="APcalendarOuterDiv" id="APcalendarHolder">

    <table class="APcalnedarTable" style="border:1px solid #acacac;" cellspacing="0">
    
    
{for $foo=1 to $numDays}
        <th class="APcalendarHeader" align="center" valign="middle">
        {"+{$foo-1} day"|date_format:"%a"}
        </th>  
{/for}
   
     {$digits=""}
     {$digitsPre=""}
     {$df=array()}
     {foreach $finalizedDays as $dd}
         
         {$df[]=$dd|date_format:"%d"}
        
{/foreach}
         {for $foo=1 to $numWeeks}
    {for $foo=1 to $numWeeks}
    
    <tr class="APcalendarTr">
     
        {for $foo2=$dayCounter to $numDays*$weekCounter}
        {$ddd=""}
        {if isset($slotsCount[{"+{$foo2-1} day"|date_format:"%d%m%Y"}])}
            {$ddd=true}
        {if $slotsCount[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]|strlen ==2}
           {$digits="&nbsp;&nbsp;"}
            {$digitsPre=""}
        {elseif  $slotsCount[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]|strlen >2}
            {$digits=""}
             {$digitsPre=""}
            {else}
            {$digits="&nbsp;&nbsp;"}
            {$digitsPre="&nbsp;&nbsp;"}
            {/if}
            {/if}
        <td 
            id="CalendarDay {"+{$foo2-1} day"|date_format:"%Y-%m-%d"} {if isset($slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}])}{$slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]} {/if}" 
           {if !in_array({"+{$foo2-1} day"|date_format:"%Y-%m-%d"},$bankHolidays)}
            class="
            {$ddd={"+{$foo2-1} day"|date_format:"%d"}}
     {$ddd2={"+{$foo2-1} day"|date_format:"%d%m"}}
            {if isset($df) && in_array($ddd,$df)} cmenu44{/if}
            
            {if isset($slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}])}
{if $slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]=='AM'}
    APcalendarAM cmenu2 hovermenu"
{elseif $slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]=='PM'}
    APcalendarPM cmenu2 hovermenu"
{elseif $slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]=='FULL'}
    APcalendarFull cmenu3 hovermenu"
    {else}APcalendarALL cmenu2 hovermenu"{/if}
    {else}APcalendarNone"{$none=true}
    {/if}
    {else}
        {$none=true}
        {$holiday=true}
    {/if}
    align="center" valign="top" 
    {if !$none}
        {if !isset($daysOnlyView)}
            onclick="setDate('{"+{$foo2-1} day"|date_format:"%d"}'+'{"+{$foo2-1} day"|date_format:"%m"}'+$('#APcalendarYearID_{$foo2}').html()+'{$slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]}');" 
       {else}
           onclick="setDate2('{"+{$foo2-1} day"|date_format:"%d"}'+'{"+{$foo2-1} day"|date_format:"%m"}'+$('#APcalendarYearID_{$foo2}').html()+'{$slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]}');" 
           {/if}
            ph="{"+{$foo2-1} day"|date_format:"%d"}{"+{$foo2-1} day"|date_format:"%m"}{"+{$foo2-1} day"|date_format:"%Y"}{$slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]}"
            
    {else}
    {$none=false}
    {if $holiday}
       
        onclick="alert('This date is National Bank Holiday')";
        {else}
            onclick="alert('Sorry no booking slots available in this day!')"
            {/if}
    
    
    {/if}
     {$ddd={"+{$foo2-1} day"|date_format:"%d"}}
     {$ddd2={"+{$foo2-1} day"|date_format:"%d%m"}}
     
     {if isset($df) && in_array($ddd,$df)}
     
       style="background:#4c4c4c !important;color:white !important;" title="Day Finalised"
        
        {/if}
    {if $ddd2==$daySelected|date_format:"%d%m"}
       style="background:#F07745 !important; color: #ffffff;" title="Selected Day"
        
        {/if}
        
    date="{"+{$foo2-1} day"|date_format:"%Y"}-{"+{$foo2-1} day"|date_format:"%m"}-{"+{$foo2-1} day"|date_format:"%d"}"
    >
        {if !$holiday}
        <div id="APcalendarMonthID_{$foo2}"  style="display:none">{"+{$foo2-1} day"|date_format:"%m"}</div>
        <div id="APcalendarYearID_{$foo2}" style="display:none">{"+{$foo2-1} day"|date_format:"%Y"}</div>
       {if !isset($daysOnlyView)} <div class="APcalendarSlotsLeftDiv" style="float:left;">{if isset($slotsCount[{"+{$foo2-1} day"|date_format:"%d%m%Y"}])}<span style="right: -10px!important;">{$digitsPre}{$slotsCount[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]}{$digits}</span>{else}<p>&nbsp;</p>{/if}</div>
     
           <div class="APcalendarBottomTextDiv">{if isset($slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}])}{$slotsType[{"+{$foo2-1} day"|date_format:"%d%m%Y"}]}{/if}</div>
       
       {/if}
       <span  id="APcalendarDayNumID_{$foo2}">
           <br>
    {"+{$foo2-1} day"|date_format:"%d"}
       </span>
    <BR>
    {$dd={"+{$foo2-1} day"|date_format:"%A"}}
    {if isset($tradeAccountsDays[0][$dd])}
    {if $tradeAccountsDays[0][$dd]=='Yes'}
         <span style="color:white;font-size:12px;position:relative;">
        <img src="{$_subdomain}/css/Skins/skyline/images/ta.png">
        </span>
  
            
            
    {/if} 
    {/if} 
    {if in_array("+{$foo2-1} day"|date_format:"%d%m%Y",$recomendet)&&!isset($tradeAccountsDays[0][$dd])}
        <span style="color:white;font-size:12px;position:relative;">
            <img src="{$_subdomain}/css/Skins/skyline/images/green_tick.png">
        </span>
    {else }
        {if isset($tradeAccountsDays[0][$dd])&&$tradeAccountsDays[0][$dd]=='Yes'}
            {else}
        <span style="font-size:12px">{"+{$foo2-1} day"|date_format:"%b"}</span>
    {/if}
   
    {/if}
    {else}
        <div style="font-size:11px;margin-top:15px">Bank Holiday</div>
       {/if}
        {$holiday=false}
        </td>
        {$dayCounter=$dayCounter+1}
        {/for}
      {$weekCounter=$weekCounter+1}
    </tr>   
{/for}
{/for}
</table>
<script>
var mouseX;
var mouseY;
$(document).mousemove( function(e) {
   mouseX = e.pageX; 
   mouseY = e.pageY;
  // console.log($("input[name='SPID[]']:checked").val());
}); 
$(document).mousedown( function(e) {
    $('#DivToShow').hide();
}); 

$(".hovermenu").mouseenter(function(){
 var SWBDate=$(this).attr('date');
    $.post("{$_subdomain}/Diary/getCalendarInfoTable",{ 'date':SWBDate },
function(data) {
en=jQuery.parseJSON(data);
t=0;
//console.log(en);
if ( typeof en['2'] !== "undefined" && en['2']) {
$('#wb_brownTR').show();
t=1;
      $('#wb_brownTotal').html(en['2']['apCountType']);
      $('#wb_brownEng').html(en['2']['totalEngineers']);
      qb=en['2']['apCountType']/en['2']['totalEngineers'];
      
      if(Math.round(qb) !== qb) {
            qb = qb.toFixed(1);
        }
        $('#wb_brownAvg').html(qb);
} else{
 $('#wb_brownTotal').html(0);
      $('#wb_brownEng').html(0);
       $('#wb_brownAvg').html(0);
       $('#wb_brownTR').hide();
}
if ( typeof en['4'] !== "undefined" && en['4']) {
$('#wb_whiteTR').show();
t=1;
      $('#wb_whiteTotal').html(en['4']['apCountType']);
      $('#wb_whiteEng').html(en['4']['totalEngineers']);
      qb=en['4']['apCountType']/en['4']['totalEngineers'];
      
      if(Math.round(qb) !== qb) {
            qb = qb.toFixed(1);
        }
        $('#wb_whiteAvg').html(qb);
}else{
$('#wb_whiteTR').hide();
}
//alert(en.length);

size = Object.size(en)
if ( size >0) {
var ignore= new Array(1,2,4);
var countType=0;
var totalEngineers=0;


 $.each(en, function(key, value) { 
  //alert(key + ': ' + value); 

    
     if(!(key in ignore) ){
     countType+=en[key]['apCountType']*1;
     totalEngineers+=en[key]['totalEngineers']*1;
     }
});
if(countType>0){
$('#wb_otherTR').hide();
t=1;
      $('#wb_otherTotal').html(countType);
      $('#wb_otherEng').html(totalEngineers);
      qb=countType/totalEngineers;
      
      if(Math.round(qb) !== qb) {
            qb = qb.toFixed(1);
        }
        $('#wb_otherAvg').html(qb);
 }
}else{
$('#wb_otherTR').hide();
}

if ( typeof en['1'] !== "undefined" && en['1']) {
$('#wb_arialTR').show();
t=1;
      $('#wb_arialTotal').html(en['1']['apCountType']);
      $('#wb_arialEng').html(en['1']['totalEngineers']);
      qb=en['1']['apCountType']/en['1']['totalEngineers'];
      
      if(Math.round(qb) !== qb) {
            qb = qb.toFixed(1);
        }
        $('#wb_arialAvg').html(qb);
}else{
$('#wb_arialTR').hide();
}

if(t===0){
$('#DivToShow').hide();
}else{


da=SWBDate.split('-');

 d=new Date(Date.today().set({
    
    day: da[2]*1,
    month: da[1]*1-1,
    year: da[0]*1
    }));
 
 weekday=new Array(7);
weekday[0]="Sunday";
weekday[1]="Monday";
weekday[2]="Tuesday";
weekday[3]="Wednesday";
weekday[4]="Thursday";
weekday[5]="Friday";
weekday[6]="Saturday";

n = weekday[d.getDay()];
var month=new Array();
month[0]="January";
month[1]="February";
month[2]="March";
month[3]="April";
month[4]="May";
month[5]="June";
month[6]="July";
month[7]="August";
month[8]="September";
month[9]="October";
month[10]="November";
month[11]="December";
var n2 = month[d.getMonth()];
smallWBposition=$('#box-wrap').position();
$('#swbDate').html(n+" "+d.getDate()+" "+n2);
 $('#DivToShow').css({ 'top':smallWBposition.top+230{if $browser=="IE7"}+30{/if},'left':smallWBposition.left+10}).fadeIn('fast');
}

});
});
$('.hovermenu').mouseleave(function(){
  $('#DivToShow').hide();
});
$('.hovermenu').mousedown(function(event) {
   if(event.which==3){
    $('#DivToShow').hide();
   }
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// Get the size of an object


//$(".hovermenu").mouseout(function(){
//  $('#DivToShow').fadeOut('slow');
//});



</script>

<div onclick="$(this).hide()" id="DivToShow" style="border:1px solid black;display:none;background:#FFF8DC;position:absolute;z-index:200;padding:0;width:330px">
    <div style="position:absolute;top:-20px;background:white;width:200px" id="swbDate">Monday 03 December</div>
    <table  cellspacing=0 cellpadding=0 style="margin:0;text-align: center;font-size: 10px" align=center>
        <thead>
            <th style="text-align: center !important;">Skill Type</th>
            <th style="border-left:1px solid black;text-align: center !important;">Apps</th>
            <th style="border-left:1px solid black;text-align: center !important;">Engineers</th>
            <th style="border-left:1px solid black;text-align: center !important;">Average Jobs</th>
        </thead>
        <tbody>
        <tr id="wb_brownTR">
            <td class="tooltipWallboardTD" style="border-left:none">
                Brown Goods
            </td>
            <td class="tooltipWallboardTD" id="wb_brownTotal">0</td>
            <td class="tooltipWallboardTD" id="wb_brownEng">0</td>
            <td class="tooltipWallboardTD" id="wb_brownAvg">0</td>
        </tr><tr id="wb_whiteTR">
            <td class="tooltipWallboardTD" style="border-left:none">
                White Goods
            </td>
            <td class="tooltipWallboardTD" id="wb_whiteTotal">0</td>
            <td class="tooltipWallboardTD" id="wb_whiteEng">0</td>
            <td class="tooltipWallboardTD" id="wb_whiteAvg">0</td>
        </tr><tr id="wb_otherTR" style="display:none">
            <td class="tooltipWallboardTD" style="border-left:none">
                Other
            </td>
            <td class="tooltipWallboardTD" id="wb_otherTotal">0</td>
            <td class="tooltipWallboardTD" id="wb_otherEng">0</td>
            <td class="tooltipWallboardTD" id="wb_otherAvg">0</td>
        
        </tr><tr id="wb_arialTR">
            <td class="tooltipWallboardTD" style="border-left:none">
                Arial
            </td>
            <td class="tooltipWallboardTD" id="wb_arialTotal">0</td>
            <td class="tooltipWallboardTD" id="wb_arialEng">0</td>
            <td class="tooltipWallboardTD" id="wb_arialAvg">0</td>
        </tbody>
    </table>
</div>
</div>



