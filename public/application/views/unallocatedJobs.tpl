{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $UnallocatedJobs}
    {/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


    <script type="text/javascript">
    
     
    function openJob($sRow)
    {
        
        if($sRow[0])
            window.open("{$_subdomain}/index/jobupdate/"+urlencode($sRow[0]));
            
       
    }

    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/unallocatedJobs/'+urlencode($("#nId").val()));
            }
        });



                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/jobAllocation');

                                });



                 /* Add a change handler to the network dropdown - strats here*/
                /*$(document).on('change', '#nId', 
                    function() {
                         
                        $(location).attr('href', '{$_subdomain}/JobAllocation/unallocatedJobs/'+urlencode($("#nId").val())); 
                    }      
                );*/
              /* Add a change handler to the network dropdown - ends here*/
              




                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        


                    var  displayButtons = "PUD";
                     
                     
                    $('#UnallocatedJobsResults').PCCSDataTable( {
                              "aoColumns": [ 
                                                        /* JobID */  null,    
                                                        /* ClientName */   null,
                                                        /* BranchName */  null,
                                                        /* Consumer Name */  null,
                                                        /* Postcode */  null,
                                                        /* ManufactureID */  null,
                                                        /* Unit Type */  null,
                                                        /* Repair Skill */  null,
                                                        /* Booked Date */  null
                                                ],
                            
                            
                            
                            
                            
                               
                            displayButtons:  displayButtons,
                            frmErrorRules:   {
                                                    
                                                   
                                                        
                                             },
                                                
                            frmErrorMessages: {
                                                
                                                  
                                              },                     
                            
                            popUpFormWidth:  950,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['allocate']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/JobAllocation/unallocatedJobs/allocate/',
                            //updateDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/Job/',
                            updateFormTitle: '{$page['Text']['allocate_page_legend']|escape:'html'}',
                            formUpdateButton:"update_save_btn",
                            
                            
                            deleteButtonText: "{$page['Buttons']['cancel_job']|escape:'html'}",
                            deleteButtonId:   'deleteButtonId',
                            deleteAppUrl:     '{$_subdomain}/JobAllocation/unallocatedJobs/cancelJob/',
                            deleteDataUrl:    '{$_subdomain}/JobAllocation/ProcessData/Job/cancelJob',
                            deleteFormTitle:  '{$page['Text']['cancel_job_legend']|escape:'html'}',
                            formDeleteButton: "cancel_job_btn",
                            
                            
                            
                            
                            colorboxFormId:  "UnallocatedJobsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'UnallocatedJobsResultsPanel',
                            htmlTableId:     'UnallocatedJobsResults',
                            fetchDataUrl:    '{$_subdomain}/JobAllocation/ProcessData/Job/fetchUnallocatedJobs/'+urlencode("{$nId}"),
                            formCancelButton:'cancel_btn',
                            
                            pickButtonText:'{$page['Buttons']['view']|escape:'html'}', 
                            pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'openJob',
                            
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/jobAllocation" >{$page['Text']['job_allocation']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ClientsTopForm" name="ClientsTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="UnallocatedJobsResultsPanel" >
                    
                        {if $SuperAdmin}
                            
                            <form id="nIdForm" class="nidCorrections">
                            {$page['Labels']['service_networ_label']|escape:'html'}
                            <select name="nId" id="nId" >
                                <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                            </form>
                        {else}
                            
                            <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >
                            
                        {/if}
                    
                    <table id="UnallocatedJobsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        
                                        <th title="{$page['Text']['sl_number']|escape:'html'}" >{$page['Text']['sl_number']|escape:'html'}</th>
                                        <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                        <th title="{$page['Text']['branch']|escape:'html'}"  >{$page['Text']['branch']|escape:'html'}</th>
                                        <th title="{$page['Text']['consumer']|escape:'html'}"  >{$page['Text']['consumer']|escape:'html'}</th>
                                        <th title="{$page['Text']['postcode']|escape:'html'}"  >{$page['Text']['postcode']|escape:'html'}</th>
                                        <th title="{$page['Text']['manufacturer']|escape:'html'}"  >{$page['Text']['manufacturer']|escape:'html'}</th>
                                        <th title="{$page['Text']['unit_type']|escape:'html'}"  >{$page['Text']['unit_type']|escape:'html'}</th>
                                        <th title="{$page['Text']['repair_skill']|escape:'html'}"  >{$page['Text']['repair_skill']|escape:'html'}</th>
                                        <th title="{$page['Text']['booked_date']|escape:'html'}"  >{$page['Text']['booked_date']|escape:'html'}</th>
                                     
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 
                <input type="hidden" name="addButtonId" id="addButtonId" > 
               


    </div>
                        
                        



{/block}



