<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Clients Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class Clients extends CustomModel {
    
    private $conn;
    private $dbColumns  = array('t1.ClientID', 't2.ClientName', 't3.CompanyName',  't2.AccountNumber', 't2.InvoiceNetwork',  't4.CompanyName', 't2.Status');
    private $tables     = "network_client AS t1 LEFT JOIN client AS t2 ON t1.ClientID=t2.ClientID LEFT JOIN network AS t3 ON t1.NetworkID=t3.NetworkID LEFT JOIN service_provider AS t4 ON t2.ServiceProviderID=t4.ServiceProviderID";
    private $table = "client";
    private $table_network_client = "network_client";
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID = isset($args['firstArg']) ? $args['firstArg'] : '';
        
        if($NetworkID!='')
        {
            $args['where'] = "t1.NetworkID='".$NetworkID."'";
            
        }
        //$this->controller->log(var_export($args, true));
        
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['ClientID']) || !$args['ClientID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param interger $ClientName  
     * @param interger $ClientID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($ClientName, $ClientID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ClientID FROM '.$this->table.' WHERE ClientName=:ClientName AND ClientID!=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ClientName' => $ClientName, ':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
        
       // $this->controller->log(var_export($result, true));
        
        if(is_array($result) && $result['ClientID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert row into network_client table.
     *
     * @param interger $ClientID  
     * @param interger $NetworkID.
     * @global $this->table_network_client
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function insertNetworkAgent($ClientID, $NetworkID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table_network_client.' (ClientID, NetworkID) VALUES (:ClientID, :NetworkID)';
        $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result = $insertQuery->execute(array(':ClientID' => $ClientID, ':NetworkID' => $NetworkID));
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to delete rows of client in network_client table.
     *
     * @param interger $ClientID  
     * @global $this->table_network_client
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function deleteNetworkAgent($ClientID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'DELETE FROM '.$this->table_network_client.' WHERE ClientID=:ClientID';
        $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $result = $deleteQuery->execute(array(':ClientID' => $ClientID));
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to get client's network from network_client table.
     *
     * @param interger $ClientID  
     * @global $this->table_network_client
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getNetworkAgent($ClientID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkID FROM '.$this->table_network_client.' WHERE ClientID=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (ClientName, BuildingNameNumber, PostalCode, Street, LocalArea, TownCity, CountyID, CountryID, ContactPhone, ContactPhoneExt, 
                        ContactEmail, AccountNumber, GroupName, ServiceProviderID, InvoiceNetwork, VATRateID, VATNumber, InvoicedCompanyName, InvoicedPostalCode,
                        InvoicedBuilding, InvoicedStreet, InvoicedTownCity, InvoicedArea, PaymentDiscount, PaymentTerms, VendorNumber, PurchaseOrderNumber, SageAccountNumber, 
                        SageNominalCode, EnableProductDatabase, Status, ForceReferralNumber, EnableCatalogueSearch, DefaultTurnaroundTime)
                                      VALUES(:ClientName, :BuildingNameNumber, :PostalCode, :Street, :LocalArea, :TownCity, :CountyID, :CountryID, :ContactPhone, :ContactPhoneExt, 
                        :ContactEmail, :AccountNumber, :GroupName, :ServiceProviderID, :InvoiceNetwork, :VATRateID, :VATNumber, :InvoicedCompanyName, :InvoicedPostalCode,
                        :InvoicedBuilding, :InvoicedStreet, :InvoicedTownCity, :InvoicedArea, :PaymentDiscount, :PaymentTerms, :VendorNumber, :PurchaseOrderNumber, :SageAccountNumber, 
                        :SageNominalCode, :EnableProductDatabase, :Status, :ForceReferralNumber, :EnableCatalogueSearch, :DefaultTurnaroundTime)';
        
       
        
        if($this->isValid($args['ClientName'], 0))
        {
            
            
            if(!isset($args['InvoiceNetwork']))
            {
                $args['InvoiceNetwork'] = 'No';
            }
            else
            {
                 $args['InvoiceNetwork'] = 'Yes';
            }  
            
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
           
            
            $result =  $insertQuery->execute(array(
                ':ClientName' => $args['ClientName'], 
                ':BuildingNameNumber' => $args['BuildingNameNumber'], 
                ':PostalCode' => $args['PostalCode'],
                ':Street' => $args['Street'],
                ':LocalArea' => $args['LocalArea'],
                ':TownCity' => $args['TownCity'],
                ':CountyID' => ($args['CountyID']=="")?NULL:$args['CountyID'],
                ':CountryID' => ($args['CountryID']=="")?NULL:$args['CountryID'],
                ':ContactPhone' => $args['ContactPhone'],
                ':ContactPhoneExt' => $args['ContactPhoneExt'],
                ':ContactEmail' => $args['ContactEmail'],
                ':AccountNumber' => $args['AccountNumber'],
                ':GroupName' => $args['GroupName'],
                ':ServiceProviderID' => ($args['ServiceProviderID']=="")?NULL:$args['ServiceProviderID'],
                ':InvoiceNetwork' => $args['InvoiceNetwork'],
                ':VATRateID' => ($args['VATRateID']=="")?NULL:$args['VATRateID'],
                ':VATNumber' => $args['VATNumber'],
                ':InvoicedCompanyName' => $args['InvoicedCompanyName'],
                ':InvoicedPostalCode' => $args['InvoicedPostalCode'],
                ':InvoicedBuilding' => $args['InvoicedBuilding'],
                ':InvoicedStreet' => $args['InvoicedStreet'],
                ':InvoicedTownCity' => $args['InvoicedTownCity'],
                ':InvoicedArea' => $args['InvoicedArea'],
                ':PaymentDiscount' => $args['PaymentDiscount'],
                ':PaymentTerms' => $args['PaymentTerms'],
                ':VendorNumber' => $args['VendorNumber'],
                ':PurchaseOrderNumber' => $args['PurchaseOrderNumber'],
                ':SageAccountNumber' => $args['SageAccountNumber'],
                ':SageNominalCode' => $args['SageNominalCode'],
                ':EnableProductDatabase' => $args['EnableProductDatabase'],
                ':Status' => $args['Status'],
                ':ForceReferralNumber' => $args['ForceReferralNumber'],
                ':EnableCatalogueSearch' => $args['EnableCatalogueSearch'],
                ':DefaultTurnaroundTime' => $args['DefaultTurnaroundTime']
                
                ));
        
            
               $ClientID = $this->conn->lastInsertId();
            
               $result2 = false; 
               if($result)//if the client details are inserted into database then only we are storing clientid and network id in table network_client
               {
                    $result2 = $this->insertNetworkAgent($ClientID, $args['NetworkID']);
               }
               
              if($result && $result2)
              {
                  
                    if(isset($args['AutomaticallyCreateBrand']))//Creating brand for this client automatically.
                    {
                        if(isset($this->controller->secondModel))
                        {
                            
                            //Getting next brand Id.
                            $BrandID = $this->controller->secondModel->getNextBrandID();
                            
                            
                            $brandArgs = array(
                                
                                'NewBrandID' => $BrandID,
                                'BrandName' => $args['ClientName'], 
                                'NetworkID' => $args['NetworkID'], 
                                'ClientID' => $ClientID,
                                'BrandLogo' => '',
                                'EmailType' => 'Generic',
                                'AutoSendEmails'=>1,
                                'Status' => $args['Status'],
                                'UploadedBrandLogo' => ''
                                
                                );

                            $this->controller->secondModel->create($brandArgs);
                            
                           
                        }
                      
                        if(isset($args['AutomaticallyCreateBranch']) && $BrandID)//Creating branch for this client automatically.
                        {
                           $branchModel  = $this->controller->loadModel("Branches");
                            
                           $branchArgs =  array(

                                'BranchName' => $args['ClientName'], 
                                'BuildingNameNumber' => $args['BuildingNameNumber'], 
                                'Street' => $args['Street'],
                                'LocalArea' => $args['LocalArea'],
                                'TownCity' => $args['TownCity'],
                                'CountyID' => ($args['CountyID']=='')?NULL:$args['CountyID'],
                                'CountryID' => ($args['CountryID']=='')?NULL:$args['CountryID'],
                                'PostalCode' => $args['PostalCode'],
                                'ContactEmail' => $args['ContactEmail'],
                                'ContactPhoneExt' => $args['ContactPhoneExt'],
                                'ContactPhone' => $args['ContactPhone'],
                                'BranchNumber' => NUll,
                                'BranchType' => 'Store',
                                'Status' => $args['Status'],
                                'ClientID' => $ClientID, 
                                'NetworkID' => $args['NetworkID'],
                                'BrandID' => $BrandID
                               
                                );
                           
                           $branchModel->create($branchArgs);
                        
                        }    
                    }
                   
                  
                  
                  
                    return array('status' => 'OK',
                            'message' => $this->controller->page['Text']['data_inserted_msg']);
              }
              else
              {
                  return array('status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed']);
              }
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT	ClientID, 
			ClientName, 
			BuildingNameNumber, 
			PostalCode, 
			Street, 
			LocalArea, 
			TownCity, 
			CountyID, 
			CountryID, 
			ContactPhone, 
			ContactPhoneExt, 
                        ContactEmail, 
			AccountNumber, 
			GroupName, 
			ServiceProviderID, 
			InvoiceNetwork, 
			VATRateID, 
			VATNumber, 
			InvoicedCompanyName, 
			InvoicedPostalCode,
                        InvoicedBuilding, 
			InvoicedStreet, 
			InvoicedTownCity, 
			InvoicedArea, 
			PaymentDiscount, 
			PaymentTerms, 
			VendorNumber, 
			PurchaseOrderNumber, 
			SageAccountNumber, 
			SageNominalCode, 
			EnableProductDatabase, 
			Status, 
			EnableCatalogueSearch,
			DefaultTurnaroundTime,
                        ForceReferralNumber
			
		FROM	' . $this->table . ' 
		    
		WHERE	ClientID=:ClientID';
	
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ClientID' => $args['ClientID']));
        $result = $fetchQuery->fetch();
        
        $networksArray = $this->getNetworkAgent($args['ClientID']);
        
        if(is_array($result))
        {
            $result['NetworkID'] = '';
            if(isset($networksArray['NetworkID']))
            {
                 $result['NetworkID'] = $networksArray['NetworkID'];
            }
        }    
        
        return $result;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used to know the product database enable or not for given client id.
     *
     * @param array $ClientID
     * @global $this->table  
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     ****/ 
     public function isProductDatabaseEnabled($ClientID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT EnableProductDatabase FROM '.$this->table.' WHERE ClientID=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['EnableProductDatabase']=="Yes")
        {
            return true;
        }    
        else
        {
            return false;
        }
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used to know  Referral Number required or not.
     *
     * @param array $ClientID
     * @global $this->table  
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     ****/ 
     public function isReferralNumberRequired($ClientID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ForceReferralNumber FROM '.$this->table.' WHERE ClientID=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['ForceReferralNumber']=="Yes")
        {
            return true;
        }    
        else
        {
            return false;
        }
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValid($args['ClientName'], $args['ClientID']))
        {        
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE	' . $this->table . ' 
		
		    SET		ClientName=:ClientName, 
				BuildingNameNumber=:BuildingNameNumber, 
				PostalCode=:PostalCode, 
				Street=:Street, 
				LocalArea=:LocalArea, 
				TownCity=:TownCity, 
				CountyID=:CountyID, 
				CountryID=:CountryID, 
				ContactPhone=:ContactPhone, 
				ContactPhoneExt=:ContactPhoneExt, 
				ContactEmail=:ContactEmail, 
				AccountNumber=:AccountNumber, 
				GroupName=:GroupName, 
				ServiceProviderID=:ServiceProviderID, 
				InvoiceNetwork=:InvoiceNetwork, 
				VATRateID=:VATRateID, 
				VATNumber=:VATNumber, 
				InvoicedCompanyName=:InvoicedCompanyName, 
				InvoicedPostalCode=:InvoicedPostalCode,
				InvoicedBuilding=:InvoicedBuilding, 
				InvoicedStreet=:InvoicedStreet, 
				InvoicedTownCity=:InvoicedTownCity, 
				InvoicedArea=:InvoicedArea, 
				PaymentDiscount=:PaymentDiscount, 
				PaymentTerms=:PaymentTerms, 
				VendorNumber=:VendorNumber, 
				PurchaseOrderNumber=:PurchaseOrderNumber, 
				SageAccountNumber=:SageAccountNumber, 
				SageNominalCode=:SageNominalCode,
				EnableProductDatabase=:EnableProductDatabase, 
				Status=:Status,
				EnableCatalogueSearch=:EnableCatalogueSearch,
				DefaultTurnaroundTime = :DefaultTurnaroundTime,
                                ForceReferralNumber = :ForceReferralNumber
				
		    WHERE	ClientID = :ClientID';
        
            
            if(!isset($args['InvoiceNetwork'])) {
                $args['InvoiceNetwork'] = 'No';
            } else {
		$args['InvoiceNetwork'] = 'Yes';
            }
	    
	    //$args["EnableCatalogueSearch"] = (isset($args["EnableCatalogueSearch"])) ? "Yes" : "No";
            
            
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $result = $updateQuery->execute(
                      
                      array(
                          
                            ':ClientName' => $args['ClientName'], 
                            ':BuildingNameNumber' => $args['BuildingNameNumber'], 
                            ':PostalCode' => $args['PostalCode'],
                            ':Street' => $args['Street'],
                            ':LocalArea' => $args['LocalArea'],
                            ':TownCity' => $args['TownCity'],
                            ':CountyID' => ($args['CountyID']=="")?NULL:$args['CountyID'],
                            ':CountryID' => ($args['CountryID']=="")?NULL:$args['CountryID'],
                            ':ContactPhone' => $args['ContactPhone'],
                            ':ContactPhoneExt' => $args['ContactPhoneExt'],
                            ':ContactEmail' => $args['ContactEmail'],
                            ':AccountNumber' => $args['AccountNumber'],
                            ':GroupName' => $args['GroupName'],
                            ':ServiceProviderID' => ($args['ServiceProviderID']=="")?NULL:$args['ServiceProviderID'],
                            ':InvoiceNetwork' => $args['InvoiceNetwork'],
                            ':VATRateID' => ($args['VATRateID']=="")?NULL:$args['VATRateID'],
                            ':VATNumber' => $args['VATNumber'],
                            ':InvoicedCompanyName' => $args['InvoicedCompanyName'],
                            ':InvoicedPostalCode' => $args['InvoicedPostalCode'],
                            ':InvoicedBuilding' => $args['InvoicedBuilding'],
                            ':InvoicedStreet' => $args['InvoicedStreet'],
                            ':InvoicedTownCity' => $args['InvoicedTownCity'],
                            ':InvoicedArea' => $args['InvoicedArea'],
                            ':PaymentDiscount' => $args['PaymentDiscount'],
                            ':PaymentTerms' => $args['PaymentTerms'],
                            ':VendorNumber' => $args['VendorNumber'],
                            ':PurchaseOrderNumber' => $args['PurchaseOrderNumber'],
                            ':SageAccountNumber' => $args['SageAccountNumber'],
                            ':SageNominalCode' => $args['SageNominalCode'],
                            ':EnableProductDatabase' => $args['EnableProductDatabase'],
                            ':Status' => $args['Status'],
                            ':ClientID' => $args['ClientID'],
			    ':EnableCatalogueSearch' => $args["EnableCatalogueSearch"],
			    ':DefaultTurnaroundTime' => $args["DefaultTurnaroundTime"],
                            ':ForceReferralNumber' => $args["ForceReferralNumber"]
                          
                          
                          )
                      
                      );
        
                
              
               $result2 = false; 
               if($result)//if the client details are updated into database then only we are deleting and inserting clientid and network id in table network_client
               {
                    $this->deleteNetworkAgent($args['ClientID']);
                    $result2 = $this->insertNetworkAgent($args['ClientID'], $args['NetworkID']);
               }
               
                if($result && $result2)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_updated_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
              
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * getClientAccNoId
     *  
     * Get a client ID and prefered ServiceProviderID (if exists) from a Clinet
     * AccountNumber
     * 
     * @param string $cAccNo    The Account number to be searched
     * 
     * @return array    (ClientID, ServiceProviderID)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getClientAccNoId($cAccNo) {
        $sql = "
                SELECT
			`ClientID`, `ServiceProviderID`
		FROM
			`client`
		WHERE
			`AccountNumber` = '$cAccNo'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Client Account Number exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    public function getEnabledCatalogueSearch($clientID) {
	
	$q = "SELECT * FROM client WHERE ClientID=:clientID";
	$values = array("clientID" => $clientID);
	$result = $this->query($this->conn,$q,$values);
	if(count($result)>0) {
	    if($result[0]["EnableCatalogueSearch"]=="Yes") {
		return true;
	    } else {
		return false;
	    }
	} else {
	    return false;
	}
	
    }
    
    
    
    //Returns clients for provided $networkID
    //2012-01-23 © Vic <v.rutkunas@pccsuk.com>
    
    public function getNetworkClients($networkID) {
	
	$q = "	SELECT	    * 
		FROM	    network_client AS nc
		LEFT JOIN   client ON nc.ClientID = client.ClientID
		WHERE	    nc.NetworkID = :networkID
		ORDER BY    client.ClientName ASC
	     ";
	$values = ["networkID" => $networkID];
	$result = $this->query($this->conn, $q, $values);
	return $result;
	
    }
    
    public function Select( $sql, $params=null ) {
        return $this->Query( $this->conn, $sql, $params );
    }
    
    
    
}
?>
