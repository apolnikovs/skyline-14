<?php
require_once('CustomModel.class.php');

class Messages extends CustomModel {  
    
    public function  __construct($Controller) {
                  
        parent::__construct($Controller);        
        
    }
       
    public function getError($id, $customer='default', $lang='en') {
        
        switch ($id) {
            
            case 1001:
                return 'The User Name you have entered does not exist.';
            case 1004:
                return 'The Username & Password combination you have entered is invalid.  Please re-enter and try again or ask your supervisor for assistance.';
            case 1005:
                return 'You must complete fields marked with *';
            case 1006:
                return 'Username cannot contain spaces or special characters, however, you may use an underscore, for example: John_Doe.  Please re-enter and try again.';
            case 1007:
                return 'Passwords do not match.  Please re-enter and try again.';
            case 1008:
                return 'Username currently exists within the system. Please re-enter and try again.';                                                
            case 1018:
                return 'Your password has expired. Please enter a new password that complies with the following format: %%password_format%%';
            case 1020:
                return 'You do not have permission to Login to this system.  Please contact your supervisor and quote: EM:%%error_id%%/U:%%user_id%% /AP: Login Page';
            case 1021:
                return 'The Username & Password you entered in the Temporary assword fields are registred to a user.  Although, this may be you it is necessary to answer a security question before you will be granted access to the system';
        
            case 1023:
                return 'This is a system record that cannot be edited, however, you may copy the record and edit the details in the copy.';
            case 1024:
                //return 'Duplicate Record.  The Action ID for this record already exists in the database.'; 
                return 'Duplicate Record.  This record already exists in the database.  Please check your data once.';
            case 1028:
                return 'Missing Brand ID';
            
                
        }
        
        return '';

    }
    
   
    
    
    public function getAFN($id, $customer='default', $lang='en') {
    
        switch (strtoupper($id)) {
            
            case 'AFN-JOB':
                return 'Job';
                
            case 'AFN-STOCK CODE':
               // return 'Stock Code';
                return 'Catalogue No';
                
                
            case 'AFN-PRODUCT':
                return 'Product';
                
            case 'AFN-PRODUCT CODE':
                return 'Product Code';   
                                
        }
        
        throw new Exception('Unrecocognised AFN code: '.$id);
        
    }
    
    public function getPage($id, $lang='en') {
        
        $page = $this->controller->readConfig("$lang/$id.ini", INI_SCANNER_RAW);
        
        if ($page === false) {
            // if language file not found - default to English
            $page = $this->controller->readConfig("en/$id.ini", INI_SCANNER_RAW);   
        }
        
	//echo("<pre>"); print_r($this->controller->user); echo("</pre>");
	//echo("<pre>"); print_r($page); echo("</pre>");
        
	if(isset($this->controller->user->AlternativeFields)) {
	    if(isset($page["Labels"])) {
		$page=$this->alternativeFields($page);
	    }
	}

	//echo("<pre>"); print_r($page); echo("</pre>");
	//echo("<pre>"); print_r($this->controller->user->AlternativeFields); echo("</pre>");
		
	return $page;
    
    }
    

    
    // This function looks if there are any alternative fields assigned to current brand
    // If there is - overwrites with values from alternative_fields table
    // 2012-10-06 © Vic <v.rutkunas@pccsuk.com>
    
    public function alternativeFields($page) {
	
	$labels=$page["Labels"];
	
	$altFields=$this->controller->user->AlternativeFields;

	$primaryFields=array();
	foreach($altFields as $key => $value) {
	    $primaryFields[]=$key;
	}

	foreach($labels as $key => $value) {
	    if(in_array($value, $primaryFields)) {
		$page["Labels"][$key]=$altFields[$value];
	    }
	}

	return $page;
	
    }
    
    
    
}
?>
