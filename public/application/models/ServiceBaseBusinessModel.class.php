<?php

require_once('CustomBusinessModel.class.php');
require_once('Functions.class.php');
require_once('Constants.class.php');

/**
 * Short Description of ServiceBase Business Model.
 * 
 * Long description of ServiceBase Business Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.2
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 09/07/2013  1.0     Brian Etherington     Initial Version
 * 17/07/2013  1.1     Brian Etherington     Bug Fix: PutJobDetails
 *                                           return valid response array
 *                                           if platform not = ServiceBase
 * 17/07/2013  1.2     Brian Etherington     Bug Fix: PutJobDetails
 *                                           this->execute returns a 2 dimensional array
 * **************************************************************************** */
class ServiceBaseBusinessModel extends CustomBusinessModel {
    
    private $saved_job = null;
    private $queries;
    private $api;
    
    public function __construct($controller) {
        parent::__construct($controller);
        
        $this->queries = $this->loadModel('ServiceBaseQueries');
        $this->api = $this->loadModel('ServiceBaseAPIClient');
    }
    
    public function FetchSkylineJob($job_id) {
        $this->saved_job = $this->queries->fetchJob($job_id);
        
    }
    
    public function PutJobDetails($job_id) {
        
        $connection = $this->queries->fetchServiceProviderConnectionDetails($job_id);
        //$this->log($connection);
        if ($connection == null) {
            $details = array (
                 'SLNumber' => $job_id,
                 'ASCJobNo' => $this->saved_job['ServiceCentreJobNo'],
                 'RMANumber' => $this->saved_job['RMANumber'],
                 'ResponseCode' => 'SC0005',
                 'ResponseDescription' => 'Service Provider Connection Details not found');
            $response = array('response' => $details,
                              'info' => '',
                              'raw' => '');
        } else if ($connection['Platform'] == 'ServiceBase') {
            /* only send updates to ServiceBase the service provider  platform is marked as ServiceBase */
            //$this->log($this->saved_job);
            $updates = $this->UpdatedFields($job_id);
            //$this->log($updates);
            $this->api->setUsername($connection['Username']);
            $this->api->setPassword($connection['Password']);
            $response = $this->api->PutJobDetails($connection['URL'], $updates);
        } else {
            $details = array (
                 'SLNumber' => $job_id,
                 'ASCJobNo' => $this->saved_job['ServiceCentreJobNo'],
                 'RMANumber' => $this->saved_job['RMANumber'],
                 'ResponseCode' => 'SC0001',
                 'ResponseDescription' => 'Service Provider Platform = '.$connection['Platform']); 
            $response = array('response' => $details,
                              'info' => '',
                              'raw' => '');
        }
        /* Check Response and Log errors*/
        if ($response['response']['ResponseCode'] != 'SC0001') {
            $this->log(__METHOD__,'Errors_');
            $this->log('error uploading job updates to ServiceBase','Errors_');
            $this->log($response,'Errors_');  
        }
        //$this->log($response);
        return $response['response']; 
    }
       
    private function UpdatedFields($job_id) {
        
        $updated_job = $this->queries->fetchJob($job_id);
        if ($this->saved_job == null || $this->saved_job['SLNumber'] != $updated_job['SLNumber']) {
            throw new Exception('Saved JobID '.$this->saved_job['SLNumber'].' does not match Updated '.$updated_job['SLNumber']);
        }
                    
        $updates = array();
        
        // make sure record identification fields are included in update fields
        $updates['ClientAccountNo'] = $updated_job['ClientAccountNo'];
        $updates['SLNumber'] = $updated_job['SLNumber'];
        $updates['RMANumber'] = $updated_job['RMANumber'];
        $updates['SCJobNo'] = $updated_job['SCJobNo'];
        
        // UNIT TEST :-) *********************** //
        // 
        //$updated_job['PolicyNo'] = 'TEST1234';
        //$updated_job['ChargeableLabourCost'] = 100.01;
        //$updated_job['Parts'][2]['Quantity'] = 2;
        //unset($updated_job['Parts'][0]);
       // unset($this->saved_job['Parts'][0]);
       // unset($this->saved_job['StatusUpdates'][0]);
        //unset($this->saved_job['ContactHistory'][0]);
        //unset($this->saved_job['ClaimResponseErrors'][0]);
        // END UNIT TEST :-) *********************** //
        
        $this->log($updated_job);
        
        // check job fields for changes....
        foreach($updated_job as $key => $value) {
            if (!is_array($value) && $this->saved_job[$key] != $value) {
                $updates[$key] = $value;
            } 
        }
        
        $updates['Parts'] = json_encode($this->UpdatedParts($updated_job['Parts']));
        //$updates['StatusUpdates'] = $this->UpdatedStatusHistory($updated_job['StatusUpdates']);
        $updates['ContactHistory'] = json_encode($this->UpdatedContactHistory($updated_job['ContactHistory']));
        $updates['ClaimResponseErrors'] = json_encode($this->UpdatedClaimResponse($updated_job['ClaimResponseErrors']));
        
        return $updates; 
    }
    
    private function UpdatedParts($parts) {
        
        $updated_parts = array();
        
        // check parts for inserts & changes....
        foreach($parts as $parts_row) {
            $part_inserted = true;
            $part_updated = false;
            $updated_parts_row = array('SBPartID' => $parts_row['SBPartID']);
            foreach($this->saved_job['Parts'] as &$saved_parts_row) {
                if ($parts_row['SkylinePartID'] == $saved_parts_row['SkylinePartID']) {
                    $saved_parts_row['#found'] = true;
                    $part_inserted = false;
                    foreach($parts_row as $key => $value) {
                        if (!array_key_exists($key, $saved_parts_row) || $saved_parts_row[$key] !== $value) {
                            $part_updated = true;
                            $updated_parts_row[$key] = $value;
                        }
                    }
                    break;
                }
            }
            if ($part_inserted) {
                // 12/07/2013 only include updated parts.  
                // ignore inserted parts & deleted parts
                //$updated_parts[] = $parts_row;
            } else if ($part_updated) {
                $updated_parts[] = $updated_parts_row;               
            }
        }
        // check for deleted parts...   
        // 12/07/2013 only include updated parts.  
        // ignore inserted parts & deleted parts
        /*foreach($this->saved_job['Parts'] as $parts_row) {
            if (array_key_exists('#found',$parts_row) === false) {
                $updated_parts[] = array('SBPartID' => $parts_row['SBPartID'], 
                                         'Deleted' => 'Y'); 
            }
        }*/
        
        return $updated_parts;
        
    }
    
    /*private function UpdatedStatusHistory($status_history) {
        
        $updated_rows = array();
        
        // check for inserts....
        foreach($status_history as $row) {
            $inserted = true;
            foreach($this->saved_job['StatusUpdates'] as $saved_row) {
                if ($row['SkylineStatusHistoryID'] == $saved_row['SkylineStatusHistoryID']) {
                    $inserted = false;
                    break;
                }
                
            }
            if ($inserted) {
                $updated_rows[] = $row;
            }
        }
        
        return $updated_rows;
        
    }*/
    
    private function UpdatedContactHistory($contact_history) {
        
        $updated_rows = array();
        
        // check for inserts....
        foreach($contact_history as $row) {
            $inserted = true;
            foreach($this->saved_job['ContactHistory'] as $saved_row) {
                if ($row['SkylineContactHistoryID'] == $saved_row['SkylineContactHistoryID']) {
                    $inserted = false;
                    break;
                }
                
            }
            if ($inserted) {
                $updated_rows[] = $row;
            }
        }
        
        return $updated_rows;
        
    }
    
    private function UpdatedClaimResponse($claim_response) {
        
        $updated_rows = array();
        
        // check for inserts....
        foreach($claim_response as $row) {
            $inserted = true;
            foreach($this->saved_job['ClaimResponseErrors'] as $saved_row) {
                if ($row['SkylineClaimResponseID'] == $saved_row['SkylineClaimResponseID']) {
                    $inserted = false;
                    break;
                }
                
            }
            if ($inserted) {
                $updated_rows[] = $row;
            }
        }
        
        return $updated_rows;
        
    }
}
