<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Engineers Page in Appointment Diary section under Site Map
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class Engineers extends CustomModel {
    
    private $conn;
    
    private $table     = "service_provider_engineer";
    
    private $weekArray = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    private $maxNoWeeks = 4;//This is no of weeks.
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    
    
    
        /**
        * Description
        * 
        * This method is for fetching data from database
        * 
        * @param array $args Its an associative array contains where clause, limit and order etc.
        * @global $this->conn
        * @global $this->table
       
        * @return array used for data table.
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */  
    
        public function fetch($args) {

            $ServiceProviderID = isset($args['firstArg'])?$args['firstArg']:false;
            $Status = (isset($args['secondArg']) && $args['secondArg'] )?false:'Active';
           
            if($ServiceProviderID)
            {
                
                $args['where'] = "Deleted='No' AND ServiceProviderID='".$ServiceProviderID."'";
                
                if($Status)
                {
                    $args['where'] = $args['where']." AND Status='".$Status."'";
                }
                
                $args['where'] = $args['where']." ORDER BY CONCAT(UCASE(EngineerFirstName), ' ', UCASE(EngineerLastName))";
                
                //$this->controller->log(var_export($args, true));
                
                $output = $this->ServeDataTables($this->conn, $this->table, array("ServiceProviderEngineerID", "CONCAT(UCASE(EngineerFirstName), ' ', UCASE(EngineerLastName)) As name", "Status"), $args);
                
                return  $output;
               
            }

            

        }
    
    
        
        
        /**
        * Description
        * 
        * This method is for fetching data from database
        * 
        * @param int     $ServiceProviderID 
        * @param string  $Status 
        *  
        * @global $this->table
       
        * @return array 
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */  
    
        public function fetchAll($ServiceProviderID, $Status='All') {

            $result = array();
            
            if($ServiceProviderID)
            {
                
                if($Status=='Active' || $Status='In-active')
                {    
                    $sql = "SELECT ServiceProviderEngineerID, CONCAT(UCASE(EngineerFirstName), ' ', UCASE(EngineerLastName)) As EngineerName FROM ".$this->table." WHERE ServiceProviderID=:ServiceProviderID AND Status=:Status";

                    $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                    $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':Status'=>$Status));
                    
                }
                else
                {
                    
                    $sql = "SELECT ServiceProviderEngineerID, CONCAT(UCASE(EngineerFirstName), ' ', UCASE(EngineerLastName)) As EngineerName FROM ".$this->table." WHERE ServiceProviderID=:ServiceProviderID";

                    $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                    $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID));
                    
                }
                
                $result = $fetchQuery->fetchAll();
                
               
            }
            
            return  $result;

        }
    
        
       /**
        * Description
        * 
        * This method is for fetching enginners who are eligible to replicate their week data.
        * 
        *  
        * @global $this->table
       
        * @return array 
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */  
    
        public function fetchReplicateEngineers() {

                    
                $sql = "SELECT ServiceProviderEngineerID, ReplicateType, ReplicateStatusFlag, ReplicatePostcodeAllocation, ServiceProviderID FROM ".$this->table." WHERE Deleted='No' AND Status='Active' AND SetupType='Replicate' AND (ReplicateType='WeeklyCycle' OR ReplicateType='MonthlyCycle') ORDER BY ReplicateType";

                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                $fetchQuery->execute();
                    
                $result = $fetchQuery->fetchAll();
                
                return  $result;

        } 
        
        
        
        
        
         /**
        * Description
        * 
        * This method is for to fetch enginners skills set.
        * 
        * @param int $ServiceProviderEngineerDetailsID
        * 
        * @global $this->table
       
        * @return array 
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */  
    
        public function fetchEngineerSkillSet($ServiceProviderEngineerDetailsID) {

               $result = array();

               //Getting model details.
               $sql2        = "SELECT ServiceProviderSkillsetID FROM service_provider_engineer_skillset_day WHERE ServiceProviderEngineerDetailsID=:ServiceProviderEngineerDetailsID";
               $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
               $fetchQuery2->execute(array(':ServiceProviderEngineerDetailsID' => $ServiceProviderEngineerDetailsID));
               $result2     = $fetchQuery2->fetchAll();

               foreach($result2 as $esi)
               {
                   $result[] = $esi['ServiceProviderSkillsetID'];
               } 
               
               return  $result;

        }
        
        
        
    
   
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['ServiceProviderEngineerID']) || !$args['ServiceProviderEngineerID'])
         {
               return $this->create($args);
         }
         else
         {
             if(isset($args['Deleted']) && $args['Deleted']!='')
             {
                 return $this->delete($args);
             }
             else
             {
                return $this->update($args);
             }
         }
     }
    
    
     
     
       /**
     * Description
     * 
     * This method is used for to check whether ServiceBaseUserCode exists for service provider.
     *
     * @param interger $ServiceProviderID
     * @param interger $EngineerFirstName
     * @param interger $EngineerLastName 
     * @param string   $ServiceProviderEngineerID
    
     * @global $this->table
     * 
     * @return boolean 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($ServiceProviderID, $EngineerFirstName, $EngineerLastName, $ServiceProviderEngineerID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderEngineerID FROM '.$this->table.' WHERE ServiceProviderID=:ServiceProviderID AND Deleted=:Deleted AND EngineerFirstName=:EngineerFirstName AND EngineerLastName=:EngineerLastName AND ServiceProviderEngineerID!=:ServiceProviderEngineerID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceProviderID' => $ServiceProviderID, ':Deleted' => 'No', ':EngineerFirstName' => strtoupper($EngineerFirstName), ':EngineerLastName' => strtoupper($EngineerLastName), ':ServiceProviderEngineerID' => $ServiceProviderEngineerID));
        $result = $fetchQuery->fetch();
        
//        $this->controller->log(var_export("ServiceProviderID: ".$ServiceProviderID, true));
//        $this->controller->log(var_export("ServiceBaseUserCode: ".$ServiceBaseUserCode, true));
//        $this->controller->log(var_export("ServiceProviderEngineerID: ".$ServiceProviderEngineerID, true));
//        $this->controller->log(var_export($result, true));
        
        if(is_array($result) && $result['ServiceProviderEngineerID'])
        {
                return false;
        }

        return true;
        
    }
     
     
    
    
    
     /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        //Validaing postcodes for active days...
        $start_date_time = strtotime($args['start_date']);
        
        
        
        $PostcodeLatLongModel = $this->controller->loadModel("PostcodeLatLong");
        $Days = array();
        $WeekErrors = array();
        $SkillSetsError = array();
        $SSWeekErrors = array();
        $ShiftError   = array();
        $ShiftWeekErrors = array();
        
        if($args['SetupType']=="Unique")
        {
            $args['ReplicateType'] = 'FourWeeksOnly';
        }    
        
        if($args['ReplicateStatusFlag']!='Yes')
        {
            $args['ReplicateStatusFlag'] = 'No';
        }
        
        if($args['ReplicatePostcodeAllocation']!='Yes')
        {
            $args['ReplicatePostcodeAllocation'] = 'No';
        }
        
        
        for($i=1;$i<=$this->maxNoWeeks;$i++)
        {
            $j=0;
            foreach($this->weekArray as $week)
            {
                
                //Replicate to  Current 4 Weeks Only/future weeks (weekly cycle)...
                if($args['SentToViamente']=='Yes' && $args['SetupType']=="Replicate" && ($args['ReplicateType']=="FourWeeksOnly" || $args['ReplicateType']=="WeeklyCycle"))
                {
                    $args[$week.'StartShift'.$i]    = $args[$week.'StartShift1'];
                    $args[$week.'EndShift'.$i]      = $args[$week.'EndShift1'];
                    $args[$week.'StartPostcode'.$i] = $args[$week.'StartPostcode1'];
                    $args[$week.'EndPostcode'.$i]   = $args[$week.'EndPostcode1'];
                    $args[$week.'SkillsSet'.$i]     = (isset($args[$week.'SkillsSet1']))?$args[$week.'SkillsSet1']:array();
                
                    if($args['ReplicateStatusFlag']=='Yes' && isset($args[$week.'Active1']))
                    {
                            $args[$week.'Active'.$i] = 'Active';
                    }    
                }
                
                
                
                $WorkDateTime = strtotime("+".((($i-1)*7)+$j)." day", $start_date_time);
                $WorkDate     = date("Y-m-d", $WorkDateTime);
                $WorkWeek     = date("W", $WorkDateTime);
                
                
                if(isset($args[$week.'Active'.$i]) && $args['Status']=='Active')
                {
                        if($args[$week.'StartPostcode'.$i])
                        {        
                              if(!preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $args[$week.'StartPostcode'.$i])){
                           $validPostcode =  $PostcodeLatLongModel->getLatLong($args[$week.'StartPostcode'.$i]);

                            if($validPostcode['latitude']== -1 && $validPostcode['longitude'] == -1)
                            {
                                $Days[]       = $WorkDate;
                                $WeekErrors[] = $WorkWeek;
                            }    

                        }}
                        else
                        {
                            $Days[] = $WorkDate;
                            $WeekErrors[] = $WorkWeek;
                        }

                        if($args[$week.'EndPostcode'.$i])
                        {        
                            if(!preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $args[$week.'EndPostcode'.$i])){
                                
                            
                           $validPostcode =  $PostcodeLatLongModel->getLatLong($args[$week.'EndPostcode'.$i]);
                            
                            if($validPostcode['latitude']== -1 && $validPostcode['longitude'] == -1)
                            {
                                $Days[]       = $WorkDate;
                                $WeekErrors[] = $WorkWeek;
                            } 
                            }

                        }
                        else
                        {
                            $Days[] = $WorkDate;
                            $WeekErrors[] = $WorkWeek;
                        }
                    
                       //Validating skill sets
                        if(!isset($args[$week.'SkillsSet'.$i]))
                        {
                            $SkillSetsError[] = $WorkDate;
                            $SSWeekErrors[] = $WorkWeek;
                        }
                        
                        //Validating Start and End Shift..
                        if($args[$week.'StartShift'.$i]=="00:00" || !$this->isTime($args[$week.'StartShift'.$i]) || !$this->isTime($args[$week.'EndShift'.$i]) || $args[$week.'EndShift'.$i]=="00:00")
                        {
                            $ShiftError[] = $WorkDate;
                            $ShiftWeekErrors[] = $WorkWeek;
                        }
                    
                    
                }
               
               $j++; 
            }
            
        }
        
        if(count($Days))//Postcode Error message..stars here..
        {    
            
            $WeekErrors =  array_unique ( $WeekErrors );

            if(count($WeekErrors)==1)
            {
                $WeekErrorsString =  "( Week ".implode(", ", $WeekErrors)." )";  
            }   
            else
            {
               $WeekErrorsString =  "( Weeks ".implode(", ", $WeekErrors)." )";  
            }   
            
            return array(

                       'status' => 'ERROR',
                       'message' => "The postcodes you have entered are not valid for days ".$WeekErrorsString." which are in red.",
                       'days' => $Days

                       );
        
        
        }//Postcode Error message..ends here..
        

        //Skill set error message - starts here..
        if(count($SkillSetsError))
        {
            $WeekErrorsString = '';


            $SSWeekErrors =  array_unique ( $SSWeekErrors );

            if(count($SSWeekErrors)==1)
            {
                $WeekErrorsString =  "( Week ".implode(", ", $SSWeekErrors)." )";  
            }   
            else
            {
               $WeekErrorsString =  "( Weeks ".implode(", ", $SSWeekErrors)." )";  
            }

            return array(

                       'status' => 'ERROR',
                       'message' => "No Skill Sets are selected for the days ".$WeekErrorsString." which are in red.",
                       'days' => $SkillSetsError

                       );

        }  
        //Skill set error message - ends here..

        //Shift error message - starts here..
        if(count($ShiftError))
        {
            $WeekErrorsString = '';


            $ShiftWeekErrors =  array_unique ( $ShiftWeekErrors );

            if(count($ShiftWeekErrors)==1)
            {
                $WeekErrorsString =  "( Week ".implode(", ", $ShiftWeekErrors)." )";  
            }   
            else
            {
               $WeekErrorsString =  "( Weeks ".implode(", ", $ShiftWeekErrors)." )";  
            }



            return array(

                       'status' => 'ERROR',
                       'message' => "No Engineer Start and/or End Shift Time has been defined for the days ".$WeekErrorsString." which are in red.",
                       'days' => $ShiftError

                       );

        } 
        //Shift error message - ends here..

        
        
        
        
         //if($this->isValid($args['ServiceProviderID'], $args['ServiceBaseUserCode'], 0))
         if($this->isValid($args['ServiceProviderID'], $args['EngineerFirstName'], $args['EngineerLastName'], 0))
         {
               
                
                
                    
                /* Execute a prepared statement by passing an array of values */

                $sql = 'INSERT INTO '.$this->table.' (ServiceProviderID, EngineerFirstName, EngineerLastName, ServiceBaseUserCode, EmailAddress, MobileNumber, RouteColour, SpeedFactor, LunchBreakDuration, LunchPeriodFrom, LunchPeriodTo, Status, StartHomePostcode, EndHomePostcode, SentToViamente, ViamenteExclude,ExcludeFromAppTable,ViamenteStartType, SetupType, ReplicateType, ReplicateStatusFlag, ReplicatePostcodeAllocation,AppsBeforeOptimise,AvarageAppPerHour,PrimarySkill)
                VALUES(:ServiceProviderID, :EngineerFirstName, :EngineerLastName, :ServiceBaseUserCode, :EmailAddress, :MobileNumber, :RouteColour, :SpeedFactor, :LunchBreakDuration, :LunchPeriodFrom, :LunchPeriodTo, :Status, :StartHomePostcode, :EndHomePostcode, :SentToViamente,:ViamenteExclude,:ExcludeFromAppTable,:ViamenteStartType, :SetupType, :ReplicateType, :ReplicateStatusFlag, :ReplicatePostcodeAllocation,:AppsBeforeOptimise,:AvarageAppPerHour,:PrimarySkill)';

  
                
                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                $result =  $insertQuery->execute(array(

                    ':ServiceProviderID' => $args['ServiceProviderID'],
                    ':EngineerFirstName' => strtoupper($args['EngineerFirstName']), 
                    ':EngineerLastName' => strtoupper($args['EngineerLastName']), 
                    ':ServiceBaseUserCode' => strtoupper($args['ServiceBaseUserCode']), 
                    ':EmailAddress' => $args['EmailAddress'], 
                    ':MobileNumber' => $args['MobileNumber'], 
                    ':RouteColour' => $args['RouteColour'], 
                    ':SpeedFactor' => $args['SpeedFactor'],                     
                    ':LunchBreakDuration' => $args['LunchBreakDuration'],
                    ':LunchPeriodFrom' => $args['LunchPeriodFrom'],
                    ':LunchPeriodTo' => $args['LunchPeriodTo'],
                    ':Status' => $args['Status'],
                    ':StartHomePostcode' => $args['StartHomePostcode'],
                    ':EndHomePostcode' => $args['EndHomePostcode'],
                    ':SentToViamente' => $args['SentToViamente'],
                    ':ViamenteExclude' => $args['ViamenteExclude'],
                    ':ExcludeFromAppTable' => $args['ExcludeFromAppTable'],
                    ':ViamenteStartType' => $args['ViamenteStartType'],
                    ':SetupType' => $args['SetupType'],
                    ':ReplicateType' => $args['ReplicateType'],
                    ':ReplicateStatusFlag' => $args['ReplicateStatusFlag'],
                    ':AppsBeforeOptimise' => $args['AppsBeforeOptimise'],
                    ':AvarageAppPerHour' => $args['AvarageAppPerHour'],
                    ':PrimarySkill' => $args['PrimarySkill'],
                    ':ReplicatePostcodeAllocation' => $args['ReplicatePostcodeAllocation']
                    
                    
                    
                    ));
                

                if($result)
                {
                      $ServiceProviderEngineerID = $this->conn->lastInsertId();
                     
                      
                     
                      
//                      if(isset($args['RollingMonth']) && $args['RollingMonth'])
//                      {
//                          $maxWeeks = 8;
//                      }    
//                      else
//                      {
                          $maxWeeks  = 4;
                      //}
                      
                      for($w=1;$w<=$maxWeeks;$w++)
                      {
                            $i = $w%4;
                            if(!$i)
                            {
                                $i = 4;
                            }    
                          
                            $j=0;
                            foreach($this->weekArray as $week)
                            {
                                  if(isset($args[$week.'Active'.$i]) && $args[$week.'Active'.$i]!='In-active' && $args['Status']=='Active')
                                  {
                                      $args[$week.'Active'.$i] = "Active";
                                  }
                                  else
                                  {
                                      $args[$week.'Active'.$i] = "In-active";
                                  }

                                  $WorkDate  = date("Y-m-d", strtotime("+".((($w-1)*7)+$j)." day", $start_date_time));
                                  
                                  $DataArray = array('ServiceProviderEngineerID'=>$ServiceProviderEngineerID, 'WorkDate'=>$WorkDate, 'StartShift'=>$args[$week.'StartShift'.$i], 'EndShift'=>$args[$week.'EndShift'.$i], 'StartPostcode'=>$args[$week.'StartPostcode'.$i], 'EndPostcode'=>$args[$week.'EndPostcode'.$i], 'Status'=>$args[$week.'Active'.$i]);

                                  $ServiceProviderEngineerDetailsID = $this->processEngineerDetails($DataArray);
                                  
                                  if($ServiceProviderEngineerDetailsID)
                                  {
                                      // Insert engineer skill set.
                                      if(!isset($args[$week.'SkillsSet'.$i]))
                                      {
                                          $args[$week.'SkillsSet'.$i] = array();
                                      }

                                      $this->insertSkillSet($args[$week.'SkillsSet'.$i], $ServiceProviderEngineerDetailsID);
                                  }    

                                  $j++;        
                            }
                     
                      }
                    
                      
                      //Replicating postcode allocation - starts here..
                      if($args['ReplicatePostcodeAllocation']=='Yes' && $args['SentToViamente']=='Yes' && $args['SetupType']=="Replicate" && ($args['ReplicateType']=="FourWeeksOnly" || $args['ReplicateType']=="WeeklyCycle"))
                      {
                          
                          $this->replicatePostcodeAllocation($ServiceProviderEngineerID, $args['ServiceProviderID']);
                          
                      }
                      //Replicating postcode allocation - ends here..
                        
                    
                     
                     //We are creating engineers in Viamente only if they are active.
                     if($args['Status']=='Active' && $args['SentToViamente']=='Yes')
                     {
                          if(!isset($args['defaultsOnly'])){
                    $api_viamente=$this->controller->loadModel('APIViamente');
                       

                      
                   $api_viamente->ProcessEngineer($ServiceProviderEngineerID, 'create');
                       $this->controller->log("via - create");
                          }
                     }   
                     
                     
                     return array(
                         
                                'status' => 'OK',
                                'message' => "Your data has been inserted successfully.",
                                'eID'=>$ServiceProviderEngineerID
                         
                         );
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => "Sorry, your data has not been processed since there is a problem.  Please check it.");
                }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to replicate postcode allocation.
     *
     
     * @param  int   $ServiceProviderEngineerID
     * @param  int   $ServiceProviderID
     * @param  int   $CronJob //This variable is true when method is called from cron job.
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function replicatePostcodeAllocation($ServiceProviderEngineerID, $ServiceProviderID, $CronJob=false) {
    
        
        $ServiceProvidersModel             = $this->controller->loadModel('ServiceProviders');
        $ServiceProviderDetails            = $ServiceProvidersModel->fetchRow(array('ServiceProviderID'=>$ServiceProviderID));
        
    
        $ServiceProviderSkillset             = $this->controller->loadModel('ServiceProviderSkillsSet');
        $AppointmentAllocationSlots          = $ServiceProviderSkillset->getAppointmentAllocationSlot($ServiceProviderID, true);

        
        
        
//        $this->log("AppointmentAllocationSlots:");
//        $this->log($AppointmentAllocationSlots);
        
       $apCntTotal = 0;
       
       if(isset($ServiceProviderDetails['SetupUniqueTimeSlotPostcodes']) && $ServiceProviderDetails['SetupUniqueTimeSlotPostcodes']=='Yes')
       {
           $apCntTotal = count($AppointmentAllocationSlots);
       } 
       else if(count($AppointmentAllocationSlots)>=1)
       {
           $apCntTotal = 1;
       }    
        
       for($apCnt=0;$apCnt<$apCntTotal;$apCnt++)
       {
           
            $AppointmentAllocationSlotID = isset($AppointmentAllocationSlots[$apCnt][0])?$AppointmentAllocationSlots[$apCnt][0]:false;

            if($AppointmentAllocationSlotID)
            {
                $AllocatedDateTemp   = date("Y-m-d", strtotime('monday this week'));

                $AllocatedDate   = date("d/m/Y", strtotime('monday this week'));
                $AllocatedToDate = date('d/m/Y', strtotime('+6 days, '.$AllocatedDateTemp));



                $result  = $ServiceProviderSkillset->getDiaryAllocation($ServiceProviderEngineerID, $AllocatedDate, $AllocatedToDate, $AppointmentAllocationSlotID);

                //$this->log($result);

                if(count($result))    
                {    
                    $postcodeAllocationData = array();

                    if($CronJob)
                    {
                        $postcodeAllocationData['AllocatedDate']   = date('d/m/Y', strtotime('+28 days, '.$AllocatedDateTemp));
                        $postcodeAllocationData['AllocatedToDate'] = date('d/m/Y', strtotime('+34 days, '.$AllocatedDateTemp));

                        $noOfWeeks = 1;
                    }   
                    else
                    {
                        $postcodeAllocationData['AllocatedDate']   = date('d/m/Y', strtotime('+7 days, '.$AllocatedDateTemp));
                        $postcodeAllocationData['AllocatedToDate'] = date('d/m/Y', strtotime('+27 days, '.$AllocatedDateTemp));

                        $noOfWeeks = 3;
                    }

                    $postcodeAllocationData['ReplicateWeeks']  = 0; 
                    
                    
                    if(isset($ServiceProviderDetails['SetupUniqueTimeSlotPostcodes']) && $ServiceProviderDetails['SetupUniqueTimeSlotPostcodes']=='Yes')
                    {
                        $postcodeAllocationData['ReplicateToOtherSlot']  = 0;
                    }
                    else
                    {
                        $postcodeAllocationData['ReplicateToOtherSlot']  = 1;
                    }
                    
                      

                    for($i=1;$i<=$noOfWeeks;$i++)
                    {
                        for($j=1;$j<=7;$j++)
                        {
                            $v = ((($i-1)*7)+$j);

                            $postcodeAllocationData['hiddenAllocatedSlots_'.$v]  = isset($result[$j]['NumberOfAllocations'])?$result[$j]['NumberOfAllocations']:0; 
                            $postcodeAllocationData['hiddenPostcode_'.$v]        = isset($result[$j]['PostCode'])?$result[$j]['PostCode']:''; 
                        }
                    }

                    $postcodeAllocationData['ServiceProviderEngineerID']   = $ServiceProviderEngineerID;
                    $postcodeAllocationData['AppointmentAllocationSlotID'] = $AppointmentAllocationSlotID;
                    $postcodeAllocationData['ServiceProviderID']           = $ServiceProviderID;

                    $postcodeAllocationData['SkillsAreaMap'] = 1;


                    $this->log($postcodeAllocationData,"pallcoation____");

                    $ServiceProviderSkillset->processData($postcodeAllocationData);

                }

               // $this->log('dddsssss');
               // $this->log($result);

            }   
        
        }
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert service provider engineer skill set.
     *
     * @param  array $skillSet
     * @param  int   $ServiceProviderEngineerDetailsID
     
     * 
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function insertSkillSet($skillSet, $ServiceProviderEngineerDetailsID) {
        
        
        if($ServiceProviderEngineerDetailsID)
        {    
            /* Execute a prepared statement by passing an array of values */
           $sql = 'DELETE FROM service_provider_engineer_skillset_day WHERE ServiceProviderEngineerDetailsID=:ServiceProviderEngineerDetailsID';
           $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


           $deleteQuery->execute(array(':ServiceProviderEngineerDetailsID' => $ServiceProviderEngineerDetailsID));
           
           if(is_array($skillSet))
           {
                $sql = 'INSERT INTO service_provider_engineer_skillset_day (ServiceProviderEngineerDetailsID, ServiceProviderSkillsetID) VALUES (:ServiceProviderEngineerDetailsID, :ServiceProviderSkillsetID)';
                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                foreach($skillSet as $skSetValue)
                {    
                    $insertQuery->execute(array(':ServiceProviderEngineerDetailsID' => $ServiceProviderEngineerDetailsID, ':ServiceProviderSkillsetID'=>$skSetValue));
                }
           }
        }
        
    }
    
    
     /**
     * Description
     * 
     * This method is used for to update status for service provider engineer details.
     *
     * @param  string $Status
     * @param  date $WorkDate
     * @param  int $ServiceProviderEngineerID  
          
     * return int $ServiceProviderEngineerDetailsID
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateEngineerDetailsStatus($Status, $WorkDate, $ServiceProviderEngineerID) {
    
        
        
            $DataArray = array('ServiceProviderEngineerID'=>$ServiceProviderEngineerID, 'WorkDate'=>$WorkDate, 'StartShift'=>'00:00:00', 'EndShift'=>'00:00:00', 'StartPostcode'=>'', 'EndPostcode'=>'', 'Status'=>$Status);

            $ServiceProviderEngineerDetailsID = $this->processEngineerDetails($DataArray);
            return $ServiceProviderEngineerDetailsID;
        
    }
    
    
    
    
    
    /**
     * updateEngineerReplicateWeekSettings
     * 
     * Description
     * 
     * This method is used for to update engineer replicate week settings.
     *
     * @param  array $args
    
     * return void
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateEngineerReplicateWeekSettings($args) {
    
        
            $sql = 'UPDATE service_provider_engineer SET SetupType=:SetupType, ReplicateType=:ReplicateType, ReplicateStatusFlag=:ReplicateStatusFlag, ReplicatePostcodeAllocation=:ReplicatePostcodeAllocation WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID';
               $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                   
                $updateQuery->execute(
                      
                        array(
                            
                             ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'], 
                             ':SetupType' => $args['SetupType'], 
                             ':ReplicateType' => $args['ReplicateType'], 
                             ':ReplicateStatusFlag' => $args['ReplicateStatusFlag'], 
                             ':ReplicatePostcodeAllocation' => $args['ReplicatePostcodeAllocation']
                         )
                        
                        );
                
    }
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to insert/update service provider engineer details.
     *
     * @param  array $DataArray
     
     
     * return int $ServiceProviderEngineerDetailsID
    
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function processEngineerDetails($DataArray) {
        
        
        $EngDetails = $this->fetchEngineerDetails($DataArray);
        
        if(isset($EngDetails['ServiceProviderEngineerDetailsID']) && $EngDetails['ServiceProviderEngineerDetailsID'])
        {  
               $sql = 'UPDATE service_provider_engineer_details SET StartShift=:StartShift, EndShift=:EndShift, StartPostcode=:StartPostcode, EndPostcode=:EndPostcode, Status=:Status WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND WorkDate=:WorkDate AND ServiceProviderEngineerDetailsID=:ServiceProviderEngineerDetailsID';
               $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                   
                $updateQuery->execute(
                      
                        array(
                            
                            ':ServiceProviderEngineerID' => $DataArray['ServiceProviderEngineerID'], 
                            ':WorkDate'=>$DataArray['WorkDate'],
                            ':StartShift'=>$DataArray['StartShift'],
                            ':EndShift'=>$DataArray['EndShift'],
                            ':StartPostcode'=>  strtoupper($DataArray['StartPostcode']),
                            ':EndPostcode'=> strtoupper($DataArray['EndPostcode']),
                            ':Status'=>$DataArray['Status'],
                            ':ServiceProviderEngineerDetailsID'=>$EngDetails['ServiceProviderEngineerDetailsID']
                    
                         )
                        
                        );
                
              $ServiceProviderEngineerDetailsID = $EngDetails['ServiceProviderEngineerDetailsID'];
        }
        else
        {
           
                $sql = 'INSERT INTO service_provider_engineer_details (ServiceProviderEngineerID, WorkDate, StartShift, EndShift, StartPostcode, EndPostcode, Status) VALUES (:ServiceProviderEngineerID, :WorkDate, :StartShift, :EndShift, :StartPostcode, :EndPostcode, :Status)';
                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                   
                $insertQuery->execute(
                      
                        array(
                            
                            ':ServiceProviderEngineerID' => $DataArray['ServiceProviderEngineerID'], 
                            ':WorkDate'=>$DataArray['WorkDate'],
                            ':StartShift'=>$DataArray['StartShift'],
                            ':EndShift'=>$DataArray['EndShift'],
                            ':StartPostcode'=>strtoupper($DataArray['StartPostcode']),
                            ':EndPostcode'=>strtoupper($DataArray['EndPostcode']),
                            ':Status'=>$DataArray['Status']
                    
                         )
                        
                        );
                
              $ServiceProviderEngineerDetailsID = $this->conn->lastInsertId();
        }
        
        
        return $ServiceProviderEngineerDetailsID; 
    }
    
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
    
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */

        
        $sql='SELECT ServiceProviderEngineerID,
            ServiceProviderID, UCASE(EngineerFirstName) AS EngineerFirstName, 
            UCASE(EngineerLastName) AS EngineerLastName, UCASE(ServiceBaseUserCode) AS ServiceBaseUserCode, 
            EmailAddress, MobileNumber, RouteColour, SpeedFactor, LunchBreakDuration, LunchPeriodFrom, 
            LunchPeriodTo, StartHomePostcode, EndHomePostcode, Status, SentToViamente, EngineerImportance,
            ViamenteExclude,ExcludeFromAppTable,ViamenteStartType, SetupType, ReplicateType, ReplicateStatusFlag,
            ReplicatePostcodeAllocation,AppsBeforeOptimise,AvarageAppPerHour,PrimarySkill,
             (select NumberOfVehicles-(select count(*)  from service_provider_engineer spe where spe.PrimarySkill="Brown Goods"  and spe.ServiceProviderID=sp.ServiceProviderID ) from service_provider sp join service_provider_engineer eng where eng.ServiceProviderEngineerID=:ServiceProviderEngineerID and sp.ServiceProviderID=eng.ServiceProviderID) as BrownEngLeft
             ,(select ViamenteKey2MaxEng-(select count(*)  from service_provider_engineer spe where spe.PrimarySkill="White Goods" and spe.ServiceProviderID=sp.ServiceProviderID ) from service_provider sp join service_provider_engineer eng where eng.ServiceProviderEngineerID=:ServiceProviderEngineerID and sp.ServiceProviderID=eng.ServiceProviderID) as WhiteEngLeft

FROM '.$this->table.' WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID';
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID']));
        $result = $fetchQuery->fetch();
        
        
        if(is_array($result) && isset($result['ServiceProviderEngineerID']))
        {
            
            $start_date_time = strtotime('monday this week'); 
            
            $result['start_date'] = date("Y-m-d", $start_date_time);
                     
            for($i=1;$i<=$this->maxNoWeeks;$i++)
            {
                  $j=0;
                  $DataArrayMain = array();
                  
                  foreach($this->weekArray as $week)
                  {
                        
                        $WorkDate  = date("Y-m-d", strtotime("+".((($i-1)*7)+$j)." day", $start_date_time));

                        
                        $EngDetails =  $this->fetchEngineerDetails(array("ServiceProviderEngineerID"=>$result['ServiceProviderEngineerID'], 'WorkDate'=>$WorkDate));
                        
                        if(isset($EngDetails['ServiceProviderEngineerDetailsID']) && $EngDetails['ServiceProviderEngineerDetailsID'])
                        {
                            
                            $EngDetails['StartShift'] = substr($EngDetails['StartShift'], 0, -3);
                            $EngDetails['EndShift'] = substr($EngDetails['EndShift'], 0, -3);
                            
                            $DataArray = array($week.'Date'=>date("Y-m-d", strtotime($WorkDate)), $week.'Label'=>date("D d M", strtotime($WorkDate)), $week.'StartShift'=>$EngDetails['StartShift'], $week.'EndShift'=>$EngDetails['EndShift'], $week.'StartPostcode'=>$EngDetails['StartPostcode'], $week.'EndPostcode'=>$EngDetails['EndPostcode'], $week.'Active'=>$EngDetails['Status']);
                        
                            
                            $DataArray[$week.'SkillsSet'] = array();
            
                            //Getting model details.
                            $sql2        = "SELECT ServiceProviderSkillsetID FROM service_provider_engineer_skillset_day WHERE ServiceProviderEngineerDetailsID=:ServiceProviderEngineerDetailsID";
                            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                            $fetchQuery2->execute(array(':ServiceProviderEngineerDetailsID' => $EngDetails['ServiceProviderEngineerDetailsID']));
                            $result2     = $fetchQuery2->fetchAll();

                            foreach($result2 as $esi)
                            {
                                $DataArray[$week.'SkillsSet'][] = $esi['ServiceProviderSkillsetID'];
                            } 
                            
                            
                        }    
                        else
                        {
                            $DataArray = array($week.'SkillsSet'=>array(), $week.'Date'=>date("Y-m-d", strtotime($WorkDate)), $week.'Label'=>date("D d M", strtotime($WorkDate)), $week.'StartShift'=>'', $week.'EndShift'=>'', $week.'StartPostcode'=>'', $week.'EndPostcode'=>'', $week.'Active'=>'In-active');
                        }
                        
                        $DataArrayMain = array_merge($DataArrayMain, $DataArray) ;
                        
                        $j++;        
                  }
                  
                  
                  $DataArrayMain["WeekClassName"] = "weekButtons";
                  $DataArrayMain["WeekName"]      = "Week ".date("W", strtotime("+".(($i-1)*7)." day", $start_date_time));
                 
                        
                  $result[$i] = $DataArrayMain;

            }
            
            
               
        }
        
        $this->controller->log($result,"rrr");
        
        return $result;
     }
    
     
     
     
     
      /**
     * Description
     * 
     * This method is used for to get list of engineers who are eligible for given appointment details.
     *
     * @param  array 
       
    
     * 
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getEligibleEngineers($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
         
        $sql = "SELECT T1.`ServiceProviderEngineerID`, CONCAT_WS(' ', UCASE(T1.`EngineerFirstName`), UCASE(T1.`EngineerLastName`)) AS Name
        FROM `service_provider_engineer` AS T1
        LEFT JOIN service_provider_engineer_details AS T2 ON T1.ServiceProviderEngineerID = T2.ServiceProviderEngineerID
        LEFT JOIN service_provider_engineer_skillset_day AS T3 ON T2.ServiceProviderEngineerDetailsID = T3.ServiceProviderEngineerDetailsID
        WHERE T1.`ServiceProviderID`=:ServiceProviderID AND  T1.`ServiceProviderEngineerID`!=:ServiceProviderEngineerID AND T2.WorkDate=:WorkDate  AND T2.Status=:Status  AND T1.Status=:Status AND T3.ServiceProviderSkillsetID=:ServiceProviderSkillsetID";
         
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceProviderID' => $args['ServiceProviderID'], ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'], ':WorkDate' => $args['WorkDate'], ':Status' => $args['Status'], ':ServiceProviderSkillsetID' => $args['ServiceProviderSkillsetID']));
        
        
        $result = $fetchQuery->fetchAll();
        
        $this->controller->log("tessss");
        $this->controller->log($result);
        
        return $result;
       
     }
     
     
     
     
     
     
     
     
     
      /**
     * Description
     * 
     * This method is used for to fetch a row from service_provider_engineer_details.
     *
     * @param  array $args
       
    
     * 
     * @return array It contains row for given service provider id and work date.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchEngineerDetails($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderEngineerDetailsID, ServiceProviderEngineerID, WorkDate, StartShift, EndShift, StartPostcode, EndPostcode, Status FROM service_provider_engineer_details WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND WorkDate=:WorkDate';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'], ':WorkDate' => $args['WorkDate']));
        $result = $fetchQuery->fetch();
        
        
        if(is_array($result) && isset($result['ServiceProviderEngineerDetailsID']))
        {
             return $result;
        }
        else
        {
            return array();
        }
        
       
     }
     
     
     /**
     * Validates Time in 24 hour format (e.g. 20:32).
     *
     * @param string $time Name of submitted value to be checked.
     */
      public function isTime($time)
      {
                return preg_match("#([0-1]{1}[0-9]{1}|[2]{1}[0-3]{1}):[0-5]{1}[0-9]{1}#", $time);
      }
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        
            //Validaing postcodes for active days...
            $start_date_time = strtotime($args['start_date']);


            $PostcodeLatLongModel = $this->controller->loadModel("PostcodeLatLong");
            $Days = array();
            $WeekErrors = array();
            $SkillSetsError = array();
            $SSWeekErrors = array();
            $ShiftError   = array();
            $ShiftWeekErrors = array();
            
            
            if($args['SetupType']=="Unique")
            {
                $args['ReplicateType'] = 'FourWeeksOnly';
            }
            
            if($args['ReplicateStatusFlag']!='Yes')
            {
                $args['ReplicateStatusFlag'] = 'No';
            }
            
            if($args['ReplicatePostcodeAllocation']!='Yes')
            {
                $args['ReplicatePostcodeAllocation'] = 'No';
            }
            
            

            for($i=1;$i<=$this->maxNoWeeks;$i++)
            {
                $j=0;
                foreach($this->weekArray as $week)
                {
                    
                    //Replicate to  Current 4 Weeks Only/future weeks (weekly cycle)...
                    if($args['SentToViamente']=='Yes' && $args['SetupType']=="Replicate" && ($args['ReplicateType']=="FourWeeksOnly" || $args['ReplicateType']=="WeeklyCycle"))
                    {
                        $args[$week.'StartShift'.$i]    = $args[$week.'StartShift1'];
                        $args[$week.'EndShift'.$i]      = $args[$week.'EndShift1'];
                        $args[$week.'StartPostcode'.$i] = $args[$week.'StartPostcode1'];
                        $args[$week.'EndPostcode'.$i]   = $args[$week.'EndPostcode1'];
                        $args[$week.'SkillsSet'.$i]     = (isset($args[$week.'SkillsSet1']))?$args[$week.'SkillsSet1']:array();
                    
                        if($args['ReplicateStatusFlag']=='Yes' && isset($args[$week.'Active1']))
                        {
                                $args[$week.'Active'.$i] = 'Active';
                        }
                    
                    }
                  
                    

                    $WorkDateTime = strtotime("+".((($i-1)*7)+$j)." day", $start_date_time);
                    
                    $WorkDate     = date("Y-m-d", $WorkDateTime);
                    $WorkWeek     = date("W", $WorkDateTime);


                    if(isset($args[$week.'Active'.$i]) && $args['Status']=='Active')
                    {
                        
                        //Validating postcodes..
                       if($args[$week.'StartPostcode'.$i])
                        {        
                              if(!preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $args[$week.'StartPostcode'.$i])){
                           $validPostcode =  $PostcodeLatLongModel->getLatLong($args[$week.'StartPostcode'.$i]);

                            if($validPostcode['latitude']== -1 && $validPostcode['longitude'] == -1)
                            {
                                $Days[]       = $WorkDate;
                                $WeekErrors[] = $WorkWeek;
                            }    

                        }}
                        else
                        {
                            $Days[] = $WorkDate;
                            $WeekErrors[] = $WorkWeek;
                        }

                        if($args[$week.'EndPostcode'.$i])
                        {        
                            if(!preg_match("^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$^", $args[$week.'EndPostcode'.$i])){
                                
                            
                           $validPostcode =  $PostcodeLatLongModel->getLatLong($args[$week.'EndPostcode'.$i]);
                            
                            if($validPostcode['latitude']== -1 && $validPostcode['longitude'] == -1)
                            {
                                $Days[]       = $WorkDate;
                                $WeekErrors[] = $WorkWeek;
                            } 
                            }

                        }
                        else
                        {
                            $Days[] = $WorkDate;
                            $WeekErrors[] = $WorkWeek;
                        }
                        
                        
                        //Validating skill sets
                        if(!isset($args[$week.'SkillsSet'.$i]))
                        {
                            $SkillSetsError[] = $WorkDate;
                            $SSWeekErrors[] = $WorkWeek;
                        }
                        
                        //Validating Start and End Shift..
                        if($args[$week.'StartShift'.$i]=="00:00" || !$this->isTime($args[$week.'StartShift'.$i]) || !$this->isTime($args[$week.'EndShift'.$i]) || $args[$week.'EndShift'.$i]=="00:00")
                        {
                            $ShiftError[] = $WorkDate;
                            $ShiftWeekErrors[] = $WorkWeek;
                        }
                        
                        
                    }

                     $j++;
                }
               
            }

            if(count($Days))//Postcode Error message..stars here..
            {    
                
               // $this->controller->log($Days);
                
                $WeekErrors =  array_unique ( $WeekErrors );

                if(count($WeekErrors)==1)
                {
                    $WeekErrorsString =  "( Week ".implode(", ", $WeekErrors)." )";  
                }   
                else
                {
                   $WeekErrorsString =  "( Weeks ".implode(", ", $WeekErrors)." )";  
                }   
                
                return array(

                           'status' => 'ERROR',
                           'message' => "The postcodes you have entered are not valid for days ".$WeekErrorsString." which are in red.",
                           'days' => $Days

                           );


            }//Postcode Error message..ends here..
            
            //Skill set error message - starts here..
            if(count($SkillSetsError))
            {
                $WeekErrorsString = '';
                
                
                $SSWeekErrors =  array_unique ( $SSWeekErrors );

                if(count($SSWeekErrors)==1)
                {
                    $WeekErrorsString =  "( Week ".implode(", ", $SSWeekErrors)." )";  
                }   
                else
                {
                   $WeekErrorsString =  "( Weeks ".implode(", ", $SSWeekErrors)." )";  
                }
                
                return array(

                           'status' => 'ERROR',
                           'message' => "No Skill Sets are selected for the days ".$WeekErrorsString." which are in red.",
                           'days' => $SkillSetsError

                           );
                
            }  
            //Skill set error message - ends here..
            
            //Shift error message - starts here..
            if(count($ShiftError))
            {
                $WeekErrorsString = '';
                
                
                $ShiftWeekErrors =  array_unique ( $ShiftWeekErrors );

                if(count($ShiftWeekErrors)==1)
                {
                    $WeekErrorsString =  "( Week ".implode(", ", $ShiftWeekErrors)." )";  
                }   
                else
                {
                   $WeekErrorsString =  "( Weeks ".implode(", ", $ShiftWeekErrors)." )";  
                }
                
               
                
                return array(

                           'status' => 'ERROR',
                           'message' => "No Engineer Start and/or End Shift Time has been defined for the days ".$WeekErrorsString." which are in red.",
                           'days' => $ShiftError

                           );
                
            } 
            //Shift error message - ends here..
            
        
        
         //if($this->isValid($args['ServiceProviderID'], $args['ServiceBaseUserCode'], $args['ServiceProviderEngineerID']))
         if($this->isValid($args['ServiceProviderID'], $args['EngineerFirstName'], $args['EngineerLastName'], $args['ServiceProviderEngineerID']))
         {      
             
             //Getting existing record details.
             $OldEngineerRecord = $this->fetchRow(array("ServiceProviderEngineerID"=>$args['ServiceProviderEngineerID']));
             
             
           
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET ServiceProviderID=:ServiceProviderID, EngineerFirstName=:EngineerFirstName, EngineerLastName=:EngineerLastName, ServiceBaseUserCode=:ServiceBaseUserCode, EmailAddress=:EmailAddress, MobileNumber=:MobileNumber, 

                RouteColour=:RouteColour, SpeedFactor=:SpeedFactor, LunchBreakDuration=:LunchBreakDuration, LunchPeriodFrom=:LunchPeriodFrom, LunchPeriodTo=:LunchPeriodTo, Status=:Status, StartHomePostcode=:StartHomePostcode,EndHomePostcode=:EndHomePostcode ,
                SentToViamente=:SentToViamente,EngineerImportance=:EngineerImportance,ViamenteExclude=:ViamenteExclude,ExcludeFromAppTable=:ExcludeFromAppTable,ViamenteStartType=:ViamenteStartType, SetupType=:SetupType, ReplicateType=:ReplicateType, ReplicateStatusFlag=:ReplicateStatusFlag,
                ReplicatePostcodeAllocation=:ReplicatePostcodeAllocation ,AppsBeforeOptimise=:AppsBeforeOptimise,AvarageAppPerHour=:AvarageAppPerHour,PrimarySkill=:PrimarySkill

               
                WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID';
            
            
            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            $result = $updateQuery->execute(


                    array(

                            ':ServiceProviderID' => $args['ServiceProviderID'],
                            ':EngineerFirstName' => strtoupper($args['EngineerFirstName']), 
                            ':EngineerLastName' => strtoupper($args['EngineerLastName']), 
                            ':ServiceBaseUserCode' => strtoupper($args['ServiceBaseUserCode']), 
                            ':EmailAddress' => $args['EmailAddress'], 
                            ':MobileNumber' => $args['MobileNumber'], 
                            ':RouteColour' => $args['RouteColour'], 
                            ':SpeedFactor' => $args['SpeedFactor'],                      
                            ':LunchBreakDuration' => $args['LunchBreakDuration'],
                            ':LunchPeriodFrom' => $args['LunchPeriodFrom'],
                            ':LunchPeriodTo' => $args['LunchPeriodTo'],
                            ':Status' => $args['Status'],
                            ':StartHomePostcode' => $args['StartHomePostcode'],
                            ':EndHomePostcode' => $args['EndHomePostcode'],
                            ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                            ':SentToViamente' => $args['SentToViamente'],
                            ':EngineerImportance' => $args['EngineerImportance'],
                            ':ViamenteExclude' => $args['ViamenteExclude'],
                            ':ExcludeFromAppTable' => $args['ExcludeFromAppTable'],
                            ':ViamenteStartType' => $args['ViamenteStartType'],
                            ':SetupType' => $args['SetupType'],
                            ':ReplicateType' => $args['ReplicateType'],
                            ':ReplicateStatusFlag' => $args['ReplicateStatusFlag'],
                            ':ReplicatePostcodeAllocation' => $args['ReplicatePostcodeAllocation'],
                            ':AppsBeforeOptimise' => $args['AppsBeforeOptimise'],
                            ':AvarageAppPerHour' => $args['AvarageAppPerHour'],
                            ':PrimarySkill' => $args['PrimarySkill']
                        
                        )

                    );


            if($result)
            {
                   
                      $ServiceProviderEngineerID = $args['ServiceProviderEngineerID'];
                    
                    
                     
                     
//                      if(isset($args['RollingMonth']) && $args['RollingMonth'])
//                      {
//                          $maxWeeks = 8;
//                      }    
//                      else
//                      {
                          $maxWeeks  = 4;
                      //}
                      
                      for($w=1;$w<=$maxWeeks;$w++)
                      {
                            $i = $w%4;
                            if(!$i)
                            {
                                $i = 4;
                            }
                          
                            $j=0;
                            foreach($this->weekArray as $week)
                            {
                                
                                  $WorkDate  = date("Y-m-d", strtotime("+".((($w-1)*7)+$j)." day", $start_date_time));  
                                
                                  
                                  
                                  if(isset($args[$week.'Active'.$i]) && $args[$week.'Active'.$i]!='In-active' && $args['Status']=='Active')
                                  {
                                      $args[$week.'Active'.$i] = "Active";
                                  }
                                  else
                                  {
                                      $args[$week.'Active'.$i] = "In-active";
                                      
                                      
                                      //Deleting row from table service_provider_engineer_workload
                                      
                                      
                                    $delete_sql = 'DELETE FROM service_provider_engineer_workload WHERE ServiceProviderEngineerID=:ServiceProviderEngineerID AND ServiceProviderID=:ServiceProviderID AND WorkingDay=:WorkingDay';


                                    $deleteQuery = $this->conn->prepare($delete_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                                    $deleteQuery->execute(


                                            array(

                                                    ':ServiceProviderID' => $args['ServiceProviderID'],
                                                    ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID'],
                                                    ':WorkingDay' => $WorkDate
                                             
                                                )

                                            );
                                      
                                      
                                  }

                                  
                                  $DataArray = array('ServiceProviderEngineerID'=>$ServiceProviderEngineerID, 'WorkDate'=>$WorkDate, 'StartShift'=>$args[$week.'StartShift'.$i], 'EndShift'=>$args[$week.'EndShift'.$i], 'StartPostcode'=>$args[$week.'StartPostcode'.$i], 'EndPostcode'=>$args[$week.'EndPostcode'.$i], 'Status'=>$args[$week.'Active'.$i]);

                                  $ServiceProviderEngineerDetailsID = $this->processEngineerDetails($DataArray);
                                  
                                  if($ServiceProviderEngineerDetailsID)
                                  {
                                      // Insert engineer skill set.
                                      if(!isset($args[$week.'SkillsSet'.$i]))
                                      {
                                          $args[$week.'SkillsSet'.$i] = array();
                                      }

                                      $this->insertSkillSet($args[$week.'SkillsSet'.$i], $ServiceProviderEngineerDetailsID);
                                  }    

                                  $j++;        
                            }
                     
                      }
                    
                      //Handling appointments....starts here..
                      if(isset($args['AppointmentHandlingType']))
                      {
                          $AppointmentsCount = count($args['AppointmentIDs']);
                          
                          if($args['AppointmentHandlingType']=="modifyAppointment" && $AppointmentsCount)
                          {
                              $appointment_model = $this->controller->loadModel("Appointment");
                              
                              for($i=0;$i<$AppointmentsCount;$i++)
                              {
                                    if(isset($args['NewServiceProviderEngineerID_'.$args['AppointmentIDs'][$i]]) && $args['NewServiceProviderEngineerID_'.$args['AppointmentIDs'][$i]])
                                    {
                                        $spEngID = $args['NewServiceProviderEngineerID_'.$args['AppointmentIDs'][$i]];
                                    
                                        $ForceEngineerToViamente = 1;
                                    }
                                    else
                                    {
                                        $spEngID = NULL;
                                        $ForceEngineerToViamente = 0;
                                    }
                                  
                                    $appointment_model->update(array("AppointmentID"=>$args['AppointmentIDs'][$i], "ServiceProviderEngineerID"=>$spEngID, "ForceEngineerToViamente"=>$ForceEngineerToViamente));
                              }
                          }
                          
                      }
                      
                      //Handling appointments ...ends here...
                      
                      
                      
                      
                      //Replicating postcode allocation - starts here..
                      if($args['ReplicatePostcodeAllocation']=='Yes' && $args['SentToViamente']=='Yes' && $args['SetupType']=="Replicate" && ($args['ReplicateType']=="FourWeeksOnly" || $args['ReplicateType']=="WeeklyCycle"))
                      {
                          
                          $this->replicatePostcodeAllocation($ServiceProviderEngineerID, $args['ServiceProviderID']);
                          
                      }
                      //Replicating postcode allocation - ends here..
                    
                    
                      if($args['SentToViamente']=="Yes")
                      {     if(!isset($args['defaultsOnly'])){
                          $api_viamente=$this->controller->loadModel('APIViamente');
                      }
                            if($args['Status']=='Active')
                            {

                               if($OldEngineerRecord['Status']=="Active")
                               {

                                    if(!isset($args['defaultsOnly'])){
                                $api_viamente->ProcessEngineer($args['ServiceProviderEngineerID'], 'update');
                                    }
                                   //$this->controller->log("via - update");
                               } 
                               else
                               {
                                    if(!isset($args['defaultsOnly'])){
                                   $api_viamente->ProcessEngineer($args['ServiceProviderEngineerID'], 'create');
                                    }
                                  // $this->controller->log("via - create");
                               }

                            }
                            else
                            {
                                if(!isset($args['defaultsOnly'])){
                                    
                               $api_viamente->DeleteEngineer($args['ServiceProviderEngineerID']);
                                    
                                }
                               // $this->controller->log("via - delete");
                            }
                     
                    }
                    
                    
                
                    return array('status' => 'OK',
                            'message' => "Your data has been updated successfully.",
                            'eID'=>$args['ServiceProviderEngineerID']
                            );
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, your data has not been processed since there is a problem.  Please check it.");
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }   
       
    }
    
    public function delete($args) {
        
         
         if($args['ServiceProviderID']!='' && $args['Status']=='In-active' && $args['ServiceProviderEngineerID']!='' && $args['Deleted']=="Yes")
         {      
             
             
           
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET Deleted=:Deleted  
                WHERE  Status=:Status AND ServiceProviderID=:ServiceProviderID AND ServiceProviderEngineerID=:ServiceProviderEngineerID';


            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $updateQuery->execute(


                    array(

                            ':ServiceProviderID' => $args['ServiceProviderID'],
                            ':Status' => $args['Status'],
                            ':Deleted' => $args['Deleted'],
                            ':ServiceProviderEngineerID' => $args['ServiceProviderEngineerID']
                           
                        )

                    );


            if($result)
            {
                   
                
                    return array('status' => 'OK',
                            'message' => "Engineer has been deleted successfully."
                            );
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => "Sorry, Engineer has not been deleted since there is a problem.  Please check it.");
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => "Sorry, Engineer has not been deleted since some data is missing.  Please check it.");
         }  
        
        
    }
    
   
    
    
}
?>
