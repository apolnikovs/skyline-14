<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Job Types Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class JobTypes extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.JobTypeID', 't1.TypeCode',  't1.Type', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "job_type AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID";
    private $table     = "job_type";
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
        //$output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
        
       
        return  $output;
        
    }
    
      /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['JobTypeID']) || !$args['JobTypeID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
     
      /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT TypeCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY JobTypeID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $TypeCode  
     * @param interger $BrandID.
     * @param interger $JobTypeID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
     public function isValidCode($TypeCode, $BrandID, $JobTypeID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT JobTypeID FROM '.$this->table.' WHERE TypeCode=:TypeCode AND BrandID=:BrandID AND JobTypeID!=:JobTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':TypeCode' => $TypeCode, ':BrandID' => $BrandID, ':JobTypeID' => $JobTypeID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['JobTypeID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (TypeCode, Type, Status, BrandID)
//            VALUES(:TypeCode, :Type, :Status, :BrandID)';
        
        $fields = explode(', ', 'TypeCode, Type, Priority, Status, BrandID, CreatedDate, ModifiedUserID, ModifiedDate');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::JobType()->insertCommand($fields);        
        
        if(!isset($args['TypeCode']) || !$args['TypeCode'])
        {
            $args['TypeCode'] = $this->getCode($args['BrandID'])+1;//Preparing next title code.
        }
        
        if(!isset($args['Priority']) || !$args['Priority'])
        {
            $args['Priority'] = $args['TypeCode'];
        }    
        
        
        $args['CreatedDate'] = date("Y-m-d H:i:s");
        $args['ModifiedUserID'] = $this->controller->user->UserID;
        $args['ModifiedDate'] = date("Y-m-d H:i:s");
        
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidCode($args['TypeCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(
                    array(':TypeCode' => $args['TypeCode'], 
                        ':Type' => $args['Type'], 
                        ':Priority' => $args['Priority'], 
                        ':Status' => $args['Status'], 
                        ':BrandID' => $args['BrandID'],
                        ':CreatedDate' => $args['CreatedDate'],
                        ':ModifiedUserID' => $args['ModifiedUserID'],
                        ':ModifiedDate' => $args['ModifiedDate']
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.',
                         'id' => $this->conn->lastInsertId() );
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang),
                        'id' => 0 );
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT JobTypeID, TypeCode, Type, Priority, Status, BrandID, EndDate FROM '.$this->table.' WHERE JobTypeID=:JobTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':JobTypeID' => $args['JobTypeID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidCode($args['TypeCode'], $args['BrandID'], $args['JobTypeID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($args['Status']=="In-Active")
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                    $EndDate = $row_data['EndDate'];
                }    
            }   
        
            $args['EndDate'] = $EndDate;
            $args['ModifiedUserID'] = $this->controller->user->UserID;
            $args['ModifiedDate'] = date("Y-m-d H:i:s");
            
            
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET TypeCode=:TypeCode, Type=:Type, Priority=:Priority, Status=:Status, BrandID=:BrandID, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate 
            WHERE JobTypeID=:JobTypeID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':TypeCode' => $args['TypeCode'], ':Type' => $args['Type'], ':Priority' => $args['Priority'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':JobTypeID' => $args['JobTypeID'],
                  
                  ':EndDate' => $args['EndDate'],
                  ':ModifiedUserID' => $args['ModifiedUserID'],
                  ':ModifiedDate' => $args['ModifiedDate']
                  
                  ));
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.',
                        'id'=> $args['JobTypeID']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang),
                        'id'=> 0);
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
    /**
     * getJobTypeId
     *  
     * Get the JobTypeID from the name of a job type
     * 
     * @param string $jtName    The type of job
     * 
     * @return integer  Job Type ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getJobTypeId($jtName) {
        $sql = "
                SELECT
			`JobTypeID`
		FROM
			`job_type`
		WHERE
			`Type` = '$jtName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['JobTypeID']);                                    /* Job Type exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    
}
?>