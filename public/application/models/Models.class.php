<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Models Page in Product Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */
class Models extends CustomModel {

    private $conn;
    private $tbl;                                                               /* For Table Factory Class */
    private $table = "model";
    private $tables = "model AS T1 LEFT JOIN manufacturer AS T2 ON T1.ManufacturerID=T2.ManufacturerID LEFT JOIN unit_type AS T3 ON T1.UnitTypeID=T3.UnitTypeID";
    private $dbTableColumns = array('T1.ModelID', 'T1.ModelNumber', 'T2.ManufacturerName', 'T3.UnitTypeName', 'T1.Status');

    #protected $debug = true;

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);

        $this->tbl = TableFactory::Model();
        $this->comonFields =
                [
                    "ModelNumber",
                    "ModelDescription",
                    "Status",
                    "Features",
                    "UnitTypeID",
                    "IMEILengthFrom",
                    "IMEILengthTo",
                    "MSNLengthFrom",
                    "MSNLengthTo",
                    "WarrantyRepairLimit",
                    "ExcludeFromRRCRepair",
                    "AllowIMEIAlphaChar",
                    "UseReplenishmentProcess",
                    "HandsetWarranty1Year",
                    "ReplacmentValue",
                    "ExchangeSellingPrice",
                    "LoanSellingPrice",
                    "RRCOrderCap"
        ];
    }

    /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables 
     * 
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function fetch($args) {


        $UnitTypeID = isset($args['firstArg']) ? $args['firstArg'] : '';
        $ManufacturerID = isset($args['secondArg']) ? $args['secondArg'] : '';

        if ($UnitTypeID != '') {
            $args['where'] = "T1.UnitTypeID='" . $UnitTypeID . "'";
        }

        if ($ManufacturerID != '') {
            if (isset($args['where']) && $args['where']) {
                $args['where'] .= " AND T1.ManufacturerID='" . $ManufacturerID . "'";
            } else {
                $args['where'] = "T1.ManufacturerID='" . $ManufacturerID . "'";
            }
        }

        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns, $args);

        return $output;
    }

    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.


     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
    public function processData($args) {

        if (!isset($args['ModelID']) || !$args['ModelID']) {
            return $this->create($args);
        } else {
            return $this->update($args);
        }
    }

    /**
     * Description
     * 
     * This method is used for to check whether Model number exists for manufacturer.
     *
     * @param interger $ModelID  
     * @param interger $ModelNumber
     * @param interger $ManufacturerID
     * @param interger $ReturnModel default null 
     * @global $this->table
     * 
     * @return boolean if retur.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function isValid($ModelID, $ModelNumber, $ManufacturerID, $ReturnModel = null) {

        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ModelID, UnitTypeID FROM ' . $this->table . ' WHERE ModelNumber=:ModelNumber AND ManufacturerID=:ManufacturerID AND ModelID!=:ModelID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ModelID' => $ModelID, ':ModelNumber' => $ModelNumber, ':ManufacturerID' => $ManufacturerID));
        $result = $fetchQuery->fetch();


        if ($ReturnModel) {
            if (is_array($result)) {
                return $result;
            } else {
                return 0;
            }
        } else {
            if (is_array($result) && $result['ModelID']) {
                return false;
            }

            return true;
        }
    }

    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function fetchRow($args) {


        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT *  FROM ' . $this->table . ' WHERE ModelID=:ModelID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


        $fetchQuery->execute(array(':ModelID' => $args['ModelID']));
        $result = $fetchQuery->fetch();


        return $result;
    }

    /**
     * create
     *  
     * Create a Model. 
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new model
     * 
     * @return array    Containing status code (SUCCESS / FAIL), modelId cotaining
     *                  ID of newly inserted model, message containg any error
     *                  on failure.
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function create($args) {

        $args['CreatedDate'] = date("Y-m-d H:i:s");
        $args['ModifiedUserID'] = $this->controller->user->UserID;

        $cmd = $this->tbl->insertCommand($args);

        if ($this->Execute($this->conn, $cmd, $args)) {
            $result = array(
                'status' => 'SUCCESS',
                'modelId' => $this->conn->lastInsertId()
            );
        } else {
            $result = array(
                'status' => 'FAIL',
                'modelId' => 0,
                'message' => $this->lastPDOError()
            );
        }

        return ( $result );
    }

    /**
     * Description
     * 
     * This method is used for to fetch active model nos for given client and network.
     *
     * @global $this->table 
     * 
     * @return array It contains list of product numbers.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function fetchModelNumbers($fields = false, $startWith = false, $ModelNumber = false, $ManufacturerID = false) {

        if (!$fields) {
            $fields = 'ModelNumber, ModelDescription, UnitTypeID, ManufacturerID';
        }

        if ($startWith) {
            /* Execute a prepared statement by passing an array of values */
            if ($ManufacturerID) {
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE ModelNumber LIKE :ModelNumber AND ManufacturerID=:ManufacturerID AND Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => '%' . $startWith . '%', ':Status' => 'Active', ':ManufacturerID' => $ManufacturerID));
            } else {
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE ModelNumber LIKE :ModelNumber AND Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => '%' . $startWith . '%', ':Status' => 'Active'));
            }
        } else if ($ModelNumber) {
            if ($ManufacturerID) {
                /* Execute a prepared statement by passing an array of values */
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE ModelNumber=:ModelNumber AND ManufacturerID=:ManufacturerID AND Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => $ModelNumber, ':Status' => 'Active', ':ManufacturerID' => $ManufacturerID));
            } else {
                /* Execute a prepared statement by passing an array of values */
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE ModelNumber=:ModelNumber AND Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':ModelNumber' => $ModelNumber, ':Status' => 'Active'));
            }
        } else {
            if ($ManufacturerID) {
                /* Execute a prepared statement by passing an array of values */
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE ManufacturerID=:ManufacturerID AND Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':Status' => 'Active', ':ManufacturerID' => $ManufacturerID));
            } else {
                /* Execute a prepared statement by passing an array of values */
                $sql = 'SELECT ' . $fields . ' FROM ' . $this->table . ' WHERE Status=:Status';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':Status' => 'Active'));
            }
        }

        $result = $fetchQuery->fetchAll();



        return $result;
    }

    /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args

     * @global $this->table 

     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function update($args) {


        if ($this->isValid($args['ModelID'], $args['ModelNumber'], $args['ManufacturerID'])) {
            $EndDate = "0000-00-00 00:00:00";

            // set default status to active...
            if (!isset($args['Status']))
                $args['Status'] = 'Active';

            if ($args['Status'] == 'In-active') {
                $row_data = $this->fetchRow($args);
                if ($row_data['Status'] != $args['Status']) {
                    $EndDate = date("Y-m-d H:i:s");
                }
            }

            $params = array('ModelNumber' => $args['ModelNumber'],
                'ManufacturerID' => $args['ManufacturerID'],
                'UnitTypeID' => $args['UnitTypeID'],
                'ModelDescription' => $args['ModelDescription'],
                'Status' => $args['Status'],
                'EndDate' => $EndDate,
                'ModifiedUserID' => $this->controller->user->UserID,
                'ModifiedDate' => date("Y-m-d H:i:s"),
                'ModelID' => $args['ModelID']);

            $result = $this->UpdateRow($this->conn, $this->tbl, $params);



            if ($result !== false) {
                return array('status' => 'OK',
                    'message' => $this->controller->page['Text']['data_updated_msg']);
            } else {
                return array('status' => 'ERROR',
                    'message' => $this->controller->page['Errors']['data_not_processed']);
            }
        } else {
            return array('status' => 'ERROR',
                'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }

    public function delete(/* $args */) {
        return array('status' => 'OK',
            'message' => $this->controller->page['data_deleted_msg']);
    }

    /**
     * getModelIdFromNo
     *  
     * Get the Model ID from the model number
     * 
     * @param string $mNo  Account Number
     * 
     * @return integer  Model ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getModelIdFromNo($mNo) {
        $sql = "
                SELECT
			`ModelID`
		FROM
			`model`
		WHERE
			`ModelNumber` = '$mNo'
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) {
            return($result[0]['ModelID']);                                      /* Model exists so return Model ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
     * getUnitTypeIdFromNo
     *  
     * Get the Model ID from the model number
     * 
     * @param string $mNo  Account Number
     * 
     * @return integer  Model ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getUnitTypeIdFromNo($mNo) {
        $sql = "
                SELECT
			`UnitTypeID`
		FROM
			`model`
		WHERE
			`ModelNumber` = '$mNo'
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) > 0) {
            return($result[0]['UnitTypeID']);                                      /* Model exists so return Model ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }

    /**
      Returns all models
      2012-01-24 © Vic <v.rutkunas@pccsuk.com>
     */
    public function getAllModels() {
	
	$q = "SELECT * FROM model order by ModelNumber";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }

    /**
      Returns all manufacturer active models
      2012-01-24 © Andris <a.polnikovs@pccsuk.com>
     */
    public function getManufacturerModels($mid) {

        $q = "SELECT * FROM model where Status='Active' and ManufacturerID=$mid";
        $result = $this->query($this->conn, $q);
        return $result;
    }

    public function Select($sql, $params = null) {
        return $this->Query($this->conn, $sql, $params);
    }

    public function deleteModel($id, $table) {
        ($table == "model") ? $idcol = "ModelID" : $idcol = "ServiceProviderModelID";
        $sql = "update $table set Status='In-active' where $idcol=$id";

        $this->execute($this->conn, $sql);
    }

    //linking functions
    //this is used to link accessory to model
    public function addAccessory($m, $a) {
        $sql = "insert into accessory_model  (ServiceProviderModelID,AccessoryID) values ($m,$a)";
        $this->execute($this->conn, $sql);
    }

    //this is used to unlink accessory to model
    public function delAccessory($m, $a) {
        $sql = "delete from accessory_model  where ServiceProviderModelID=$m and AccessoryID=$a";
        $this->execute($this->conn, $sql);
    }

    public function loadAccessory($m) {
        $sql = "select a.AccessoryName as `name`,a.AccessoryID as `id`  from service_provider_model m 
            join accessory_model am on am.ServiceProviderModelID=m.ServiceProviderModelID
            join accessory a on a.AccessoryID=am.AccessoryID
                where m.ServiceProviderModelID=$m";

        $res = $this->query($this->conn, $sql);
        return $res;
    }

    //this is used to link colour to model
    public function addColour($m, $a) {
        $sql = "insert into colour_model  (ServiceProviderModelID,ColourID) values ($m,$a)";
        $this->execute($this->conn, $sql);
    }

    //this is used to unlink colour to model
    public function delColour($m, $a) {
        $sql = "delete from colour_model  where ServiceProviderModelID=$m and ColourID=$a";
        $this->execute($this->conn, $sql);
    }

    public function loadColour($m) {
        $sql = "select a.ColourName as `name`,a.ColourID as `id`  from service_provider_model m 
            join colour_model am on am.ServiceProviderModelID=m.ServiceProviderModelID
            join colour a on a.ColourID=am.ColourID
                where m.ServiceProviderModelID=$m";
        $res = $this->query($this->conn, $sql);
        return $res;
    }

    //linking functions
    //this is used to link productCode to model
    public function addProductCode($m, $a) {
        $sql = "insert into product_code_model  (ServiceProviderModelID,ProductCodeID) values ($m,$a)";
        $this->execute($this->conn, $sql);
    }

    //this is used to unlink productCode to model
    public function delProductCode($m, $a) {
        $sql = "delete from product_code_model  where ServiceProviderModelID=$m and ProductCodeID=$a";
        $this->execute($this->conn, $sql);
    }

    public function loadProductCode($m) {
        $sql = "select a.ProductCodeName as `name`,a.ProductCodeID as `id`  from service_provider_model m 
            join product_code_model am on am.ServiceProviderModelID=m.ServiceProviderModelID
            join product_code a on a.ProductCodeID=am.ProductCodeID
                where m.ServiceProviderModelID=$m";
        $res = $this->query($this->conn, $sql);
        return $res;
    }

    //linking functions

    public function getData($id, $table) {
        if ($table == "model") {
            $idcol = "ModelID";
            $ex = "";
        } else {
            $idcol = "ServiceProviderModelID";
            $ex = ",ServiceProviderManufacturerID as ManufacturerID";
        };
        $sql = "select * $ex from $table where $idcol=$id";
        $res = $this->Query($this->conn, $sql);

        if (isset($res[0])) {
            return $res[0];
        } else {
            false;
        }
    }

    public function getIDFromMain($spid) {
        $sql = "select ServiceProviderID from service_provider_model ss where ss.ModelID=$spid";
        $res = $this->Query($this->conn, $sql);

        if (isset($res[0])) {
            return $res[0]['ServiceProviderID'];
        } else {
            false;
        }
    }

    public function saveModel($p, $table, $secondaryTable = false) {
        $extraCol = "";
        $extraToken = "";
        $extraParam = array();
        if (!isset($p['Status'])) {
            $p['Status'] = "Active";
        }
        if (!isset($p['CollectSupplierOrderNo'])) {
            $p['CollectSupplierOrderNo'] = "No";
        }
        if (!isset($p['PostageChargePrompt'])) {
            $p['PostageChargePrompt'] = "No";
        }
        if (!isset($p['TaxExempt'])) {
            $p['TaxExempt'] = "No";
        }
        ($table == "model") ? $idcol = "ModelID" : $idcol = "ServiceProviderModelID";
        ($table == "model") ? $spp = "" : $spp = "ServiceProvider";
        $fields = [
            "ModelNumber",
            "ModelDescription",
            "Status",
            "Features",
            "UnitTypeID",
            "IMEILengthFrom",
            "IMEILengthTo",
            "MSNLengthFrom",
            "MSNLengthTo",
            "WarrantyRepairLimit",
            "ExcludeFromRRCRepair",
            "AllowIMEIAlphaChar",
            "UseReplenishmentProcess",
            "HandsetWarranty1Year",
            "ReplacmentValue",
            "ExchangeSellingPrice",
            "LoanSellingPrice",
            "RRCOrderCap"];






        if ($p['mode'] == "insert" || $p['mode'] == "copyNew") {


            if ($p['mode'] == "copyNew") {
                $fields[] = "ServiceProviderManufacturerID";
                $extraCol = ",ServiceProviderID,ModelID,CreatedDate";
                $extraToken = ",:ServiceProviderID,:ModelID,:CreatedDate";
                $extraParam = $params = array(
                    "ServiceProviderID" => $p["ServiceProviderID"],
                    "ModelID" => $p["id"],
                    
                );
            }
            if ($p['mode'] == "insert") {
                $fields[] = "ManufacturerID";
            }
            //      "ManufacturerID",
            //"",


            $sql = "insert into $table 
        (
    ";
            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=" $fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }
            $sql.="$extraCol";

            $sql.=") values(";

            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=":$fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }

            $sql.="$extraToken";
            $sql.= ")";
        }
        //this is used when new entry is created it must be insertedfirst in main table, then secondary
        if ($p['mode'] == "New") {

            $extraCol = ",ApproveStatus";
            $extraToken = ",:ApproveStatus";
            $extraParam = $params = array(
                "ApproveStatus" => "Pending",
            );
            $sql = "insert into $secondaryTable 
        (";
            $fields2 = $fields;
            $fields[] = "ManufacturerID";
            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=" $fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }

            $sql.=$extraCol;

            $sql.=") 
        values
         (
         ";
            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=":$fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }


            $sql.= $extraToken;
            $sql.="  ) 
";



            $params = array(
                "ModelNumber" => $p['ModelNumber'],
                "ModelDescription" => $p['ModelDescription'],
                "Status" => $p['Status'],
                "Features" => $p['Features'],
                "UnitTypeID" => $p['UnitTypeID'],
                "IMEILengthFrom" => $p['IMEILengthFrom'],
                "IMEILengthTo" => $p['IMEILengthTo'],
                "MSNLengthFrom" => $p['MSNLengthFrom'],
                "MSNLengthTo" => $p['MSNLengthTo'],
                "WarrantyRepairLimit" => $p['WarrantyRepairLimit'],
                "ExcludeFromRRCRepair" => (isset($p["ExcludeFromRRCRepair"]) ? "Yes" : "No"),
                "AllowIMEIAlphaChar" => (isset($p["AllowIMEIAlphaChar"]) ? "Yes" : "No"),
                "UseReplenishmentProcess" => (isset($p["UseReplenishmentProcess"]) ? "Yes" : "No"),
                "HandsetWarranty1Year" => (isset($p["HandsetWarranty1Year"]) ? "Yes" : "No"),
                "ReplacmentValue" => $p['ReplacmentValue'],
                "ExchangeSellingPrice" => $p['ExchangeSellingPrice'],
                "LoanSellingPrice" => $p['LoanSellingPrice'],
                "RRCOrderCap" => $p['RRCOrderCap']
            );

            $params1 = array_merge($params, $extraParam);

            $manufacturerModel = $this->controller->loadModel('Manufacturers');

            $params1["ManufacturerID"] = $manufacturerModel->getMainManufacturerIDFromSpMan($p['ManufacturerID']);
//      echo"<pre>";
//      print_r($params1);
//      echo"</pre>";
            $this->Execute($this->conn, $sql, $params1);

            $fields = $fields2;
            $mainID = $this->conn->lastInsertId();
            $extraCol = ",ServiceProviderID,ModelID";
            $extraToken = ",:ServiceProviderID,:ModelID";
            $extraParam = array(
                "ServiceProviderID" => $p["ServiceProviderID"],
                "ModelID" => $mainID,
            );
            $fields[] = "ServiceProviderManufacturerID";
            $sql = "insert into $table 
        (
    ";
            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=" $fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }
            $sql.="$extraCol";

            $sql.=") values(";

            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=":$fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }
            $sql.="$extraToken";
            $sql.= ")";


            $params['ServiceProviderManufacturerID'] = $p['ManufacturerID'];
            $params = array_merge($params, $extraParam);


            $this->Execute($this->conn, $sql, $params);
        }





        if ($p['mode'] == "update") {
            $fields[] = $spp . "ManufacturerID";
            $id = $p['id'];
            if (isset($p['ApproveStatus'])) {
                $st = $p['ApproveStatus'];
                $ApproveStatus = ",ApproveStatus='$st'";
            } else {
                $ApproveStatus = "";
            }
            $fields[]="CreatedDate";
            $sql = "
             update $table set";

            for ($i = 0; $i < sizeof($fields); $i++) {
                $sql.=" $fields[$i]=:$fields[$i]";
                if ($i != sizeof($fields) - 1) {
                    $sql.=",";
                }
            }

            $sql.=" $ApproveStatus
        where $idcol=$id

            ";
        }

//      "
//    "UnitTypeID" => $p['UnitTypeID'],
 if($p['CreatedDate']=="0000-00-00 00:00:00"){
             $p['CreatedDate']=date('Y-m-d H:i:s');
          }

        $params = array(
            "ModelNumber" => $p['ModelNumber'],
            "ModelDescription" => $p['ModelDescription'],
            "Status" => $p['Status'],
            "Features" => $p['Features'],
            "UnitTypeID" => $p['UnitTypeID'],
            "IMEILengthFrom" => $p['IMEILengthFrom'],
            "IMEILengthTo" => $p['IMEILengthTo'],
            "MSNLengthFrom" => $p['MSNLengthFrom'],
            "MSNLengthTo" => $p['MSNLengthTo'],
            $spp . "ManufacturerID" => $p['ManufacturerID'],
            "WarrantyRepairLimit" => $p['WarrantyRepairLimit'],
            "ExcludeFromRRCRepair" => (isset($p["ExcludeFromRRCRepair"]) ? "Yes" : "No"),
            "AllowIMEIAlphaChar" => (isset($p["AllowIMEIAlphaChar"]) ? "Yes" : "No"),
            "UseReplenishmentProcess" => (isset($p["UseReplenishmentProcess"]) ? "Yes" : "No"),
            "HandsetWarranty1Year" => (isset($p["HandsetWarranty1Year"]) ? "Yes" : "No"),
            "ReplacmentValue" => $p['ReplacmentValue'],
            "ExchangeSellingPrice" => $p['ExchangeSellingPrice'],
            "LoanSellingPrice" => $p['LoanSellingPrice'],
            "RRCOrderCap" => $p['RRCOrderCap'],
            "CreatedDate" => $p['CreatedDate']
        );


        if ($p['mode'] != "New") {
            
//            echo"$sql";
//            echo"<pre>";
//            print_r($params);
//            echo"</pre>";
            $params = array_merge($params, $extraParam);

            $this->Execute($this->conn, $sql, $params);
        }
    }

    public function getAllSPModels($SpID) {
        $sql = "select * from service_provider_model where Status='Active' and ServiceProviderID=$SpID";
        $result = $this->Query($this->conn, $sql);
        return $result;
    }

    public function getAllAprovedModels() {
        $sql = "select *,ModelID as `ServiceProviderModelID` from model where Status='Active' and ApproveStatus='Approved'";
        $result = $this->Query($this->conn, $sql);
        return $result;
    }

    public function CopyFromMain($ids, $spid) {
        $fields = $this->comonFields;
        $sql = "insert into service_provider_model (";
        foreach ($fields as $c) {
            $sql.="$c,";
        }
        $sql.="ServiceProviderID,ModelID,CreatedDate,ServiceProviderManufacturerID";

        $sql = trim($sql, ",");
        $sql.=") select ";
        foreach ($fields as $c) {
            $sql.="$c,";
        }
        $sql = trim($sql, ",");
        //extra paramtres
        $sql.=" ,'$spid' as ServiceProviderID,ModelID,'',
            (select fdf.ServiceProviderManufacturerID from service_provider_manufacturer fdf where ManufacturerID=mmm.ManufacturerID) 
        ";
        $sql.=" from model mmm where ModelID in (";
        foreach ($ids as $c) {
            $sql.="$c,";
        }
        $sql = trim($sql, ",");
        $sql.=")";
       
        $this->Execute($this->conn, $sql);
    }

    public function getMainTableDistinctRecords($spid) {
        $sql = "select count(*) as c from model where ModelID not in (select ModelID from service_provider_model where ServiceProviderID=$spid) and ApproveStatus='Approved' and Status='Active'";
        $result = $this->query($this->conn, $sql);
        return $result[0]["c"];
    }

    //this function counts how many records have been copied from model table, but have not been modified by user yet
    public function countSPUnmodifiedRecords($spID) {
        $sql = "select count(*) as con from service_provider_model spm where CreatedDate='0000-00-00 00:00:00'";
        $result = $this->query($this->conn, $sql);
        return $result[0]['con'];
    }
    
    
     /**
     * UpdateModel
     *  
     * update a Model. 
     * 
     * @param array $args   Associative array of field values for the update of
     *                      the model
     * 
     * @return array    Containing status code (SUCCESS / FAIL), message containg any error
     *                  on failure.
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    public function UpdateModel($args) {
                
        $args['ModifiedDate']   = date("Y-m-d H:i:s");
        $args['ModifiedUserID'] = $this->controller->user->UserID;
        $args['EndDate']        = "0000-00-00 00:00:00";
         
                    
        // set default status to active...
        if (!isset($args['Status']))
            $args['Status'] = 'Active';

        if($args['Status']=='In-active')
        {
            $row_data = $this->fetchRow($args);
            if($row_data['Status']!=$args['Status'])
            {
                    $args['EndDate'] = date("Y-m-d H:i:s");
            }
        }
        
        $cmd = $this->tbl->updateCommand( $args );
        
        if ( $this->Execute($this->conn, $cmd, $args) ) {            
            $result = array (
                             'status' => 'SUCCESS',
                             'modelId' => $args['ModelID']
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'modelId' => 0,
                            'message' => $this->lastPDOError()
                           );
        }
        
        return ( $result );
    }
    

}

?>
