<?php

require_once('CustomModel.class.php');
include_once(APPLICATION_PATH . '/models/PostcodeLatLong.class.php');

/**
 * Diary.class.php
 *
 * This class is used for handling database actions of Diary Main Page
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version     74.00
 * 
 * Changes
 * Date        Version Author                Reason
 * ??/??/????  1.00    Andris Polnikovs      Initial Version
 * .
 * . (various changes)
 * .
 * ??/??/????  71.8    Andris Polnikovs      Various Changes
 * 01/02/2013  71.9    Andrew J. Williams    Added isSLotForToday
 * 22/02/2013  71.10   Andrew J. Williams    Added getAppointmentTimeFromDiaryAllocation
 * 22/02/2013  72.00   Andris Polnikovs     Samsung one touch allocation only addet
 * 03/03/2013  72.01    Andris Polnikovs     Samsung one touch finalized day and full day fix
 * 05/03/2013  73.00    Andris Polnikovs     Samsung one touch stand alone
 * 22/03/2013  74.00    Andris Polnikovs     Bank Holidays, ForceEngineer One touch
 * **************************************************************************** */
class Diary extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->pc = new PostcodeLatLong($controller);
        $this->debug = false;
        //$this->debug=true;
    }

    /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * 
     * @author Andris Polnikovs <a.polnikovs@gmail.com>
     */
    public function getSlots($spID, $skillSetID, $postCode, $days = 27, $geotag = false, $eng = false, $diaryType = "FullViamente") {
        //echo "$skillSetID<--";

        if ($diaryType == "FullViamente") {
            $this->calculateSlots($spID, $skillSetID, $postCode, $days, $geotag, $eng);
        }
        $filters = "";
        $postCodeAlpha = "";
        $postcodeSQL = "";
        $geofilter = "";
        $engfilter = "";
        if (!$geotag) {

            $postCode2 = $postCode;
            if (strlen($postCode) > 4) {
                $postCode2 = trim(substr(trim($postCode), 0, -3));
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            } else {
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            }
            $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
            if (!is_numeric(substr($postCodeOutCode, -1))) {
                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
            }
            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


            $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";
            $postcodeSQL = "AND 
                          (
                            dpa.`PostCode` = '$postCode'
                            OR dpa.`PostCode` = '$postCodeAlpha'
                            OR dpa.`PostCode` = '$postCodeOutCode'
                            OR dpa.`PostCode` = '$postCodeArea'
                          )";
        } else {
            $lat = $geotag['lat'];
            $lng = $geotag['lng'];
            $allocationTable = "join sp_geo_cells sgc on sgc.DiaryAllocationID=da.DiaryAllocationID";
            $geofilter = "and sgc.Lat1>=$lat and sgc.Lat2<=$lat and sgc.Lng1<=$lng and sgc.Lng2>=$lng";
        }

        if ($eng) {
            $engfilter = "and spe.ServiceBaseUserCode='$eng'";
        }
        if ($diaryType == "FullViamente") {

            ///will couse wrong slot numbers if same dpa.Postcode assigned more than once
            $sql = "select  da.DiaryAllocationID,Type,da.AllocatedDate as AppointmentDate ,SlotsLeft as 'slotsLeft', 
DATE_FORMAT(da.AllocatedDate,'%d%m%Y') as dayOnly 
 ,(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate) as anyAppCount  
from diary_allocation da 
left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
 join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
 $allocationTable
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 

join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
where da.ServiceProviderID=:ServiceProviderID
 $postcodeSQL
and da.AllocatedDate>=curdate() 
and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL $days DAY) 

and spe.Status='Active'
  $filters $geofilter $engfilter
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID

group by da.DiaryAllocationID";


            $params = array('ServiceProviderID' => $spID, 'ServiceProviderSkillsetID' => $skillSetID);
            $res = $this->Query($this->conn, $sql, $params);
            //$this->controller->log($res,"aa");
        }
        if ($diaryType == "NoViamente") {
            $sql = "
         
select  da.DiaryAllocationID,Type,da.AllocatedDate as AppointmentDate ,
DATE_FORMAT(da.AllocatedDate,'%d%m%Y') as dayOnly   ,    
         (da.NumberOfAllocations-(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime=aas.Type and da.DiaryAllocationID=aaa.DiaryAllocationID)) as 'slotsLeft'
       
       ,(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime='ANY') as anyAppCount  
 from diary_allocation da 
 left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
 join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID
  $allocationTable
   join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
	 where da.ServiceProviderID=:ServiceProviderID
	$postcodeSQL  $filters $geofilter
	  and da.AllocatedDate>=curdate() 
and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL $days DAY) 
	  and spe.Status='Active' and sped.`Status`='Active'
	   and
 sped.WorkDate=da.AllocatedDate and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
 and aas.Type!='ANY'
  group by da.DiaryAllocationID                     
    
            ";
            $params = array('ServiceProviderID' => $spID, 'ServiceProviderSkillsetID' => $skillSetID);
            $res = $this->Query($this->conn, $sql, $params);
            if ($this->debug)
                $this->controller->log($res);
        }
        return $res;
    }

    public function getSkillsets($spID, $repSkillID = null, $man = 1) {
        if ($repSkillID) {
            $repSkill = "and rs.RepairSkillID=$repSkillID";
        } else {
            $repSkill = null;
        }
        if ($man === 1) {
            $manreq = " and sps.EngineersRequired=1";
        } elseif ($man === 2) {
            $manreq = " and sps.EngineersRequired=2";
        }
        $sql = "select ss.SkillsetName, sps.ServiceProviderSkillsetID, rs.RepairSkillName,rs.RepairSkillID ,apt.Type, apt.AppointmentTypeID
               from service_provider_skillset sps 
               join skillset ss on sps.SkillsetID=ss.SkillsetID
               join repair_skill rs on rs.RepairSkillID=ss.RepairSkillID
               join appointment_type apt on apt.AppointmentTypeID=ss.AppointmentTypeID
               where sps.ServiceProviderID=:ServiceProviderID and ss.status='Active' and sps.status='Active' $repSkill 
               $manreq
        order by SkillsetName ASC";

        $params = array('ServiceProviderID' => $spID);
        $res = $this->Query($this->conn, $sql, $params);

        return $res;
    }

    //this method gets all appointments to given day, 

    public function getFilledSlots($spID, $date, $spSkillsetIDs, $slotTypeE = "", $postcode, $appID = false, $englist) {

        //  echo($englist);
        $postcode2 = explode(' ', $postcode);
        $postcode = $postcode2[0];
        $slotFilter = "";
        $appFilter = "";
        if ($spSkillsetIDs == 0 || $spSkillsetIDs == null) {
            $spfilt = "";
        } else {
            $spfilt = "and ap.ServiceProviderSkillsetID in  ($spSkillsetIDs)";
        }
        if ($slotTypeE != "") {
            $slotFilter = "and aas.Description='$slotTypeE'";
        }
        if ($appID) {
            $appFilter = "and ap.AppointmentID=$appID";
        }
        $sql = "select distinct 
ap.AppointmentID,
        ap.*,
        ap.BookedBy as bbuk,
       sec_to_time(ap.BreakStartSec) as brStart,
         sec_to_time(time_to_sec(ap.ViamenteStartTime)-ap.ViamenteTravelTime) as `EngstartTime`,
        sec_to_time((ap.BreakStartSec+ap.BreakDurationSec)) as brEnd,
        c.ContactLastName ,
        ct.Title,
        if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as PostalCode,
        
         SEC_TO_TIME( ap.ViamenteTravelTime ) as ViamenteTravelTime,
         ap.ViamenteDepartTime as ViamenteDepart,
  j.JobID,
        ss.SkillsetName,
        cl.ClientName as `Client`,
      if(j.ManufacturerID is null,j.ServiceBaseManufacturer,m.ManufacturerName) as ServiceBaseManufacturer,
        if(j.ModelID is null,j.ServiceBaseModel,mm.ModelNumber) as  ServiceBaseModel,
       if(j.ModelID is null,j.ServiceBaseUnitType,ut.UnitTypeName) as ServiceBaseUnitType,
         if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        concat_ws(' ',c.BuildingNameNumber,c.Street,c.TownCity),
        concat_ws(' ',j.ColAddBuildingNameNumber, j.ColAddStreet,j.ColAddLocalArea,j.ColAddTownCity))
        as fulladress,
         if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        if (c.TownCity is null,c.LocalArea,c.TownCity),
        j.ColAddTownCity)
        as TownCity,
         SEC_TO_TIME( ap.ViamenteTravelTime ) as ViamenteTravelTime,
        j.ServiceCentreJobNo as sbJobNo,
        DATE_FORMAT(ap.ViamenteStartTime,'%h:%m') as `ViamenteTime`,
        (select RouteColour from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as RouteColour,
          (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as EngineerAssigned
            ,aas.Colour
            ,sType.ServiceTypeName as `ServiceType`,
            (
                    SELECT
                            Reason
                    FROM
                            diary_holiday_slots dhs
                    JOIN diary_holiday_diary dhd ON dhd.DiaryHolidayID = dhs.DiaryHolidayDiaryID
                    WHERE
                            dhs.HolidayDate =:AppointmentDate
                    AND dhs.ServiceProviderEngineerID = ap.ServiceProviderEngineerID
            ) AS hasdHoliday

            
         from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        left join service_provider_engineer spe on spe.ServiceProviderEngineerID=ap.ServiceProviderEngineerID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        left join service_type sType on sType.ServiceTypeID=j.ServiceTypeID
        where 
        ap.ServiceProviderID=:ServiceProviderID 
        and AppointmentDate=:AppointmentDate 
        and (spe.ExcludeFromAppTable!='Yes' or spe.ExcludeFromAppTable is null)
         and aas.ServiceProviderID=:ServiceProviderID 
          $spfilt $appFilter
         
       union all
       select 
        distinct 
		  ap.AppointmentID,
        ap.*,
         ap.BookedBy as bbuk,
        sec_to_time(ap.BreakStartSec) as brStart,
        sec_to_time(time_to_sec(ap.ViamenteStartTime)-ap.ViamenteTravelTime) as `EngstartTime`,
        sec_to_time((ap.BreakStartSec+ap.BreakDurationSec)) as brEnd,
         
        nsj.CustomerSurname as ContactLastName,
        nsj.CustomerTitle as Title,
        nsj.Postcode as PostalCode,
        
        SEC_TO_TIME( ap.ViamenteTravelTime ) as ViamenteTravelTime,
          ap.ViamenteDepartTime as ViamenteDepart,
          nsj.NonSkylineJobID as JobID,
          ss.SkillsetName,
        nsj.`Client`,
        nsj.Manufacturer as ServiceBaseManufacturer,
        nsj.ModelNumber  as ServiceBaseModel,
        nsj.ProductType as ServiceBaseUnitType,
		  concat(nsj.CustomerAddress1,' ',nsj.CustomerAddress2,' ',nsj.CustomerAddress3,' ',nsj.CustomerAddress4) as fulladress,
                  CustomerAddress3 as TownCity,
		   SEC_TO_TIME( ap.ViamenteTravelTime ) as ViamenteTravelTime,
		 nsj.ServiceProviderJobNo as sbJobNo,
        DATE_FORMAT(ap.ViamenteStartTime,'%h:%m') as `ViamenteTime`,
       (select RouteColour from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as RouteColour,
       (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as EngineerAssigned
         , aas.Colour
         ,nsj.ServiceType as `ServiceType`,
        (
                SELECT
                        Reason
                FROM
                        diary_holiday_slots dhs
                JOIN diary_holiday_diary dhd ON dhd.DiaryHolidayID = dhs.DiaryHolidayDiaryID
                WHERE
                        dhs.HolidayDate = :AppointmentDate
                AND dhs.ServiceProviderEngineerID = ap.ServiceProviderEngineerID
        ) AS hasdHoliday
        
        from appointment ap
          left join service_provider_engineer spe on spe.ServiceProviderEngineerID=ap.ServiceProviderEngineerID
         join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
         
        
         ap.ServiceProviderID=:ServiceProviderID 
        and AppointmentDate=:AppointmentDate 
         and aas.ServiceProviderID=:ServiceProviderID 
         and (spe.ExcludeFromAppTable!='Yes' or spe.ExcludeFromAppTable is null)
        $spfilt $appFilter
         order by EngineerAssigned,`ViamenteStartTime` asc 
        ";


        $params = array('ServiceProviderID' => $spID, 'AppointmentDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log($res, "timesliders");
        
        $slotsResult = array();
        
        foreach ($res as $key => $value) {
            
            $slotsResult[$key] = $value;
            $getHolidayEntries = $this->getEngineerHolidayEntries($value['ServiceProviderEngineerID'], $value['AppointmentDate']);
            if (!empty($getHolidayEntries)) {
                $slotsResult[$key]['HolidayStart']  = substr($getHolidayEntries[0]['StartTime'],-8,-3);
                $slotsResult[$key]['HolidayEnd']    = substr($getHolidayEntries[0]['EndTime'],-8,-3);
            }
        }
        
        return $slotsResult;
    }
    
    /*@author Dileep Bhimineni
     * 
     * Function to get Holiday Entries for Service Engineer based on date
     * 
     * @params 
     *      $id     - Service Engineer ID
     *      $date   - Appointment Date of holiday Entries
     * 
     * $rerurn Array of Holyday Entries
     */
    public function getEngineerHolidayEntries($id,$date) {
        
        if (empty($id) || empty($date)) {
            
            return NULL;
        }
        
        $sql = "SELECT  StartTime,
                        EndTime
                FROM 
                    diary_holiday_diary dhd 
                    JOIN diary_holiday_slots dhs 
                ON 
                dhs.ServiceProviderEngineerID = dhd.ServiceProviderEngineerID
                WHERE dhs.HolidayDate =:date
                AND dhd.DiaryHolidayID = dhs.DiaryHolidayDiaryID
                AND dhs.ServiceProviderEngineerID =:id";
        $params = array('date' => $date, 'id' => $id);
        $res = $this->Query($this->conn, $sql, $params);
        return $res;
        
    }


    public function insertSlot($slot, $numOfAlloc = 1) {




        $sql = "Update diary_allocation da set da.NumberOfAllocations=da.NumberOfAllocations+1,da.SlotsLeft=da.SlotsLeft+1 where AppointmentAllocationSlotID=:AppointmentAllocationSlotID and AllocatedDate=:AllocatedDate order by AppointmentAllocationSlotID asc limit 1";
        $params = array('AppointmentAllocationSlotID' => $slot, 'AllocatedDate' => $this->controller->session->SQLSelectedDate);

        $res = $this->execute($this->conn, $sql, $params);
        return;
    }

    //this method gets default start and finish times for add new appointment window @param Slot (am,pm,any)
    public function getSlotsTimes($spID) {
        $sql = "select * from appointment_allocation_slot where ServiceProviderID=$spID group by Type ";

        $res = $this->Query($this->conn, $sql);
        $res2 = array();
        foreach ($res as $key => $value) {
            $res2[$value['Type']] = $value;
        }



        return $res2;
    }

    public function getSlotsTimesInsert($spID, $date) {
        $sql = "select * from appointment_allocation_slot aas
            join diary_allocation da on da.AppointmentAllocationSlotID=aas.AppointmentAllocationSlotID
           where aas.ServiceProviderID=$spID and da.AllocatedDate='$date' group by type ";

        $res = $this->Query($this->conn, $sql);



        return $res;
    }

    public function insertAppointment($post, $spID, $date, $bulk = false, $autoForce = false, $exclude = false) {
// echo"<pre>";
//           echo print_r($post);
//           echo"</pre>";
//           die();
        if (!$bulk) {
            if (isset($post['Postcode'])) {
                $postcodeTMP = explode(' ', $post['Postcode']);
                $postcode = $postcodeTMP[0];
            } else {
                $postcode = $this->controller->session->JobPostCode;
            }
            if ($this->controller->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                $GO = $this->controller->session->addressGeoTag;
            } else {
                $GO = null;
            }

            $dalocID = $this->getDiaryAllocationID($post['timeSlot'], $post['ServiceProviderSkillsetID'], $postcode, $spID, false, false, false, $GO);
            if (isset($post['ForceEngineerToViamente']) && $post['ServiceProviderEngineerID'] != "null" && $this->controller->session->SPInfo['DiaryType'] == "FullViamente") {

                $fdID = $this->getDiaryAllocationIDForce($date, $post['ServiceProviderEngineerID']);
                if ($fdID) {
                    $dalocID = $fdID;
                }
            }

            if (!key_exists(0, $dalocID)) {
                Die("Error 0001: ' Slot is full or  Given skillset cannot be allocated to this day please check information you input' - " . print_r($dalocID));
            }
        } else {
            $this->controller->log($bulk, "bulk__");
            $appDatetmp = explode('/', $bulk['AppointmentDate']);
            $appDate = $appDatetmp[2] . "-" . $appDatetmp[1] . "-" . $appDatetmp[0];
            $postcodeTMP = explode(' ', $bulk['CustPostcode']);
            $postcode = $postcodeTMP[0];
            if (strlen($postcode) > 4) {
                $postcode = str_replace(substr($postcode, -3, 3), '', $postcode);
            }

            //echo($bulk['TimeSlot'].'<br>'.$bulk['ServiceProviderSkillsetID'].'<br>'.$postcode.'<br>'.$bulk['ServiceProviderID'].'<br>'.$appDate); 
            if ($bulk['TimeSlot'] == "SLOT") {
                $bulk['TimeSlot'] = "ANY";
            }
            $spdata = $this->getAllServiceProviderInfo($bulk['ServiceProviderID']);
            if ($spdata['DiaryAllocationType'] == "GridMapping") {
                if ($bulk['CollAddress1'] != "") {
                    $geotag = $this->getGeoTagFromAddress($bulk['CollAddress1'] . " " . $bulk['CollAddress2'] . " " . $bulk['CollAddress3'] . " " . $bulk['CollAddress4'] . " " . $bulk['CollPostcode'], $bulk['ServiceProviderID']);
                } else {
                    $geotag = $this->getGeoTagFromAddress($bulk['CustAddress1'] . " " . $bulk['CustAddress2'] . " " . $bulk['CustAddress3'] . " " . $bulk['CustAddress4'] . " " . $bulk['CustPostcode'], $bulk['ServiceProviderID']);
                }
            } else {
                $geotag = false;
            }
            $dalocID = $this->getDiaryAllocationID($bulk['TimeSlot'], $bulk['ServiceProviderSkillsetID'], $postcode, $bulk['ServiceProviderID'], $appDate, $bulk, false, $geotag); //need add bulk to sort out postcode issue
            // die($bulk['TimeSlot'].'<br>'.$bulk['ServiceProviderSkillsetID'].'<br>'.$postcode.'<br>'.$bulk['ServiceProviderID'].'<br>'.$appDate); 
            if (!key_exists(0, $dalocID)) {
                $this->controller->log('Diary allocation cannot be found (may be full, or no engineers set up for this skillset) this job' . $bulk['SBJobNo'] . ' is not inserted ' . $bulk['TimeSlot'] . 'skillset-> ' . $bulk['ServiceProviderSkillsetID'] . 'postcode(should be with only 1st part)->' . $postcode . ' geotag-> -' . $bulk['ServiceProviderID'] . '-' . $appDate, 'NotImported_');
                $this->controller->log($geotag, 'NotImported_');
                $result = array('status' => 'FAIL', 'appointmentId' => null);
                return $result;
            }
        }
        $JobID = null;
        $nonSJN = null;
        if (isset($post['actionType']) && $post['actionType'] == 6) {
            $sql = "insert into non_skyline_job 
             (
             ServiceProviderID,
             CustomerSurname,
             CustomerTitle,
             Postcode,
             JobType,
             RepairSkillID,
             ServiceType,
             CustomerAddress1,
             CustomerAddress2,
             ProductType,
             Manufacturer,
             ModelNumber
             )
             values(
             :ServiceProviderID,
             :CustomerSurname,
             :CustomerTitle,
             :Postcode,
             :JobType,
             :RepairSkillID,
             :ServiceType,
             :CustomerAddress1,
             :CustomerAddress2,
             :ProductType,
             :Manufacturer,
             :ModelNumber
             )";
            $params = array(
                "ServiceProviderID" => $spID,
                "CustomerSurname" => $post['CustomerSurname'],
                "CustomerTitle" => $post['CustomerTitle'],
                "Postcode" => $post['Postcode'],
                "JobType" => $post['RepairSkillID'],
                "RepairSkillID" => $post['RepairSkillID'],
                "ServiceType" => $post['ServiceTypeID'],
                "CustomerAddress1" => $post['CustomerAddress1'],
                "CustomerAddress2" => $post['CustomerAddress2'],
                "ProductType" => $post['ProductType'],
                "Manufacturer" => $post['Manufacturer'],
                "ModelNumber" => $post['ModelNumber']
            );
            $res = $this->execute($this->conn, $sql, $params);
            $nonSJN = $this->conn->lastInsertId();
        } else {

            $sql = "select JobID from job where ServiceCentreJobNo=:ServiceCentreJobNo and ServiceProviderID=:ServiceProviderID";
            $sql2 = "select * from non_skyline_job where ServiceProviderJobNo=:ServiceCentreJobNo and ServiceProviderID=:ServiceProviderID";

            if ($bulk) {
                $params = array('ServiceCentreJobNo' => $bulk['SBJobNo'], 'ServiceProviderID' => $bulk['ServiceProviderID']);
            } else {
                $params = array('ServiceCentreJobNo' => $this->controller->session->idata['SBJobNo'], 'ServiceProviderID' => $spID);
            }
            $res = $this->Query($this->conn, $sql, $params);

            if (!key_exists(0, $res)) {
                $res = $this->Query($this->conn, $sql2, $params);
                if (key_exists(0, $res)) {
                    $nonSJN = $res[0]['NonSkylineJobID'];
                    if (!$bulk) {
                        if ($this->controller->session->idata['CollPostcode'] != "") {
                            $postcode2 = $this->controller->session->idata['CollPostcode'];
                        } else {
                            $postcode2 = $this->controller->session->idata['CustPostcode'];
                        }

                        if (isset($res[0]['NonSkylineJobID'])) {


                            $sql = "update non_skyline_job set Postcode='$postcode2',ReportedFault=:ReportedFault where NonSkylineJobID=" . $res[0]['NonSkylineJobID'];
                            $params = array('ReportedFault' => $this->controller->session->idata['FaultDescription']);
                            $this->execute($this->conn, $sql, $params);
                        }
                    }
                }
            } else {
                $JobID = $res[0]['JobID'];
            }

            if (!$bulk) {
///update skyline with delivery adress

                if (isset($JobID)) {
                    $sql = "update job j set 
           ColAddStreet=:ColAddStreet,
           ColAddTownCity=:ColAddTownCity,
           ColAddPostcode=:ColAddPostcode,
           ColAddBuildingNameNumber=:ColAddBuildingNameNumber,
           ReportedFault=:ReportedFault
           where JobID=$JobID 
                ";

                    if (isset($this->controller->session->idata['CollBuildingName'])) {
                        $buldingNr = $this->controller->session->idata['CollBuildingName'];
                    } else {
                        $buldingNr = null;
                    }
                    $params = array(
                        'ColAddStreet' => $this->controller->session->idata['CollAddress1'] . " " . $this->controller->session->idata['CollAddress2'],
                        'ColAddTownCity' => $this->controller->session->idata['CollAddress3'],
                        'ColAddPostcode' => $this->controller->session->idata['CollPostcode'],
                        'ReportedFault' => $this->controller->session->idata['FaultDescription'],
                        'ColAddBuildingNameNumber' => $buldingNr
                    );
                    $result = $this->execute($this->conn, $sql, $params);
                    if ($this->debug)
                        $this->controller->log(var_export($params, true), "update_job_diary_");
                    if ($this->debug)
                        $this->controller->log(var_export($sql, true), "update_job_diary_");
                }
                ///update skyline with delivery adress
            }
            if (!isset($JobID) && !isset($nonSJN)) {
                $sql = "insert into non_skyline_job (
             ServiceProviderID,
             ServiceProviderJobNo,
             CustomerSurname,
             CustomerTitle,
             Postcode,
             CustomerAddress1,
             CustomerAddress2,
             CustomerAddress3,
             CustomerAddress4,
             ProductType,
             Manufacturer,
             ModelNumber,
             Client,
             ServiceType,
             ReportedFault,
             CustHomeTelNo,
             CustWorkTelNo,
             CustMobileNo,
             CollContactNumber
             )
             values
                (
                :ServiceProviderID,
             :ServiceProviderJobNo,
             :CustomerSurname,
             :CustomerTitle,
             :Postcode,
             :CustomerAddress1,
             :CustomerAddress2,
             :CustomerAddress3,
             :CustomerAddress4,
             :ProductType,
             :Manufacturer,
             :ModelNumber,
             :Client,
             :ServiceType,
             :ReportedFault,
             :CustHomeTelNo,
             :CustWorkTelNo,
             :CustMobileNo,
             :CollContactNumber
                )
                ";
                if (!$bulk) {
                    if ($this->controller->session->idata['CollPostcode'] != "") {
                        $postcode2 = $this->controller->session->idata['CollPostcode'];
                    } else {
                        $postcode2 = $this->controller->session->idata['CustPostcode'];
                    }
                    if (!isset($this->controller->session->idata['CustHomeTelNo'])) {
                        $CustHomeTelNo = null;
                    } else {
                        $CustHomeTelNo = $this->controller->session->idata['CustHomeTelNo'];
                    }
                    if (!isset($this->controller->session->idata['CustWorkTelNo'])) {
                        $CustWorkTelNo = null;
                    } else {
                        $CustWorkTelNo = $this->controller->session->idata['CustWorkTelNo'];
                    }
                    if (!isset($this->controller->session->idata['CustMobileNo'])) {
                        $CustMobileNo = null;
                    } else {
                        $CustMobileNo = $this->controller->session->idata['CustMobileNo'];
                    }
                    if (!isset($this->controller->session->idata['CollContactNumber'])) {
                        $CollContactNumber = null;
                    } else {
                        $CollContactNumber = $this->controller->session->idata['CollContactNumber'];
                    }
                    $building = "";
                    if ($this->controller->session->idata['CollBuildingName'] != "") {
                        $building = $this->controller->session->idata['CollBuildingName'] . " ";
                    } elseif ($this->controller->session->idata['CustBuildingName'] != "") {
                        $building = $this->controller->session->idata['CustBuildingName'] . " ";
                    }
                    $params = array(
                        'ServiceProviderID' => $spID,
                        'ServiceProviderJobNo' => $this->controller->session->idata['SBJobNo'],
                        'CustomerSurname' => $this->controller->session->idata['CustSurname'],
                        'CustomerTitle' => $this->controller->session->idata['CustTitle'],
                        'Postcode' => $postcode2,
                        'CustomerAddress1' => $building . $this->controller->session->idata['CustAddress1'],
                        'CustomerAddress2' => $this->controller->session->idata['CustAddress2'],
                        'CustomerAddress3' => $this->controller->session->idata['CustAddress3'],
                        'CustomerAddress4' => $this->controller->session->idata['CustAddress4'],
                        'ProductType' => $this->controller->session->idata['ProductType'],
                        'Manufacturer' => $this->controller->session->idata['Manufacturer'],
                        'ModelNumber' => $this->controller->session->idata['ModelNumber'],
                        'Client' => $this->controller->session->idata['Client'],
                        'ServiceType' => $this->controller->session->idata['ServiceType'],
                        'ReportedFault' => $this->controller->session->idata['FaultDescription'],
                        'CustHomeTelNo' => $CustHomeTelNo,
                        'CustWorkTelNo' => $CustWorkTelNo,
                        'CustMobileNo' => $CustMobileNo,
                        'CollContactNumber' => $CollContactNumber
                    );
                } else {
                    if ($bulk['CollPostcode'] != "") {
                        $b_postcode = $bulk['CollPostcode'];
                        $b_a1 = $bulk['CollAddress1'];
                        $b_a2 = $bulk['CollAddress2'];
                        $b_a3 = $bulk['CollAddress3'];
                        $b_a4 = $bulk['CollAddress4'];
                    } else {
                        $b_postcode = $bulk['CustPostcode'];
                        $b_a1 = $bulk['CustAddress1'];
                        $b_a2 = $bulk['CustAddress2'];
                        $b_a3 = $bulk['CustAddress3'];
                        $b_a4 = $bulk['CustAddress4'];
                    }
                    $params = array(
                        'ServiceProviderID' => $bulk['ServiceProviderID'],
                        'ServiceProviderJobNo' => $bulk['SBJobNo'],
                        'CustomerSurname' => $bulk['CustSurname'],
                        'CustomerTitle' => $bulk['CustTitle'],
                        'Postcode' => $b_postcode,
                        'CustomerAddress1' => $b_a1,
                        'CustomerAddress2' => $b_a2,
                        'CustomerAddress3' => $b_a3,
                        'CustomerAddress4' => $b_a4,
                        'ProductType' => $bulk['ProductType'],
                        'Manufacturer' => $bulk['Manufacturer'],
                        'ModelNumber' => $bulk['ModelNumber'],
                        'Client' => $bulk['Client'],
                        'ServiceType' => $bulk['ServiceType'],
                        'ReportedFault' => $bulk['FaultDescription'],
                        'CustHomeTelNo' => $bulk['CustHomeTelNo'],
                        'CustWorkTelNo' => $bulk['CustWorkTelNo'],
                        'CustMobileNo' => $bulk['CustMobileNo'],
                        'CollContactNumber' => $bulk['CollContactNumber']
                    );
                }
                $res = $this->execute($this->conn, $sql, $params);


                $nonSJN = $this->conn->lastInsertId();
            }
        }//end type 6

        if ($bulk) {
            $sql2 = "select * from appointment where  ServiceProviderID=" . $bulk['ServiceProviderID'] . " and SBAppointID=" . $bulk['SBAppointID'] . "";

            $res2 = $this->Query($this->conn, $sql2);

            if (key_exists(0, $res2)) {

                $sql = "update appointment set  `DiaryAllocationID`=:DiaryAllocationID, Duration=:Duration, BookedBy=:BookedBy, Notes=:Notes, AppointmentTime=:AppointmentTime where AppointmentID=:AppointmentID ";
                $params = array(
                    'DiaryAllocationID' => $dalocID[0]['DiaryAllocationID'],
                    'Duration' => $bulk['Duration'],
                    'BookedBy' => 'SB Bulk Import',
                    'Notes' => $bulk['Notes'],
                    'AppointmentTime' => $bulk['TimeSlot'],
                    'AppointmentID' => $res2[0]['AppointmentID']
                );
                $this->execute($this->conn, $sql, $params);

                $result = array('status' => 'SUCCESS', 'appointmentId' => $res2[0]['AppointmentID'], 'SLNumber' => $nonSJN . $JobID);

                return $result;                              /* Successful */
            }
        }

        $extraSQL1 = "";
        $extraSQL2 = "";
        $extraParams = array();
        if ($exclude) {
            $extraSQL1 = "
           
            
             ,ViamenteStartTime
             ,ViamenteTravelTime
             ,ViamenteDepartTime
             ,ViamenteServiceTime
             ,ViamenteReturnTime
             ,ViamenteExclude
                    ";
            $extraSQL2 = "
         
             ,:ViamenteStartTime
             ,:ViamenteTravelTime
             ,:ViamenteDepartTime
             ,:ViamenteServiceTime
             ,:ViamenteReturnTime
             ,:ViamenteExclude
                    ";
            $extraParams = array(
                "ViamenteStartTime" => "00:00:00",
                "ViamenteTravelTime" => "00:00:00",
                "ViamenteDepartTime" => "00:00:00",
                "ViamenteServiceTime" => "00:00:00",
                "ViamenteReturnTime" => "00:00:00",
                "ViamenteExclude" => "Yes"
            );
        }

        $sql = "Insert into appointment (
           AppointmentDate,
           AppointmentType,
           JobID,
           UserID,
           ServiceProviderID,
           NonSkylineJobID,
           DiaryAllocationID,
           AppointmentStartTime,
           AppointmentEndTime,
           AppointmentStartTime2,
           AppointmentEndTime2,
           importance,
           Notes,
           Duration,
           MenRequired, 
           BookedBy,
           ScreenSize,
           ServiceProviderSkillsetID,
           AppointmentTime,
           SBAppointID,
           SBusercode,
           WallMount,
           ServiceProviderEngineerID,
           ForceEngineerToViamente,
           CreatedTimeStamp,
           Latitude,
           Longitude,
           LockPartsEngineer,
           CustContactTime,
           CustContactType,
           PreVisitContactRequired,
           PreVisitContactInfo,
           TempSpecified,
           ForceAddReason
            $extraSQL1
           ) 
           values
           (
           :AppointmentDate,
           :AppointmentType,
           :JobID,
           :UserID,
           :ServiceProviderID,
           :NonSkylineJobID,
           :DiaryAllocationID,
           :AppointmentStartTime,
           :AppointmentEndTime,
           :AppointmentStartTime2,
           :AppointmentEndTime2,
           :importance,
           :Notes,
           :Duration,
           :MenRequired,
           :BookedBy,
           :ScreenSize,
           :ServiceProviderSkillsetID,
           :AppointmentTime,
           :SBAppointID,
           :SBusercode,
           :WallMount,
           :ServiceProviderEngineerID,
           :ForceEngineerToViamente,
           now(),
           :Latitude,
           :Longitude,
           :LockPartsEngineer,
           :CustContactTime,
           :CustContactType,
           :PreVisitContactRequired,
           :PreVisitContactInfo,
           :TempSpecified,
           :ForceAddReason
            $extraSQL2
           
           
           )
        ";

        if ($bulk) {
            $this->controller->log(111, "bulk__");
            $apType = $this->getApType($bulk['ServiceProviderSkillsetID']);

            $slotTM = $this->getSlotsTimes($bulk['ServiceProviderID']);

            if (isset($slotTM[$bulk['TimeSlot']])) {
                $timefrom = $slotTM[$bulk['TimeSlot']]['TimeFrom'];
                $timeto = $slotTM[$bulk['TimeSlot']]['TimeTo'];
            } else {
                $timefrom = "08:30:00"; //default times shouldnt be used in stable live version
                $timeto = "18:30:00";
            }
            if (!$geotag) {
                $lat = null;
                $lng = null;
            } else {
                $lat = $geotag['lat'];
                $lng = $geotag['lng'];
            }
            $engid = false;
            $LockPartsEngineer = "No";
            $ForceEngineerToViamente = '0';


            if (isset($bulk['PartsEngineer'])) {

                $engid = $this->getEngIdFromSBCode($bulk['PartsEngineer'], $bulk['ServiceProviderID']);
            }
            if ($engid) {
                $LockPartsEngineer = "Yes";
                $ForceEngineerToViamente = '1';
            } else {
                $engid = null;
            }
            if ($engid != null) {

                $edata = $this->getEngineerData($engid);
                $engName = $edata[0]['EngineerFirstName'] . " " . $edata[0]['EngineerLastName'] . " ";
                $bulk['Notes'] = "Part delivered to " . $engName . $bulk['Notes'];
            }
            if ($bulk['ServiceProviderID'] == 64) {
                $bulk['username'] = "Whirlpool Import";
            }

            $params = array(
                'AppointmentDate' => $appDate,
                'AppointmentType' => $apType['Type'],
                'JobID' => $JobID,
                'UserID' => $bulk['UserID'],
                'ServiceProviderID' => $bulk['ServiceProviderID'],
                'NonSkylineJobID' => $nonSJN,
                'DiaryAllocationID' => $dalocID[0]['DiaryAllocationID'],
                'AppointmentStartTime' => $timefrom,
                'AppointmentEndTime' => $timeto,
                'AppointmentStartTime2' => null,
                'AppointmentEndTime2' => null,
                'Notes' => $bulk['Notes'],
                'Duration' => $bulk['Duration'],
                'MenRequired' => $bulk['MenRequired'],
                'BookedBy' => $bulk['username'],
                'ScreenSize' => null,
                'AppointmentTime' => $bulk['TimeSlot'],
                'ServiceProviderSkillsetID' => $bulk['ServiceProviderSkillsetID'],
                'SBAppointID' => $bulk['SBAppointID'],
                'SBusercode' => '',
                'WallMount' => $bulk['WallMount'],
                'ServiceProviderEngineerID' => $engid,
                'ForceEngineerToViamente' => $ForceEngineerToViamente,
                'importance' => 0,
                'Latitude' => $lat,
                'Longitude' => $lng,
                'LockPartsEngineer' => $LockPartsEngineer,
                'CustContactTime' => 60,
                'CustContactType' => 'SMS',
                'PreVisitContactRequired'=>'Y',
                'PreVisitContactInfo'=>'Please SMS the customer 60 minutes before arrival.',
                'TempSpecified' => 'No',
                'ForceAddReason' => null
            );
        } else {
            $apType = $this->getApType($post['ServiceProviderSkillsetID']);
            if (isset($this->controller->session->idata['CreatedBy'])) {
                $bookedBy = $this->controller->session->idata['CreatedBy'];
            } else {
                if (isset($post['BookedBy'])) {
                    $bookedBy = $post['BookedBy'];
                } else {
                    $bookedBy = "Service Base";
                }
            }
            if (isset($post['ForceEngineerToViamente'])) {
                $fV = 1;
            } else {
                $fV = 0;
            }
            if ($post['AppointmentStartTime2'] == "") {
                $post['AppointmentStartTime2'] = null;
                $post['AppointmentEndTime2'] = null;
            }

            if (!isset($post['ForceEngineerToViamente']) && $autoForce && sizeof($dalocID) == 1) {
                $fV = 1;
                $post['ServiceProviderEngineerID'] = $dalocID[0]['ServiceProviderEngineerID'];
            }

            if (round($post['lat']) == 0) {
                $lat = null;
                $lng = null;
            } else {
                $lat = $post['lat'];
                $lng = $post['lng'];
            }
            if (!isset($post['BookedBy'])) {
                $post['BookedBy'] = "Service Base";
            }
            if($post['contType'] != "None")
            {
                $contactReq = 'Y';
                $contactReqInfo = $post['PreVisitContactInfo'];
            }
            else
            {
                $contactReq = 'N';
                $contactReqInfo = "";
            }
            $params = array(
                'AppointmentDate' => $this->controller->session->SQLSelectedDate,
                'AppointmentType' => $apType['Type'],
                'JobID' => $JobID,
                'UserID' => $this->controller->user->UserID,
                'ServiceProviderID' => $spID,
                'NonSkylineJobID' => $nonSJN,
                'DiaryAllocationID' => $dalocID[0]['DiaryAllocationID'],
                'AppointmentStartTime' => $post['AppointmentStartTime'],
                'AppointmentEndTime' => $post['AppointmentEndTime'],
                'AppointmentStartTime2' => $post['AppointmentStartTime2'],
                'AppointmentEndTime2' => $post['AppointmentEndTime2'],
                'Notes' => $post['Notes'],
                'Duration' => $post['Duration'],
                'MenRequired' => $post['MenRequired'],
                'BookedBy' => $post['BookedBy'],
                'ScreenSize' => $post['ScreenSize'],
                'AppointmentTime' => $post['timeSlot'],
                'ServiceProviderSkillsetID' => $post['ServiceProviderSkillsetID'],
                'SBAppointID' => null,
                'SBusercode' => $this->controller->session->idata['usercode'],
                'WallMount' => $this->controller->session->idata['WallMount'],
                'ServiceProviderEngineerID' => $post['ServiceProviderEngineerID'],
                'ForceEngineerToViamente' => $fV,
                'importance' => $post['importance'],
                'Latitude' => $lat,
                'Longitude' => $lng,
                'LockPartsEngineer' => $post['lockEng'],
                'CustContactTime' => $post['contTime'],
                'CustContactType' => $post['contType'],
                'PreVisitContactRequired'=>$contactReq,
                'PreVisitContactInfo'=>$contactReqInfo,
                'TempSpecified' => $post['TempSpecified'],
                'ForceAddReason' => $post['ForceAddReason']
            );
        }
        if ($exclude) {
            $params = array_merge($params, $extraParams);
        }
//        echo"<pre>";
//        print_r($params);
//        echo"</pre>";
//        die();
        $res = $this->execute($this->conn, $sql, $params);
        $apID = $this->conn->lastInsertId();
        //  $this->updateSlotsLeft($dalocID[0]['DiaryAllocationID'],-1);
        if (!$bulk) {

            $EmailModel = $this->controller->loadModel('Email');
            $EmailModel->sendAppointmentDetailsEmail($apID, "date_confirmed");

            return $apID;
        } else {
            $result = array('status' => 'SUCCESS', 'appointmentId' => $apID, 'SLNumber' => $nonSJN . $JobID);
            $this->controller->log($result, "Imported_Appointments_For_SPID_" . $bulk['ServiceProviderID'] . "_");
            return $result;
        }
    }

    //this method gets diary allocation id where to insert new appointment
    public function getDiaryAllocationID($slot, $skillset, $postcode, $spID, $date = false, $bulk = false, $name = false, $geotag = false, $limit = false) {
        if ($slot == "LL") {
            $slot == "ANY";
            $trr = "";
        } ELSE {
            //$trr="and da.SlotsLeft>0";
            $trr = "";
        }

        if ($date) {
            $dateFilter = "and da.AllocatedDate='$date'";
        } else {
            $dateFilter = "";
        }
        if ($name) {
            $sqlName = ",concat(spe.EngineerFirstName,' ',spe.EngineerLastName) as 'name',
              (select count(*) from appointment aa 
              where aa.ServiceProviderEngineerID=da.ServiceProviderEngineerID and aa.AppointmentDate=:AllocatedDate) as 'appCount',
              if(dhs.DiaryHolidaySlotsID is null,'',concat_ws(' ','Holiday',sec_to_time(dhs.StartTimeSec),'-',sec_to_time(dhs.EndTimeSec))) as 'holiday'
              
                    ";
        } else {
            $sqlName = "";
        }

        if ($limit) {
            $order = "and dhs.StartTimeSec is null order by 'appCount' asc limit 1";
        } else {
            $order = "order by da.SlotsLeft Desc";
        }


        $postCodeAlpha = "";
        $postcodeSQL = "";
        $geofilter = "";
        $postCode = $postcode;
        if (!$geotag) {
            $postCode2 = $postCode;
            if (strlen($postCode) > 4) {
                $postCode2 = trim(substr(trim($postCode), 0, -3));
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            } else {
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            }
            $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
            if (!is_numeric(substr($postCodeOutCode, -1))) {
                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
            }
            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */

            $postcodeSQL = "AND 
                          (
                            dpa.`PostCode` = '$postcode'
                            OR dpa.`PostCode` = '$postCodeAlpha'
                            OR dpa.`PostCode` = '$postCodeOutCode'
                            OR dpa.`PostCode` = '$postCodeArea'
                          )";
            $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";
        } else {
            $lat = $geotag['lat'];
            $lng = $geotag['lng'];
            $allocationTable = "join sp_geo_cells sgc on sgc.DiaryAllocationID=da.DiaryAllocationID";
            $geofilter = "and sgc.Lat1>=$lat and sgc.Lat2<=$lat and sgc.Lng1<=$lng and sgc.Lng2>=$lng";
        }


        $sql = "
             select distinct(da.DiaryAllocationID),da.ServiceProviderEngineerID $sqlName from diary_allocation da 
 $allocationTable
join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
left join diary_holiday_slots dhs on dhs.ServiceProviderEngineerID=sped.ServiceProviderEngineerID and dhs.HolidayDate=:AllocatedDate
where da.ServiceProviderID=:ServiceProviderID

and aas.Type=:Type 
and da.AllocatedDate=:AllocatedDate

and spe.Status='Active' 
  $postcodeSQL
          $geofilter
and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
$order 
$trr



            ";



        if ($this->debug)
            $this->controller->log("Diary allocator Sql=" . $sql, 'diary_log');
        if ($date) {
            $sqlDate = $date;
        } else {
            $sqlDate = $this->controller->session->SQLSelectedDate;
        }
        $params = array(
            'ServiceProviderSkillsetID' => $skillset,
            'ServiceProviderID' => $spID,
            'AllocatedDate' => $sqlDate,
            'Type' => $slot
        );

        if ($this->debug)
            $this->controller->log("Diary allocator Params=" . var_export($params, true), 'diary_log');


        $res = $this->Query($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log("Diary allocator Params=" . var_export($res, true), 'diary_log');
        if ($bulk != false) {

            if (!key_exists(0, $res)) {
                $sql2 = "select da.DiaryAllocationID from diary_allocation da 
            join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
         
            where 
           da.ServiceProviderID=:ServiceProviderID
          
          
          
           and da.AllocatedDate=:AllocatedDate
           and aas.Type=:Type 
           order by da.SlotsLeft Desc limit 1
           ";

                $params2 = array(
                    'ServiceProviderID' => $spID,
                    'AllocatedDate' => $sqlDate,
                    'Type' => $slot
                );
                $res2 = $this->Query($this->conn, $sql2, $params2);


                if (key_exists(0, $res2)) {
                    return $res2;
                } else {
                    return array('error' => 'true');
                }
            } else {
                return $res;
            }
        } else {
            if ($this->debug)
                $this->controller->log($res, "dalcolisteng_____");
            if ($this->debug)
                $this->controller->log($sql, "dalcolisteng_____");
            return $res;
        }
    }

    public function getEngineersList($spID, $date = false, $skillset = false, $eng = false, $postCode = false) 
    {
        $sqlDateFilter = "";
        $sqlJoin = "";
        $engfilter = "";
        $selectSql ="";
        
        if ($date) 
        {
            $selectSql       = ",(select Reason 
                                from 
                                diary_holiday_slots dhs 
                                join diary_holiday_diary dhd on dhd.DiaryHolidayID=dhs.DiaryHolidayDiaryID
                                where dhs.HolidayDate='$date' and dhs.ServiceProviderEngineerID=spe.ServiceProviderEngineerID limit 1) as hasHoliday"; 
            $sqlJoin         = "join service_provider_engineer_details sped 
                               ON 
                               sped.ServiceProviderEngineerID=spe.ServiceProviderEngineerID";

            $sqlDateFilter   = "and sped.`Status`='Active' 
                                and sped.WorkDate='$date'";
        }
        if ($skillset && $date) 
        {
            $sqlJoin        .=" join service_provider_engineer_skillset_day spes 
                                on spes.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID";
            $sqlDateFilter  .=" and spes.ServiceProviderSkillsetID=$skillset";
        }
        if ($postCode && $date) 
        {
            
            $postCode2 = $postCode;
            if (strlen($postCode) > 4) {
                
                $postCode2 = trim(substr(trim($postCode), 0, -3));
                
                if (!is_numeric(substr($postCode2, -1))) 
                {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            } else 
            {
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            }
            $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

            $postCodeOutCode = $postCode2;     
            
            /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
            
            if (!is_numeric(substr($postCodeOutCode, -1))) 
            {
                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
            }
            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); 
            
            /* Get the Post code Area (eg CV) */
            $sqlDateFilter.= " AND 
                          (
                            dpa.`PostCode` = '$postCode'
                            OR dpa.`PostCode` = '$postCodeAlpha'
                            OR dpa.`PostCode` = '$postCodeOutCode'
                            OR dpa.`PostCode` = '$postCodeArea'
                          )";
            
            $sqlJoin.=" join diary_allocation da 
                        on da.ServiceProviderEngineerID=spe.ServiceProviderEngineerID 
                        and da.AllocatedDate='$date'
                        join diary_postcode_allocation dpa 
                        on dpa.DiaryAllocationID=da.DiaryAllocationID";
        }
        if ($eng) {
            $engfilter = " and spe.ServiceProviderEngineerID=$eng";
        }
        
          $sql = "select spe.EngineerFirstName, 
                       spe.EngineerLastName, 
                       spe.ServiceProviderEngineerID
                       $selectSql
                       from service_provider_engineer spe 
                       $sqlJoin  
                       where  spe.ServiceProviderID=$spID 
                       and    spe.status='Active' 
                       $sqlDateFilter
                       $engfilter
                       group by spe.ServiceProviderEngineerID 
                       order by spe.EngineerFirstName asc,  spe.EngineerLastName asc";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

//   public function getIDDetails($JobTypeID,$ServiceTypeID,$UnitTypeID,$ManufacturerID,$ModelID,$spID){
//       $sql="select sps.ServiceProviderSkillsetID,
//           ut.UnitTypeName as'UnitType',
//           rs.RepairSkillName,
//          
//           sps.ServiceDuration,
//           (select ServiceTypeName from service_type where ServiceTypeID=:ServiceTypeID) as 'ServiceType',
//          (select ManufacturerName from manufacturer where ManufacturerID=:ManufacturerID) as 'Manufacturer',
//           (select ModelNumber  from model where ModelID=:ModelID) as 'Model'
//            from service_provider_skillset sps
//join skillset sk on sk.SkillsetID=sps.SkillsetID
//join service_provider_engineer_skillset spes on spes.ServiceProviderSkillsetID=sps.ServiceProviderSkillsetID
//join unit_type ut on ut.RepairSkillID=sk.RepairSkillID
//join repair_skill rs on rs.RepairSkillID=sk.RepairSkillID
//
//           
//                where sps.ServiceProviderID=$spID 
//                    and sps.Status='Active'
//                    and sk.Status='Active'
//                    and ut.UnitTypeID=:UnitTypeID
//                  
//       
//       ";
//      
//      $params=array(
//           'ServiceTypeID'=>$ServiceTypeID,
//           'JobTypeID'=>$JobTypeID,
//           'UnitTypeID'=>$UnitTypeID,
//           'ManufacturerID'=>$ManufacturerID,
//           'ModelID'=>$ModelID
//           );
//      $res=$this->Query($this->conn, $sql,$params);
//
//       return $res;
//      
//    }

    public function EditAppointment($appID) {

        $sql = "select 
            *,
           nsj.ServiceType as ServiceType ,
            IF (ISNULL(j.JobID),
				nj.NonSkylineJobID
			,
				j.JobID
			) AS JobID,
            IF (ISNULL(j.JobID),
				nj.CustMobileNo
			,
				c.ContactMobile
			) AS Cust_MobileNo,
            IF (ISNULL(j.JobID),
            
				nj.CollContactNumber
			,
				j.ColAddPhone
			) AS Coll_ContactNumber,
            IF (ISNULL(j.JobID),
            
				nj.CustHomeTelNo
			,
				c.ContactHomePhone
			) AS Cust_HomeTelNo,
                        
            IF (ISNULL(j.JobID),
				nj.CollContactNumber
			,
				c.ContactWorkPhone
			) AS Cust_WorkTelNo,
                        
            concat(nj.CustomerTitle,' ',nj.CustomerSurname) as 'name2',  
            CONCAT(c.ContactFirstName,' ',c.ContactLastName) AS 'name',
            ap.AppointmentID,
            ap.Notes,
            IF (ISNULL(j.JobID), nsj.CustomerEmail, c.ContactEmail) AS custEmail,
             concat(date_format(j.DateBooked,'%d/%m/%Y'),' (',date_format(j.TimeBooked,'%H:%i'),')') as 'jobDateTimeBooked',
            IF (ISNULL(c.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            c.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS  postCode,
            nj.ModelNumber as Number2,
            if((j.ReportedFault is null or j.ReportedFault=''),
                nsj.ReportedFault,
                j.ReportedFault)as ReportedFault,
                ap.BookedBy
            
            from appointment ap 
          
           
           left join job j on j.JobID=ap.JobID
           left join non_skyline_job nj on nj.NonSkylineJobID=ap.NonSkylineJobID
           left join customer c on c.CustomerID=j.CustomerID
           left join  non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
           left join  customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
           left join  service_type st on st.ServiceTypeID=j.ServiceTypeID
           left join  model mo on mo.ModelID=j.ModelID
           left join  unit_type ut on ut.UnitTypeID=mo.UnitTypeID
           left join  manufacturer m on m.ManufacturerID=j.ManufacturerID
           
    where ap.AppointmentID=:AppointmentID ";
        $params = array('AppointmentID' => $appID);
        $res = $this->Query($this->conn, $sql, $params);

        return $res;
    }

    public function getModifiedGeoTag($pc, $spID) {
        $sql = "select * from service_provider_geotags where Postcode='$pc' and ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        if (sizeof($res) > 0) {

            return true;
        } else {
            return false;
        }
    }

    public function updateAppointment($post, $spID, $exclude = false) {

        $postcodeTMP = explode(' ', $post['postcode']);
        $postcode = $postcodeTMP[0];
        if (isset($post['timeSlot']) && !isset($post['fullslot']) && !isset($post['DiaryAllocationID'])) {
            if ($this->controller->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                $GO = $this->controller->session->addressGeoTag;
            } else {
                $GO = null;
            }
            $dalocID = $this->getDiaryAllocationID($post['timeSlot'], $post['ServiceProviderSkillsetID'], $postcode, $spID, false, false, false, $GO);

            if (!isset($dalocID[0]['DiaryAllocationID'])) {
                die("Error:0002 : Diary allocation slot cannot be allocated for your selected skillset in given day: Possible reasons:<br>1.Appointment postcode does not match given slot<br>2. Skillset does not mach given slot");
            }
            if ($dalocID[0]['DiaryAllocationID'] != $post['DiaryAllocationID']) {
                //$this->updateSlotsLeft($post['DiaryAllocationID'],1);
                // $this->updateSlotsLeft($dalocID[0]['DiaryAllocationID'],-1);
            }
        } else {
            $dalocID[0]['DiaryAllocationID'] = $post['DiaryAllocationID'];
        }

        $extraSQL1 = ",ViamenteExclude=:ViamenteExclude";

        $extraParams = array("ViamenteExclude" => "No");
        if ($exclude) {
            $extraSQL1 = "
           
             
             ,ViamenteStartTime=:ViamenteStartTime
             ,ViamenteTravelTime=:ViamenteTravelTime
             ,ViamenteDepartTime=:ViamenteDepartTime
             ,ViamenteServiceTime=:ViamenteServiceTime
             ,ViamenteReturnTime=:ViamenteReturnTime
             ,ViamenteExclude=:ViamenteExclude
             ,ViamenteUnreached=:ViamenteUnreached
                    ";

            $extraParams = array(
                "ViamenteStartTime" => "00:00:00",
                "ViamenteTravelTime" => "00:00:00",
                "ViamenteDepartTime" => "00:00:00",
                "ViamenteServiceTime" => "00:00:00",
                "ViamenteReturnTime" => "00:00:00",
                "ViamenteExclude" => "Yes",
                "ViamenteUnreached" => 0
            );
        }
        $sql = "update appointment set 
            AppointmentType=:AppointmentType,
            AppointmentStartTime=:AppointmentStartTime,
            AppointmentEndTime=:AppointmentEndTime,
           AppointmentStartTime2=:AppointmentStartTime2,
            AppointmentEndTime2=:AppointmentEndTime2,
            MenRequired=:MenRequired,
            importance=:importance,
            ServiceProviderSkillsetID=:ServiceProviderSkillsetID,
            Duration=:Duration,
            Notes=:Notes,
            BookedBy=:BookedBy,
            ScreenSize=:ScreenSize,
            AppointmentTime=:AppointmentTime,
            DiaryAllocationID=:daID,
            ServiceProviderEngineerID=:ServiceProviderEngineerID,
            ForceEngineerToViamente=:ForceEngineerToViamente,
            CustContactTime=:CustContactTime,
            CustContactType=:CustContactType,
            PreVisitContactRequired=:PreVisitContactRequired,
            PreVisitContactInfo=:PreVisitContactInfo,
            TempSpecified=:TempSpecified,
            SamsungOneTouchChangeReason=:SamsungOneTouchChangeReason
            $extraSQL1
            where AppointmentID=:AppointmentID and ServiceProviderID=$spID
            ";
        $apType = $this->getApType($post['ServiceProviderSkillsetID']);
        if (!isset($post['timeSlot'])) {
            $post['timeSlot'] = "ANY";
        }
        if (isset($post['ForceEngineerToViamente'])) {
            $fv = '1';
            $eng = $post['ServiceProviderEngineerID'];
        } else {
            $fv = '0';
            $eng = null;
        }
        if ($post['AppointmentStartTime2'] == "") {
            $post['AppointmentStartTime2'] = null;
            $post['AppointmentEndTime2'] = null;
        }
        if($post['contType'] != "None")
        {
            $contactReq = 'Y';
            $contactReqInfo = $post['PreVisitContactInfo'];
        }
        else
        {
            $contactReq = 'N';
            $contactReqInfo = "";
        }
        $params = array(
            'AppointmentType' => $apType['Type'],
            'AppointmentStartTime' => $post['AppointmentStartTime'],
            'AppointmentEndTime' => $post['AppointmentEndTime'],
            'AppointmentStartTime2' => $post['AppointmentStartTime2'],
            'AppointmentEndTime2' => $post['AppointmentEndTime2'],
            'MenRequired' => $post['MenRequired'],
            'importance' => $post['importance'],
            'ServiceProviderSkillsetID' => $post['ServiceProviderSkillsetID'],
            'Duration' => $post['Duration'],
            'AppointmentID' => $post['appID'],
            'Notes' => $post['Notes'],
            'BookedBy' => $post['BookedBy'],
            'ScreenSize' => $post['ScreenSize'],
            'AppointmentTime' => $post['timeSlot'],
            'daID' => $dalocID[0]['DiaryAllocationID'],
            'ServiceProviderEngineerID' => $eng,
            'ForceEngineerToViamente' => $fv,
            'CustContactTime' => $post['contTime'],
            'CustContactType' => $post['contType'],
            'PreVisitContactRequired' => $contactReq,
            'PreVisitContactInfo' => $contactReqInfo,
            'TempSpecified' => $post['TempSpecified'],
            'SamsungOneTouchChangeReason' => $post['SamsungOneTouchChangeReason']
        );
        $params = array_merge($params, $extraParams);
        $this->controller->log($sql,'Viamenteupdatelog');
        $res = $this->execute($this->conn, $sql, $params);
        $sql = "";
        return;
    }

    public function deleteAppointment($delID, $spID) {
        $sql = "select DiaryAllocationID from appointment  where AppointmentID=:AppointmentID and ServiceProviderID=:ServiceProviderID";
        $sql2 = "delete from appointment where AppointmentID=:AppointmentID and ServiceProviderID=:ServiceProviderID";
        $params = array('AppointmentID' => $delID, 'ServiceProviderID' => $spID);
        $res = $this->Query($this->conn, $sql, $params);
        $res2 = $this->execute($this->conn, $sql2, $params);
        //$this->updateSlotsLeft($res[0]['DiaryAllocationID'],1);
        return;
    }

    ////Version 0.01 This method should optimise any given day and postcode to fit maximum appointments
    //V1 - will optimize if given skillset is full 

    public function diaryLocationOptimize($fullSkillSetID, $dAlocationIDs) {
        // echo"$dAlocationIDs";
        if (substr($dAlocationIDs, -1) == ",") {
            $dAlocationIDs = substr($dAlocationIDs, 0, -1);
            $dAlocationIDs = str_replace(",,", ",", $dAlocationIDs);
            // echo"$dAlocationIDs <br>";
        }
        //getting DiaryAllocationID and ServiceProviderSkillsetID for currently full diary allocation
        $sql = "select da.DiaryAllocationID,spesd.ServiceProviderSkillsetID,sps.EngineersRequired,da.ServiceProviderEngineerID
            
            from diary_allocation da
            
           join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
        join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
             join service_provider_skillset sps on sps.ServiceProviderSkillsetID=spesd.ServiceProviderSkillsetID
    where da.DiaryAllocationID in ($dAlocationIDs) and spesd.ServiceProviderSkillsetID=$fullSkillSetID  and sped.`Status`='Active'
			  and sped.WorkDate=da.AllocatedDate
        
        ";
        // $this->controller->log($sql,'test_log');
        $res = $this->Query($this->conn, $sql);

        if (!array_key_exists(0, $res)) {
            return false;
        }
        for ($l = 0; $l < sizeof($res); $l++) {
            $engID = $res[$l]['DiaryAllocationID']; //curent diary allcoation id
            $fullman = $res[$l]['EngineersRequired']; //curent man required
            //getting other skill for current engineer //TODO later needs to be list of skills
            $sql2 = "select distinct(spesd.ServiceProviderSkillsetID) ,da.ServiceProviderEngineerID,sps.EngineersRequired,da.AllocatedDate
        from diary_allocation da 
           join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
        join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
            join appointment ap on da.DiaryAllocationID=ap.DiaryAllocationID
          join service_provider_skillset sps on sps.ServiceProviderSkillsetID=spesd.ServiceProviderSkillsetID
            where da.DiaryAllocationID=$engID and spesd.ServiceProviderSkillsetID !=$fullSkillSetID and sps.EngineersRequired!=$fullman
        and sped.WorkDate=da.AllocatedDate and sped.`Status`='Active'
    ";
            $res2 = $this->Query($this->conn, $sql2);

//echo"---------------------------------";
            if (!array_key_exists(0, $res2)) {
                return false;
            }

            //getting another engineer who got same secondary skill and have slots left
            for ($u = 0; $u < sizeof($res2); $u++) {
                $otherSkillID = $res2[$u]['ServiceProviderSkillsetID']; //other skill id TODO need create array of skills in future
                $otherman = $res2[$u]['EngineersRequired']; //other skill id TODO need create array of skills in future
                $thisEngID = $res2[$u]['ServiceProviderEngineerID']; //curent engineer id
                $day = $res2[$u]['AllocatedDate']; //curent engineer id

                $sql3 = "select da.DiaryAllocationID,sped.ServiceProviderEngineerID
            
            from diary_allocation da
              join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
        join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
            
    where da.DiaryAllocationID not in ($dAlocationIDs) and spesd.ServiceProviderSkillsetID=$otherSkillID and da.AllocatedDate='$day'
          and sped.WorkDate=da.AllocatedDate and sped.`Status`='Active'
         ";

                $res3 = $this->Query($this->conn, $sql3);

                if (array_key_exists(0, $res3)) {
                    $otherDaID = $res3[0]['DiaryAllocationID'];
                    $otherEngID = $res3[0]['ServiceProviderEngineerID'];
                    $sql4 = "select AppointmentID from appointment ap where ap.DiaryAllocationID=$engID and ServiceProviderSkillsetID=$otherSkillID";
//       echo"-------------res4--------------------";
                    $res4 = $this->Query($this->conn, $sql4);

//echo"-------------res4--------------------";
                    if (array_key_exists(0, $res4)) {
                        $newAppID = $res4[0]['AppointmentID'];
                        $sql5 = "Update appointment set DiaryAllocationID=$otherDaID where AppointmentID=$newAppID";

                        if ($this->debug)
                            $this->controller->log($sql5, 'update_optimize');
                        $this->execute($this->conn, $sql5);
                        return true;
                    }
                }
            }
        }
        // $this->updateSlotsLeft($engID,1);//seting correct slot left values
        // $this->updateSlotsLeft($otherDaID,-1);//seting correct slot left values
        return true;
    }

    public function updateSlotsLeft($daID, $value) {
        $sql = "update diary_allocation set SlotsLeft=SlotsLeft+$value where DiaryAllocationID=$daID";
        $this->execute($this->conn, $sql);
    }

    public function addDiarySlot($post, $spID) {

        $sql = "insert into diary_allocation 
            (
            ServiceProviderID,
            AllocatedDate,
            AppointmentAllocationSlotID,
            NumberOfAllocations,
            ServiceProviderEngineerID,
            SlotsLeft
            ) values (
            :ServiceProviderID,
            :AllocatedDate,
            :AppointmentAllocationSlotID,
            :NumberOfAllocations,
            :ServiceProviderEngineerID,
            :SlotsLeft
            )
            ";

        $params = array(
            'ServiceProviderID' => $spID,
            'AllocatedDate' => $post['dat'],
            'AppointmentAllocationSlotID' => $post['timeSlot'],
            'NumberOfAllocations' => $post['NumberOfAllocations'],
            'ServiceProviderEngineerID' => $post['ServiceProviderEngineerID'],
            'SlotsLeft' => $post['NumberOfAllocations']
        );
        $this->execute($this->conn, $sql, $params);
        $diaryAlocID = $this->conn->lastInsertId();
        $postcodes = explode(',', $post['Postcodes']);
        $sql = "insert into diary_postcode_allocation (DiaryAllocationID,Postcode) values";
        for ($i = 0; $i < sizeof($postcodes); $i++) {
            $sql.="(" . "$diaryAlocID,'" . $postcodes[$i] . "')";
            if ($i < sizeof($postcodes) - 1) {
                $sql.=",";
            }
        }
        $this->execute($this->conn, $sql);
        return;
    }

    //this method returns is job skyline, non skyline or not in db
    public function getJobSource($sbID, $spID) {
        $sql = "select * from job where ServiceCentreJobNo=:jobNo and ServiceProviderID=:spID";
        $params = array('jobNo' => $sbID, 'spID' => $spID);
        $res = $this->Query($this->conn, $sql, $params);
        if (array_key_exists(0, $res)) {
            return array('JobID' => $res[0]['JobID']);
        } else {

            $sql = "select * from non_skyline_job where ServiceProviderJobNo=:jobNo and ServiceProviderID=:spID";

            $res = $this->Query($this->conn, $sql, $params);
            if (array_key_exists(0, $res)) {
                return array('NonSkylineJobID' => $res[0]['NonSkylineJobID']);
            } else {
                return NULL;
            }
        }
    }

    public function getSkillInfo($spSkill) {
        $sql = "select sps.ServiceDuration,Type,RepairSkillID,at.AppointmentTypeID from service_provider_skillset  sps
            join skillset s on s.SkillsetID=sps.SkillsetID
            join appointment_type at on at.AppointmentTypeID=s.AppointmentTypeID       
            where ServiceProviderSkillsetID=$spSkill";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return array('RepairSkillID' => null, 'ServiceDuration' => null, 'Type' => null);
        }
    }

    public function getApType($spSkill) {
        $sql = "select at.Type from service_provider_skillset sps 
            join skillset s on s.SkillsetID=sps.SkillsetID
            join appointment_type at on at.AppointmentTypeID=s.AppointmentTypeID

where sps.ServiceProviderSkillsetID=:ServiceProviderSkillsetID";
        $params = array("ServiceProviderSkillsetID" => $spSkill);
        $res = $this->Query($this->conn, $sql, $params);
        return $res[0];
    }

    public function getRepairTypeIDFromSPSkillset($spSkill) {
        $sql = "select RepairSkillID from service_provider_skillset sps 
            join skillset s on s.SkillsetID=sps.SkillsetID
            

where sps.ServiceProviderSkillsetID=:ServiceProviderSkillsetID";
        $params = array("ServiceProviderSkillsetID" => $spSkill);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0]['RepairSkillID'])) {
            return $res[0]['RepairSkillID'];
        } else {
            return 0;
        }
    }

    public function getRepairType($appID) {
        $sql = "select s.RepairSkillID,RepairSkillName from appointment ap  
            join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
            join skillset s on s.SkillsetID=sps.SkillsetID
            join repair_skill rs on rs.RepairSkillID=s.RepairSkillID
            where AppointmentID=:AppointmentID
            ";
        $params = array("AppointmentID" => $appID);
        $res = $this->Query($this->conn, $sql, $params);

        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function getAppointmentDetails($appID) {
        $sql = "select * ,
           
            
                ap.BookedBy as BookedBy2,
            IF (ISNULL(c.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            c.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS  postCode
                           
            from appointment ap 
            left join diary_allocation da on da.DiaryAllocationID=ap.DiaryAllocationID
            left join job j on j.JobID=ap.JobID
            left join customer c on c.CustomerID=j.CustomerID
            left join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
            join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID	
            join skillset sk on sk.SkillsetID=sps.SkillsetID
            where ap.AppointmentID=:AppointmentID 
            ";
        $params = array('AppointmentID' => $appID);
        $res = $this->Query($this->conn, $sql, $params);

        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function confirmRebook($date, $appID, $slotType, $skillSet, $postcode, $spID, $grid = false) {
        $apDetails = $this->getAppointmentDetails($appID);

        $skillSet = $apDetails['ServiceProviderSkillsetID'];
        $postcode = $apDetails['postCode'];
        $postcodeArr = explode(' ', $postcode);
        $postcode = $postcodeArr[0];
        if (!$grid) {
            $newDiaryID = $this->getDiaryAllocationID($slotType, $skillSet, $postcode, $spID, $date, false, false, false);
        } else {
            $newDiaryID = $this->getDiaryAllocationIDForce($date, 0, $spID);
        }
        if (isset($newDiaryID[0]['DiaryAllocationID'])) {
            $newDiaryID = $newDiaryID[0]['DiaryAllocationID'];
        } else {
            return false;
        }

        //  $this->updateSlotsLeft($apDetails['DiaryAllocationID'],1);
        // $this->updateSlotsLeft($newDiaryID,-1);
        if (isset($this->controller->session->samsung_reason)) {
            $samChange = $this->controller->session->samsung_reason;
        } else {
            $samChange = null;
        }
        $sql = "Update  appointment set AppointmentDate=:AppointmentDate,DiaryAllocationID=$newDiaryID, AppointmentTime=:AppointmentTime,SamsungOneTouchChangeReason=:SamsungOneTouchChangeReason  where AppointmentID=:AppointmentID";
        $params = array('AppointmentDate' => $date, 'AppointmentID' => $appID, 'AppointmentTime' => $slotType, 'SamsungOneTouchChangeReason' => $samChange);
        $res = $this->execute($this->conn, $sql, $params);


        return true;
    }

//    public function skillsets2engineers($sSets){
//     
//        $sql="select * from service_provider_engineer_skillset spes 
//        left join service_provider_engineer spe on spe.ServiceProviderEngineerID=spes.ServiceProviderEngineerID
//        where  spes.ServiceProviderSkillsetID in ($sSets) and spe.Status='Active'";
//        $res=$this->Query($this->conn, $sql);
//
//         return $res;
//        
//    }

    public function getAssignedEngineers($date, $postcode = null, $spID, $mode = false) {
        $date2 = explode('-', $date);
        $extraWhere = "";
        if ($mode == "Brown") {
            $extraWhere = "and spe.PrimarySkill='Brown Goods'";
        }
        if ($mode == "White") {
            $extraWhere = "and spe.PrimarySkill='White Goods'";
        }

        $sql = "select distinct sped.ServiceProviderEngineerID from service_provider_engineer_details sped 
           
            join service_provider_engineer spe on spe.ServiceProviderEngineerID=sped.ServiceProviderEngineerID
       
            where  
           spe.Status='Active' and sped.Status='Active'  and sped.WorkDate=:AllocatedDate and spe.ServiceProviderID=$spID and spe.ViamenteExclude='No' 
               $extraWhere
           
            ";
        // die($sql);

        $params = array('AllocatedDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);

        for ($i = 0; $i < sizeof($res); $i++) {

            $res2[$i] = $res[$i]['ServiceProviderEngineerID'];
        }

        if (isset($res2) && sizeof($res) > 0) {
            return $res2;
        } else {
            return array();
        }
    }

    public function checkIfUseSB($spID) {
        $sql = "select * from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        if ($res[0]['IPAddress'] != "") {
            return true;
        } else {
            return false;
        }
    }

    public function getRepairsList() {
        $sql = "select * from repair_skill";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getUUID($d, $p) {
        $sql = "select * from diary_sp_map where ServiceProviderID=$p and MapDate='$d'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0]['MapUUID'])) {
            return $res[0]['MapUUID'];
        } else {
            return false;
        }
    }

    public function SaveMapUUID($id, $sp, $date) {
        $sql = "select * from diary_sp_map  where ServiceProviderID=$sp and MapDate='$date' 
        ";
        $res = $this->Query($this->conn, $sql);

        if (key_exists(0, $res)) {
            $sql = "update diary_sp_map dsp set MapUUID='$id', ServiceProviderID=$sp , MapDate='$date' where DiarySPMapID=" . $res[0]['diarySPMapID'];
        } else {
            $sql = "insert into diary_sp_map (ServiceProviderID, MapDate, MapUUID) values ( $sp,'$date','$id')";
        }
        $res = $this->execute($this->conn, $sql);
    }

    public function removeUnsyncApp($spID) {
        $sql = "select * from appointment ap where ServiceProviderID=$spID and (SBAppointID is null or SBAppointID=0) and not DATE_SUB(now(),INTERVAL 5 MINUTE)<=CreatedTimeStamp and ap.BookedBy!='Samsung One Touch'";
        $res = $this->Query($this->conn, $sql);

        foreach ($res as $r) {
            $sql = "delete from appointment  where AppointmentID=" . $r['AppointmentID'];
            $res = $this->execute($this->conn, $sql);
            //    $this->updateSlotsLeft($r['DiaryAllocationID'],1);//seting correct slot left values
        }
    }

    public function repairSkillName($id) {
        $sql = "select RepairSkillName from repair_skill where RepairSkillID=$id";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['RepairSkillName'];
    }

    public function AppTypeName($name) {
        $sql = "select AppointmentTypeID from appointment_type where Type=:type";
        $params = array('type' => $name);
        $res = $this->Query($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log($sql, "ap_type_name_");
        if ($this->debug)
            $this->controller->log($params, "ap_type_name_");
        if (isset($res[0])) {
            return $res[0]['AppointmentTypeID'];
        } else {
            return 5;
        }
    }

    public function updateEngineerWorkload($data, $date, $spID, $type = 'all') {
        //clean old values for seleceted day !important
        if ($type == 'all'&&$spID!=64) {//not run on visualfx becouse of 2 keys used
            $sql = "delete from service_provider_engineer_workload where WorkingDay='$date' and ServiceProviderID=$spID";
            $this->execute($this->conn, $sql);
            //clean old values for seleceted day !important
        }
        for ($i = 0; $i < sizeof($data); $i++) {

            $engID = $this->enginnerName2ID($data[$i]->vehicleName, $spID);
            if ($engID) {
                $sql = "select * from service_provider_engineer_workload where ServiceProviderEngineerID=:ServiceProviderEngineerID and WorkingDay=:WorkingDay";
                $params = array("ServiceProviderEngineerID" => $engID, "WorkingDay" => $date);
                $res = $this->Query($this->conn, $sql, $params);

                if (isset($res[0]['ServiceProviderEngineerWorkloadID'])) {
                    $sql = "update service_provider_engineer_workload set 
                        TotalWorkTimeSec=:TotalWorkTimeSec,
                        TotalIdleTimeSec=:TotalIdleTimeSec,
                        TotalServiceTimeSec=:TotalServiceTimeSec,
                        TotalDriveTimeSec=:TotalDriveTimeSec,
                        TotalSteps=:TotalSteps
                        where ServiceProviderEngineerWorkloadID=:ServiceProviderEngineerWorkloadID";
                    $params = array(
                        'ServiceProviderEngineerWorkloadID' => $res[0]['ServiceProviderEngineerWorkloadID'],
                        'TotalWorkTimeSec' => $data[$i]->totWorkTimeSec,
                        'TotalServiceTimeSec' => $data[$i]->totServiceTimeSec,
                        'TotalDriveTimeSec' => $data[$i]->totDriveTimeSec,
                        'TotalSteps' => sizeof($data[$i]->steps),
                        'TotalIdleTimeSec' => $data[$i]->totIdleTimeSec
                    );
                } else {
                    $sql = "insert into service_provider_engineer_workload  
                        (
                        TotalWorkTimeSec,
                        TotalIdleTimeSec,
                        TotalServiceTimeSec,
                        TotalDriveTimeSec,
                        TotalSteps,
                        ServiceProviderID,
                        WorkingDay,
                        ServiceProviderEngineerID
                        )  
                        values
                        (
                        :TotalWorkTimeSec,
                        :TotalIdleTimeSec,
                        :TotalServiceTimeSec,
                        :TotalDriveTimeSec,
                        :TotalSteps,
                        :ServiceProviderID,
                        :WorkingDay,
                        :ServiceProviderEngineerID
                             )
                       ";
                    $params = array(
                        'TotalWorkTimeSec' => $data[$i]->totWorkTimeSec,
                        'TotalServiceTimeSec' => $data[$i]->totServiceTimeSec,
                        'TotalDriveTimeSec' => $data[$i]->totDriveTimeSec,
                        'TotalSteps' => sizeof($data[$i]->steps),
                        'TotalIdleTimeSec' => $data[$i]->totIdleTimeSec,
                        'WorkingDay' => $date,
                        'ServiceProviderID' => $spID,
                        'ServiceProviderEngineerID' => $engID
                    );
                }
                $res = $this->execute($this->conn, $sql, $params);
            }
            $steps = $data[$i]->steps;
            for ($z = 1; $z < sizeof($steps) - 1; $z++) {
                if (isset($steps[$z]->waypointName)) {
                    $appIDTmp = explode(' ', $steps[$z]->waypointName);
                    $appID = $appIDTmp[sizeof($appIDTmp) - 1];
//               echo $appID;
//               echo"<br>";
                    $sql = "update appointment set ViamenteTravelTime=:ViamenteTravelTime where AppointmentID=:AppointmentID";
                    if ($z == sizeof($steps) + 10) {
                        // $time=$steps[$z]->nextStepDriveTimeSec+$steps[($z-1)]->nextStepDriveTimeSec;
                    } else {
                        $time = $steps[($z - 1)]->nextStepDriveTimeSec;
                    }
                    $params = array('ViamenteTravelTime' => $time, 'AppointmentID' => $appID);
                    $this->execute($this->conn, $sql, $params);
                }
            }
        }
    }

    public function enginnerName2ID($name, $spID) {

        $name = explode(' ', $name);
        $fname = $name[0];
        $lnameTMP = explode('_', $name[1]);
        $lname = $lnameTMP[0];
        if (sizeof($name) > 2) {
            $fname = "";
            $lname = $name[sizeof($name) - 1];
            for ($i = 0; $i < sizeof($name) - 1; $i++) {
                $fname.=$name[$i] . " ";
            }
            $fname = trim($fname);
            $lnameTMP = explode('_', $lname);
            $lname = $lnameTMP[0];
        }
        // echo $fname;
        // echo $lname;
        $sql = "select ServiceProviderEngineerID from service_provider_engineer where EngineerFirstName=:EngineerFirstName and EngineerLastName=:EngineerLastName and ServiceProviderID=:ServiceProviderID";
        $params = array('EngineerFirstName' => $fname, 'EngineerLastName' => $lname, 'ServiceProviderID' => $spID);

        $res = $this->Query($this->conn, $sql, $params);

        if (isset($res[0])) {
            return $res[0]['ServiceProviderEngineerID'];
        } else {
            return false;
        }
    }

    public function calculateSlots($spID, $skillSetID, $postCode, $days = 27, $geotag, $eng = false, $samsung = false) {
//for not used slots        


        $sql_update = "update diary_allocation set SlotsLeft=case DiaryAllocationID ";
        $sql_update2 = "update diary_allocation set SlotsLeft=case DiaryAllocationID ";
        $sql_dids = "";
        $sql_dids2 = "";
        $filters = "";
        $engfilter = "";
        $postCodeAlpha = "";
        $postcodeSQL = "";
        $geofilter = "";
        if (!$geotag) {
            $postCode2 = $postCode;
            if (strlen($postCode) > 4) {
                $postCode2 = trim(substr(trim($postCode), 0, -3));
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            } else {
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            }
            $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
            if (!is_numeric(substr($postCodeOutCode, -1))) {
                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
            }
            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


            $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";
            $postcodeSQL = "AND 
                          (
                            dpa.`PostCode` = '$postCode'
                            OR dpa.`PostCode` = '$postCodeAlpha'
                            OR dpa.`PostCode` = '$postCodeOutCode'
                            OR dpa.`PostCode` = '$postCodeArea'
                          )";
        } else {
            $lat = $geotag['lat'];
            $lng = $geotag['lng'];
            $allocationTable = "join sp_geo_cells sgc on sgc.DiaryAllocationID=da.DiaryAllocationID";
            $geofilter = "and sgc.Lat1>=$lat and sgc.Lat2<=$lat and sgc.Lng1<=$lng and sgc.Lng2>=$lng";
        }
        if ($eng) {
            $engfilter = "and spe.ServiceBaseUserCode='$eng'";
        }

        $sql = "select da.DiaryAllocationID ,sp.DefaultTravelTime,spe.ServiceProviderEngineerID,aas.Type,
 sp.DefaultTravelTime*2*60 as travelTime, sps.ServiceDuration*60 as serviceDuration,da.AllocatedDate,
  
			CASE WHEN aas.Type = 'ANY' THEN floor((TIME_TO_SEC(timediff(sped.EndShift,sped.StartShift))-(select if(isnull(sum(dhs.TotalTimeSec)),0,sum(dhs.TotalTimeSec)) from diary_holiday_slots dhs where dhs.HolidayDate=da.AllocatedDate and dhs.ServiceProviderEngineerID=da.ServiceProviderEngineerID  ))/(sp.DefaultTravelTime*2*60+sps.ServiceDuration*60)) 
			WHEN aas.Type = 'AM' THEN floor((TIME_TO_SEC(timediff(aas.TimeTo,sped.StartShift)))/(sp.DefaultTravelTime*2*60+sps.ServiceDuration*60))
			 WHEN aas.Type = 'PM' THEN floor((TIME_TO_SEC(timediff(sped.EndShift,aas.TimeFrom)))/(sp.DefaultTravelTime*2*60+sps.ServiceDuration*60)) END as `slotsLeft`,
			

(TIME_TO_SEC(timediff(sped.EndShift,sped.StartShift))) as shiftTime 
			  from diary_allocation da 
			  join service_provider sp on sp.ServiceProviderID=da.ServiceProviderID 
			  join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
			  $allocationTable
			   join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID
			
				  join service_provider_skillset sps on sps.ServiceProviderID=da.ServiceProviderID 
				  join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
            join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
				where
				da.ServiceProviderID=:ServiceProviderID
                                $postcodeSQL
				 
				   and spe.Status='Active' 
				   and spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID
				   and sped.`Status`='Active'
					and da.AllocatedDate>=CURDATE() 
					and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL $days DAY) 
				$geofilter $engfilter
				   and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
				
               and sped.WorkDate=da.AllocatedDate
					and da.ServiceProviderEngineerID not in (select aa.ServiceProviderEngineerID 
					from service_provider_engineer_workload aa where aa.WorkingDay=da.AllocatedDate and ServiceProviderID=:ServiceProviderID)
    group by da.DiaryAllocationID";
        $params = array('ServiceProviderID' => $spID, 'ServiceProviderSkillsetID' => $skillSetID);
        if ($this->debug)
            $this->controller->log($sql);
        if ($this->debug)
            $this->controller->log($params);

        $qr = $this->Query($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log($qr, "aba");
        $exectime = microtime();
        $exectime = explode(" ", $exectime);
        $exectime = $exectime[1] + $exectime[0];
        $starttime = $exectime;

        for ($t = 0; $t < sizeof($qr); $t++) {
            $sql_dids2.=$qr[$t]['DiaryAllocationID'] . ",";
            $dID = $qr[$t]['DiaryAllocationID'];
            $slotsLeft = $qr[$t]['slotsLeft'];
            if ($slotsLeft == null or $slotsLeft < 0) {
                $slotsLeft = 0;
            }
            $sql_update2.="when $dID then $slotsLeft ";

            //  $params=array('SlotsLeft'=>$qr[$t]['slotsLeft'],'DiaryAllocationID'=>$qr[$t]['DiaryAllocationID']);
            //$this->execute($this->conn, $sql, $params);
        }
        $sql_dids2.="0";
        $sql_update2.="when 0 then 0 ";
        $exectime = microtime();
        $exectime = explode(" ", $exectime);
        $exectime = $exectime[1] + $exectime[0];
        $endtime = $exectime;
        $totaltime = ($endtime - $starttime);
//echo "This page was created in ".$totaltime." seconds";

        if (isset($qr[0]['DefaultTravelTime'])) {
            $travelTIme = $qr[0]['DefaultTravelTime'];
        } else {
            $travelTIme = 22;
        }

//for already used slots
        $sql = "select WorkingDay,ee.ServiceProviderEngineerID from service_provider_engineer_workload  ee
               join service_provider_engineer spe on spe.ServiceProviderEngineerID=ee.ServiceProviderEngineerID
               where ee.ServiceProviderID=$spID and WorkingDay>=CURDATE() ";
        $res = $this->Query($this->conn, $sql);






        $c = 0;


        for ($i = 0; $i < sizeof($res); $i++) {
            $c+=1;
            $sql = "select distinct(da.DiaryAllocationID), aas.Type ,da.AllocatedDate,spe.ServiceProviderEngineerID,
               
           (select  if(isnull(SUM(ViamenteTravelTime)+sum(Duration*60)),0,SUM(ViamenteTravelTime)+sum(Duration*60)) from appointment aa join diary_allocation da on da.DiaryAllocationID=aa.DiaryAllocationID where aa.AppointmentDate=:AllocatedDate and aa.ServiceProviderID=$spID and da.ServiceProviderEngineerID=:ServiceProviderEngineerID) as ttIME,
              (TIME_TO_SEC(timediff(sped.EndShift,sped.StartPostcode))) as totalTime,  
            CASE 
      WHEN aas.Type = 'ANY' THEN (TIME_TO_SEC(timediff(sped.EndShift,sped.StartShift)))-(select  if(isnull(SUM(ViamenteTravelTime)+sum(Duration*60)),0,SUM(ViamenteTravelTime)+sum(Duration*60))+(select if(isnull(sum(dhs.TotalTimeSec)),0,sum(dhs.TotalTimeSec)) from diary_holiday_slots dhs where dhs.HolidayDate=:AllocatedDate and dhs.ServiceProviderEngineerID=:ServiceProviderEngineerID  ) from appointment aa where aa.AppointmentDate=:AllocatedDate and aa.ServiceProviderEngineerID=:ServiceProviderEngineerID)
      WHEN aas.Type = 'AM' THEN  (TIME_TO_SEC(timediff(aas.TimeTo,sped.StartShift)))-(select     if(isnull(SUM(ViamenteTravelTime)+sum(Duration*60)),0,SUM(ViamenteTravelTime)+sum(Duration*60))  from appointment aa where aa.AppointmentDate=:AllocatedDate and aa.ServiceProviderEngineerID=:ServiceProviderEngineerID and aa.AppointmentTime='AM')
      WHEN aas.Type = 'PM' THEN  (TIME_TO_SEC(timediff(sped.EndShift,aas.TimeFrom)))-(select     if(isnull(SUM(ViamenteTravelTime)+sum(Duration*60)),0,SUM(ViamenteTravelTime)+sum(Duration*60))  from appointment aa1 where aa1.AppointmentDate=:AllocatedDate and aa1.ServiceProviderEngineerID=:ServiceProviderEngineerID and aa1.AppointmentTime='PM')
    END as totalSlotTimeSec
            
             
             from diary_allocation da
            
            join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID
            join service_provider_engineer_workload spew on spew.ServiceProviderEngineerID=da.ServiceProviderEngineerID		
join appointment_allocation_slot aas    on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID
join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
          where
			  AllocatedDate=:AllocatedDate 
			  and da.ServiceProviderEngineerID=:ServiceProviderEngineerID 
			  and spe.Status='Active' 
			  and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate

            
            group by da.DiaryAllocationID";

            $params = array('AllocatedDate' => $res[$i]['WorkingDay'], 'ServiceProviderEngineerID' => $res[$i]['ServiceProviderEngineerID']);
            $res2 = $this->Query($this->conn, $sql, $params);



            for ($u = 0; $u < sizeof($res2); $u++) {
                $c+=1;
                $errrr = microtime();
                $errrr = explode(" ", $errrr);
                $errrr = $errrr[1] + $errrr[0];
                $starttime2 = $errrr;
                $errrr = microtime();
                $errrr = explode(" ", $errrr);
                $errrr = $errrr[1] + $errrr[0];
                $endtime2 = $errrr;
                $totaltime2 = ($endtime2 - $starttime2);
//echo "This page was created in ".$totaltime2." seconds<br>"; 
                if (!isset($this->controller->session->SPInfo['ViamenteRunType'])) {
                    $tmp = $this->getAllServiceProviderInfo($spID);
                    $ViamenteRunType = $tmp['ViamenteRunType'];
                } else {
                    $ViamenteRunType = $this->controller->session->SPInfo['ViamenteRunType'];
                }

                if ($ViamenteRunType == "CurrentEngineer_tempRemoved") {
                    $sql = "
select  da.DiaryAllocationID from appointment a
join service_provider_engineer spe on spe.ServiceProviderEngineerID=a.ServiceProviderEngineerID
join diary_allocation da on da.DiaryAllocationID=a.DiaryAllocationID
 where a.AppointmentDate=:AppointmentDate and a.ViamenteUnreached=1 and a.ServiceProviderID=:ServiceProviderID               
";
                } else {


                    $sql = "
                  select distinct(sk.RepairSkillID)
            from diary_allocation da
            join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID
         
        
           
             $allocationTable
            join  service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
            join skillset sk on sk.SkillsetID=sps.SkillsetID
            where da.ServiceProviderID=:ServiceProviderID 
            and ap.ViamenteUnreached='1'
            and da.AllocatedDate=:AppointmentDate
           
            
             ";
                }
                $params = array('AppointmentDate' => $res2[$u]['AllocatedDate'], 'ServiceProviderID' => $spID);

                $res3 = $this->Query($this->conn, $sql, $params);

                $full = "0";
                $skillInfo = $this->getSkillInfo($skillSetID);
                if (isset($res3[0])) {

                    for ($d = 0; $d < sizeof($res3); $d++) {
                        if (in_array($skillInfo['RepairSkillID'], $res3[$d]) || $ViamenteRunType == "CurrentEngineer") {

                            $full = "1";
                        }
                    }
                }

                if ($full === "0") {

                    $slotsLeft = intval(round(($res2[$u]['totalSlotTimeSec']) / ($this->controller->session->ServiceProviderServiceDuration * 60 + $travelTIme * 60 * 2)));
                } else {
                    if ($ViamenteRunType != "CurrentEngineer") {
                        $slotsLeft = -128;
                    } else {

                        $slotsLeft = -1;
                    }
                }
                ///  echo "<br>";
                $dID = $res2[$u]['DiaryAllocationID'];
                if ($ViamenteRunType != "CurrentEngineer_TempRemoved") {//_tempRemoved must be removed if visualfx will want single engineer slot calculation
                    if ($full === "1") {
                        $date = $res2[$u]['AllocatedDate'];



//            echo "avg=$avgApps<br>";
//            echo "totAps=".$c2[0]['total_app']."<br>";
//            echo "capacity=".($c2[0]['total_app']/$avgApps)*100 ."<br>";

                        $EngineersCurrentLoad = $this->checkEngineersCapacity($spID, $date, $skillSetID);
                        $serviceProviderCapacityDefault = $this->getServiceProviderAppointmentCapaityDefault($spID);



                        if ($serviceProviderCapacityDefault > $EngineersCurrentLoad) {
                            $slotsLeft = 1;
                        } else {
                            $slotsLeft = 0;
                        }
                    }
                }

                $sql_dids.=$res2[$u]['DiaryAllocationID'] . ",";
                if ($slotsLeft == null or $slotsLeft == 0) {
                    $slotsLeft = 0;
                }
                $sql_update.=" when $dID then $slotsLeft ";
            }
        }
        $sql_update2.="end where DiaryAllocationID in ($sql_dids2)";
        if ($postCode != null) {
            // $this->controller->log("$sql_update2","sql_update2_");


            $this->execute($this->conn, $sql_update2);
        }
        if (isset($res2)) {
            if (sizeof($res2) > 0) {
                $sql_dids.="0";
                $sql_update.="end where DiaryAllocationID in ($sql_dids)";


                // echo $sql_update;
                // $this->controller->log("$sql_update","sql_update_");

                $this->execute($this->conn, $sql_update);
            }
        }
        if ($this->debug)
            $this->controller->log("c=$c");
    }

    public function getAnyTimeSlot($spID) {
        $sql = "select AppointmentAllocationSlotID from  appointment_allocation_slot where Type='ANY' and ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['AppointmentAllocationSlotID'];
    }

    public function getSPSkillsetInfo($skillID) {
        if ($skillID != "" or $skillID != null) {
            $sql = "select ServiceDuration from service_provider_skillset where ServiceProviderSkillsetID=$skillID";
            $res = $this->Query($this->conn, $sql);
            if (isset($res[0]['ServiceDuration'])) {
                return $res[0]['ServiceDuration'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

//    public function realocateAppointments($spID,$postcode){
//       
//        $sql="select ap.AppointmentID,da.AllocatedDate,AppointmentTime,ap.ServiceProviderSkillsetID from service_provider_engineer spe
//            join diary_allocation da on da.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
//            join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID
//            
//                where spe.Status='In-active' and da.AllocatedDate>=CURDATE() and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY) and spe.ServiceProviderID=$spID
//            
//                ";
//         $res=$this->Query($this->conn, $sql);
//         
//         for($k=0;$k<sizeof($res);$k++){
//             $apID=$res[$k]['AppointmentID'];
//            $dID=$this->getDiaryAllocationID($res[$k]['AppointmentTime'],$res[$k]['ServiceProviderSkillsetID'],$postcode,$spID,$res[$k]['AllocatedDate']);
//                if(isset($dID[0]['DiaryAllocationID'])){
//            $dID2=$dID[0]['DiaryAllocationID'];
//         
//            $sql="update appointment set 
//             DiaryAllocationID=$dID2 where AppointmentID=$apID";
//         $this->execute($this->conn, $sql);
//         }else{
//          return false;   
//         }
//         }
//                echo"<pre>";
//        echo"---------------rea-------------------";
//        print_r($res);
//        echo"---------------rea-------------------";
//         echo"</pre>";
    //return $res[0]['ServiceDuration'];
    //  }

//marks appointments as unreachable
    public function setUnreached($data, $insertedAppID = null) {
        $unreachedRepairType = array();
        $insertedAppRepairType = $this->getRepairType($insertedAppID);
        for ($i = 0; $i < sizeof($data); $i++) {
            $datatmp = explode(' ', $data[$i]);
            $da = $datatmp[sizeof($datatmp) - 1];
            if ($da != "") {
                $sql = "update appointment set ViamenteUnreached=1,ViamenteUnreachedTimestamp=now() where AppointmentID=$da";
                $this->execute($this->conn, $sql);
                $unreachedRepairType[] = $this->getRepairType($da);
            }
        }

        if (in_array($insertedAppRepairType, $unreachedRepairType)) {
            return true;
        } else {
            return false;
        }
    }

    public function ViamenteFinalize($date, $spID, $mode = false) {
        if ($mode) {
            if ($mode == 'Both') {
                $sql = "insert into viamente_end_day (ServiceProviderID,RouteDate,ModifiedUserID,ModifiedDate,EndDayType) 
                     values 
                     (:ServiceProviderID,:RouteDate,:ModifiedUserID,now(),'White'),
                     (:ServiceProviderID,:RouteDate,:ModifiedUserID,now(),'Brown')";
                $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) 
                     values 
                     (:ServiceProviderID,:CreatedUserID,:RouteDate,'White',now(),'Finalise Day'),
                     (:ServiceProviderID,:CreatedUserID,:RouteDate,'Brown',now(),'Finalise Day')";
            } else {
                $sql = "insert into viamente_end_day (ServiceProviderID,RouteDate,ModifiedUserID,ModifiedDate,EndDayType) 
                     values (:ServiceProviderID,:RouteDate,:ModifiedUserID,now(),'$mode')";
                $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) 
                     values (:ServiceProviderID,:CreatedUserID,:RouteDate,'$mode',now(),'Finalise Day')";
            }
            $params = array('ServiceProviderID' => $spID, 'RouteDate' => $date, 'ModifiedUserID' => $this->controller->user->UserID);
            $this->execute($this->conn, $sql, $params);
            $historyParams = array('ServiceProviderID' => $spID, 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $date );
            $this->execute($this->conn, $historySql, $historyParams);
        } else {
            $sql = "insert into viamente_end_day (ServiceProviderID,RouteDate,ModifiedUserID,ModifiedDate) values (:ServiceProviderID,:RouteDate,:ModifiedUserID,now())";
            $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,CreatedDate,ActionType) values (:ServiceProviderID,:CreatedUserID,:RouteDate,now(),'Finalise Day')";
            $params = array('ServiceProviderID' => $spID, 'RouteDate' => $date, 'ModifiedUserID' => $this->controller->user->UserID);
            $this->execute($this->conn, $sql, $params);
            $historyParams = array('ServiceProviderID' => $spID, 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $date );
            $this->execute($this->conn, $historySql, $historyParams);
        }
    }

    public function getFinalizedDays($spID, $repSkill, $ts = false) {
        // echo"->>$repSkill";
        if ($repSkill == 2) {
            $repSkill = "Brown";
        }
        if ($repSkill == 4) {
            $repSkill = "White";
        }

        if ($spID == 64 && $repSkill != "") {
            $repSkillAdd = "and EndDayType='$repSkill'";
        } elseif ($repSkill == "" && $spID == 64 && !$ts) {
            $repSkillAdd = "and EndDayType='Both'";
        } else {
            $repSkillAdd = "";
        }

        $sql = "select RouteDate,EndDayType from viamente_end_day where ServiceProviderID=$spID and RouteDate>=curdate() $repSkillAdd";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            for ($i = 0; $i < sizeof($res); $i++) {
                $data[$i] = $res[$i]['RouteDate'];
            }
        } else {
            $data = array();
        }

        return $data;
    }

    public function checkFinalized($spID, $date, $skillset = false) {
        if ($spID == 64 && $skillset != "" && $skillset != "Both") {
            $m = '';
            if ($skillset != "Brown" && $skillset != "White") {
                $repSkill = $this->getRepairTypeIDFromSPSkillset($skillset);
                $m = '';
                if ($repSkill == 2) {
                    $m = "Brown";
                }
                if ($repSkill == 4) {
                    $m = "White";
                }
            } else {
                $m = $skillset;
            }
            $sql = "select RouteDate from viamente_end_day where ServiceProviderID=$spID and RouteDate=:Rd and EndDayType='$m'";
            $params = array("Rd" => $date);
            $res = $this->Query($this->conn, $sql, $params);
        } else {
            $sql = "select RouteDate from viamente_end_day where ServiceProviderID=$spID and RouteDate=:Rd";
            $params = array("Rd" => $date);
            $res = $this->Query($this->conn, $sql, $params);
        }
        $this->controller->log($sql, "finalize___");
        $this->controller->log($res, "finalize___");
        if (isset($res[0])) {
            for ($i = 0; $i < sizeof($res); $i++) {
                return true;
            }
        } else {
            return false;
        }
        return $data;
    }

    public function updateDuration($post) {

        $sql = "update appointment set Duration=:Duration where AppointmentID=:AppointmentID";
        $params = array('AppointmentID' => $post['row_id'], 'Duration' => $post['value']);
        $this->execute($this->conn, $sql, $params);
    }

    public function getSlotsRecomendet($spID, $skillSetID, $postCode, $addressGeoTag = false) {

        // echo "$fp";
        if (!$addressGeoTag) {
            $filters = "";
           

            $postCode2 = $postCode;
            if (strlen($postCode) > 4) {
                $postCode2 = trim(substr(trim($postCode), 0, -3));
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            } else {
                if (!is_numeric(substr($postCode2, -1))) {
                    $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
                }
            }
            $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

            $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
            if (!is_numeric(substr($postCodeOutCode, -1))) {
                $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
            }
            $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


            $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";

            $postcodeSQL = "
         and( (nsj.PostCode REGEXP '^$postCode') or (nsj.PostCode REGEXP '^$postCodeOutCode')
         or ((c.PostalCode REGEXP '^$postCode' or c.PostalCode REGEXP '^$postCodeOutCode')and (j.ColAddPostcode is null or j.ColAddPostcode='' or j.ColAddPostcode='0') )
         or j.ColAddPostcode REGEXP '^$postCode' or j.ColAddPostcode REGEXP '^$postCodeOutCode')
         ";
            
            
            $repskill = $this->getRepairTypeIDFromSPSkillset($skillSetID);
//die($repskill);
            ///will couse wrong slot numbers if same dpa.Postcode assigned more than once

         $sql = " select distinct( DATE_FORMAT(da.AllocatedDate,'%d%m%Y')) as dayOnly 
from diary_allocation da 
 join appointment ap on ap.AppointmentDate=da.AllocatedDate 


 join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID
 join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
 join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID 
 left join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID 
 left join job j on j.JobID=ap.JobID 
 left join customer c on c.CustomerID=j.CustomerID
 join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
 join skillset sk on sk.SkillsetID=sps.SkillsetID
 where da.ServiceProviderID=:ServiceProviderID
     $postcodeSQL and
          sk.RepairSkillID=:RepairSkillID and
	da.AllocatedDate>=curdate() and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY) and spe.Status='Active' and sped.`Status`='Active' and
	 sped.WorkDate=da.AllocatedDate and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID 
         $filters
                  and ap.ServiceProviderID=:ServiceProviderID
	 order by da.AllocatedDate asc ";




            $params = array('ServiceProviderID' => $spID, 'ServiceProviderSkillsetID' => $skillSetID, 'RepairSkillID' => $repskill);
            $res = $this->Query($this->conn, $sql, $params);


            return $res;
        } else {
            return false;
        }
    }

    public function getSPID($name, $pass) {
        $sql = "select ServiceProviderID from user where Username=:Username and Password=:Password";
        $params = array('Username' => $name, 'Password' => $pass);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0]['ServiceProviderID'])) {
            return $res[0]['ServiceProviderID'];
        } else {
            return false;
        }
    }

    public function updateJobDetails($data, $spID) {
        $sql = "select JobID,CustomerID from job where ServiceCentreJobNo=:ServiceCentreJobNo and ServiceProviderID=:ServiceProviderID";
        $sql2 = "select * from non_skyline_job where ServiceProviderJobNo=:ServiceCentreJobNo and ServiceProviderID=:ServiceProviderID";
        $params = array('ServiceProviderID' => $spID, 'ServiceCentreJobNo' => $data['SBJobNo']);
        $res1 = $this->Query($this->conn, $sql, $params);
        if (!isset($data['CollContactNumber'])) {
            $data['CollContactNumber'] = null;
        }
        if (!isset($data['CustHomeTelNo'])) {
            $data['CustHomeTelNo'] = null;
        }
        if (!isset($data['CustWorkTelNo'])) {
            $data['CustWorkTelNo'] = null;
        }
        if (!isset($data['CustMobileNo'])) {
            $data['CustMobileNo'] = null;
        }
        if (isset($res1[0])) {
            $sqlU = "update job  set 
           ColAddStreet=:ColAddStreet,
           ColAddTownCity=:ColAddTownCity,
           ColAddPostcode=:ColAddPostcode,
           ColAddBuildingNameNumber=:ColAddBuildingNameNumber,
           ColAddPhone=:ColAddPhone
           where JobID=:JobID";
            $params = array(
                'ColAddStreet' => $data['CollAddress1'],
                'ColAddTownCity' => $data['CollAddress2'],
                'ColAddPostcode' => $data['CollPostcode'],
                'ColAddBuildingNameNumber' => $data['CollBuildingName'],
                'ColAddPhone' => $data['CollContactNumber'],
                'JobID' => $res1[0]['JobID']
            );
            $this->execute($this->conn, $sqlU, $params);

            $sqlU = "update customer set 
              PostalCode=:PostalCode,
              Street=:Street,
              TownCity=:TownCity,
              BuildingNameNumber=:BuildingNameNumber,
              ContactHomePhone=:ContactHomePhone,
              ContactWorkPhone=:ContactWorkPhone,
              ContactMobile=:ContactMobile 
              where CustomerID=:CustomerID";

            $params = array(
                'PostalCode' => $data['CustPostcode'],
                'Street' => $data['CustAddress1'],
                'TownCity' => $data['CustAddress2'],
                'BuildingNameNumber' => $data['CustBuildingName'],
                'ContactHomePhone' => $data['CustHomeTelNo'],
                'ContactWorkPhone' => $data['CustWorkTelNo'],
                'ContactMobile' => $data['CustMobileNo'],
                'CustomerID' => $res1[0]['CustomerID']
            );
            $this->execute($this->conn, $sqlU, $params);
        } else {
            $res2 = $this->Query($this->conn, $sql2, $params);
            if (isset($res2[0]['NonSkylineJobID'])) {
                $sqlU = "update non_skyline_job set 
            Postcode=:Postcode,
            CustomerAddress1=:CustomerAddress1,
            CustomerAddress2=:CustomerAddress2,
            CustomerAddress3=:CustomerAddress3,
            CustomerAddress4=:CustomerAddress4,
            CustHomeTelNo=:CustHomeTelNo,
            CustWorkTelNo=:CustWorkTelNo,
            CustMobileNo=:CustMobileNo,
            CollContactNumber=:CollContactNumber
            where NonSkylineJobID=:NonSkylineJobID";
                $building = "";
                if ($data['CollBuildingName'] != "") {
                    $building = $data['CollBuildingName'] . " ";
                } elseif ($data['CustBuildingName'] != "") {
                    $building = $data['CustBuildingName'] . " ";
                }
                if ($data['CollAddress1'] != "") {
                    $ad1 = $building . $data['CollAddress1'];
                } else {
                    $ad1 = $building . $data['CustAddress1'];
                }
                if ($data['CollAddress2'] != "") {
                    $ad2 = $data['CollAddress2'];
                } else {
                    $ad2 = $data['CustAddress2'];
                }
                if ($data['CollAddress3'] != "") {
                    $ad3 = $data['CollAddress3'];
                } else {
                    $ad3 = $data['CustAddress3'];
                }
                if ($data['CollAddress4'] != "") {
                    $ad4 = $data['CollAddress4'];
                } else {
                    $ad4 = $data['CustAddress4'];
                }
                if ($data['CollPostcode'] != "") {
                    $ad5 = $data['CollPostcode'];
                } else {
                    $ad5 = $data['CustPostcode'];
                }
                $params = array(
                    'CustomerAddress1' => $ad1,
                    'CustomerAddress2' => $ad2,
                    'CustomerAddress3' => $ad3,
                    'CustomerAddress4' => $ad4,
                    'Postcode' => $ad5,
                    'CustHomeTelNo' => $data['CustHomeTelNo'],
                    'CustWorkTelNo' => $data['CustWorkTelNo'],
                    'CustMobileNo' => $data['CustMobileNo'],
                    'CollContactNumber' => $data['CollContactNumber'],
                    'NonSkylineJobID' => $res2[0]['NonSkylineJobID']
                );

                $this->execute($this->conn, $sqlU, $params);
            }
        }
    }

    public function getSkillsetID($rep, $app, $spID) {
        $sql = "select sps.ServiceProviderSkillsetID from skillset s
               join service_provider_skillset sps on sps.SkillsetID=s.SkillsetID
        where RepairSkillID=:RepairSkillID and AppointmentTypeID=:AppointmentTypeID and sps.ServiceProviderID=$spID";
        $params = array('RepairSkillID' => $rep, 'AppointmentTypeID' => $app);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0]['ServiceProviderSkillsetID'])) {
            return $res[0]['ServiceProviderSkillsetID'];
        } else {
            return null;
        }
    }

    public function getDaysOnly($spID, $geotag = false) {
        if (!$geotag) {

            $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";
        } else {

            $allocationTable = "join sp_geo_cells sgc on sgc.DiaryAllocationID=da.DiaryAllocationID";
        }

        $sql = "select distinct(da.DiaryAllocationID) ,da.DiaryAllocationID,Type,ap.AppointmentDate
         ,20 as 'slotsLeft',
           
            DATE_FORMAT(da.AllocatedDate,'%d%m%Y') as dayOnly
            from diary_allocation da
            left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID
            join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
            $allocationTable
            where da.ServiceProviderID=$spID
            and da.AllocatedDate>=CURDATE()  and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY)
            order by da.AllocatedDate asc";
        $res = $this->Query($this->conn, $sql);

        return $res;
    }

    public function getSummary($spID, $diaryType) {
        if ($diaryType == "FullViamente") {
            $extraSQL = "and spew.WorkingDay=da.AllocatedDate";
            $EX = ",sec_to_time(spew.TotalWorkTimeSec) as TotalWorkTimeSec";
        } else {
            $extraSQL = "";
            $EX = ",'0' as TotalWorkTimeSec";
        }
        $sql = "select distinct(AllocatedDate),
           (select count(AppointmentID) from appointment where ServiceProviderID=$spID and AppointmentDate=da.AllocatedDate) as `TotalApp`,
           (select distinct(concat(EngineerFirstName,' ',EngineerLastName)) from service_provider_engineer spe join diary_allocation dda on dda.ServiceProviderEngineerID=spe.ServiceProviderEngineerID join diary_postcode_allocation dpa on dpa.DiaryAllocationID=dda.DiaryAllocationID where spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID and dda.DiaryAllocationID=da.DiaryAllocationID and EngineerLastName!='' and spe.Status='Active') as Engineer,
           (select count(AppointmentID) from appointment ap where ap.ServiceProviderEngineerID=da.ServiceProviderEngineerID and ap.AppointmentDate=da.AllocatedDate) as EngineerAPP
           ,(select count(AppointmentID) from appointment aa where ServiceProviderID=$spID and AppointmentDate=da.AllocatedDate and (aa.ServiceProviderEngineerID is NULL or aa.ViamenteUnreached=1) ) as problemApp
           $EX
           from diary_allocation da 
           left join service_provider_engineer_workload spew on spew.ServiceProviderEngineerID=da.ServiceProviderEngineerID
           
           where 
           da.ServiceProviderID=$spID
           and da.AllocatedDate>=curdate() and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY) and (select count(AppointmentID) from appointment where ServiceProviderID=$spID and AppointmentDate=da.AllocatedDate)>0 $extraSQL 
           order by da.AllocatedDate asc, Engineer asc
           
           ";

        $params = array();
        $res = $this->Query($this->conn, $sql, $params);

        for ($i = 0; $i < sizeof($res); $i++) {

            $engineers[$res[$i]['AllocatedDate']][] = array('name' => $res[$i]['Engineer'], 'appcount' => $res[$i]['EngineerAPP'], 'apptime' => $res[$i]['TotalWorkTimeSec']);
            $totalDay[$res[$i]['AllocatedDate']] = $res[$i]['TotalApp'];
            $problemDay[$res[$i]['AllocatedDate']] = $res[$i]['problemApp'];
        }
        $sql = "select count(AppointmentID) as apCountType,rs.RepairSkillName,ap.AppointmentDate from appointment ap 
           join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
           join skillset sk on sk.SkillsetID=sps.SkillsetID
           join repair_skill rs on rs.RepairSkillID=sk.RepairSkillID
             where ap.AppointmentDate>=curdate() and ap.ServiceProviderID=$spID group by rs.RepairSkillName,ap.AppointmentDate";
        $params = array();
        $res2 = $this->Query($this->conn, $sql, $params);

        $sql = "select 
             distinct(IF (ISNULL(c.`CustomerID`),
                                  
                                   trim(SUBSTRING(nsj.`Postcode`, 1, LENGTH(nsj.`Postcode`)-3))
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                           
                                            trim(SUBSTRING(c.`PostalCode`, 1, LENGTH(c.`PostalCode`)-3))
                                    ,
                                           
                                            trim(SUBSTRING(j.`ColAddPostcode`, 1, LENGTH(j.`ColAddPostcode`)-3))
                                    )
                            )) AS  'Postcode'
            
            ,da.AllocatedDate from diary_allocation da 
           join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID
           left join job j on j.JobID=ap.JobID
           left join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
           left join customer c on c.CustomerID=j.CustomerID
            where   
            da.ServiceProviderID=$spID
           and da.AllocatedDate>=curdate() and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY)  order by Postcode ASC
          ";
        $res3 = $this->Query($this->conn, $sql, $params);
        for ($i = 0; $i < sizeof($res3); $i++) {
            $val = $res3[$i]['Postcode'];

            $postcodes[$res3[$i]['AllocatedDate']][] = array('postcode' => $val);
        }
        for ($i = 0; $i < sizeof($res2); $i++) {
            $skillsets[$res2[$i]['AppointmentDate']][] = array('skill' => $res2[$i]['RepairSkillName'], 'count' => $res2[$i]['apCountType']);
        }

        $sql34 = "select concat(spe.EngineerFirstName,' ', spe.EngineerLastName) as Engineer,da.AllocatedDate,
             da.ServiceProviderEngineerID,count(distinct(ap.AppointmentID)) as EngineerAPP
             ,sec_to_time(spew.TotalWorkTimeSec) as TotalWorkTimeSec from diary_allocation da 
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID
 join appointment ap on ap.ServiceProviderEngineerID=da.ServiceProviderEngineerID
  join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
              left join service_provider_engineer_workload spew on spew.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
 where da.DiaryAllocationID not in (select da1.DiaryAllocationID from diary_allocation da1 join diary_postcode_allocation dpa 
on dpa.DiaryAllocationID=da1.DiaryAllocationID where da1.AllocatedDate>=curdate() and da1.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY) and da1.ServiceProviderID=$spID) and da.AllocatedDate>=curdate() and da.AllocatedDate<= DATE_ADD(curdate(), INTERVAL 27 DAY)
and da.ServiceProviderID=$spID and ap.AppointmentDate=da.AllocatedDate and ap.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
           and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate   
             
             group by da.ServiceProviderEngineerID
";
        $params = array();
        $res4 = $this->Query($this->conn, $sql34, $params);
        for ($i = 0; $i < sizeof($res4); $i++) {

            $engineers[$res4[$i]['AllocatedDate']][] = array('name' => $res4[$i]['Engineer'], 'appcount' => $res4[$i]['EngineerAPP'], 'apptime' => $res4[$i]['TotalWorkTimeSec'], 'error' => 1);
        }


        if (isset($totalDay)) {
            foreach ($totalDay as $k => $v) {
                if (isset($skillsets[$k])) {
                    $val = $skillsets[$k];
                } else {
                    $val = null;
                }
                if (isset($postcodes[$k])) {

                    $val2 = $postcodes[$k];
                } else {
                    $val2 = null;
                }

                $new[$k] = array(
                    'total' => $totalDay[$k],
                    'postcode' => $val2,
                    'engineers' => $engineers[$k],
                    'skills' => $val,
                    'problem' => $problemDay[$k]
                );
            }
        } else {
            return false;
        }

        ksort($new);
        if ($this->debug)
            $this->controller->log($new, "summary____");
        return $new;
    }

    public function cleanDatabase() {
        $sql = "select da.DiaryAllocationID  from diary_allocation da 

where  da.AllocatedDate<curdate()  
";
        $res = $this->Query($this->conn, $sql);
        for ($i = 0; $i < sizeof($res); $i++) {
            $sql = "delete from diary_postcode_allocation where DiaryAllocationID=:DiaryAllocationID";

            $params = array('DiaryAllocationID' => $res[$i]['DiaryAllocationID']);
            $this->execute($this->conn, $sql, $params);
            $sql = "delete from diary_allocation where DiaryAllocationID=:DiaryAllocationID";
            $this->execute($this->conn, $sql, $params);
        }
    }

    public function getDiaryAllocationIDForce($date, $eID, $spID = null) {
        $sql = "select DiaryAllocationID,AllocatedDate from diary_allocation da where AllocatedDate='$date' and ServiceProviderEngineerID=$eID order by AppointmentAllocationSlotID desc";
        if ($spID) {
            $sql = "select DiaryAllocationID,AllocatedDate from diary_allocation da where AllocatedDate='$date' and ServiceProviderID=$spID order by AppointmentAllocationSlotID desc ";
        }
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0]['DiaryAllocationID'])) {
            return $res;
        } else {

            return false;
        }
    }

    /**
     * getDiaryWallboardPerformance
     * This method is used for to get all appointments booked on Skyline diary
     * 

     * @return array
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     * ****************************************** */
    public function getDiaryWallboardPerformance($spID, $NetworkID = false, $aType = 'all', $getUnreachedOnly = false, $nonSkylineJobs = false) {


        if ($getUnreachedOnly) {
            $TotalUnreachedApps = " AND app.ViamenteUnreached='1' ";
            $UnreachedApps = " AND ap.ViamenteUnreached='1' ";
        } else {
            $TotalUnreachedApps = "";
            $UnreachedApps = "";
        }



        if ($NetworkID) {

            if ($nonSkylineJobs) {
                $extraTable = ' LEFT JOIN non_skyline_job AS nsj ON ap.NonSkylineJobID=nsj.NonSkylineJobID ';

                if ($NetworkID == 2) {//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
                    $subQuery = "select count(app.AppointmentID) from appointment app LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID where app.AppointmentDate=ap.AppointmentDate " . $TotalUnreachedApps . "  and app.ServiceProviderID='" . $spID . "' and (nsj.NetworkID='" . $NetworkID . "'  OR nsj.NetworkRefNo IS NOT NULL OR nsj.ServiceType LIKE 'warranty') AND nsj.Manufacturer LIKE 'SAMSUNG' ";
                    $extraCondition = " and ((nsj.NetworkID='" . $NetworkID . "' OR nsj.NetworkRefNo IS NOT NULL OR nsj.ServiceType LIKE 'warranty') AND nsj.Manufacturer LIKE 'SAMSUNG' ) ";
                } else {
                    $subQuery = "select count(app.AppointmentID) from appointment app LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID where app.AppointmentDate=ap.AppointmentDate " . $TotalUnreachedApps . "  and app.ServiceProviderID='" . $spID . "' and nsj.NetworkID='" . $NetworkID . "'";
                    $extraCondition = " and nsj.NetworkID='" . $NetworkID . "' ";
                }
            } else {

                $extraTable = ' LEFT JOIN job AS j ON ap.JobID=j.JobID ';

                if ($NetworkID == 2) {//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
                    $subQuery = "select count(app.AppointmentID) from appointment app LEFT JOIN job AS j ON app.JobID=j.JobID where app.AppointmentDate=ap.AppointmentDate " . $TotalUnreachedApps . "  and app.ServiceProviderID='" . $spID . "' and j.NetworkID='" . $NetworkID . "' AND j.ManufacturerID='106'";
                    $extraCondition = " and j.NetworkID='" . $NetworkID . "' AND j.ManufacturerID='106' ";
                } else {
                    $subQuery = "select count(app.AppointmentID) from appointment app LEFT JOIN job AS j ON app.JobID=j.JobID where app.AppointmentDate=ap.AppointmentDate " . $TotalUnreachedApps . "  and app.ServiceProviderID='" . $spID . "' and j.NetworkID='" . $NetworkID . "'";
                    $extraCondition = " and j.NetworkID='" . $NetworkID . "'  ";
                }
            }
        } else {
            $subQuery = "select count(app.AppointmentID) from appointment app  where app.AppointmentDate=ap.AppointmentDate " . $TotalUnreachedApps . " and app.ServiceProviderID='" . $spID . "'";

            $extraTable = '';
            $extraCondition = '';
        }

        $appointmentTypeCondition = '';
        $appointmentTypeCondition1 = '';

        if ($aType == "av") {
            $appointmentTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
            $appointmentTypeCondition1 = " AND rs1.RepairSkillName = 'BROWN GOODS' ";
        } else if ($aType == "ha") {
            $appointmentTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
            $appointmentTypeCondition1 = " AND rs1.RepairSkillName = 'WHITE GOODS' ";
        } else if ($aType == "os") {
            $appointmentTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
            $appointmentTypeCondition1 = " AND rs1.RepairSkillName != 'WHITE GOODS' AND rs1.RepairSkillName != 'BROWN GOODS' ";
        }



        //This query is basically copied from getDiaryWallboardInfo
        $sql = "select count(ap.AppointmentID) as apCountType,rs.RepairSkillName,ap.AppointmentDate,time_to_sec(TimeDiff(now(),ap.ViamenteUnreachedTimestamp)) as uTime,
            (select count(distinct(da.ServiceProviderEngineerID))  
         from diary_allocation da 
         join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
         join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID
         join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
        
         join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
         join service_provider_skillset sps1 on sps1.ServiceProviderSkillsetID=spesd.ServiceProviderSkillsetID
         join skillset sk1 on sk1.SkillsetID=sps1.SkillsetID
         join repair_skill rs1 on rs1.RepairSkillID=sk1.RepairSkillID

         where da.AllocatedDate=ap.AppointmentDate and spe.Status='Active' and spe.ServiceProviderID=$spID
          and sped.`Status`='Active'
          and sped.WorkDate=da.AllocatedDate " . $appointmentTypeCondition1 . "
          

         ) as totalEngineers,
           (" . $subQuery . ") as apCountTotal
         
         
            from appointment ap 
           join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
           join skillset sk on sk.SkillsetID=sps.SkillsetID
           join repair_skill rs on rs.RepairSkillID=sk.RepairSkillID
           " . $extraTable . "
             where ap.AppointmentDate>=curdate() and ap.AppointmentDate<DATE_ADD(curdate(), INTERVAL 14 DAY) " . $extraCondition . " " . $UnreachedApps . " and ap.ServiceProviderID=$spID " . $appointmentTypeCondition . " group by rs.RepairSkillName,ap.AppointmentDate ";

        // $this->controller->log($sql);
        //  $this->controller->log("--------------------------------------");

        $params = array();
        $res2 = $this->Query($this->conn, $sql, $params);
        $data = array();
        for ($i = 0; $i < sizeof($res2); $i++) {
            $data[$res2[$i]['AppointmentDate']][$res2[$i]['RepairSkillName']] = $res2[$i];
        }
        ksort($data);

        return $data;
    }

    public function getDiaryWallboardInfo($spID) {
        //needchange
        $sql = "select count(AppointmentID) as apCountType,rs.RepairSkillName,ap.AppointmentDate,
            (select count(distinct(da.ServiceProviderEngineerID))  
         from diary_allocation da 
         join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
        
         join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
         where da.AllocatedDate=ap.AppointmentDate and spe.Status='Active' and spe.ServiceProviderID=$spID
          and sped.`Status`='Active'
          and sped.WorkDate=da.AllocatedDate
         ) as totalEngineers,
           (select count(AppointmentID) from appointment app  where app.AppointmentDate=ap.AppointmentDate  and app.ServiceProviderID=$spID) as apCountTotal
         
         
            from appointment ap 
           join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
           join skillset sk on sk.SkillsetID=sps.SkillsetID
           join repair_skill rs on rs.RepairSkillID=sk.RepairSkillID
             where ap.AppointmentDate>=curdate() and ap.AppointmentDate<DATE_ADD(curdate(), INTERVAL 14 DAY)  and ap.ServiceProviderID=$spID  group by rs.RepairSkillName,ap.AppointmentDate ";
        $params = array();
        $res2 = $this->Query($this->conn, $sql, $params);
        $data = array();
        for ($i = 0; $i < sizeof($res2); $i++) {
            $data[$res2[$i]['AppointmentDate']][$res2[$i]['RepairSkillName']] = $res2[$i];
        }
        ksort($data);

        return $data;
    }

    public function updateGeoTag($post, $spID) {
        if (isset($post['reset'])) {
            $sql = "delete from service_provider_geotags where PostCode=:PostCode and ServiceProviderID=$spID";
            $params = array('PostCode' => $post['postcode']);
            $this->execute($this->conn, $sql, $params);
            return "deleted";
        }
        $sql = "select * from service_provider_geotags  where PostCode=:PostCode and ServiceProviderID=$spID";
        $params = array('PostCode' => $post['postcode']);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0])) {
            $sql = "update service_provider_geotags set Latitude=:Latitude ,Longitude=:Longitude where PostCode=:PostCode and ServiceProviderID=$spID";
            $params = array('PostCode' => $post['postcode'], 'Latitude' => $post['lat'], 'Longitude' => $post['lng']);
            $this->execute($this->conn, $sql, $params);
        } else {
            $sql = "insert into service_provider_geotags (PostCode,Longitude,Latitude,ServiceProviderID) values (:PostCode,:Longitude,:Latitude,$spID)";
            $params = array('PostCode' => $post['postcode'], 'Latitude' => $post['lat'], 'Longitude' => $post['lng']);
            $this->execute($this->conn, $sql, $params);
        }
    }

    public function printReport1($engs, $date, $spID, $diaryType) {

        if ($diaryType == "FullViamente") {
            $engs = explode(',', $engs);
            $res = array();
            foreach ($engs as $e) {
                $sql = "select spe.ServiceProviderEngineerID, concat(EngineerFirstName,' ',EngineerLastName) as name, 
             date_format(spew.WorkingDay,'%a') as day ,
             '$date' as date,
                 
             (select sec_to_time(time_to_sec(aa.ViamenteStartTime)-(aa.ViamenteTravelTime)+60) from appointment aa where aa.AppointmentDate='$date'
                 and aa.ServiceProviderEngineerID=$e and aa.ServiceProviderID=$spID order by aa.ViamenteStartTime asc limit 1) as startTime,
             sped.StartPostcode as startLocation,
                sped.EndPostcode as endLocation,
             if ((startLocation=StartHomePostcode),
                 'home',
                 'Workshop') as startPlace,
            if ((endLocation=EndHomePostcode),
                 'home',
                 'Workshop') as endPlace,
                 SEC_TO_TIME(spew.TotalServiceTimeSec) as serviceInterval,
                 SEC_TO_TIME(spew.TotalDriveTimeSec) as travelTime,
                 SEC_TO_TIME(spew.TotalWorkTimeSec) as totalHours,
                 spew.TotalSteps-2 as totalAppointments,
               (select aaa.ViamenteReturnTime from appointment aaa where aaa.AppointmentDate='$date' and aaa.ServiceProviderEngineerID=$e order by aaa.ViamenteReturnTime desc limit 1) as endTime,
                 round(((spew.TotalServiceTimeSec/spew.TotalWorkTimeSec)*100)) as siP,
                 round(((spew.TotalDriveTimeSec/spew.TotalWorkTimeSec)*100)) as tiP,
                sec_to_time(spew.TotalWorkTimeSec/(spew.TotalSteps-2)) as averageAp
             from service_provider_engineer spe 
                 join service_provider_engineer_workload spew on spew.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
                 	left join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
		
                 where spe.ServiceProviderEngineerID=$e and spew.WorkingDay='$date' and sped.`Status`='Active' and spe.ServiceProviderID=$spID
         
					  and sped.WorkDate='$date'
         ";
                $rest = $this->Query($this->conn, $sql);
                $res = array_merge($res, $rest);
            }
            if ($this->debug)
                $this->controller->log($sql, "print_report_sql___");
        }else {
            $sql = "select spe.ServiceProviderEngineerID, concat(EngineerFirstName,' ',EngineerLastName) as name, 
             '$date' as day ,
             '$date' as date,
            '' as startTime,
             '' as startLocation,
               
            '' as endLocation,
            
                 '' as startPlace,
           
                 '' as endPlace,
                 '' as serviceInterval,
                 '' as travelTime,
                 '' as totalHours,
                 (select count(*) from appointment ap where ap.ServiceProviderEngineerID=spe.ServiceProviderEngineerID and ap.AppointmentDate='$date') as totalAppointments,
               '' as endTime,
                 '' as siP,
                 '' as tiP,
                '' as averageAp
             from service_provider_engineer spe 
              join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
		
                 where spe.ServiceProviderEngineerID in ($engs) and sped.WorkDate='$date' and sped.`Status`='Active' and spe.ServiceProviderID=$spID";
            $res = $this->Query($this->conn, $sql);
        }


        $data2 = array();
        if ($diaryType == "FullViamente") {
            $sorting = "ViamenteStartTime asc";
        } else {
            $sorting = "`SortOrder` asc";
        }
        for ($i = 0; $i < sizeof($res); $i++) {
            //$res[$i]['endTime']=  strtotime($res[$i]['startTime'])
            $sql = "select  
            if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as Postcode,
              ServiceCentreJobNo as jobNo,
            concat_ws(' ',ct.Title,ContactLastName) as name,
            ap.`SortOrder`,
            if(c.ContactMobile='' or c.ContactMobile is null,
                if(j.ColAddPhone is  null or j.ColAddPhone='',
                   if(c.ContactHomePhone='' or c.ContactHomePhone is null,c.ContactWorkPhone,c.ContactHomePhone)
                ,j.ColAddPhone),c.ContactMobile
                ) as phone,
              
               
              ap.AppointmentType,
              ap.AppointmentTime,
              j.ServiceBaseUnitType as ProductType,
              ap.Notes,
              ap.PreVisitContactRequired,
              ap.PreVisitContactInfo,
              ap.Duration,
              AppointmentStartTime,
                 AppointmentEndTime,
                 AppointmentStartTime2,
                 AppointmentEndTime2,
              ap.ViamenteReturnTime,
              sec_to_time(ap.ViamenteTravelTime) as travelTime,
              ap.ViamenteStartTime,
               if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        concat_ws(' ',c.BuildingNameNumber,c.Street,c.TownCity),
        concat_ws(' ',j.ColAddCompanyName, j.ColAddBuildingNameNumber, j.ColAddStreet,j.ColAddLocalArea,j.ColAddTownCity))
        as location,
              ap.ViamenteDepartTime as DepartTime
              from appointment ap 
              
             
               join job j on j.JobID=ap.JobID
               join customer c on c.CustomerID=j.CustomerID
              left join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID 
               
              where ServiceProviderEngineerID=:ServiceProviderEngineerID and AppointmentDate = '$date' 
              
              union
              
              
              select  Postcode,
              ServiceProviderJobNo as jobNo,
              concat_ws(' ',CustomerTitle,CustomerSurname) as name,
              
             ap.`SortOrder`,
               if(nsj.CustMobileNo='' or nsj.CustMobileNo is null,
                if(nsj.CollContactNumber is  null or nsj.CollContactNumber='',
                   if(nsj.CustHomeTelNo='' or nsj.CustHomeTelNo is null,nsj.CustWorkTelNo,nsj.CustHomeTelNo)
                ,CollContactNumber),nsj.CustMobileNo
                ) as phone,
              ap.AppointmentType,
              ap.AppointmentTime,
              nsj.ProductType,
              ap.Notes,
              ap.PreVisitContactRequired,
              ap.PreVisitContactInfo,
              ap.Duration,
              AppointmentStartTime,
                 AppointmentEndTime,
                 AppointmentStartTime2,
                 AppointmentEndTime2,
               
              ap.ViamenteReturnTime,
              sec_to_time(ap.ViamenteTravelTime) as travelTime,
              ap.ViamenteStartTime,
              concat_ws(' ',CustomerAddress1,CustomerAddress2,CustomerAddress3,CustomerAddress4 ) as location,
              ap.ViamenteDepartTime as DepartTime
              from appointment ap 
                  join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
               where ServiceProviderEngineerID=:ServiceProviderEngineerID and AppointmentDate = '$date'  order by $sorting
              ";

$this->controller->log($sql,"sql");
            $params = array('ServiceProviderEngineerID' => $res[$i]['ServiceProviderEngineerID']);
            $res2 = $this->Query($this->conn, $sql, $params);

            $data2[$res[$i]['ServiceProviderEngineerID']] = $res2;
        }



        return array('engineers' => $res, 'app' => $data2);
        //  die();
    }

    public function getSlotsTradeAccount($spID, $fp) {
        if ($fp != "multimap") {
            $sql = "select 
          *
            
            from service_provider_trade_account
             
            where ServiceProviderID=:ServiceProviderID 
         
            and Status='Active'
         and Postcode=:fullP

        ";


            $params = array('ServiceProviderID' => $spID, 'fullP' => $fp);
        } else {
            $sql = "select 
          TradeAccount,
          spta.Postcode,
          Latitude,
          Longitude,
         Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
            from service_provider_trade_account spta
            join postcode_lat_long pll on pll.PostCode=spta.Postcode
            where ServiceProviderID=:ServiceProviderID 
         
            and Status='Active'
         ";
            $params = array('ServiceProviderID' => $spID);
        }
        $res = $this->Query($this->conn, $sql, $params);


        return $res;
    }

    public function getExportData($type = 1, $dateFrom, $dateTo, $spID, $engs) {


        $dateFromTMP = explode("/", $dateFrom);
        $dateToTMP = explode("/", $dateTo);
        $dateFrom = "$dateFromTMP[2]-$dateFromTMP[1]-$dateFromTMP[0]";
        $dateTo = "$dateToTMP[2]-$dateToTMP[1]-$dateToTMP[0]";
        $dbtmp = explode(';', $this->controller->config['DataBase']['Conn']);
        $db = substr($dbtmp[1], 7);
        $dbhost = substr($dbtmp[0], 11);
        $dbc = mysql_connect($dbhost, $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']) or die(mysql_error());
        mysql_select_db($db);
        $days = $this->count_days($dateFrom, $dateTo);
        switch ($type) {
            case 1: {



                    $q = "";
                    for ($i = 0; $i <= $days; $i++) {
                        $dtmp = explode('-', $dateFrom);
                        $dt = date('Y-m-d', mktime(0, 0, 0, $dtmp[1], $dtmp[2] + $i, $dtmp[0]));





                        $q.="select 
                date_format(spew.WorkingDay,'%d/%m/%Y') as 'Date',
          concat(EngineerFirstName,' ',EngineerLastName) as 'Engineer', 
         
             sped.StartShift  as 'Start Time',
              SEC_TO_TIME(TIME_TO_SEC(sped.StartShift)+spew.TotalWorkTimeSec) as 'End Time',
                
            SEC_TO_TIME(spew.TotalWorkTimeSec) as 'Working Time',
                 SEC_TO_TIME(spew.TotalServiceTimeSec) as 'Service Interval',
                 SEC_TO_TIME(spew.TotalDriveTimeSec) as 'Travel Time',
                 
                 spew.TotalSteps-2 as 'Visited Orders',
              
                sec_to_time(spew.TotalWorkTimeSec/(spew.TotalSteps-2)) as 'Average Visit Time'
                
             from service_provider_engineer spe 
                 join service_provider_engineer_workload spew on spew.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
                  	left join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
                 where spew.WorkingDay='$dt'   and spe.ServiceProviderID=$spID and spew.ServiceProviderEngineerID in ($engs) 
                
					  and sped.WorkDate='$dt'
                    
                 
                 
                 
                 ";
                        if ($i != $days) {
                            $q.=" 
                union 
                ";
                        }
                    }
                    break;
                }
            case 2: {
                    $q = "";
                    for ($i = 0; $i <= $days; $i++) {
                        $dtmp = explode('-', $dateFrom);
                        $dt = date('Y-m-d', mktime(0, 0, 0, $dtmp[1], $dtmp[2] + $i, $dtmp[0]));





                        $q.="select
             j.JobID as 'Skyline Job ID',
             j.ServiceCentreJobNo as 'SB Job No',
             ap.BookedBy as 'Booked By',
           if(isnull(ap.CreatedTimeStamp), concat(date_format(j.DateBooked,'%d/%m/%Y'),' (',date_format(j.TimeBooked,'%H:%i'),')'),  date_format(ap.CreatedTimeStamp,'%d/%m/%Y (%H:%i)')) as 'Booked Date/Time',
             (select date_format(CreatedDate,'%d/%m/%Y (%H:%i)') from viamente_end_day where ServiceProviderID=$spID and RouteDate='$dt' limit 1) as `Finalised Date/Time`,
date_format(ap.AppointmentDate,'%d/%m/%Y') as 'Date',
          
          ap.AppointmentTime as 'Time Slot',
          ap.AppointmentStartTime as 'From',
          ap.AppointmentStartTime2 as 'From2',
          ap.AppointmentEndTime as 'To',
          ap.AppointmentEndTime2 as 'To2',
          SEC_TO_TIME( ap.ViamenteTravelTime ) as 'Travel Time' ,
          DATE_FORMAT(ap.ViamenteStartTime,'%H:%i') as `Arrive Time`,
          ap.Duration as 'Visit Time',
          DATE_FORMAT(DATE_ADD(ap.ViamenteStartTime, INTERVAL ap.Duration MINUTE),'%H:%i')  as 'Depart Time',
          DATE_FORMAT(ap.ViamenteReturnTime,'%H:%i')  as 'ETA',
          
          
          if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as 'Postal Code',
           if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        concat_ws(' ',c.BuildingNameNumber,c.Street,c.TownCity),
        concat_ws(' ',j.ColAddBuildingNameNumber, j.ColAddStreet,j.ColAddLocalArea,j.ColAddTownCity))
        as `Full Address`,
           (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `Engineer Assigned`,
           (select ServiceBaseUserCode from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `SB Engineer Code`,
          if (isnull(ct.Title), c.ContactLastName , concat(ct.Title,' ',c.ContactLastName)) as 'Customer Name',
          m.ManufacturerName as 'Manufacturer',
          cl.ClientName as 'Client',
           j.ServiceBaseUnitType as 'Product Type',
           j.ServiceBaseModel as 'Model Number',
          
          ss.SkillsetName as 'Appointment Type',
          
          (case 
        when ap.importance='0' then 'Standard'
        when ap.importance='1' then 'Medium'
        when ap.importance='2' then 'High'
          
          end)
            as 'Importance',
          ap.Notes,
          ap.PreVisitContactInfo as 'Pre-visit Contact Information',
          (case
         when ap.CompletedBy='Manual' then 'Completed (By user)'
         when ap.CompletedBy='Remote Engineer' then 'Completed (By Remote Engineer)'
         
         end)  as 'Status',
         
          sert.ServiceTypeName as 'Charge Type',
           j.ReportedFault as 'Reported Fault'
           
        from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        left join service_type sert on sert.ServiceTypeID=j.ServiceTypeID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
        and ap.ServiceProviderEngineerID in ($engs) 
       union 
        
        ";
                        $q.="select 
         nsj.NonSkylineJobID as 'Skyline Job ID',
         nsj.ServiceProviderJobNo as 'SB Job No',
         ap.BookedBy as 'Booked By',
          date_format(ap.CreatedTimeStamp,'%d/%m/%Y (%H:%i)') as 'Booked Date/Time',
          (select date_format(CreatedDate,'%d/%m/%Y (%H:%i)') from viamente_end_day where ServiceProviderID=$spID and RouteDate='$dt' limit 1) as `Finalised Date/Time`,
         date_format(ap.AppointmentDate,'%d/%m/%Y') as 'Date',
        ap.AppointmentTime as 'Time Slot',
          ap.AppointmentStartTime as 'From',
          ap.AppointmentStartTime2 as 'From2',
          ap.AppointmentEndTime as 'To',
          ap.AppointmentEndTime2 as 'To2',
          SEC_TO_TIME( ap.ViamenteTravelTime ) as 'Travel Time' ,
          DATE_FORMAT(ap.ViamenteStartTime,'%H:%i') as `Arrive Time`,
          ap.Duration as 'Visit Time',
           DATE_FORMAT(DATE_ADD(ap.ViamenteStartTime, INTERVAL ap.Duration MINUTE),'%H:%i')  as 'Depart Time',
          DATE_FORMAT(ap.ViamenteReturnTime,'%H:%i')  as 'ETA',
          
          nsj.Postcode  as 'Postal Code',
            concat(nsj.CustomerAddress1,' ',nsj.CustomerAddress2,' ',nsj.CustomerAddress3,' ',nsj.CustomerAddress4) as `Full Address`,
         (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `Engineer Assigned`,
         (select ServiceBaseUserCode from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `SB Engineer Code`,
         concat(nsj.CustomerTitle,' ',nsj.CustomerSurname) as 'Customer Name',
         nsj.Manufacturer as Manufacturer,
          nsj.Client,
         nsj.ProductType as 'Product Type',
         nsj.ModelNumber as 'Model Number',
        
        
         ss.SkillsetName as 'Appointment Type',
          (case 
        when ap.importance='0' then 'Standard'
        when ap.importance='1' then 'Medium'
        when ap.importance='2' then 'High'
          
          end)
            as 'Importance',
         
        ap.Notes,
        ap.PreVisitContactInfo as 'Pre-visit Contact Information',
         
         (case
         when ap.CompletedBy='Manual' then 'Completed (By user)'
         when ap.CompletedBy='Remote Engineer' then 'Completed (By Remote Engineer)'
         
         end)  as 'Status',
           
          nsj.ServiceType as 'Charge Type',
          nsj.ReportedFault as 'Reported Fault'
          
         from appointment ap
         
        
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
        and ap.ServiceProviderEngineerID in ($engs) 
      
        ";
                        if ($i != $days) {
                            $q.=" 
                union 
                ";
                        } else {
                            $q.=" order by Date,`Engineer Assigned`,`Arrive Time`";
                        }
                    }
                    break;
                }
            case 3: {
                    $q = "";
                    for ($i = 0; $i <= $days; $i++) {
                        $dtmp = explode('-', $dateFrom);
                        $dt = date('Y-m-d', mktime(0, 0, 0, $dtmp[1], $dtmp[2] + $i, $dtmp[0]));





                        $q.="select
    date_format(ap.AppointmentDate,'%d/%m/%Y') as 'Date',
            ap.SortOrder,
          ap.AppointmentTime as 'Time Slot',
          ap.AppointmentStartTime as 'From',
          ap.AppointmentStartTime2 as 'From2',
          ap.AppointmentEndTime as 'To',
          ap.AppointmentEndTime2 as 'To2',
          
          ap.Duration as 'Visit Time',
          
          
          
          if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as 'Postal Code',
           (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `Engineer Assigned`,
          if (isnull(ct.Title), c.ContactLastName , concat(ct.Title,' ',c.ContactLastName)) as 'Customer Name',
          m.ManufacturerName as 'Manufacturer',
          cl.ClientName as 'Client',
           j.ServiceBaseUnitType as 'Product Type',
           j.ServiceBaseModel as 'Model Number',
          j.ServiceCentreJobNo as 'SB Job No',
           j.JobID as 'Skyline Job ID',
          ss.SkillsetName as 'Appointment Type',
          
          (case 
        when ap.importance='0' then 'Standard'
        when ap.importance='1' then 'Medium'
        when ap.importance='2' then 'High'
          
          end)
            as 'Importance',
          ap.Notes,
          ap.PreVisitContactInfo as 'Pre-visit Contact Information',
          ap.CompletedBy
        from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
        and ap.ServiceProviderEngineerID in ($engs) 
       union 
        
        ";
                        $q.="select 
         date_format(ap.AppointmentDate,'%d/%m/%Y') as 'Date',
         ap.SortOrder,
        ap.AppointmentTime as 'Time Slot',
          ap.AppointmentStartTime as 'From',
          ap.AppointmentStartTime2 as 'From2',
          ap.AppointmentEndTime as 'To',
          ap.AppointmentEndTime2 as 'To2',
           ap.Duration as 'Visit Time',
          nsj.Postcode  as 'Postal Code',
         (select concat(EngineerFirstName,' ',EngineerLastName) from service_provider_engineer sss where sss.ServiceProviderEngineerID= ap.ServiceProviderEngineerID limit 1) as `Engineer Assigned`,
         concat(nsj.CustomerTitle,' ',nsj.CustomerSurname) as 'Customer Name',
         nsj.Manufacturer as Manufacturer,
          nsj.Client,
         nsj.ProductType as 'Product Type',
         nsj.ModelNumber as 'Model Number',
        
         nsj.ServiceProviderJobNo as 'SB Job No',
          nsj.NonSkylineJobID as 'Skyline Job ID',
         ss.SkillsetName as 'Appointment Type',
          (case 
        when ap.importance='0' then 'Standard'
        when ap.importance='1' then 'Medium'
        when ap.importance='2' then 'High'
          
          end)
            as 'Importance',
         
        ap.Notes,
         ap.PreVisitContactInfo as 'Pre-visit Contact Information',
         (case
         when ap.CompletedBy='Manual' then 'Completed (By user)'
         when ap.CompletedBy='Remote Engineer' then 'Completed (By Remote Engineer)'
         
         end)  as 'Status'
         from appointment ap
         
        
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
        and ap.ServiceProviderEngineerID in ($engs) 
      
        ";
                        if ($i != $days) {
                            $q.=" 
                union 
                ";
                        } else {
                            $q.=" order by Date,`Engineer Assigned`, SortOrder asc";
                        }
                    }
                    break;
                }
            case 4: {
                    $q = "";
                    for ($i = 0; $i <= $days; $i++) {
                        $dtmp = explode('-', $dateFrom);
                        $dt = date('Y-m-d', mktime(0, 0, 0, $dtmp[1], $dtmp[2] + $i, $dtmp[0]));





                        $q.="select distinct 

       ap.AppointmentTime as 'Time Slot',
        date_format(ap.AppointmentDate,'%d/%m/%Y') as 'Date',
        concat_ws(' ',ct.Title,c.ContactLastName ) as 'Customer Name',
        if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as 'Post Code',
        
        
  
        ss.SkillsetName  as 'Appointment Type',
     
        
      'Samsung' as 'Manufacturer',
      
        
        if(j.ModelID is null,j.ServiceBaseUnitType,ut.UnitTypeName) as 'Product Type',
        
         if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        concat_ws(' ',c.BuildingNameNumber,c.Street,c.TownCity),
        concat_ws(' ',j.ColAddBuildingNameNumber, j.ColAddStreet,j.ColAddLocalArea,j.ColAddTownCity))
        as fulladress,
         
      
        
         j.NetworkRefNo  as 'S/O NO.'  , 
            ap.ConfirmedByUser as 'Status'
            
         from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        where 
       
        
          aas.ServiceProviderID=$spID
        and  ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
         
         union
       ";
                        $q.="select 
       
        
         ap.AppointmentTime as 'Time Slot',
        date_format(ap.CreatedTimeStamp,'%d/%m/%Y') as 'Date',
        
        concat_ws(' ',nsj.CustomerTitle,nsj.CustomerSurname ) as 'Customer Name',
        
        nsj.Postcode as 'Post Code',
        
        
         
          ss.SkillsetName as 'Appointment Type',
        
        'Samsung' as 'Manufacturer',
       
        nsj.ProductType  as 'Product Type',
		  concat(nsj.CustomerAddress1,' ',nsj.CustomerAddress2,' ',nsj.CustomerAddress3,' ',nsj.CustomerAddress4) as fulladress,
		  
		
        nsj.NetworkRefNo as 'S/O NO.',
          ap.ConfirmedByUser as 'Status'
        from appointment ap
         
         join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        aas.ServiceProviderID=$spID
         and  ap.ServiceProviderID=$spID
        and AppointmentDate='$dt'  
      
        ";
                        if ($i != $days) {
                            $q.=" 
                union 
                ";
                        } else {
                            $q.="  order by 'Date' asc";
                        }
                    }
                    break;
                }
        }


        // die($q);
        $qr = mysql_query($q) or die(mysql_error());
        //$qr=$this->Query($this->conn, $q); //this used for xls export
        return $qr;
    }

    public function count_days($a, $b) {
        // First we need to break these dates into their constituent parts:
        $gd_a = getdate(strtotime($a));
        $gd_b = getdate(strtotime($b));

        // Now recreate these timestamps, based upon noon on each day
        // The specific time doesn't matter but it must be the same each day
        $a_new = mktime(12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year']);
        $b_new = mktime(12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year']);

        // Subtract these two numbers and divide by the number of seconds in a
        //  day. Round the result since crossing over a daylight savings time
        //  barrier will cause this time to be off by an hour or two.
        return round(abs($a_new - $b_new) / 86400);
    }

    public function getGeoTag($postcode, $spID, $full = false) {
        if (!$full) {
            $sql = "select * from service_provider_geotags where Postcode=:Postcode and ServiceProviderID=$spID";
        } else {
            $sql = "select * from postcode_lat_long where Postcode=:Postcode";
        }
        $params = array('Postcode' => $postcode);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function checkEngineer($engID) {
        $sql = "select * from service_provider_engineer where ServiceProviderEngineerID=$engID";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function getCalendarInfoTable($date, $spID) {

        $sql = "select count(AppointmentID) as apCountType,rs.RepairSkillName,ap.AppointmentDate,rs.RepairSkillID,
            (select count(distinct(da.ServiceProviderEngineerID))  
         from diary_allocation da 
        join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
       
       
       	join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
			join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
			 join service_provider_skillset sps2 on sps2.ServiceProviderSkillsetID=spesd.ServiceProviderSkillsetID
			 join skillset sk2 on sk2.SkillsetID=sps2.SkillsetID
			 join repair_skill rs2 on rs2.RepairSkillID=sk2.RepairSkillID
         where 
			da.AllocatedDate=:AppointmentDate 
			and spe.Status='Active'
			and spe.ServiceProviderID=$spID
			and sk2.RepairSkillID=rs.RepairSkillID 
			and sped.WorkDate=da.AllocatedDate
         and sped.Status='Active'
                        
                            ) as totalEngineers,
   
           (select count(AppointmentID) from appointment app  where app.AppointmentDate=:AppointmentDate  and app.ServiceProviderID=$spID) as apCountTotal
         
         
            from appointment ap 
           join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
           join skillset sk on sk.SkillsetID=sps.SkillsetID
           join repair_skill rs on rs.RepairSkillID=sk.RepairSkillID
             where ap.AppointmentDate=:AppointmentDate and ap.ServiceProviderID=$spID  group by rs.RepairSkillName,ap.AppointmentDate ";
        $params = array('AppointmentDate' => $date);
        $res2 = $this->Query($this->conn, $sql, $params);
        $data = array();
        for ($i = 0; $i < sizeof($res2); $i++) {
            $data[$res2[$i]['RepairSkillID']] = $res2[$i];
        }
        ksort($data);

        return $data;
    }

    public function specifyEngineerBulk($apis, $engId) {
        for ($i = 0; $i < sizeof($apis); $i++) {

            $sql = "update appointment set ServiceProviderEngineerID=$engId,ForceEngineerToViamente=1,TempSpecified='No' where AppointmentID in ($apis) and LockPartsEngineer!='Yes'";
            $res = $this->execute($this->conn, $sql);
        }
    }

    public function unspecifyEngineerBulk($apis, $engId) {
        if ($this->debug)
            $this->controller->log($apis);
        for ($i = 0; $i < sizeof($apis); $i++) {

            $sql = "update appointment set ServiceProviderEngineerID = null,ForceEngineerToViamente=0 where AppointmentID in ($apis) and LockPartsEngineer!='Yes'";
            $res = $this->execute($this->conn, $sql);
        }
    }

    public function markAsComplited($apis, $type, $spID = false) {
        if ($type == '1') {
            $status = "'Manual'";
        }
        if ($type == '2') {
            $status = "NULL";
        }
        switch ($type) {
            case 1: $status = "'Manual'";
                break;
            case 2: $status = "NULL";
                break;
            case 3: $status = "'Remote Engineer'";
                break;
            case 4: $status = "'Out Card Left'";
                break;
        }
        $extrasql = "";
        if ($spID) {
            $extrasql = "and ServiceProviderID=$spID";
        }


        $sql = "update appointment set CompletedBy=$status where AppointmentID in ($apis) $extrasql";
        $res = $this->execute($this->conn, $sql);
    }

    public function getUnreached($spID) {
        $sql = "select
          ServiceCentreJobNo as sbjobid
           ,DATE_FORMAT(ap.AppointmentDate,'%d/%m/%Y') as AppointmentDate
			,c.PostalCode as pc
			,TIME_FORMAT(ap.AppointmentStartTime,'%H:%i') as AppointmentStartTime
			,TIME_FORMAT(ap.AppointmentEndTime,'%H:%i') as AppointmentEndTime
			,TIME_FORMAT(ap.AppointmentStartTime2,'%H:%i') as AppointmentStartTime2
			,TIME_FORMAT(ap.AppointmentEndTime2,'%H:%i') as AppointmentEndTime2
			,ap.AppointmentType
			,ap.Duration
			,ap.BookedBy
			   ,DATE_FORMAT(ap.CreatedTimeStamp,'%d/%m/%Y %H:%i') as CreatedTimeStamp
        from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ViamenteUnreached='1' and
        ap.ServiceProviderID=$spID
        and AppointmentDate>=curdate()  
     
       union all 
        
        
select 
         ServiceProviderJobNo as sbjobid
      ,DATE_FORMAT(ap.AppointmentDate,'%d/%m/%Y') as AppointmentDate
			,nsj.Postcode as pc
			,TIME_FORMAT(ap.AppointmentStartTime,'%H:%i') as AppointmentStartTime
			,TIME_FORMAT(ap.AppointmentEndTime,'%H:%i') as AppointmentEndTime
			,TIME_FORMAT(ap.AppointmentStartTime2,'%H:%i') as AppointmentStartTime2
			,TIME_FORMAT(ap.AppointmentEndTime2,'%H:%i') as AppointmentEndTime2
			,ap.AppointmentType
			,ap.Duration
			,ap.BookedBy
			
       ,DATE_FORMAT(ap.CreatedTimeStamp,'%d/%m/%Y %H:%i') as CreatedTimeStamp
         from appointment ap
         
        
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ViamenteUnreached='1' and
        ap.ServiceProviderID=$spID
          and AppointmentDate>=curdate()  
       
      
        ";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res;
        } else {
            return false;
        }
    }

    public function getSuperviserEmail($spID) {
        $sql = "select DiaryAdministratorEmail from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['DiaryAdministratorEmail'];
    }

    public function getAdminSupervisorEmail($spID) {
        $sql = "select AdminSupervisorEmail from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['AdminSupervisorEmail'];
    }

    public function getErrorApp($spID, $days = 6, $date = false, $notConfirmed = false) {
        if (!$date) {
            $datesql = "and ap.AppointmentDate>=curdate()  
        and ap.AppointmentDate<=DATE_ADD(curdate(), INTERVAL " . $days . " DAY) ";
        } else {
            $datesql = "and ap.AppointmentDate='$date'";
        }

        if ($notConfirmed) {
            $datesql = $datesql . " AND ap.ConfirmedByUser='Not Confirmed' ";
        }

        $sql = "select
          ServiceCentreJobNo as sbjobid
           ,DATE_FORMAT(ap.AppointmentDate,'%d/%m/%Y') as AppointmentDate
			,c.PostalCode as pc
			,TIME_FORMAT(ap.AppointmentStartTime,'%H:%i') as AppointmentStartTime
			,TIME_FORMAT(ap.AppointmentEndTime,'%H:%i') as AppointmentEndTime
			,TIME_FORMAT(ap.AppointmentStartTime2,'%H:%i') as AppointmentStartTime2
			,TIME_FORMAT(ap.AppointmentEndTime2,'%H:%i') as AppointmentEndTime2
			,ap.AppointmentType
			,ap.Duration
			,ap.BookedBy
			   ,DATE_FORMAT(ap.CreatedTimeStamp,'%d/%m/%Y %H:%i') as CreatedTimeStamp
        from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
    isnull(ap.ServiceProviderEngineerID) and
        ap.ViamenteUnreached='0' and
        ap.ServiceProviderID=$spID
        $datesql  
      
       union all 
        
        
select 
         ServiceProviderJobNo as sbjobid
      ,DATE_FORMAT(ap.AppointmentDate,'%d/%m/%Y') as AppointmentDate
			,nsj.Postcode as pc
			,TIME_FORMAT(ap.AppointmentStartTime,'%H:%i') as AppointmentStartTime
			,TIME_FORMAT(ap.AppointmentEndTime,'%H:%i') as AppointmentEndTime
			,TIME_FORMAT(ap.AppointmentStartTime2,'%H:%i') as AppointmentStartTime2
			,TIME_FORMAT(ap.AppointmentEndTime2,'%H:%i') as AppointmentEndTime2
			,ap.AppointmentType
			,ap.Duration
			,ap.BookedBy
			
       ,DATE_FORMAT(ap.CreatedTimeStamp,'%d/%m/%Y %H:%i') as CreatedTimeStamp
         from appointment ap
         
        
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
        ap.ViamenteUnreached='0' and
         isnull(ap.ServiceProviderEngineerID) and
        ap.ServiceProviderID=$spID
          $datesql
      
        ";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res;
        } else {
            return false;
        }
    }

    public function getForceAppointmentDetails($appID) {
        $sql = "select
          
           DATE_FORMAT(ap.AppointmentDate,'%d/%m/%Y') as AppointmentDate
			
			,TIME_FORMAT(ap.AppointmentStartTime,'%H:%i') as AppointmentStartTime
			,TIME_FORMAT(ap.AppointmentEndTime,'%H:%i') as AppointmentEndTime
			,TIME_FORMAT(ap.AppointmentStartTime2,'%H:%i') as AppointmentStartTime2
			,TIME_FORMAT(ap.AppointmentEndTime2,'%H:%i') as AppointmentEndTime2
			,ap.AppointmentType
			,ap.Duration
			,ap.BookedBy
			   ,DATE_FORMAT(ap.CreatedTimeStamp,'%d/%m/%Y %H:%i') as CreatedTimeStamp,
           
            
                
            IF (ISNULL(c.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            c.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS  pc,
                            IF (ISNULL(c.`CustomerID`),
                                    nsj.`ServiceProviderJobNo`
                            ,
                                   
                                            j.`ServiceCentreJobNo`
                                    
                            ) AS  sbjobid,
                            IF (ISNULL(c.`CustomerID`),
                                    CONCAT(nsj.CustomerAddress1, ' ', nsj.CustomerAddress2, ' ', nsj.CustomerAddress3)
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            CONCAT(c.BuildingNameNumber, ' ', c.Street, ' ', c.LocalArea)
                                    ,
                                            CONCAT(j.ColAddBuildingNameNumber, ' ', j.ColAddStreet, ' ', j.ColAddLocalArea)
                                    )
                            ) AS  apptAddr,
                            sk.SkillsetName,
                            IF (ISNULL(ap.`JobID`),
                                CONCAT(nsj.ProductType, ' ', nsj.ModelNumber)
                            ,
                                CONCAT(ut.UnitTypeName, ' ', mo.ModelNumber)
                            ) AS prType,
                            ap.ForceAddReason
            from appointment ap 
            left join diary_allocation da on da.DiaryAllocationID=ap.DiaryAllocationID
            left join job j on j.JobID=ap.JobID
            left join customer c on c.CustomerID=j.CustomerID
            left join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
            join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID	
            join skillset sk on sk.SkillsetID=sps.SkillsetID
            left join  model mo on mo.ModelID=j.ModelID
            left join  unit_type ut on ut.UnitTypeID=mo.UnitTypeID
            where ap.AppointmentID=:AppointmentID 
            ";
        $params = array('AppointmentID' => $appID);
        $res = $this->Query($this->conn, $sql, $params);

        if (isset($res[0])) {
            return $res;
        } else {
            return false;
        }
    }

    public function getDiarySupervisorPass($spID) {
        $sql = "select UnlockingPassword from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);


        return $res[0]['UnlockingPassword'];
    }

    public function getNextDayBookingOffTime($spID) {
        $sql = "select PasswordProtectNextDayBookingsTime,PasswordProtectNextDayBookings from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return array(0 => $res[0]['PasswordProtectNextDayBookingsTime'], 1 => $res[0]['PasswordProtectNextDayBookings']);
    }

    public function getAllServiceProviderInfo($spID) {
        $sql = "select *,time_to_sec(EngineerDefaultStartTime) as startSec,time_to_sec(EngineerDefaultEndTime) as endSec from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);


        return $res[0];
    }

//    public function getSpecificPostcodeEngineers($slot,$skillset,$postcode,$spID,$date){
//      if($slot=="LL"){
//          $slot=="ANY";
//          $trr="";
//      }ELSE
//      {
//          //$trr="and da.SlotsLeft>0";
//          $trr="";
//      }
//      
//      if($date){
//          $dateFilter="and da.AllocatedDate='$date'";
//      }else{
//          $dateFilter="";
//      }
//       $postCodeAlpha=preg_replace('/[^\\/\-a-z\s]/i', '', $postcode);
//         $postcodeSQL="and (dpa.PostCode='$postcode' or dpa.PostCode='$postCodeAlpha')";
//         
//         $sql="
//             select distinct(da.DiaryAllocationID) from diary_allocation da 
//join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID 
//join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
//join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
//join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
//join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
//
//where da.ServiceProviderID=:ServiceProviderID
//
//and aas.Type=:Type 
//and da.AllocatedDate=:AllocatedDate
//and dpa.DiaryAllocationID=da.DiaryAllocationID 
//and spe.Status='Active' 
//  $postcodeSQL
//and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
//and sped.`Status`='Active'
//and sped.WorkDate=da.AllocatedDate
//order by da.SlotsLeft Desc 
//$trr
//
//
//
//            ";
//         
//     
//       
//      if ($this->debug) $this->controller->log("Diary allocator Sql=".$sql,'diary_log');
//       if($date){
//           $sqlDate=$date;
//       }else{
//           $sqlDate=$this->controller->session->SQLSelectedDate;
//       }
//       $params=array(
//           'ServiceProviderSkillsetID'=>$skillset,
//           'ServiceProviderID'=>$spID,
//           'AllocatedDate'=>$sqlDate,
//           'Type'=>$slot
//           
//           );
//       
//       
//      
//       $res=$this->Query($this->conn, $sql, $params); 
//    }

    public function getEngEmailFromName($name, $spID) {
        $name = explode(' ', $name);
        $fname = "";
        $lname = $name[sizeof($name) - 1];
        for ($i = 0; $i < sizeof($name) - 1; $i++) {
            $fname.=$name[$i];
        }
        //echo"$fname $lname";
        $sql = "select EmailAddress from service_provider_engineer where EngineerFirstName=:EngineerFirstName and EngineerLastName=:EngineerLastName and ServiceProviderID=$spID";
        $params = array(
            'EngineerFirstName' => $fname,
            'EngineerLastName' => $lname,
        );
        $res = $this->Query($this->conn, $sql, $params);
        return $res[0]['EmailAddress'];
    }

    public function getEngWithApp($spID, $date) {

        $sql = "select spe.EngineerFirstName as nn,  spe.EngineerLastName as ss,  spe.ServiceProviderEngineerID as dd,spe.EmailAddress
 from service_provider_engineer spe 
 join appointment ap on ap.ServiceProviderEngineerID=spe.ServiceProviderEngineerID 
 where  spe.ServiceProviderID=$spID and  ap.AppointmentDate=:AppointmentDate group by spe.ServiceProviderEngineerID order by EngineerFirstName asc,EngineerLastName asc";
        $datetmp = explode('/', $date);
        if (sizeof($datetmp) > 1) {
            $date = $datetmp[2] . "-" . $datetmp[1] . "-" . $datetmp[0];
        }

        $params = array('AppointmentDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);
        return $res;
    }

    public function getEngDetailsToEmail($eids) {
        $eids2 = "";
        foreach ($eids as $e) {
            $eids2.=$e . ", ";
        }
        $sql = "select spe.EngineerFirstName as nn,  spe.EngineerLastName as ss,  spe.ServiceProviderEngineerID as dd,spe.EmailAddress
 from service_provider_engineer spe 
 
 where  spe.ServiceProviderEngineerID in ($eids2 0)";

        $res = $this->Query($this->conn, $sql);

        return $res;
    }

    public function countAppDay($spID, $date) {
        $sql = "select count(AppointmentID) as ccc from appointment a where a.ServiceProviderID=$spID and a.AppointmentDate=:AppointmentDate";
        $params = array('AppointmentDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log($res, 'test_log_');
        if ($this->debug)
            $this->controller->log($spID, 'test_log_');
        if ($this->debug)
            $this->controller->log($date, 'test_log_');
        return $res[0]['ccc'];
    }

    public function checkCompleted($data) {
        $data = trim($data, ',');
        if ($data == "") {
            $data = 0;
        }
        $sql = "select * from appointment where AppointmentID in ($data) and CompletedBy is not null";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }
    
    /*
     * @author Dileep Bhimineni
     * 
     * Check to see if Engineer is Locked with Appoiuntments for that Day.
     * 
     * @input variables -
     *      $id     = Service Provider Enginner ID
     *      $date   = Appointment date  we need to verify
     * 
     * return string (true/false)
     * 
     */
    
    public function validateEngineerLocked($id,$startDate,$endDate) {
        
        if (empty($id) || empty($startDate) || empty($endDate)) {
            return "false";
        }
        
        $sql = "SELECT
                        *
                FROM
                        appointment
                WHERE
                ServiceProviderEngineerID = ". $id . 
               " AND  (
                    AppointmentDate = '" . $startDate .
                    "'OR AppointmentDate ='" . $endDate . "'
                )
                AND ForceEngineerToViamente = 1;";

        $res    = $this->Query($this->conn, $sql);
        return $res;
       
    }
    
    /*
     * @author Dileep Bhimineni
     * 
     * Check to see if Service Proder Engineer has already Appointment 
     * 
     * @input variables -
     *      $speID - Service Provider Engineer
     * 
     * return string (true/false)
     * 
     */
    
    public function validateHolidayAppointment($id,$startTime,$endTime) {
        if (empty($id) || empty($startTime) || empty($endTime))    
            return "false";
        if(isset($_POST['id']))
            $where = "DiaryHolidayID <> " . $_POST['id'] . " AND ServiceProviderEngineerID = " . $id;
        else
            $where = "ServiceProviderEngineerID = " . $id ;
        $sql = "SELECT * FROM diary_holiday_diary WHERE " . $where ;
        $res = $this->Query($this->conn, $sql);
        $newStartDate = new DateTime($startTime);
        $newEndDate = new DateTime($endTime);
        foreach ($res as $value) {
            $oldStartDate = new DateTime($value['StartTime']);
            $oldEndDate = new DateTime($value['EndTime']);
            if (($newStartDate >= $oldStartDate && $newStartDate <= $oldEndDate) || ($newEndDate >= $oldStartDate && $newEndDate <= $oldEndDate) || ($newStartDate < $oldStartDate && $newEndDate > $oldEndDate))
                  return "false";
        }
        return "true";
    }

    public function insertHoliday($post, $spID) 
    {
        
        
        $st = explode("/", $post['hStartDate']);
        $et = explode("/", $post['hEndDate']);
        $s  = $st[2] . "-" . $st[1] . "-" . $st[0];
        $e  = $et[2] . "-" . $et[1] . "-" . $et[0];
        
        $startTime  = $s . " " . $post['hStartTime'] . ":00";
        $endTime    = $e . " " . $post['hEndTime'] . ":00";
        
        $validateAppt = $this->validateHolidayAppointment($post['hEng'],$startTime,$endTime);       
        
        if ($validateAppt == "true") {
        
            $sql = "insert into diary_holiday_diary 
                       (ServiceProviderID,
                        ServiceProviderEngineerID,
                        StartTime,
                        EndTime,
                        Reason)
                    values
                       (:ServiceProviderID,
                        :ServiceProviderEngineerID,
                        :StartTime,
                        :EndTime,
                        :Reason)"; 

            if ($post['hReason'] == "") {
                $post['hReason'] =  "Holiday";
            }
            $params =
                    array(
                        'ServiceProviderID'         => $spID,
                        'ServiceProviderEngineerID' => $post['hEng'],
                        'StartTime'                 => $startTime,
                        'EndTime'                   => $endTime,
                        'Reason'                    => $post['hReason']
            );

            $this->execute($this->conn, $sql, $params);


            $this->updateHolidaySlots($s, $e, $post['hEng'], $this->conn->lastInsertId(), $post['hStartTime'] . ":00", $post['hEndTime'] . ":00");
        }
    }

    public function getHolidayEntrys($spID,$date = "") {
        
        $where = '';
        if (!empty($date)) {
            $where = " AND  dhd.StartTime >= '$date'" ;
        }
        $sql = "select  DiaryHolidayID,
                        StartTime,
                        EndTime,
                        Reason,
                        concat(spe.EngineerFirstName,' ',spe.EngineerLastName) as engName 
                from diary_holiday_diary dhd
                join service_provider_engineer spe 
                on spe.ServiceProviderEngineerID=dhd.ServiceProviderEngineerID
                where dhd.ServiceProviderID=$spID $where ;";
        $res = $this->Query($this->conn, $sql);
        return $res;
        
    }

    public function getHolidayEdit($eid) {
        
        $sql = "SELECT
                    dh.*, sp.CompanyName,
                    sp.Acronym
                FROM
                    diary_holiday_diary dh,
                    service_provider sp
                WHERE
                    dh.ServiceProviderID = sp.ServiceProviderID
                AND DiaryHolidayID =$eid";
        
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        }
        
    }

    public function updateHoliday($post, $spID) 
    {
        $sql = "update diary_holiday_diary 
                set ServiceProviderEngineerID=:ServiceProviderEngineerID,
                    StartTime=:StartTime,
                    EndTime=:EndTime,
                    Reason=:Reason 
                    where 
                    DiaryHolidayID=:DiaryHolidayID";
        
        $st = explode("/", $post['hStartDate']);
        $et = explode("/", $post['hEndDate']);
        $s = $st[2] . "-" . $st[1] . "-" . $st[0];
        $e = $et[2] . "-" . $et[1] . "-" . $et[0];
        if ($post['hReason'] == "") {
            $post['hReason'] = "Holiday";
        }
        $params =
                array(
                    'ServiceProviderEngineerID' => $post['hEng'],
                    'StartTime' => $s . " " . $post['hStartTime'] . ":00",
                    'EndTime' => $e . " " . $post['hEndTime'] . ":00",
                    'Reason' => $post['hReason'],
                    'DiaryHolidayID' => $post['id']
        );
        $res = $this->execute($this->conn, $sql, $params);
        $this->updateHolidaySlots($s, $e, $post['hEng'], $post['id'], $post['hStartTime'] . ":00", $post['hEndTime'] . ":00");
    }

    public function deleteHolidayEntry($id, $spID) {
        $sql = "delete from diary_holiday_diary where DiaryHolidayID=$id and ServiceProviderID=$spID";
        $res = $this->execute($this->conn, $sql);
        $sql = "delete from diary_holiday_slots where DiaryHolidayDiaryID=$id";
        $this->execute($this->conn, $sql);
    }

    public function deleteFaqEntry($id) {
        $sql = "update diary_faq set Status='In-Active' where DiaryFaqID=$id";
        $res = $this->execute($this->conn, $sql);
    }

    private function updateHolidaySlots($s, $e, $engID, $holID, $st, $et) {
        $sql = "delete from diary_holiday_slots where DiaryHolidayDiaryID=$holID";
        $this->execute($this->conn, $sql);
        $days = $this->count_days($s, $e);
        for ($i = 0; $i < $days + 1; $i++) {

            $sql = "insert into diary_holiday_slots 
               (DiaryHolidayDiaryID,HolidayDate,StartTimeSec,EndTimeSec,TotalTimeSec,ServiceProviderEngineerID) 
               values 
               (:DiaryHolidayDiaryID,DATE_ADD(:HolidayDate, INTERVAL $i DAY),time_to_sec(:StartTimeSec),time_to_sec(:EndTimeSec),time_to_sec(:EndTimeSec)-time_to_sec(:StartTimeSec),:ServiceProviderEngineerID)
               ";
            $et1 = $et;
            $st1 = $st;
            if ($i != $days && $days > 1 && $i != 0) {
                $st1 = "00:00:00";
                $et1 = "23:59:59";
            }
            if ($i == $days && $days > 0) {
                $st1 = "00:00:00";
                $et1 = $et;
            }
            if ($i == 0 && $days > 0) {
                $et1 = "23:59:59";
            }

            $params = array(
                'DiaryHolidayDiaryID' => $holID,
                'HolidayDate' => $s,
                'StartTimeSec' => $st1,
                'EndTimeSec' => $et1,
                'ServiceProviderEngineerID' => $engID
            );
            $res = $this->execute($this->conn, $sql, $params);
        }
    }

    ///timeline sliders start
    public function getFullEngineersList($spID, $date = false, $skillset = false) {
        $sqlDateFilter = "";
        $sqlJoin = "";
        $weekday = date('l', strtotime($date));
        if ($date) {
            $sqlSelectHolidays = " ,
    (select Reason from diary_holiday_slots dhs 
        join diary_holiday_diary dhd on dhd.DiaryHolidayID=dhs.DiaryHolidayDiaryID
        where dhs.HolidayDate='$date' and dhs.ServiceProviderEngineerID=spe.ServiceProviderEngineerID limit 1) as hasHoliday,
       ";
            $sqlJoin = "join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=spe.ServiceProviderEngineerID";

            $sqlDateFilter = "and sped.`Status`='Active' 
                             and sped.WorkDate='$date'";
        }
        if ($skillset) {
            $sqlJoin.=" join service_provider_engineer_skillset_day spes on spes.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID";
            $sqlDateFilter.=" and spes.ServiceProviderSkillsetID=$skillset";
        }
//  $sql="select spe.ServiceProviderEngineerID,
//       concat(EngineerFirstName,' ',EngineerLastName) as engName,
//       time_to_sec(sped.StartShift)as startTime,time_to_sec(sped.EndShift)as endTime,
//       RouteColour
//       from service_provider_engineer spe $sqlJoin
//       join appointment ap on ap.ServiceProviderEngineerID=spe.ServiceProviderEngineerID
//       where  spe.ServiceProviderID=$spID and  spe.status='Active' $sqlDateFilter 
//       and ap.ViamenteStartTime is not null 
//       and ap.AppointmentDate='$date' 
//       group by spe.ServiceProviderEngineerID 
//       order by spe.ServiceProviderEngineerID asc";
        
       /*
        * @author dileep Bhimineni.
        * 
        * Check if slider start/end time is available use it otherwise use Default start/end time
        *  
       */
       
        $sql = "select spe.ServiceProviderEngineerID,
       concat(EngineerFirstName,' ',EngineerLastName) as engName,
       
       CASE WHEN sped.`SliderStartShift` IS NULL or sped.SliderStartShift = '' 
            THEN time_to_sec(sped.`StartShift`)
            ELSE time_to_sec(sped.`SliderStartShift`) END
                       AS `startTime`,
       CASE WHEN sped.`SliderEndShift` IS NULL 
                    or sped.SliderEndShift = '' 
            THEN time_to_sec(sped.`EndShift`)
            ELSE time_to_sec(sped.`SliderEndShift`) END
                    AS `endTime`,

       RouteColour $sqlSelectHolidays
           
        sped.SliderStartShift, 
        sped.SliderEndShift,
        sped.StartShift,
        sped.EndShift
      
       from service_provider_engineer spe $sqlJoin
      
       where  spe.ServiceProviderID=$spID and  spe.status='Active' $sqlDateFilter 
       and spe.ViamenteExclude!='Yes'
       
       group by spe.ServiceProviderEngineerID 
       order by spe.ServiceProviderEngineerID asc";


        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getAllEngineerAppointments($spID, $date) {

        $sql = "select a.ServiceProviderEngineerID,
          (time_to_sec(a.ViamenteDepartTime)-(a.Duration*60)) as apStart,
          time_to_sec(a.ViamenteStartTime) as idle,
           time_to_sec(a.ViamenteDepartTime) as apEnd,
          (time_to_sec(a.ViamenteStartTime)-a.ViamenteTravelTime) as EngstartTimeSec,
          time_to_sec(a.ViamenteReturnTime) as EngEndTimeSec,
          a.ViamenteReturnTime as EngEndTime,
          sec_to_time(time_to_sec(a.ViamenteStartTime)-a.ViamenteTravelTime) as EngstartTime,
       IF (ISNULL(cu.`CustomerID`),
                                    CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`,a.`AppointmentID`)
                            ,
                                    CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`,a.`AppointmentID`)
                            ) AS `name`,
                            IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `postcode`,
       SkillsetName,
      aas.Colour as slotCol
           from appointment a 
       join appointment_allocation_slot aas on aas.Type=a.AppointmentTime
       LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                            LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                            LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                            LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID`  = spe.`ServiceProviderEngineerID`
       join service_provider_skillset sps on sps.ServiceProviderSkillsetID=a.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
           where a.AppointmentDate='$date' 
       and a.ViamenteStartTime is not null
       and a.ViamenteDepartTime is not null
        and a.ViamenteExclude!='Yes'
        and a.ServiceProviderID=$spID and a.ServiceProviderEngineerID is not null group by a.AppointmentID order by a.ServiceProviderEngineerID asc,a.ViamenteStartTime asc ";
        $res = $this->Query($this->conn, $sql);


        return $res;
    }
    
    
    /*
     * @author Unknown
     * @Comments added by Dileep Bhimineni.
     * 
     * Update service_provider_engineer_details table with new slider start time and Slider end time.
     * 
     * @params 
     *      $post - All POST Values from Form (updated start time,updated end time,ServiceproviderEngineerID)
     *      $date - Selected date from Calendar.
     * 
     * @returns Nothing
     * 
     */
 
    public function updateEngineerShift($post, $date) {
        
        
        $sql = "update  service_provider_engineer_details 
                set     SliderStartShift=sec_to_time(:StartShift),
                        SliderEndShift=sec_to_time(:EndShift)
                where   ServiceProviderEngineerID=:ServiceProviderEngineerID 
                and     WorkDate=:WorkDate
            ";
        
        $params = array
            (
            'StartShift'                => $post['s'],
            'EndShift'                  => $post['e'],
            'ServiceProviderEngineerID' => $post['id'],
            'WorkDate'                  => $date
        );
        
        $this->execute($this->conn, $sql, $params);
        
    }

    public function getUnreachable($spID, $date) {
        $sql = "select a.ServiceProviderEngineerID,
       a.AppointmentID,
          a.Duration,
       a.AppointmentDate,
       IF (ISNULL(cu.`CustomerID`),
                                    CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`,a.`AppointmentID`)
                            ,
                                    CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`,a.`AppointmentID`)
                            ) AS `name`,
                            IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `postcode`,
                            IF (
	a.`ForceEngineerToViamente` = '1' ,
	CONCAT(spe.EngineerFirstName, ' ',spe.EngineerLastName),NULL
        ) AS `engName`,
       SkillsetName,
       sps.ServiceProviderSkillsetID, a.ServiceProviderID
       
       from appointment a
        LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                            LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                            LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                            LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID`  = spe.`ServiceProviderEngineerID`
                                            join service_provider_skillset sps on sps.ServiceProviderSkillsetID=a.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
       where ViamenteUnreached='1' 
       and AppointmentDate=:AppointmentDate 
       and a.ServiceProviderID=$spID";
        $params = array(
            'AppointmentDate' => $date
        );
        $res = $this->Query($this->conn, $sql, $params);
        return $res;
    }

    //timeline sliders end



    public function deleteRMAAppointments($spId) {

        $sql = "update appointment set `ServiceProviderID`=0 where `ServiceProviderID`=$spId and `AppointmentDate`>=curdate() and (AppointmentTime is null or AppointmentTime='0')";
        if ($this->debug)
            $this->controller->log($sql, 'diary_rma_log');
        $this->execute($this->conn, $sql);
        $sql = "update service_provider set OnlineDiary='Active',DiaryStatus='Active' where `ServiceProviderID`=$spId";
        if ($this->debug)
            $this->controller->log($sql, 'diary_rma_log');
        $this->execute($this->conn, $sql);
    }

    public function multiMapGetData($spID, $rs = 2, $date = false) {

        if (!$date) {
            $sql = "select distinct(da.AllocatedDate) from diary_allocation da 
join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
where 
da.AllocatedDate>=curdate() 
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and da.ServiceProviderID=$spID group by da.AllocatedDate order by da.AllocatedDate asc limit 6";
            $res2 = $this->Query($this->conn, $sql);
        } else {
            $sql = "select da.AllocatedDate from diary_allocation da 
join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
where 
da.AllocatedDate=:AllocatedDate 
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and da.ServiceProviderID=$spID group by da.AllocatedDate order by da.AllocatedDate asc limit 1";
            $par = array('AllocatedDate' => $date);
            $res2 = $this->Query($this->conn, $sql, $par);
        }


        $days = "";
        for ($i = 0; $i < sizeof($res2); $i++) {
            $days.="'" . $res2[$i]['AllocatedDate'] . "'" . ",";
            $params[$res2[$i]['AllocatedDate']][] = "0";
        }
        $days.="0";

        if ($rs == 0) {
            $extraSQL = "";
            $param2 = array();
        } else {
            $param2 = array('rs' => $rs);
            $extraSQL = "and sk.RepairSkillID=:rs";
        }
        $sql = "
        SELECT
                            IF (ISNULL(cu.`CustomerID`),
                                    CONCAT_WS(' ',nsj.`CustomerTitle`,nsj.`CustomerSurname`)
                            ,
                                    CONCAT_WS(' ',cu.`ContactFirstName`,cu.`ContactLastName`)
                            ) AS `name`,
                            IF (ISNULL(cu.`CustomerID`),
                                   nsj.Manufacturer
                            ,
                                   m.ManufacturerName
                            ) AS `manufacturer`,
        IF (ISNULL(cu.`CustomerID`),
                                   nsj.Client
                            ,
                                   cc.ClientName
                            ) AS `ClientName`,
       
                            IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `location`,
       IF (ISNULL(cu.`CustomerID`),
                                   CONCAT_WS(' ',nsj.`CustomerAddress1`,nsj.`CustomerAddress2`,nsj.`CustomerAddress3`,nsj.`CustomerAddress4`,nsj.`Postcode`)
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`) OR (j.`ColAddPostcode` = '') OR (j.`ColAddPostcode` = '0'),
                                           CONCAT_WS(' ',cu.`BuildingNameNumber`,cu.`Street`,cu.`LocalArea`,cu.`TownCity`,cu.`PostalCode`)
                                    ,
                                           CONCAT_WS(' ',j.`ColAddBuildingNameNumber`,j.`ColAddStreet`,j.ColAddLocalArea,j.ColAddTownCity,j.`ColAddPostcode`)
                                    )
                            ) AS `adress`,
                            sec_to_time(a.`Duration`*60) as Duration ,
                       concat_ws(' ',time_format(a.AppointmentStartTime,'%H:%m'),time_format(a.AppointmentEndTime,'%H:%m'),'    ',time_format(a.AppointmentStartTime2,'%H:%m'),time_format(a.AppointmentEndTime2,'%H:%m')) AS `twindow`,
                          date_format(a.ViamenteStartTime,'%H:%m') as start,
                          date_format(a.ViamenteDepartTime,'%H:%m') as end,
                          sec_to_time(ViamenteTravelTime) as next,
                            a.`AppointmentID`,
                            a.`importance`,
                            a.`ForceEngineerToViamente`,
                            a.AppointmentType,
                            a.Latitude,
                            a.Longitude,
                            a.ViamenteUnreached,
                            a.ViamenteStartTime,
       a.AppointmentTime,
                            CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`) AS `engname`,
                            a.AppointmentDate as 'date',
                            CASE
                                WHEN j.ModelID IS NOT NULL THEN ut.UnitTypeName
                                WHEN j.ProductID IS NOT NULL THEN u.UnitTypeName
                                ELSE CASE	
                                    WHEN j.ServiceBaseUnitType IS NOT NULL THEN j.ServiceBaseUnitType
                                    ELSE ''
                                    END
                            END as unitTypeName

                        FROM
                            `appointment` a LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                            LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                            LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`
                                             left join client cc on cc.ClientID=j.ClientID
       
       left join manufacturer m on m.ManufacturerID=j.ManufacturerID
                                            LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID`  = spe.`ServiceProviderEngineerID`
                                            join service_provider_skillset sps on sps.ServiceProviderSkillsetID=a.ServiceProviderSkillsetID
                                            join skillset sk on sk.SkillsetID=sps.SkillsetID
                                            LEFT JOIN product AS p ON j.ProductID = p.ProductID
                                            LEFT JOIN model AS mo ON j.ModelID = mo.ModelID
                                            LEFT JOIN unit_type AS ut ON mo.UnitTypeID = ut.UnitTypeID
                                            LEFT JOIN unit_type AS u ON p.unitTypeID = u.UnitTypeID
                                          
                        
                        WHERE
                          
			     a.`AppointmentDate` in ($days)
                            $extraSQL
			    AND a.`ServiceProviderID` = $spID
                            order by a.`AppointmentDate` asc
			       
        ";
        if ($rs == 0) {
            $res = $this->Query($this->conn, $sql);
        } else {

            $res = $this->Query($this->conn, $sql, $param2);
        }



        $sql = "select 
   
          spta.Postcode
          
            from service_provider_trade_account spta
            
            where ServiceProviderID=:ServiceProviderID 
         
            and Status='Active'
         ";
        $params3 = array('ServiceProviderID' => $spID);

        $res2 = $this->Query($this->conn, $sql, $params3);
        $taa = array();
        for ($e = 0; $e < sizeof($res2); $e++) {
            $taa[] = $res2[$e]['Postcode'];
        }
        for ($n = 0; $n < count($res); $n++) { /* Generate waypoint array in paramters for each required appointment */
            $cords = false;


            if ($res[$n]['Latitude'] != "" && $res[$n]['Longitude'] != "") {
                $cords['latitude'] = $res[$n]['Latitude'];
                $cords['longitude'] = $res[$n]['Longitude'];
            }
            if (is_null($res[$n]['location']) && !$cords) {
                if ($this->debug)
                    $this->controller->log("No postcode can't optimise");
            } elseif (!$cords) {
                $cords = $this->pc->getLatLong($res[$n]['location']);      /* Generate latitude and longitude from postcode */
            }
            if (isset($res[$n]['location']) && in_array($res[$n]['location'], $taa)) {
                $ta = 1;
            } else {
                $ta = 0;
            }
            $reptype = $this->getRepairType($res[$n]['AppointmentID']);


            $params[$res[$n]['date']][] = array(
                'name' => $res[$n]['name'],
                'utName' => (($res[$n]['unitTypeName'] != "")?$res[$n]['unitTypeName']:'-'),
                'engname' => $res[$n]['engname'],
                'twindow' => $res[$n]['twindow'],
                'duration' => $res[$n]['Duration'],
                'latitude' => $cords['latitude'],
                'longitude' => $cords['longitude'],
                'start' => $res[$n]['start'],
                'end' => $res[$n]['end'],
                'next' => $res[$n]['next'],
                'adress' => $res[$n]['adress'],
                'AppointmentType' => $res[$n]['AppointmentType'],
                'ta' => $ta,
                'reptype' => $reptype['RepairSkillName'],
                'AppointmentTime' => $res[$n]['AppointmentTime'],
                'manufacturer' => $res[$n]['manufacturer'],
                'ClientName' => $res[$n]['ClientName'],
                'Unreached' => $res[$n]['ViamenteUnreached'],
                'Error' => $res[$n]['ViamenteStartTime'],
            );
        }




        if ($this->debug)
            $this->controller->log($param2, "test_log_");
        return $params;
    }

    public function updateAppointmentGeoTagAction($post, $spid) {
        $sql = "update appointment  set Latitude=:Latitude, Longitude=:Longitude where AppointmentID=:AppointmentID and ServiceProviderID=:ServiceProviderID";
        $params = array(
            'Latitude' => $post['lat'],
            'Longitude' => $post['lng'],
            'AppointmentID' => $post['appid'],
            'ServiceProviderID' => $spid
        );
        if ($this->debug)
            $this->controller->log($params);
        $res = $this->execute($this->conn, $sql, $params);
        if ($this->debug)
            $this->controller->log($res);
    }

    public function getPrevAppGeoTag($sbJob, $spID) {
        $sql = "select Latitude,Longitude from appointment a
left join job j on j.ServiceCentreJobNo=$sbJob
left join non_skyline_job nsj on nsj.ServiceProviderJobNo=$sbJob 
where a.ServiceProviderID=$spID and (a.JobID=j.JobID or a.NonSkylineJobID=nsj.NonSkylineJobID) and a.Latitude is not null order by a.AppointmentID desc limit 1";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    //used to update skillset via ajax post in app table
    public function updateSkillset($post) {

        $sql = "update appointment set ServiceProviderSkillsetID=:ServiceProviderSkillsetID,AppointmentType=:AppointmentType where AppointmentID=:AppointmentID";
        $skillid = strip_tags($post['value']);
        $skillid = explode('^-^', $skillid);
        $skillid = $skillid[1];
        $aptype = $this->getApType($skillid);

        $params = array('AppointmentID' => $post['row_id'], 'ServiceProviderSkillsetID' => $skillid, 'AppointmentType' => $aptype['Type']);
        $this->execute($this->conn, $sql, $params);
    }

    public function saveSPGeoCell($post, $spid) {
        // echo $post['saveData_2013-01-28'];
        $eid = $post['eid'];
        foreach ($post as $k => $v) {

            if (!is_array($post[$k])) {
                $data[str_replace('saveData_', '', $k)] = explode(';', $post[$k]);
            }
        }
        foreach ($data as $j => $l) {
            if (in_array($j, $post['daysSeleceted'])) {
                $daID = $this->getDiaryAllocationIDForce($j, $eid); {
                    if (!$daID) {
                        $this->createDiaryAllocation($this->controller->user->ServiceProviderID, $j, $eid);
                    } elseif (sizeof($daID) < 3 && sizeof($daID) > 0) {

                        $this->createDiaryAllocation($this->controller->user->ServiceProviderID, $j, $eid, true);
                    }
                    $daID = $this->getDiaryAllocationIDForce($j, $eid);
                }
                if (is_array($daID)) {
                    foreach ($daID as $did) {
                        $sql = "
          delete from sp_geo_cells where ServiceProviderID=:ServiceProviderID and DiaryAllocationID=:DiaryAllocationID
            ";
                        $params = array(
                            'ServiceProviderID' => $spid,
                            'DiaryAllocationID' => $did['DiaryAllocationID']);
                        $this->execute($this->conn, $sql, $params);
                        foreach ($l as $f) {
                            $da = explode(',', $f);

                            if (sizeof($da) > 1) {

                                $sql = "insert into sp_geo_cells(ServiceProviderID,DiaryAllocationID,Lat1,Lat2,Lng1,Lng2) 
        values (:ServiceProviderID,:DiaryAllocationID,:Lat1,:Lat2,:Lng1,:Lng2)    
        ";
                                $params = array(
                                    'ServiceProviderID' => $spid,
                                    'DiaryAllocationID' => $did['DiaryAllocationID'],
                                    'Lat1' => $da[0],
                                    'Lat2' => $da[1],
                                    'Lng1' => $da[2],
                                    'Lng2' => $da[3]
                                );
                                $this->execute($this->conn, $sql, $params);
                            }
                        }
                    }
                }
            }
        }
    }

    public function loadGeoCell($did) {
        $sql = "select * from sp_geo_cells where DiaryAllocationID=$did";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    //
    public function getSlotsForSamsung($spID, $skillSetID, $postCode, $days = 7, $adress = false) {
        //echo "$skillSetID<--";
        //echo "$skillSetID<--";

        $this->controller->log("ServiceP=" . $spID . " skillsetid=" . $skillSetID . " postcode" . $postCode, "samsung_one_receive_");
        if ($spID == null || $skillSetID == null ) {

            return false;
        }


        $bankHolidays = $this->getBankHolidays($spID, true);
        $bha = array();
        foreach ($bankHolidays as $a) {
            if ($a['Status'] != "Open") {
                $bha[].=$a['hDate'];
            }
        }


        $geofilter = "";
        $engfilter = "";
        $filters = "";
        $postcodeSQL = "";
        $geofilter = "";
        $geotag = false;
        $postCode2 = $postCode;
        if (strlen($postCode) > 4) {
            $postCode2 = trim(substr(trim($postCode), 0, -3));
            if (!is_numeric(substr($postCode2, -1))) {
                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
            }
        } else {
            if (!is_numeric(substr($postCode2, -1))) {
                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
            }
        }
        $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

        $postCodeOutCode = $postCode2;             /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */
        if (!is_numeric(substr($postCodeOutCode, -1))) {
            $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
        }
        $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); /* Get the Post code Area (eg CV) */


        $spID = $this->checkAllocationReassignment($spID, $postCodeOutCode, $postCodeArea, $postCode, $postCodeAlpha);
        $sql = "select ServiceProviderSkillsetID from service_provider_skillset sps where sps.SkillsetID=$skillSetID and sps.ServiceProviderID=$spID ";
        $res = $this->Query($this->conn, $sql);

        if (isset($res[0])) {
            $skillSetID = $res[0]['ServiceProviderSkillsetID'];
            $sql="select DiaryAllocationType from service_provider where ServiceProviderID=$spID";
            $rrr=$this->Query($this->conn, $sql);
            
            if ($rrr[0]['DiaryAllocationType']=="GridMapping") {
                $geotag = $this->getGeoTagFromAddress($adress." ".$postCode, $spID);
            }


            if (!$geotag) {



                $postcodeSQL = "AND 
                          (
                            dpa.`PostCode` = '$postCode'
                            OR dpa.`PostCode` = '$postCodeAlpha'
                            OR dpa.`PostCode` = '$postCodeOutCode'
                            OR dpa.`PostCode` = '$postCodeArea'
                          )";
                $allocationTable = "join diary_postcode_allocation dpa on dpa.DiaryAllocationID=da.DiaryAllocationID";
            } else {
                $lat = $geotag['lat'];
                $lng = $geotag['lng'];
                $allocationTable = "join sp_geo_cells sgc on sgc.DiaryAllocationID=da.DiaryAllocationID";
                $geofilter = "and sgc.Lat1>=$lat and sgc.Lat2<=$lat and sgc.Lng1<=$lng and sgc.Lng2>=$lng";
            }
            $useFullDiary = $this->getDiaryType($spID);

            if ($useFullDiary == "FullViamente") {
                $this->calculateSlots($spID, $skillSetID, $postCode, $days, false, false, true);
            }



            ///will couse wrong slot numbers if same dpa.Postcode assigned more than once
            $finalized = $this->getFinalizedDays($spID, $this->getRepairTypeIDFromSPSkillset($skillSetID));
            $fd = "";
            foreach ($finalized as $e) {
                $fd.="'$e',";
            }
            $fd.="'0'";
            $res3 = array();
            for ($i = 1; $i < 7; $i++) {
                if ($useFullDiary == "FullViamente") {
                    $sql = " select da.DiaryAllocationID,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d') as `AppointmentDate` ,DATE_FORMAT(da.`AllocatedDate`, '%d') as dayOnly,
       
         `SlotsLeft`,
       
         aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo`

from diary_allocation da 
left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
$allocationTable
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 

join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
where da.ServiceProviderID=:ServiceProviderID
 $postcodeSQL $geofilter
and da.AllocatedDate=DATE_ADD(curdate(), INTERVAL $i DAY) 

and da.AllocatedDate not in ($fd)
and spe.Status='Active'
  $filters
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
        and aas.Type='AM'
   order by `AppointmentDate` asc, `TimeFrom`, `SlotsLeft` desc limit 1

          ";
                    $sql2 = "
            select da.DiaryAllocationID,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d') as `AppointmentDate` ,DATE_FORMAT(da.`AllocatedDate`, '%d') as dayOnly,
       
         `SlotsLeft`,
       
         aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo`

from diary_allocation da 
left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
$allocationTable
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 

join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
where da.ServiceProviderID=:ServiceProviderID
 $postcodeSQL 
and da.AllocatedDate=DATE_ADD(curdate(), INTERVAL $i DAY) 

and da.AllocatedDate not in ($fd)
and spe.Status='Active'
  $filters $geofilter
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
        and aas.Type='PM'
   
order by `AppointmentDate` asc, `TimeFrom`, `SlotsLeft` desc limit 1
            ";
                    $sql3 = "
            select da.DiaryAllocationID,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d') as `AppointmentDate` 
            ,DATE_FORMAT(da.`AllocatedDate`, '%d') as dayOnly,
       
         `SlotsLeft`,
       
         aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo`

from diary_allocation da 
left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID 
$allocationTable
join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 

join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID
join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
where da.ServiceProviderID=:ServiceProviderID
 $postcodeSQL 
and da.AllocatedDate=DATE_ADD(curdate(), INTERVAL $i DAY) 

and da.AllocatedDate not in ($fd)
and spe.Status='Active'
  $filters $geofilter
and sped.`Status`='Active'
and sped.WorkDate=da.AllocatedDate
and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
        and aas.Type='ANY'
   
order by `AppointmentDate` asc, `TimeFrom`, `SlotsLeft` desc limit 1
            ";

//          $this->controller->log($sql,"samsung_one_sql_");
//          $this->controller->log($sql2,"samsung_one_sql_");
//          $this->controller->log($sql3,"samsung_one_sql_");
                } elseif ($useFullDiary == "AllocationOnly") {
                    $sql = "
select 
da.DiaryAllocationID,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d') as `AppointmentDate` ,DATE_FORMAT(da.`AllocatedDate`, '%d') as dayOnly,
       
         (da.NumberOfAllocations-(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime=aas.Type)) as 'SlotsLeft',
       
         aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo`
 from diary_allocation da 
 left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
 join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID
  $allocationTable
   join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
	 where da.ServiceProviderID=:ServiceProviderID
	$postcodeSQL  $filters $geofilter
	  and da.AllocatedDate= DATE_ADD(curdate(), INTERVAL $i DAY) 
	  
	  and spe.Status='Active' and sped.`Status`='Active'
	   and
 sped.WorkDate=da.AllocatedDate and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
 and aas.Type!='ANY'
  group by da.DiaryAllocationID                    
";
                }
                if ($useFullDiary == "NoViamente") {

                    $sql = "select  da.DiaryAllocationID,Type,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d')  as AppointmentDate ,
DATE_FORMAT(da.AllocatedDate,'%d%m%Y') as dayOnly   ,    
         (da.NumberOfAllocations-(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime=aas.Type)) as 'SlotsLeft'
       
       ,(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime='ANY') as anyAppCount  
 ,aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo`
 from diary_allocation da 
 left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
 join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID
  $allocationTable
   join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
	 where da.ServiceProviderID=:ServiceProviderID
	$postcodeSQL  $filters $geofilter
	  and da.AllocatedDate=DATE_ADD(curdate(), INTERVAL $i DAY) 

	  and spe.Status='Active' and sped.`Status`='Active'
	   and
 sped.WorkDate=da.AllocatedDate and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
 and aas.Type='AM'
  group by da.DiaryAllocationID     order by `SlotsLeft` desc limit 1               
    
            ";
                    $sql3 = "select  da.DiaryAllocationID,Type,DATE_FORMAT(da.`AllocatedDate`, '%Y%m%d') as AppointmentDate ,
DATE_FORMAT(da.AllocatedDate,'%d%m%Y') as dayOnly   ,    
         (da.NumberOfAllocations-(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime=aas.Type )) as 'SlotsLeft'
       
       ,(select count(*) from appointment aaa where ServiceProviderID=:ServiceProviderID and aaa.AppointmentDate=da.AllocatedDate and aaa.AppointmentTime='ANY') as anyAppCount  
,aas.Type,
         time_to_sec(aas.TimeFrom) as fromSec,
         time_to_sec(aas.TimeTo) as toSec,
         DATE_FORMAT(aas.`TimeFrom`, '%H%i%s') AS `TimeFrom`,
         DATE_FORMAT(aas.`TimeTo`, '%H%i%s') AS `TimeTo` 
 from diary_allocation da 
 left join appointment ap on ap.DiaryAllocationID=da.DiaryAllocationID 
 join appointment_allocation_slot aas on aas.AppointmentAllocationSlotID=da.AppointmentAllocationSlotID
  $allocationTable
   join service_provider_engineer spe on spe.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_details sped on sped.ServiceProviderEngineerID=da.ServiceProviderEngineerID 
	join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID
	 where da.ServiceProviderID=:ServiceProviderID
	$postcodeSQL  $filters $geofilter
	  and da.AllocatedDate=DATE_ADD(curdate(), INTERVAL $i DAY) 

	  and spe.Status='Active' and sped.`Status`='Active'
	   and
 sped.WorkDate=da.AllocatedDate and spesd.ServiceProviderSkillsetID=:ServiceProviderSkillsetID
 and aas.Type='PM'
  group by da.DiaryAllocationID           order by `SlotsLeft` desc limit 1              
    
            ";
                }



                $params = array('ServiceProviderID' => $spID, 'ServiceProviderSkillsetID' => $skillSetID);
                $res = $this->Query($this->conn, $sql, $params);

                if ($useFullDiary == "NoViamente") {

                    $res2 = $this->Query($this->conn, $sql3, $params);
                    if (isset($res[0]) && isset($res2[0])) {
                        $totalSlotsLeft = $res[0]['SlotsLeft'] + $res2[0]['SlotsLeft'] - $res2[0]['anyAppCount'];
                        if ($res[0]['SlotsLeft'] > $totalSlotsLeft) {
                            $res[0]['SlotsLeft'] = $totalSlotsLeft;
                        };
                        if ($res2[0]['SlotsLeft'] > $totalSlotsLeft) {
                            $res2[0]['SlotsLeft'] = $totalSlotsLeft;
                        };
                    }
                    if (isset($res2[0])) {
                        if (in_array($res2[0]['AppointmentDate'], $bha)) {
                            $res2[0]['SlotsLeft'] = 0;
                        }
                    }
                    $res3 = array_merge($res3, $res2);
                }

                if ($useFullDiary == "FullViamente") {
                    $res2 = $this->Query($this->conn, $sql2, $params);
                    $res4 = $this->Query($this->conn, $sql3, $params);
                    if (isset($res[0]) && isset($res4[0])) {
                        if ($res[0]['SlotsLeft'] > $res4[0]['SlotsLeft']) {
                            $res[0]['SlotsLeft'] = $res4[0]['SlotsLeft'];
                        }
                    }
                    if (isset($res2[0]) && isset($res4[0])) {
                        if ($res2[0]['SlotsLeft'] > $res4[0]['SlotsLeft']) {
                            $res2[0]['SlotsLeft'] = $res4[0]['SlotsLeft'];
                        }
                    }
                    if (isset($res2[0])) {
                        if (in_array($res2[0]['AppointmentDate'], $bha)) {
                            $res2[0]['SlotsLeft'] = 0;
                        }
                    }
                    $res3 = array_merge($res3, $res2);
                }

                if ($useFullDiary != "AllocationOnly" && isset($res[0])) {
                    if (in_array($res[0]['AppointmentDate'], $bha)) {
                        $res[0]['SlotsLeft'] = 0;
                    }
                }
                $res3 = array_merge($res3, $res);
            }

            $this->controller->log($res3, "samsung_one_return_");
//echo"<pre>";
//print_r($res3);
//echo"</pre>";
//die();
            return $res3;
        } else {
            return false;
        }
    }

    public function getActiveEngineerDays($eID, $sdate, $limit = 7) {
        $sql = "select * from service_provider_engineer_details sped where ServiceProviderEngineerID=$eID and WorkDate>='$sdate' limit $limit";
        $res = $this->Query($this->conn, $sql);

        return $res;
    }

    public function getSPAppAllocationSlots($spID) {
        $sql = "select * from appointment_allocation_slot aas where aas.ServiceProviderID=$spID order by AppointmentAllocationSlotID";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function createDiaryAllocation($spID, $date, $eID, $any = false) {
        $aas = $this->getSPAppAllocationSlots($spID);
        if ($this->debug)
            $this->controller->log($aas, "as");
        if ($any) {
            if ($this->debug)
                $this->controller->log("crete any", "as");
            $any = $this->getAnyTimeSlot($spID);
            $sql = "insert into diary_allocation (ServiceProviderID,AllocatedDate,AppointmentAllocationSlotID,ServiceProviderEngineerID,SlotsLeft)
                 values
                 (:ServiceProviderID,:AllocatedDate,:AppointmentAllocationSlotID,:ServiceProviderEngineerID,:SlotsLeft)
                    ";
            $params = array(
                'ServiceProviderID' => $spID,
                'AllocatedDate' => $date,
                'AppointmentAllocationSlotID' => $any,
                'ServiceProviderEngineerID' => $eID,
                'SlotsLeft' => 6
            );
            $this->execute($this->conn, $sql, $params);
        }else {
            foreach ($aas as $a) {
                $sql = "insert into diary_allocation (ServiceProviderID,AllocatedDate,AppointmentAllocationSlotID,ServiceProviderEngineerID,SlotsLeft)
                 values
                 (:ServiceProviderID,:AllocatedDate,:AppointmentAllocationSlotID,:ServiceProviderEngineerID,:SlotsLeft)
                    ";
                $params = array(
                    'ServiceProviderID' => $spID,
                    'AllocatedDate' => $date,
                    'AppointmentAllocationSlotID' => $a['AppointmentAllocationSlotID'],
                    'ServiceProviderEngineerID' => $eID,
                    'SlotsLeft' => 6
                );
                $this->execute($this->conn, $sql, $params);
            }
            return $this->conn->lastInsertId();
        }
    }

    public function pasteGeoMapToDatabase($did, $e, $d, $spid) {
        $daID = $this->getDiaryAllocationIDForce($d, $e);

        if (!isset($daID[0])) {
            $this->createDiaryAllocation($this->controller->user->ServiceProviderID, $d, $e);
            $daID = $this->getDiaryAllocationIDForce($d, $e);
        } else {
            if (sizeof($daID) < 3) {
                if ($this->debug)
                    $this->controller->log(sizeof($daID), "aaa");
                $this->createDiaryAllocation($this->controller->user->ServiceProviderID, $d, $e, true);
            }
            $daID = $this->getDiaryAllocationIDForce($d, $e);
        }
        $data = $this->loadGeoCell($did);
        if ($this->debug)
            $this->controller->log($data, "geologdata_");
        foreach ($daID as $dd) {
            $sql = "
          delete from sp_geo_cells where ServiceProviderID=:ServiceProviderID and DiaryAllocationID=:DiaryAllocationID
            ";
            $params = array(
                'ServiceProviderID' => $spid,
                'DiaryAllocationID' => $dd['DiaryAllocationID']);
            $this->execute($this->conn, $sql, $params);
            $sql = "insert into sp_geo_cells(ServiceProviderID,DiaryAllocationID,Lat1,Lat2,Lng1,Lng2) 
        values";
            if (sizeof($data) > 0) {
                for ($i = 0; $i < sizeof($data); $i++) {
                    $n1 = $data[$i]['Lat1'];
                    $n2 = $data[$i]['Lat2'];
                    $n3 = $data[$i]['Lng1'];
                    $n4 = $data[$i]['Lng2'];
                    $sql.="($spid," . $dd['DiaryAllocationID'] . ",$n1,$n2,$n3,$n4)    
        ";
                    if ($i != sizeof($data) - 1) {
                        $sql.=",";
                    } else {
                        $sql.=";";
                    }
                    //  $this->controller->log($sql,"geologdata_");
                    //$this->controller->log($params,"geologdata_");
                }
                $this->execute($this->conn, $sql);
            }
        }
    }

    public function getSPAllocationType($spID) {
        $sql = "select DiaryAllocationType as dat from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['dat'];
    }

    public function checkPostcodeDatbase($postcode) {

        $sql = "select Latitude,Longitude from postcode_lat_long where PostCode='$postcode'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    public function getGeoTagFromAddress($add, $spid) {

        $postcode = $add;
        if (strpos($postcode, " ") === false) {
            $postcode = substr($postcode, 0, strlen($postcode) - 3) . " " . substr($postcode, strlen($postcode) - 3, 3);
        }
        $co = $this->checkPostcodeDatbase($postcode);
        if (!$co) {
            $address = str_replace(" ", "+", $add);
            // echo"$add";
            $country = $this->getCountry($spid);
            if ($country == "UK") {
                $country = "United+Kingdom";
            }
            $headers = array(
                'Accept: application/json',
                'Content-Type: application/json'
            );
            $handle = curl_init();
            //  $this->controller->log("https://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false","geotag_links");
            //  $this->controller->log($_SERVER['HTTP_USER_AGENT'],"geotag_links");
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
            $url = trim($url, '+');
            curl_setopt($handle, CURLOPT_URL, $url);

            curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($handle, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17");
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($handle, CURLOPT_TIMEOUT, 360);


            $data = curl_exec($handle);
            $info = curl_getinfo($handle, CURLINFO_HTTP_CODE);

            curl_close($handle);
            $data = json_decode($data, true);

            $data = $data['results'];

            if ($this->debug)
                $this->controller->log($data, "google_API_RES_");
            for ($i = 0; $i < sizeof($data); $i++) {
                for ($j = 0; $j < sizeof($data[$i]['address_components']); $j++) {

                    if (in_array($data[$i]['address_components'][$j]['short_name'], array("IE", "GB"))) {
                        $geotag['lat'] = $data[$i]["geometry"]["location"]["lat"];
                        $geotag['lng'] = $data[$i]["geometry"]["location"]["lng"];
                        $this->insertPostcodeGeotag($postcode, $geotag['lat'], $geotag['lng']);
                        return $geotag;
                    }
                }
            }

            return FALSE;
        } else {
            $geotag['lat'] = $co['Latitude'];
            $geotag['lng'] = $co['Longitude'];
            return $geotag;
        }
    }

    public function insertPostcodeGeotag($postcode, $lat, $lng) {
        if (strlen($postcode) <= 8) {
            $sql = "insert into postcode_lat_long (PostCode,Latitude,Longitude) values ('$postcode',$lat,$lng)";
            $res = $this->execute($this->conn, $sql);
        }
    }

    public function getCountry($spID) {
        $sql = "select Name as c from service_provider sp
       join country co on co.CountryID=sp.CountryID
       where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0]['c'];
        } else {
            if ($this->debug)
                $this->controller->log("Service provider ID=$spID, country not set geotags could be wrong needs fixing urgently", "d_geotag_error_");
            return false;
        }
    }

    public function getSpGridData($spID) {
        $sql = "select sgd.*,sp.DiaryAllocationType from service_provider sp
        left join sp_grid_defaults sgd  on sp.ServiceProviderID=sgd.ServiceProviderID
        where sp.ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {

            return;
        }
    }

    public function saveGridDefaults($post) {
        $spid = $post['spid'];
        $sql = "select * from sp_grid_defaults where ServiceProviderID=$spid";
        $res = $this->Query($this->conn, $sql);
        $params = array(
            'Lat1' => $post['lat1'],
            'Lng1' => $post['lng1'],
            'Lat2' => $post['lat2'],
            'Lng2' => $post['lng2'],
            'CellSize' => $post['csize'],
            'ServiceProviderID' => $post['spid']
        );
        if (sizeof($res) != 0) {
            $sql = "update sp_grid_defaults set Lat1=:Lat1,Lng1=:Lng1,Lat2=:Lat2,Lng2=:Lng2,CellSize=:CellSize where ServiceProviderID=:ServiceProviderID";

            $res = $this->execute($this->conn, $sql, $params);
        } else {
            $sql = "insert into  sp_grid_defaults  (Lat1,Lng1,Lat2,Lng2,CellSize,ServiceProviderID) 
    values
        (:Lat1,:Lng1,:Lat2,:Lng2,:CellSize,:ServiceProviderID)
    ";
            $res = $this->execute($this->conn, $sql, $params);
        }
    }

    public function getGridDefaults($sp) {
        $sql = "select * from sp_grid_defaults where ServiceProviderID=$sp";
        $res = $this->Query($this->conn, $sql);
        if (!isset($res[0])) {
            return false;
        }
        return $res[0];
    }

    public function checkAllocationReassignment($spID, $postCodeOutCode, $postCodeArea, $postCode, $postCodeAlpha) {
        $sql = "select * from allocation_reassignment where MainServiceProviderID=$spID and (Postcode='$postCodeOutCode' or Postcode='$postCodeArea' or Postcode='$postCode' or Postcode='$postCodeAlpha' )";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0]['SecondaryServiceProviderID'];
        } else {
            return $spID;
        }
    }

    public function getEngineerData($eID) {
        $sql = "select * from service_provider_engineer spe where ServiceProviderEngineerID=$eID";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getEngineerIDFromAPP($appID) {
        $sql = "select ServiceProviderEngineerID,
(select count(*)-(select spe.AppsBeforeOptimise from service_provider_engineer spe 
	where spe.ServiceProviderEngineerID=ap.ServiceProviderEngineerID) from appointment a where a.AppointmentDate=ap.AppointmentDate
 and a.ServiceProviderEngineerID=ap.ServiceProviderEngineerID) as ac
 from appointment ap
 where AppointmentID=$appID";
        $res = $this->Query($this->conn, $sql);
        if ($res[0]['ac'] >= 0) {

            return $res[0]['ServiceProviderEngineerID'];
        } else {
            return -1;
        }
    }

    public function getEngineerIDFromAPPOnly($appID) {
        $sql = "select ServiceProviderEngineerID

 from appointment ap
 where AppointmentID=$appID";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {

            return $res[0]['ServiceProviderEngineerID'];
        } else {
            return -1;
        }
    }

    public function procesCSVIMport($row) {
        $sql = "select UnitTypeID from unit_type where UnitTypeName=:UnitTypeName";
        $params = array('UnitTypeName' => $row[1]);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0]['UnitTypeID'])) {
            $typeID = $res[0]['UnitTypeID'];
        } else {
            $this->controller->log($row, "Not_imported_model_");
            return false;
        }
        $sql = "select * from model where ModelNumber=:ModelNumber and ManufacturerID=106";
        $params = array('ModelNumber' => $row[0]);
        $res = $this->Query($this->conn, $sql, $params);
        if (sizeof($res) == 0) {
            $sql = "insert into model (ManufacturerID,UnitTypeID,ModelNumber,CreatedDate,Status,ModifiedUserID) values (106,:UnitTypeID,:ModelNumber,curdate(),'Active',2)";
            $params = array('UnitTypeID' => $typeID, 'ModelNumber' => $row[0]);
            $res = $this->execute($this->conn, $sql, $params);
            $this->controller->log($row, "imported_model_(insert)___");
        } else {
            $sql = "update model set UnitTypeID=:UnitTypeID where ModelNumber=:ModelNumber and ManufacturerID=106";
            $params = array('UnitTypeID' => $typeID, 'ModelNumber' => $row[0]);
            $res = $this->execute($this->conn, $sql, $params);
            $this->controller->log($row, "imported_model_(update)___");
        }
    }

    //all diary users exept "allocation only"
    public function getAllSPList() {
        $sql = "select ServiceProviderID,CompanyName from service_provider sp where sp.OnlineDiary='Active' and DiaryType!='AllocationOnly'";

        return $this->Query($this->conn, $sql);
    }

    //converting engineer service base code to skyline id
    public function getEngIdFromSBCode($c, $spid) {
        $sql = "select ServiceProviderEngineerID from service_provider_engineer where ServiceBaseUserCode='$c' and ServiceProviderID=$spid";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0]['ServiceProviderEngineerID'];
        } else {
            return false;
        }
    }

    //converting engineer service base code to skyline Engineer name
    public function getEngNameFromSBCode($c, $spid) {
        $sql = "select concat(EngineerFirstName,' ',EngineerLastName) as engName from service_provider_engineer where ServiceBaseUserCode='$c' and ServiceProviderID=$spid";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0]['engName'];
        } else {
            return false;
        }
    }

    public function addAppBreak($apid, $breakTimeSec, $durationSec) {
        if ($apid != "destination") {
            $sql = "update appointment set BreakStartSec=:BreakStartSec,BreakDurationSec=:BreakDurationSec where AppointmentID=$apid";
            $params = array('BreakStartSec' => $breakTimeSec, 'BreakDurationSec' => $durationSec);
            $res = $this->execute($this->conn, $sql, $params);
        }
    }

    public function removeBreaks($d, $sid, $eid = false) {

        //$engsql="";
        if ($d != "destination") {
            if ($eid) {
                $eidr = " and ServiceProviderEngineerID=$eid";
            } else {
                $eidr = "";
            }
            $sql = "update appointment set BreakStartSec=null,BreakDurationSec=null where ServiceProviderID=$sid and AppointmentDate=:AppointmentDate $eidr";
            $params = array('AppointmentDate' => $d);
            $res = $this->execute($this->conn, $sql, $params);
        }
    }

    /**
     * isSlotForToday
     * 
     * Check whether a given diary allocation slot is assigned to a given date
     *
     * @param   integer $daId   The Diary Allocation ID
     *          date $ad        The date to check
     *
     * @return      Boolean containing whether slot matches the date
     *
     * @author Andrew Williams <a.williams@pccsuk.com> 
     * ************************************************************************ */
    public function isSlotForToday($daId, $ad) {
        $sql = "
                SELECT
            *
        FROM
                `diary_allocation`
        WHERE
            `DiaryAllocationID` = $daId
            AND `AllocatedDate` = '$ad'
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) == 0) {
            return(false);                                                      /* No rows - return false */
        } else {
            return(true);                                                       /* Match- return true */
        }
    }

    public function checkDayErrors($spID, $date) {
        $ret = $this->getUnreachable($spID, $date);
        if (sizeof($ret) > 0) {

            return true;
        }
        $ret = $this->getErrorApp($spID, 0, $date);
        if ($ret) {


            return true;
        }
        return false;
    }

    //this will be used to proces all service provider active engineers every day
    // Â© Andris Polnikovs <a.polnikovs@gmail.com>
    //version->1=>2013-02-24
    public function processDailyEngineers() {

        $splist = $this->getAllSPList(); //all sp with online diary active;
        for ($i = 0; $i < sizeof($splist); $i++) {
            $this->controller->user->ServiceProviderID = $splist[$i]['ServiceProviderID'];
            $apiV = $this->controller->loadModel('APIViamente');
            $sql = "select spe.ServiceProviderEngineerID from service_provider_engineer 
spe where spe.ServiceProviderID=:ServiceProviderID 
and spe.ViamenteExclude='No' 
and spe.`Status`='Active'";
            $params = array('ServiceProviderID' => $splist[$i]['ServiceProviderID']);
            $r = $this->Query($this->conn, $sql, $params);

            for ($u = 0; $u < sizeof($r); $u++) {
                if ($this->debug)
                    $this->controller->log("Procesing Service ProviderID=" . $splist[$i]['ServiceProviderID'] . " EngineerID=" . $r[$u]['ServiceProviderEngineerID'], "cron_process_engineer__");
                $ret = $apiV->ProcessEngineer($r[$u]['ServiceProviderEngineerID'], 'update');
                if ($this->debug)
                    $this->controller->log($ret, "cron_process_engineer__");
                if ($this->debug)
                    $this->controller->log("-------------------------------------END---------------------------------------", "cron_process_engineer__");
            }
        }
    }

    /**
     * getAppointmentTimeFromDiaryAllocation
     *  
     * Get an appointment time period (Eg Any, AM , PM) based o a DiaryAllocationID
     * 
     * @param   integer $daId   The Diary Allocation ID
     * 
     * @return  string     The type of the appointmjent allocation
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function getAppointmentTimeFromDiaryAllocation($daId) {
        $sql = "
                SELECT
			aas.`Type`
		FROM
			`appointment_allocation_slot` aas,
		        `diary_allocation` da
		WHERE
			da.`AppointmentAllocationSlotID` = aas.`AppointmentAllocationSlotID`
			AND da.`DiaryAllocationID` = $daId
               ";

        $result = $this->Query($this->conn, $sql);

        if (count($result) == 0) {
            return('');                                                         /* No rows - return empty string */
        } else {
            return($result[0]['Type']);                                         /* Match - return text */
        }
    }

    public function getDiaryType($spID) {
        $sql = "select DiaryType as dat from service_provider where ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['dat'];
    }

    public function tableUpdateSortOrder($post) {
        if ($this->debug)
            $this->controller->log($post, "aca");

        $sql = "update appointment set SortOrder=:SortOrder where AppointmentID=:AppointmentID";
        $params = array('AppointmentID' => $post['row_id'], 'SortOrder' => $post['value']);
        $this->execute($this->conn, $sql, $params);

        return;
    }

    public function getallocationOnlyTableData($spID) {

        $sql = "select distinct 
ap.AppointmentID,
        ap.*,
       
        
        concat_ws(' ',ct.Title,c.ContactLastName ) as custName,
        if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,  c.PostalCode, j.ColAddPostcode)  as PostalCode,
        
        
  j.JobID,
        ss.SkillsetName,
        cl.ClientName as `Client`,
        
      j.ServiceBaseManufacturer,
      
        j.ServiceBaseModel,
        if(j.ModelID is null,j.ServiceBaseUnitType,ut.UnitTypeName) as ServiceBaseUnitType,
        
         if (j.ColAddPostcode='0' or j.ColAddPostcode=' ' or j.ColAddPostcode is null,
        concat_ws(' ',c.BuildingNameNumber,c.Street,c.TownCity),
        concat_ws(' ',j.ColAddBuildingNameNumber, j.ColAddStreet,j.ColAddLocalArea,j.ColAddTownCity))
        as fulladress,
         
        j.ServiceCentreJobNo as sbJobNo,
        
        
         j.NetworkRefNo   
            
            
         from appointment ap
         join job j on j.JobID=ap.JobID 
        left  join customer c  on c.CustomerID=j.CustomerID
      left  join customer_title ct on ct.CustomerTitleID=c.CustomerTitleID
        left join manufacturer m on m.ManufacturerID=j.ManufacturerID
       left  join model mm on j.ModelID=mm.ModelID
       left  join unit_type ut  on ut.UnitTypeID=mm.UnitTypeID
        left join client cl on cl.ClientID=j.ClientID
        join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        where 
        ap.ServiceProviderID=$spID 
        and AppointmentDate>=curdate()
         and aas.ServiceProviderID=$spID
          
         
       union 
       select 
        distinct 
		  ap.AppointmentID,
        ap.*,
        
        
        
        concat_ws(' ',nsj.CustomerTitle,nsj.CustomerSurname ) as custName,
        
        nsj.Postcode as PostalCode,
        
        
          nsj.NonSkylineJobID as JobID,
          ss.SkillsetName,
        nsj.`Client`,
        nsj.Manufacturer as ServiceBaseManufacturer,
        nsj.ModelNumber  as ServiceBaseModel,
        nsj.ProductType as ServiceBaseUnitType,
		  concat(nsj.CustomerAddress1,' ',nsj.CustomerAddress2,' ',nsj.CustomerAddress3,' ',nsj.CustomerAddress4) as fulladress,
		  
		 nsj.ServiceProviderJobNo as sbJobNo,
        nsj.NetworkRefNo
        
        from appointment ap
         
         join appointment_allocation_slot aas on aas.Description=ap.AppointmentTime
        join non_skyline_job nsj on nsj.NonSkylineJobID=ap.NonSkylineJobID
         join service_provider_skillset sps on sps.ServiceProviderSkillsetID=ap.ServiceProviderSkillsetID
        join skillset ss on ss.SkillsetID=sps.SkillsetID
        where 
         
        
         ap.ServiceProviderID=$spID 
        and AppointmentDate>=curdate()
         and aas.ServiceProviderID=$spID
      
       
          
         order by `CreatedTimeStamp` asc";

        $res = $this->Query($this->conn, $sql);

//    echo"<pre>";
//    print_r($res);
//    echo"</pre>";
        return $res;
    }

    public function markAsConfirmed($apis) {


        for ($i = 0; $i < sizeof($apis); $i++) {

            $sql = "update appointment set ConfirmedByUser='Confirmed' where AppointmentID in ($apis)";
            $res = $this->execute($this->conn, $sql);
        }
    }

    public function setEngineerLocation($eid, $location) {
        $sql = "select if(DATE_FORMAT(DateModified,'%d')=date_format(curdate(),'%d'),'no','yes')as newDay from sp_engineer_day_location where ServiceProviderEngineerID=$eid";
        $res = $this->query($this->conn, $sql);
        if (sizeof($res) > 0) {
            if ($res[0]['newDay'] == 'yes') {
                $sql = "update sp_engineer_day_location set Langitude=:Langitude,Longitude=:Longitude,LunchTaken='No' where ServiceProviderEngineerID=:ServiceProviderEngineerID";
            }
            if ($res[0]['newDay'] == 'no') {
                $sql = "update sp_engineer_day_location set Langitude=:Langitude,Longitude=:Longitude where ServiceProviderEngineerID=:ServiceProviderEngineerID";
            }
        } else {
            $sql = "insert into sp_engineer_day_location (Langitude,Longitude,ServiceProviderEngineerID) values (:Langitude,:Longitude,:ServiceProviderEngineerID)";
        }
        $location = explode(',', $location);
        $params = array('Langitude' => $location[1], 'Longitude' => $location[0], 'ServiceProviderEngineerID' => $eid);
        $this->execute($this->conn, $sql, $params);

        $apiV = $this->controller->loadModel('APIViamente');
        $apiV->ProcessEngineer($eid, 'update', true);
    }

    //cron function only do not use for any other task!!!
    public function processViamente($args) {
        $this->controller->log("cron preces v start", "cron_proces_viamente_______");
        $sql = "select sp.ServiceProviderID from service_provider sp where sp.ViamenteRunType='CurrentEngineer'";
        $res = $this->Query($this->conn, $sql);
        for ($i = 0; $i < sizeof($res); $i++) {

            $this->controller->user->ServiceProviderID = $res[$i]['ServiceProviderID'];
            $apiV = $this->controller->loadModel('APIViamente');
            for ($u = 1; $u < 6; $u++) {//runs for every week day 
                $date = date('Y-m-d', strtotime("+$u day"));
                $this->controller->log("running for " . $date, "cron_proces_viamente_______");
                if ($args['mode'] == "Brown") {
                    $eIds = $this->getAssignedEngineers($date, null, $this->controller->user->ServiceProviderID, "Brown");
                    if (sizeof($eIds) > 0 && !$this->checkFinalized($this->controller->user->ServiceProviderID, $date, "Brown")) {
                        $ret = $apiV->OptimiseRoute($this->controller->user->ServiceProviderID, $date, $eIds, $args['mode']);
                        $this->controller->log($ret, "cron_proces_viamente_______");
                        if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                            $this->setUnreached($ret->unreachedWaypointNames);
                        }
                        if (isset($ret->routes)) {


                            $this->updateEngineerWorkload($ret->routes, $date, $this->controller->user->ServiceProviderID);
                        }
                    }
                }
                if ($args['mode'] == "White") {
                    $eIds = $this->getAssignedEngineers($date, null, $this->controller->user->ServiceProviderID, "White");
                    if (sizeof($eIds) > 0 && !$this->checkFinalized($this->controller->user->ServiceProviderID, $date, "White")) {
                        $ret = $apiV->OptimiseRoute($this->controller->user->ServiceProviderID, $date, $eIds, $args['mode']);
                        $this->controller->log($ret, "cron_proces_viamente_______");
                        if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                            $this->setUnreached($ret->unreachedWaypointNames);
                        }
                        if (isset($ret->routes)) {


                            $this->updateEngineerWorkload($ret->routes, $date, $this->controller->user->ServiceProviderID);
                        }
                    }
                }
            }
        }
    }

    public function setUserModifiedGeoTag($appid, $mode) {
        switch ($mode) {
            case 'set': $val = 'Yes';
                break;
            case 'unset': $val = 'No';
                break;
        }
        $sql = "update appointment set GeoTagModiefiedByUser='$val' where AppointmentID=$appid";
        $this->execute($this->conn, $sql);
    }

    public function getBankHolidays($spid,$cId = '') {
        if($cId != "")
            $sql = "select *,nbh.NationalBankHolidayID,date_format(HolidayDate,'%Y%m%d') as hDate from national_bank_holiday nbh left join service_provider_bank_holidays spbh on spbh.NationalBankHolidayID=nbh.NationalBankHolidayID and spbh.ServiceProviderID=$spid where nbh.CountryID='".$cId."' AND (nbh.ServiceProviderId = '0' OR nbh.ServiceProviderId='".$spid."') order by nbh.HolidayDate asc ";
        else
            $sql = "select *,nbh.NationalBankHolidayID,date_format(HolidayDate,'%Y%m%d') as hDate from national_bank_holiday nbh left join service_provider_bank_holidays spbh on spbh.NationalBankHolidayID=nbh.NationalBankHolidayID and spbh.ServiceProviderID=$spid where (nbh.ServiceProviderId = '0' OR nbh.ServiceProviderId='".$spid."') order by nbh.HolidayDate asc ";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function setBankHolidayStatus($spid, $bid, $status) {
        $sql = "select * from service_provider_bank_holidays where ServiceProviderID=$spid and NationalBankHolidayID=$bid";
        $ret = $this->Query($this->conn, $sql);
        if (sizeof($ret) > 0) {
            $sql = "update service_provider_bank_holidays set Status='$status' where ServiceProviderID=$spid and NationalBankHolidayID=$bid";
        } else {
            $sql = "insert into service_provider_bank_holidays (Status,ServiceProviderID,NationalBankHolidayID)
            values
            ('$status',$spid,$bid)
            ";
        }
        $res = $this->execute($this->conn, $sql);
    }

    //This function cheks if service provider is using AutoSpecifyEngineerByPostcode, if yes returns engineer id
    //(with less amount of appointments if multiple engineers match all criteria), else return false;
    //Andris Polnikovs
    public function getOneTouchForcedEngineers($TimeSlot, $SkillsetID, $Postcode, $SpID, $date) {

        $use = $this->getAllServiceProviderInfo($SpID);
        if ($use['AutoSpecifyEngineerByPostcode'] == 'Yes') {
            $spSkillset = $this->getServiceProviderSkillsetFromSkillset($SpID, $SkillsetID);
            if ($spSkillset) {
                $ret = $this->getDiaryAllocationID($TimeSlot, $spSkillset, $Postcode, $SpID, $date, false, true, false, true);

                if (sizeof($ret) > 0) {
                    return $ret[0]['ServiceProviderEngineerID'];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    //converts SkillsetID to ServiceProviderSkillsetID, if not set retruns null
    //Andris Polnikovs
    public function getServiceProviderSkillsetFromSkillset($spid, $skillsetID) {
        $sql = "select ServiceProviderSkillsetID from service_provider_skillset sps where sps.SkillsetID=$skillsetID and sps.ServiceProviderID=$spid ";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            $skillSetID = $res[0]['ServiceProviderSkillsetID'];
        } else {
            $skillSetID = null;
        }
        return $skillSetID;
    }

    public function getSpAcronym($spid) {
        $sql = "select Acronym from service_provider sp where sp.ServiceProviderID=$spid ";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            $Acronym = $res[0]['Acronym'];
        } else {
            $Acronym = null;
        }
        return $Acronym;
    }

    //faq
    public function getFaqCategoryList() {
        $sql = "select * from diary_faq_category";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function addFaqCategory($name) {
        $sql = "insert into diary_faq_category (CategoryName) values ('$name')";
        $res = $this->execute($this->conn, $sql);
        return $this->conn->lastInsertId();
    }

    public function insertFaq($p) {

        $sql = "insert into diary_faq (DiaryFaqCategoryID,FaqQuestion,FaqAnswer) values (:DiaryFaqCategoryID,:FaqQuestion,:FaqAnswer)";
        $params = array(
            'DiaryFaqCategoryID' => $p['hEng'],
            'FaqQuestion' => $p['faqQuestion'],
            'FaqAnswer' => $p['faqAnswer'],
        );
        $res = $this->execute($this->conn, $sql, $params);
    }

    public function getAllFaq($cat = false) {
        if ($cat) {
            $sqland = "and dfc.DiaryFaqCategoryID=$cat";
        } else {
            $sqland = "";
        }
        $sql = "select * from diary_faq df
        join diary_faq_category dfc on dfc.DiaryFaqCategoryID=df.DiaryFaqCategoryID    
        where Status='Active' $sqland";
        $res = $this->Query($this->conn, $sql);
        if ($this->debug)
            $this->controller->log($res, "aa");
        return $res;
    }

    public function getFaqEdit($eid) {
        $sql = "select * from diary_faq df 
          join diary_faq_category dfc on dfc.DiaryFaqCategoryID=df.DiaryFaqCategoryID    
        where DiaryFaqID=$eid";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        }
    }

    public function updateFaq($post) {

        $sql = "update diary_faq set DiaryFaqCategoryID=:DiaryFaqCategoryID,FaqQuestion=:FaqQuestion,FaqAnswer=:FaqAnswer where DiaryFaqID=:DiaryFaqID";
        $params = array(
            'DiaryFaqCategoryID' => $post['hEng'],
            'FaqQuestion' => $post['faqQuestion'],
            'FaqAnswer' => $post['faqAnswer'],
            'DiaryFaqID' => $post['id'],
        );
        $res = $this->execute($this->conn, $sql, $params);
    }

    //faq end
    //cron job allocation only emails 
    public function allocationOnlyEmails() {
        $emailModel = $this->controller->loadModel('Email');
        $sql = '
        select * from appointment a 
join service_provider sp on sp.ServiceProviderID=a.ServiceProviderID
where a.BookedBy="Samsung One Touch" and sp.DiaryType="AllocationOnly" and a.ConfirmedByUser="Not Confirmed"
        ';
        $res = $this->Query($this->conn, $sql);
        foreach ($res as $r) {
            $CCEmails = "a.polnikovs@pccsuk.com;r.perry@pccsuk.com;j.berry@pccsuk.com;c.berry@pccsuk.com;n.wrighting@pccsuk.com;n.kanteti@pccsuk.com;";
            $emailModel->sendAppointmentEmail($r['AppointmentID'], 'Booked', $r['AdminSupervisorEmail'], false, false);
            $emailModel->sendAppointmentEmail($r['AppointmentID'], 'Booked', $r['DiaryAdministratorEmail'], false, $CCEmails);
            $this->controller->log("bccemails=$CCEmails", "emails_sent_log_cron_____");
            $this->controller->log("app id=" . $r['AppointmentID'], "emails_sent_log_cron_____");
            $this->controller->log("adminemail =" . $r['AdminSupervisorEmail'], "emails_sent_log_cron_____");
            $this->controller->log("Diaryadminemail =" . $r['DiaryAdministratorEmail'], "emails_sent_log_cron_____");
        }
    }

    //cron job allocation only emails end

    public function checkConfirmed($en) {
        $sql = "select ConfirmedByUser from appointment where AppointmentID=:AppointmentID";
        $params = array('AppointmentID' => $en);
        $res = $this->Query($this->conn, $sql, $params);
        if (isset($res[0]) && $res[0]['ConfirmedByUser'] == "Not Confirmed") {
            $sql = "update appointment set ConfirmedByUser='Confirmed' where  AppointmentID=:AppointmentID ";
            $res = $this->execute($this->conn, $sql, $params);
            return 'updated';
        } else {
            if (isset($res[0])) {
                return 'notupdated';
            } else {
                return 'notExist';
            }
        }
    }

    public function LockAppWindow($spID, $date = false) {

        if (!$date) {
            $date = date('Y-m-d', strtotime("+0 day"));
        }



        $sql = "update appointment a set
         a.Notes=IF(isnull(AppointmentStartTime2), concat_ws(' ',a.Notes,'Customer requested appointment to be between',
date_format(a.AppointmentStartTime,'%H:%i'),'-',date_format(a.AppointmentEndTime,'%H:%i')), 
concat_ws(' ',a.Notes,'Customer requested appointment to be between',
date_format(a.AppointmentStartTime,'%H:%i'),'-',date_format(a.AppointmentEndTime,'%H:%i'),'&',
date_format(a.AppointmentStartTime2,'%H:%i'),'-',date_format(a.AppointmentEndTime2,'%H:%i'))),
 a.AppointmentStartTime=
 
  DATE_SUB(date_sub(a.ViamenteDepartTime,interval a.Duration Minute), INTERVAL (select LockAppWindowTime from service_provider sp where sp.ServiceProviderID=$spID ) Minute)
 ,a.AppointmentEndTime=
  DATE_ADD(date_sub(a.ViamenteDepartTime,interval a.Duration Minute),  INTERVAL ((select LockAppWindowTime from service_provider sp where sp.ServiceProviderID=$spID )+a.Duration) Minute)
, a.AppointmentStartTime2=null,a.AppointmentEndTime2=null


where
a.ServiceProviderID=$spID
and a.ViamenteStartTime is not null
and  a.ViamenteStartTime!=''
 and a.AppointmentDate='$date'
";

        $this->Execute($this->conn, $sql);
        //removing old rubish
        $sql = "select Notes,AppointmentID from appointment where ServiceProviderID=$spID and AppointmentDate='$date'";
        $res = $this->Query($this->conn, $sql);
        foreach ($res as $u) {
            $cc = 0;
            $id = $u['AppointmentID'];
            $notes = preg_replace('/Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d/', '', $u['Notes'], -1, $cc);
            if ($cc > 1) {
                $notes = preg_replace('/Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d(?!.*Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d)/', '', $u['Notes'], -1);
                $sql = "Update appointment set Notes='$notes' where AppointmentID=$id";
                $this->Execute($this->conn, $sql);
            }
            $cc = 0;
            $notes = $notes = preg_replace('/Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d & \d\d:\d\d - \d\d:\d\d(?!.*Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d & \d\d:\d\d - \d\d:\d\d)/', '', $u['Notes'], -1, $cc);
            if ($cc > 1) {
                $notes = preg_replace('/Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d & \d\d:\d\d - \d\d:\d\d(?!.*Customer requested appointment to be between \d\d:\d\d - \d\d:\d\d & \d\d:\d\d - \d\d:\d\d)/', '', $u['Notes'], -1);
                $sql = "Update appointment set Notes='$notes' where AppointmentID=$id";
                $this->Execute($this->conn, $sql);
            }
        }
    }

    public function getTimeDefRow($spID) {
        $sql = "select * from deferred_time_slot dts where dts.ServiceProviderID=$spID";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getTimeDefPostcodeRow($DeferredTimeSlotID) {
        $sql = "select * from deferred_postcode dp where dp.DeferredTimeSlotID=$DeferredTimeSlotID";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function saveDeferredTime($spID, $EarliestWorkday, $LatestWorkday, $DeffPostcodeWork, $EarliestWeekend, $LatestWeekend, $DeffPostcodeWeekEnd) {

        $wop = explode(',', $DeffPostcodeWork);
        $wep = explode(',', $DeffPostcodeWeekEnd);
        if ($this->debug)
            $this->controller->log($wop, "wop");
        if ((sizeof($wop) > 0 || sizeof($wep > 0)) && ($wop[0] != "" || $wep[0] != "")) {
            $sql = "insert into deferred_time_slot (ServiceProviderID,EarliestWorkday,LatestWorkday,EarliestWeekend,LatestWeekend)
        values 
            ($spID,'$EarliestWorkday','$LatestWorkday','$EarliestWeekend','$LatestWeekend')
        ";
            $this->Execute($this->conn, $sql);
            $id = $this->conn->lastInsertId();


            foreach ($wop as $w) {
                if ($w != "") {
                    $sql = "insert into deferred_postcode (Postcode,ServiceProviderID,DpType,DeferredTimeSlotID)
            values
            ('$w',$spID,'Working',$id)
            ";
                    $this->Execute($this->conn, $sql);
                }
            }
            foreach ($wep as $w) {
                if ($w != "") {
                    $sql = "insert into deferred_postcode (Postcode,ServiceProviderID,DpType,DeferredTimeSlotID)
            values
            ('$w',$spID,'WeekEnd',$id)
            ";
                    $this->Execute($this->conn, $sql);
                }
            }
        }
    }

    public function deleteDeferedTime($spID) {
        $sql = "delete from deferred_time_slot where ServiceProviderID =$spID;";
        $this->Execute($this->conn, $sql);
        $sql = "delete from deferred_postcode where ServiceProviderID =$spID";
        $this->Execute($this->conn, $sql);
    }

    public function addDeferredTime($spid) {
        $sql = "insert into deferred_time_slot (ServiceProviderID) values ($spid)";
        $this->Execute($this->conn, $sql);
    }

    public function checkDeferredTimeSlot($spID, $date, $pc) {
        $pca = preg_replace('/[^\\/\-a-z\s]/i', '', $pc);
        $day = date('w', strtotime($date));
        if ($day == 0 || $day == 6) {
            $dtType = "WeekEnd";
        } else {
            $dtType = "Working";
        }
        $sql = "select * from deferred_postcode dp
        join deferred_time_slot  dts on dts.DeferredTimeSlotID=dp.DeferredTimeSlotID
        where dp.ServiceProviderID=$spID and DpType='$dtType' and (Postcode='$pc' or Postcode='$pca') limit 1";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    //this method is used to remove finalized day for visual fx only
    //arg=date
    //arg=mode
    //arg=service provider id
    public function removeVisualEndDay($d, $m, $s) {
        $dd = explode("/", $d);
        $date = $dd[2] . "-" . $dd[1] . "-" . $dd[0];

        $sql = "delete from viamente_end_day where ServiceProviderID=$s and RouteDate='$date' and EndDayType='$m'";
        $this->Execute($this->conn, $sql);
        $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) values (:ServiceProviderID,:CreatedUserID,:RouteDate,'$m',now(),'Reset Viamente')";
        $historyParams = array('ServiceProviderID' => $s, 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $date );
        $this->execute($this->conn, $historySql, $historyParams);
        if ($m == 'Both') {
            $sql = "delete from viamente_end_day where ServiceProviderID=$s and RouteDate='$date' and EndDayType='White'";
            $this->Execute($this->conn, $sql);
            $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) values (:ServiceProviderID,:CreatedUserID,:RouteDate,'White',now(),'Reset Viamente')";
            $historyParams = array('ServiceProviderID' => $s, 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $date );
            $this->execute($this->conn, $historySql, $historyParams);
            $sql = "delete from viamente_end_day where ServiceProviderID=$s and RouteDate='$date' and EndDayType='Brown'";
            $this->Execute($this->conn, $sql);
            $historySql = "insert into viamente_end_day_history (ServiceProviderID,CreatedUserID,RouteDate,EndDayType,CreatedDate,ActionType) values (:ServiceProviderID,:CreatedUserID,:RouteDate,'Brown',now(),'Reset Viamente')";
            $historyParams = array('ServiceProviderID' => $s, 'CreatedUserID' => $this->controller->user->UserID, 'RouteDate' => $date );
            $this->execute($this->conn, $historySql, $historyParams);
        }
    }

    public function checkEngineersCapacity($spID, $date, $SkillsetID) {
        if ($this->debug) {
            $this->controller->log("--------------------------------------==================== start for sp=$spID ====================================---------------------------", "diary_capacity_");
            $this->controller->log("skillsetid=$SkillsetID", "diary_capacity_");
            $this->controller->log("date=$date", "diary_capacity_");
        }
        $avgApps = 0;
        //total engineers capable doing this skillset skill
        $SkillsetEngineers = $this->getEngineersToSkillset($SkillsetID, $spID, $date, $appID);
        foreach ($SkillsetEngineers as $cs) {
            if ($cs['avgApps'] > 0) {
                $avgApps = $avgApps + $cs['avgApps'];
            }
        }
        $totalApp = $this->getRepairSkillAppCount($SkillsetID, $spID, $date);
        if ($totalApp != 0 && $avgApps != 0) {
            $capacity = ($totalApp / $avgApps) * 100;
        } else {
            $capacity = 0;
        }
        // echo "totAps=".$totalApp."<br>";
        //  echo "capacity=".($totalApp/$avgApps)*100 ."<br>";


        if ($this->debug) {
            $this->controller->log("Engineers active:", "diary_capacity_");
            $this->controller->log($SkillsetEngineers, "diary_capacity_");
            $this->controller->log("total app", "diary_capacity_");
            $this->controller->log($totalApp, "diary_capacity_");
            $this->controller->log("Default Average App Sum", "diary_capacity_");
            $this->controller->log($avgApps, "diary_capacity_");
            $this->controller->log("Capacity filled to $capacity %", "diary_capacity_");
            $this->controller->log("end for sp=$spID", "diary_capacity_");
        }
        return $capacity;
    }

    public function getEngineersToSkillset($skillsetID, $spid, $date, $appID) 
    {
        
        $getAppDetails = $this->getAppointmentDetails($appID);
        $postCode = $getAppDetails['postCode'];
        $postCode2 = $postCode;
        if (strlen($postCode) > 4) {

            $postCode2 = trim(substr(trim($postCode), 0, -3));

            if (!is_numeric(substr($postCode2, -1))) 
            {
                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
            }
        } else 
        {
            if (!is_numeric(substr($postCode2, -1))) {
                $postCode2 = substr($postCode2, 0, strlen($postCode2) - 1);
            }
        }
        $postCodeAlpha = preg_replace('/[^\\/\-a-z\s]/i', '', $postCode2);

        $postCodeOutCode = $postCode2;     

        /* Extract the UK postcode outcode (eg CV1) from a full UK postcode (eg CV1 5FB) */

        if (!is_numeric(substr($postCodeOutCode, -1))) 
        {
            $postCodeOutCode = substr($postCodeOutCode, 0, strlen($postCodeOutCode) - 1);
        }
        $postCodeArea = preg_replace('/[^\\/\-a-z\s]/i', '', $postCodeOutCode); 
            
        $sql = "select distinct(sped.ServiceProviderEngineerID),
(
	(
	(
		(
		time_to_sec(sped.EndShift)-time_to_sec(sped.StartShift)-if(dhs.TotalTimeSec is null,0,dhs.TotalTimeSec)
		)/60/60
	
	) * (spe.AvarageAppPerHour))
) as avgApps

 from service_provider_engineer_details sped 
 join service_provider_engineer spe on spe.ServiceProviderEngineerID=sped.ServiceProviderEngineerID 
 join service_provider_engineer_skillset_day spesd on spesd.ServiceProviderEngineerDetailsID=sped.ServiceProviderEngineerDetailsID 
left join diary_holiday_slots dhs on dhs.ServiceProviderEngineerID=sped.ServiceProviderEngineerID and dhs.HolidayDate=sped.WorkDate
                JOIN diary_allocation da ON spe.ServiceProviderEngineerID = da.ServiceProviderEngineerID
                JOIN diary_postcode_allocation dpa ON dpa.DiaryAllocationID = da.DiaryAllocationID
                Where sped.WorkDate=:WorkDate 
                AND da.AllocatedDate=:WorkDate
                and spe.ServiceProviderID=$spid 
                and spesd.ServiceProviderSkillsetID=$skillsetID 
                and sped.`Status`='Active'
                AND 
                    (
                      dpa.`PostCode` = '$postCode'
                      OR dpa.`PostCode` = '$postCodeAlpha'
                      OR dpa.`PostCode` = '$postCodeOutCode'
                      OR dpa.`PostCode` = '$postCodeArea'
                    )";
        $params = array('WorkDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);
        return $res;


        return $res;
    }

    public function getRepairSkillAppCount($skillsetID, $spid, $date) {
        $repSkill = $this->getRepairTypeIDFromSPSkillset($skillsetID);
        $sql = "select count(AppointmentID) as total_app from appointment ap
                        join  service_provider_skillset sps on ap.ServiceProviderSkillsetID=sps.ServiceProviderSkillsetID
                      join skillset s on s.SkillsetID=sps.SkillsetID
                     
                      where 
							
                     
                     ap.ServiceProviderID=$spid 
                      and ap.AppointmentDate='$date' 
                      and s.RepairSkillID=$repSkill";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['total_app'];
    }

    public function getServiceProviderAppointmentCapaityDefault($spID) {
        $sql = "select BookingCapacityLimit from service_provider where ServiceProviderID=$spID ";
        $c6 = $this->Query($this->conn, $sql);
        return $c6[0]['BookingCapacityLimit'];
    }

    public function getGlobalViamenteStatus() {
        $sql = "select * from general_default where GeneralDefaultID=5";
        $c6 = $this->Query($this->conn, $sql);
        if (!isset($c6[0])) {
            return false;
        }
        if ($c6[0]['Status'] == 'Active') {
            return true;
        } else {
            return false;
        }
    }

    //thsi will return all engineer assigned postcodes for given day
    public function getEngineersPostcodes($engID, $date) {
        $sql = "select distinct(dpa.`Postcode`) as pc from diary_postcode_allocation dpa
join diary_allocation da on dpa.DiaryAllocationID=da.DiaryAllocationID
where da.ServiceProviderEngineerID=$engID and da.AllocatedDate='$date'";
        $res = $this->Query($this->conn, $sql);

        return $res;
    }
    /**
     * This function added by Raju on 11th June 2013
     * This method is to get the service provider engineer id from the diary_holiday_diary table
     * @author Krishnam Raju Nalla <k.nalla@pccsuk.com>
     */
    public function getHolidayEntry($holidayId,$serviceProviderId)
    {
        $sql = "select ServiceProviderEngineerID from diary_holiday_diary where DiaryHolidayID='".$holidayId."' AND ServiceProviderID='".$serviceProviderId."'";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }
    /**
     * This function added by Raju on 18th June 2013
     * This method is to get the Appointment Job Type i.e. Skyline Job or Non Skyline Job.
     * @author Krishnam Raju Nalla <k.nalla@pccsuk.com>
     */
    public function getJobType($apptId)
    {
        $sql = "select if(JobID IS NULL,'nskj','skj') as jobType, JobID, NonSkylineJobID from appointment where AppointmentID='".$apptId."'";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }
    public function getJobCustomerId($jobId)
    {
        $sql = "select CustomerID from job where JobID='".$jobId."'";
        $res = $this->Query($this->conn, $sql);
        return $res;
        
    }
    /**
     * This function added by Raju on 18th June 2013
     * This method is to update customer Email Address into Customers table while Insert/Update Appointment.
     * @author Krishnam Raju Nalla <k.nalla@pccsuk.com>
     */
    public function updateAppointmentCustomerEmail($id,$custEmail,$type)
    {
        if($type == "skj")
            $sql = "UPDATE customer SET ContactEmail='".mysql_real_escape_string($custEmail)."' WHERE CustomerID = '".$id."'";
        else
            $sql = "UPDATE non_skyline_job SET CustomerEmail='".mysql_real_escape_string($custEmail)."' WHERE NonSkylineJobID = '".$id."'";
        $this->Execute($this->conn, $sql);
    }
    /**
     * This function added by Raju on 19th June 2013
     * This method is to Get Selected Date Appointments.
     * @author Krishnam Raju Nalla <k.nalla@pccsuk.com>
     */
    public function getDayAppointments($spId,$date)
    {
        $sql = "select distinct AppointmentID from appointment where ServiceProviderID=:ServiceProviderID AND AppointmentDate=:AppointmentDate";
        $params = array('ServiceProviderID' => $spId, 'AppointmentDate' => $date);
        $res = $this->Query($this->conn, $sql, $params);
        return $res;
    }
    /* New Function added by Raju for TBL 432 code Starts Here */
    public function getFinalizedDayDetails($spID, $sDate) {
        $sql = "select count(*) as cnt from viamente_end_day_history where ServiceProviderID='".$spID."' and date_format(RouteDate,'%d/%m/%Y') = '".$sDate."'";
        $res = $this->Query($this->conn, $sql);
        return $res[0];
    }
    
    public function getFinalisedDayHistoryDetails($spId,$routeDate)
    {
        $dbTables = "viamente_end_day_history AS T1 LEFT JOIN user AS T2 ON T1.CreatedUserID=T2.UserID";
        $dbTablesColumns = [
            "date_format(T1.RouteDate,'%d/%m/%Y')", 
            "CONCAT(T2.ContactFirstName, ' ',T2.ContactLastName) AS UserName",
            "T1.ActionType", 
            array("brownDate", "CASE WHEN T1.EndDayType = 'Both' OR T1.EndDayType = 'Brown' THEN date_format(T1.CreatedDate,'%d/%m/%Y (%H:%i)') END as brownDate"),
            array("whiteDate", "CASE WHEN T1.EndDayType = 'Both' OR T1.EndDayType = 'White' THEN date_format(T1.CreatedDate,'%d/%m/%Y (%H:%i)') END as whiteDate")
        ];
        $args["where"] = " T1.ServiceProviderID = '" . $spId . "' AND T1.RouteDate='$routeDate'";
        $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
        return $output;
    }
    /* New Function added by Raju for TBL 432 code Ends Here */
    /* New Function added by Raju for TBL 317 code Starts Here */
    public function insertBankHoliday($spId,$data)
    {
        $dateArray = explode("/", $data['bholidayDate']);
        $sqlDate = $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
        $sql = "insert into national_bank_holiday (HolidayDate, HolidayName, CountryID, ServiceProviderId) values ('".$sqlDate."', '".$data['holidayName']."', '".$data['cId']."', '".$spId."')";
        $res = $this->execute($this->conn, $sql);
        $hId = $this->conn->lastInsertId();
        if($data['cId'] == 1 && $data['bankHolidayStatus'] == "Open")
        {
            $sql1 = "insert into service_provider_bank_holidays (NationalBankHolidayID, ServiceProviderID, Status) values ('".$hId."', '".$spId."', '".$data['bankHolidayStatus']."')";
            $res1 = $this->execute($this->conn, $sql1);
        }
        else if($data['cId'] > 1 && $data['bankHolidayStatus'] == "Closed")
        {
            $sql2 = "insert into service_provider_bank_holidays (NationalBankHolidayID, ServiceProviderID, Status) values ('".$hId."', '".$spId."', '".$data['bankHolidayStatus']."')";
            $res2 = $this->execute($this->conn, $sql2);
        }
    }
    /* New Function added by Raju for TBL 317 code Ends Here */
}
?>
  
