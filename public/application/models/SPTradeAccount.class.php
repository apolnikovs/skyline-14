<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Service Provider Trade Account (Diary defaults page) in Site Map section
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class SPTradeAccount extends CustomModel {
    
    private $conn;
    private $dbColumns = array('ServiceProviderTradeAccountID', 'TradeAccount', 'Postcode', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Status');
    private $table     = "service_provider_trade_account";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
           $ServiceProviderID = isset($args['firstArg'])?$args['firstArg']:false;
           if($ServiceProviderID)
           {    
                $args['where'] = "ServiceProviderID='".$ServiceProviderID."'";

                $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);


                 return  $output;
           }
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['ServiceProviderTradeAccountID']) || !$args['ServiceProviderTradeAccountID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate Trade Account.
     *
     * @param string $TradeAccount  
     * @param string $Postcode  
     * @param interger $ServiceProviderID  
     * @param interger $ServiceProviderTradeAccountID
     * 
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($TradeAccount, $Postcode, $ServiceProviderID, $ServiceProviderTradeAccountID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderTradeAccountID FROM '.$this->table.' WHERE TradeAccount=:TradeAccount AND Postcode=:Postcode AND ServiceProviderID=:ServiceProviderID AND ServiceProviderTradeAccountID!=:ServiceProviderTradeAccountID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':TradeAccount' => $TradeAccount, ':Postcode' => strtoupper($Postcode), ':ServiceProviderID' => $ServiceProviderID, ':ServiceProviderTradeAccountID' => $ServiceProviderTradeAccountID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['ServiceProviderTradeAccountID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (ServiceProviderID, TradeAccount, Postcode, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, CreatedDate, Status, ModifiedUserID, ModifiedDate, EndDate)
            VALUES(:ServiceProviderID, :TradeAccount, :Postcode, :Monday, :Tuesday, :Wednesday, :Thursday, :Friday, :Saturday, :Sunday, :CreatedDate, :Status, :ModifiedUserID, :ModifiedDate, :EndDate)';
        
        
        if($this->isValidAction($args['TradeAccount'], $args['Postcode'], $args['ServiceProviderID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $week_array = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            
            foreach($week_array as $day)
            {
                if(isset($args[$day]) && $args[$day])
                {
                    $args[$day] = 'Yes';
                }    
                else
                {
                    $args[$day] = 'No';
                }
                
            }    
            
            
            $insertQuery->execute(array(
                ':ServiceProviderID' => $args['ServiceProviderID'], 
                ':TradeAccount' => $args['TradeAccount'], 
                ':Postcode' => strtoupper($args['Postcode']), 
                ':Monday' => $args['Monday'], 
                ':Tuesday' => $args['Tuesday'], 
                ':Wednesday' => $args['Wednesday'], 
                ':Thursday' => $args['Thursday'], 
                ':Friday' => $args['Friday'], 
                ':Saturday' => $args['Saturday'], 
                ':Sunday' => $args['Sunday'], 
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':Status' => $args['Status'],
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s"),
                ':EndDate' => "0000-00-00 00:00:00"
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_inserted_msg']);
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceProviderTradeAccountID, ServiceProviderID, TradeAccount, Postcode, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, CreatedDate, Status, ModifiedUserID, ModifiedDate, EndDate FROM '.$this->table.' WHERE ServiceProviderTradeAccountID=:ServiceProviderTradeAccountID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceProviderTradeAccountID' => $args['ServiceProviderTradeAccountID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['TradeAccount'], $args['Postcode'], $args['ServiceProviderID'], $args['ServiceProviderTradeAccountID']))
        {        
            
            $week_array = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
            
            foreach($week_array as $day)
            {
                if(isset($args[$day]) && $args[$day])
                {
                    $args[$day] = 'Yes';
                }    
                else
                {
                    $args[$day] = 'No';
                }
                
            } 
            
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($args['Status']=="In-active")
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                        $EndDate = $row_data['EndDate'];
                }
                
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              ServiceProviderID=:ServiceProviderID, TradeAccount=:TradeAccount, Postcode=:Postcode, Monday=:Monday, Tuesday=:Tuesday, Wednesday=:Wednesday, 
              Thursday=:Thursday, Friday=:Friday, Saturday=:Saturday, Sunday=:Sunday, Status=:Status, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate, EndDate=:EndDate


              WHERE ServiceProviderTradeAccountID=:ServiceProviderTradeAccountID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':ServiceProviderID' => $args['ServiceProviderID'], 
                        ':TradeAccount' => $args['TradeAccount'], 
                        ':Postcode' => strtoupper($args['Postcode']), 
                        ':Monday' => $args['Monday'], 
                        ':Tuesday' => $args['Tuesday'], 
                        ':Wednesday' => $args['Wednesday'], 
                        ':Thursday' => $args['Thursday'], 
                        ':Friday' => $args['Friday'], 
                        ':Saturday' => $args['Saturday'], 
                        ':Sunday' => $args['Sunday'],  
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':ServiceProviderTradeAccountID' => $args['ServiceProviderTradeAccountID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
}
?>