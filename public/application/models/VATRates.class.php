<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of VAT Rates Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class VATRates extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.VatRateID', 't1.VatCode', 't1.Code',  't1.VatRate', 't1.Status');
    private $tables    = "vat_rate AS t1 LEFT JOIN country_vat_rate AS t2 ON t1.VatRateID=t2.VatRateID
                          LEFT JOIN country AS t3 ON t2.CountryID=t3.CountryCode";
    private $table     = "vat_rate";
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
       // $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        $CountryID = isset($args['firstArg'])?$args['firstArg']:'';


        
        if($CountryID!='') {
            $args['where'] = "t2.CountryID='{$CountryID}'";
        }else{
            $args['where'] = "t2.CountryID IS NULL";
        } 
        #$this->controller->log( var_export($args , true) );        
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));

           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            #$args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
        
        
       
        return  $output;
        
    }
    
    
    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['VatRateID']) || !$args['VatRateID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getCode($CountryID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT t1.VatCode FROM '.$this->tables.' WHERE t2.CountryID=:CountryID ORDER BY t1.VatCode DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $this->controller->log(var_export($CountryID, true));
        
        $fetchQuery->execute(array(':CountryID' => $CountryID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $VatCode  
     * @param interger $BrandID.
     * @param interger $VatRateID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($VatCode, $CountryID,  $VatRateID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = "SELECT t1.VatRateID FROM {$this->tables} WHERE t1.VatCode=':VatCode' AND t2.CountryID=':CountryID' AND t1.VatRateID!=':VatRateID'";
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':VatCode' => $VatCode, ':CountryID' => $CountryID, ':VatRateID' => $VatRateID));
        $result = $fetchQuery->fetch();
        
        return ( !$result && is_array($result) ? false : true);
    
    }
  
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (Code, VatCode, VatRate, Status, BrandID)
//            VALUES(:Code, :VatCode, :VatRate, :Status, :BrandID)';
        
        $fields = explode(', ', 'Code, VatCode, VatRate, Status');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::VatRate()->insertCommand($fields);        
        
        if(!isset($args['VatCode']) || !$args['VatCode'])
        {
            $args['VatCode'] = $this->getCode($args['CountryID'])+1;//Preparing next vat code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['VatCode'], $args['CountryID'], 0)) {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

            $insertQuery->execute(array(':Code' => $args['Code'], ':VatCode' => $args['VatCode'], ':VatRate' => $args['VatRate'], ':Status' => $args['Status']));

            #insert into country_vat_code
            $vatRateID = $this->conn->lastInsertId();

            $sql = "INSERT INTO country_vat_rate(`CountryID` ,`VatRateID` ) Values(:CountryID, :VatRateID)";
            
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

            $insertQuery->execute(array(':CountryID' => $args['CountryID'], ':VatRateID' => $vatRateID));
            

            
        
            return array('status' => 'OK', 'message' => $this->controller->page['data_inserted_msg']);
        } else {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT VatRateID, VatCode, Code, VatRate, Status FROM '.$this->table.' WHERE VatRateID=:VatRateID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':VatRateID' => $args['VatRateID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch VatRate from database.
     *
     * @param int $CountryID  default UK=1
     * @param string $Code
     * 
     * @global $this->table  
     * @return float It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchVatRate($Code, $CountryID=1) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.VatRate FROM '.$this->table.' AS T1 LEFT JOIN country_vat_rate AS T2 ON T1.VatRateID=T2.VatRateID WHERE T1.Code=:Code AND T1.Status=:Status AND T2.CountryID=:CountryID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':Code' => $Code, ':Status' => 'Active', ':CountryID' => $CountryID));
        
        $result = $fetchQuery->fetch();
        if(isset($result['VatRate']))
        {
            return $result['VatRate'];
        }   
        else
        {
            return 0;
        }
        
    }
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['VatCode'], $args['CountryID'], $args['VatRateID']))
        {        
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET Code=:Code, VatCode=:VatCode, VatRate=:VatRate, Status=:Status WHERE VatRateID=:VatRateID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':Code' => $args['Code'], ':VatCode' => $args['VatCode'], ':VatRate' => $args['VatRate'], ':Status' => $args['Status'], ':VatRateID' => $args['VatRateID']));
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    
    /**
     * getCountryVatRates
     * 
     * Returns an array of VAT Rates by country
     * 
     * @param integer $ClientID     A specific country ID to be searched for
     *                              if not supplied returns an array of all
     * 
     * @return array $result    Generated array of branches for the network.
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     *************************************************************************/
    
     public function getCountryVatRates($CountryID=false) {
        
          $result = array();
          
          $where = "";
          
          if($CountryID) {
              $where = " AND cvr.CountryID = $CountryID ";
          }
          
          $sql = "
                  SELECT 
			cvr.`CountryID`,
			v.`VatRateID`,
                        v.`Code`
                  FROM 
			`country_vat_rate` AS cvr LEFT JOIN `vat_rate` AS v ON cvr.VatRateID = v.VatRateID
                  WHERE
			v.Status = 'Active'
                        $where order by v.`Code`
                 ";
          
          $query = $this->conn->query($sql);
           
          if($CountryID)
          {    
              while($row = $query->fetch()) {
                  $result[] = array("VatRateID"=>$row['VatRateID'], "Code"=>$row['Code']);
              } 
          }
          else
          {
              while($row = $query->fetch()) {
                  $result[$row['CountryID']][] = array("VatRateID"=>$row['VatRateID'], "Code"=>$row['Code']);
              }
          }
          
                
          return $result;  
                 
    } 
}
?>