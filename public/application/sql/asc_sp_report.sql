/*Dropping temp tables and procedures, required for development in local client*/
DROP TEMPORARY TABLE IF EXISTS by_clients;
DROP TEMPORARY TABLE IF EXISTS by_status;
DROP TEMPORARY TABLE IF EXISTS sp_clients;
DROP TEMPORARY TABLE IF EXISTS sp_statuses;
DROP TEMPORARY TABLE IF EXISTS by_statuses;
DROP TEMPORARY TABLE IF EXISTS by_clients;
DROP PROCEDURE IF EXISTS byClients;
DROP PROCEDURE IF EXISTS byStatus;


/*SET @networkID = 4;*/
/*SET @spID = 10;*/
SET @networkID = [networkID];
SET @spID = [serviceProviderID];


/*Create temp table of Clients of the Service Provider*/
CREATE TEMPORARY TABLE IF NOT EXISTS sp_clients AS (
	SELECT		job.ClientID,
				client.ClientName
	FROM		job
	LEFT JOIN	client ON job.ClientID = client.ClientID
	WHERE		job.ServiceProviderID = @spID
	GROUP BY	job.ClientID
);


/*Create temp table of Statuses of the Service Provider*/
CREATE TEMPORARY TABLE IF NOT EXISTS sp_statuses AS (
	SELECT		job.StatusID,
				status.StatusName
	FROM		job
	LEFT JOIN	status ON job.StatusID = status.StatusID
	WHERE		job.ServiceProviderID = @spID
	GROUP BY	job.StatusID
);


CREATE TEMPORARY TABLE IF NOT EXISTS by_clients	(	
													id INT(11), 
													name VARCHAR(100),
													lessThan7 INT(11),
													from7to14 INT(11),
													moreThan14 INT(11),
													moreThan30 INT(11),
													total INT(11)
												);

CREATE TEMPORARY TABLE IF NOT EXISTS by_statuses(	
													id INT(11), 
													stat VARCHAR(100),
													lessThan7 INT(11),
													from7to14 INT(11),
													moreThan14 INT(11),
													moreThan30 INT(11),
													total INT(11)
												);

/*DELIMITER $$*/

/*Procedure to generate first report table - By Clients*/
CREATE PROCEDURE byClients()

BEGIN

  	DECLARE done INT DEFAULT FALSE;

	DECLARE id INT(11);
	DECLARE name VARCHAR(100);
	DECLARE lessThan7 INT(11);
	DECLARE from7to14 INT(11);
	DECLARE moreThan14 INT(11);
	DECLARE moreThan30 INT(11);
	DECLARE total INT(11);
	
	DECLARE cur CURSOR FOR SELECT * FROM sp_clients;

 	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur;
	
	read_loop: LOOP
	
		FETCH cur INTO id, name;

		IF done THEN
      		LEAVE read_loop;
	   	END IF;	
		
		SET lessThan7 = (	
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) < 7 AND
									ClientID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
						
		SET from7to14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) >= 7 AND
									DATEDIFF(CURDATE(), job.DateBooked) <= 14 AND
									ClientID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
							 
		SET moreThan14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 14 AND
									ClientID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						 );
		
		SET moreThan30 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 30 AND
									ClientID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						 );
		
		SET total = lessThan7 + from7to14 + moreThan14;
						
		IF total > 0 THEN
			INSERT INTO by_clients	(id, name, lessThan7, from7to14, moreThan14, moreThan30, total) 
			VALUES 					(id, name, lessThan7, from7to14, moreThan14, moreThan30, total);
		END IF;
		
	END LOOP;

	CLOSE cur;
	
/*END$$*/
END;

/*Procedure to generate second report table - By Status*/
CREATE PROCEDURE byStatus()

BEGIN

  	DECLARE done INT DEFAULT FALSE;
  	
	DECLARE id INT(11);
	DECLARE stat VARCHAR(100);
	DECLARE lessThan7 INT(11);
	DECLARE from7to14 INT(11);
	DECLARE moreThan14 INT(11);
	DECLARE moreThan30 INT(11);
	DECLARE total INT(11);
	
	DECLARE cur CURSOR FOR SELECT * FROM sp_statuses;
	
 	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
 	
	OPEN cur;
	
	read_loop: LOOP
	
		FETCH cur INTO id, stat;
		
		IF done THEN
      		LEAVE read_loop;
	   	END IF;	
		
		SET lessThan7 = (	
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) < 7 AND
									StatusID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
						
		SET from7to14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) >= 7 AND
									DATEDIFF(CURDATE(), job.DateBooked) <= 14 AND
									StatusID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
							 
		SET moreThan14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 14 AND
									StatusID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
		
		SET moreThan30 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 30 AND
									StatusID = id AND
									ServiceProviderID = @spID AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
		
		SET total = lessThan7 + from7to14 + moreThan14;
			
		IF total > 0 THEN
			INSERT INTO by_statuses	(id, stat, lessThan7, from7to14, moreThan14, moreThan30, total) 
			VALUES 					(id, stat, lessThan7, from7to14, moreThan14, moreThan30, total);
		END IF;
		
	END LOOP;
	
	CLOSE cur;

END;
/*END$$*/

/*DELIMITER ;*/


CALL byClients();
CALL byStatus();


SELECT * from by_clients;
SELECT * from by_statuses;


DROP TEMPORARY TABLE IF EXISTS by_clients;
DROP TEMPORARY TABLE IF EXISTS by_status;
DROP TEMPORARY TABLE IF EXISTS sp_clients;
DROP TEMPORARY TABLE IF EXISTS sp_statuses;
DROP TEMPORARY TABLE IF EXISTS by_statuses;
DROP TEMPORARY TABLE IF EXISTS by_clients;
DROP PROCEDURE IF EXISTS byClients;
DROP PROCEDURE IF EXISTS byStatus;